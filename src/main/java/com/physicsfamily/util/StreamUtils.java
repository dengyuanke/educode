package com.physicsfamily.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stream工具类
 * @author 张云鹏 2014-12-26
 *
 */
public class StreamUtils {
	
	private static Charset defaultSet=Charset.forName("UTF-8");

	private static Logger log = LoggerFactory.getLogger(StreamUtils.class);
	
	/**
	 * 将InputStream转换为String
	 * @param stream
	 * @return
	 */
	public static String streamToStr(InputStream stream) {
		return streamToStr(stream,defaultSet);
	}
	
	/**
	 * 将InputStream转换为String
	 * @param stream
	 * @param charset
	 * @return
	 */
	public static String streamToStr(InputStream stream,Charset charset) {
		if(charset==null){
			charset=defaultSet;
		}
		StringBuilder string = new StringBuilder();
		BufferedReader read = null;
		try {
			read = new BufferedReader(new InputStreamReader(stream,charset), 4096);
			String line = null;
			while ((line = read.readLine()) != null) {
				string.append(line);
			}
		} catch (IOException e) {
			if(log.isErrorEnabled()){
				log.error("IOException:", e);
			}
		} finally {
			IOUtils.closeQuietly(read);
		}
		return string.toString();
	}
	
}
