package com.physicsfamily.util;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author 张云鹏
 *
 */
public class URIComposer {

  private static Logger log = LoggerFactory.getLogger(URIComposer.class);

  /**
   * 
   * @param baseUrl
   * @param params
   * @return
   */
  public static String uriBuilder(String baseUrl, Map<String, String> params) {
    URIBuilder uriBulder;
    try {
      uriBulder = new URIBuilder(baseUrl);
      // 拼接参数
      if (params != null && params.size() != 0) {
        Set<Entry<String, String>> set = params.entrySet();
        Iterator<Entry<String, String>> iter = set.iterator();
        while (iter.hasNext()) {
          Entry<String, String> entry = iter.next();
          uriBulder.setParameter(entry.getKey(), entry.getValue());
        }
      }
      String url = uriBulder.build().toString();
      if (log.isDebugEnabled()) {
        log.debug("URIBuilder:" + url);
      }
      return url;
    } catch (URISyntaxException e) {
      if (log.isErrorEnabled()) {
        log.error("URISyntaxException", e);
      }
      return null;
    }
  }


  /**
   * 
   * @param url
   * @param nvp
   * @return
   */
  public static String buildUri(String url, NameValuePair... nvp) {
    URIBuilder uriBulder;
    try {
      uriBulder = new URIBuilder(url);
      // 拼接参数
      if (nvp != null && nvp.length != 0) {
        uriBulder.setParameters(nvp);
      }
      String uri = uriBulder.build().toString();
      if (log.isDebugEnabled()) {
        log.debug("URIBuilder:" + url);
      }
      return uri;
    } catch (URISyntaxException e) {
      if (log.isErrorEnabled()) {
        log.error("URISyntaxException", e);
      }
      return null;
    }
  }

}
