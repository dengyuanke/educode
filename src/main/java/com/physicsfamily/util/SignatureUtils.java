package com.physicsfamily.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 签名工具类
 * 
 * @author 张云鹏 2015-03-01
 *
 */
public class SignatureUtils {

  private static Logger log = LoggerFactory.getLogger(SignatureUtils.class);


  /**
   * 执行通过SHA1方式进行签名 此方法比WeChat官方提供的Signature.getSign方法效率高24%
   * 
   * @param map
   * @return
   * @author 张云鹏 2015-03-07
   */
  public static String signMapWithSHA1(Map<String, Object> map) {
    Iterator<Entry<String, Object>> it = map.entrySet().iterator();
    List<String> list = new ArrayList<String>(map.size());
    while (it.hasNext()) {
      Entry<String, Object> entry = it.next();
      String value = entry.getValue().toString();
      if (StringUtils.isEmpty(value)) {
        continue;
      }
      list.add(entry.getKey() + "=" + value + "&");
    }
    int size = list.size();
    String[] sorts = list.toArray(new String[size]);
    Arrays.sort(sorts, String.CASE_INSENSITIVE_ORDER);
    StringBuilder sb = new StringBuilder(512);
    for (int i = 0; i < size; i++) {
      sb.append(sorts[i]);
    }
    sb.setLength(sb.length() - 1);// 去除最后的&
    byte[] bytes = null;
    try {
      bytes = sb.toString().getBytes("UTF-8");
    } catch (Exception e) {
      log.error("Exception", e);
    }
    return DigestUtils.sha1Hex(bytes);
  }


  /**
   * 
   * @param map
   * @return
   * @author 张云鹏 2015-03-07
   */
  public static String hmacSha1(Map<String, String> map, String content, String key) {
    Iterator<Entry<String, String>> it = map.entrySet().iterator();
    List<String> list = new ArrayList<String>(map.size());
    while (it.hasNext()) {
      Entry<String, String> entry = it.next();
      String value = entry.getValue().toString();
      if (StringUtils.isEmpty(value)) {
        continue;
      }
      list.add(entry.getKey() + "=" + value + "&");
    }
    int size = list.size();
    String[] sorts = list.toArray(new String[size]);
    Arrays.sort(sorts, String.CASE_INSENSITIVE_ORDER);
    StringBuilder sb = new StringBuilder(512);
    for (int i = 0; i < size; i++) {
      sb.append(sorts[i]);
    }
    sb.setLength(sb.length() - 1);// 去除最后的&
    byte[] bytes = null;
    try {

      if (StringUtils.isNotEmpty(content)) {
        sb.append(content);
      }
      bytes = sb.toString().getBytes("UTF-8");
    } catch (Exception e) {
      log.error("Exception", e);
    }
    byte[] keys = null;
    try {
      keys = key.getBytes("UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    return Base64.encodeBase64String(new HmacUtils(HmacAlgorithms.HMAC_SHA_1, keys).hmac(bytes));
  }

  /**
   * 对List进行排序并用SHA1签名，用于对微信的API_TICKET生成签名专用
   * 
   * @param list
   * @return
   */
  public static String signListWithSHA1(List<String> list) {
    int size = list.size();
    String[] sorts = list.toArray(new String[size]);
    Arrays.sort(sorts);
    StringBuilder sb = new StringBuilder(512);
    for (int i = 0; i < size; i++) {
      sb.append(sorts[i]);
    }
    byte[] bytes = null;
    try {
      bytes = sb.toString().getBytes("UTF-8");
    } catch (Exception e) {
      log.error("Exception", e);
    }
    return DigestUtils.sha1Hex(bytes);
  }



}
