package com.physicsfamily.http;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

public class HeaderCreator {

	public static Header[] createHeaders(Map<String, String> headers) {
		if (headers == null || headers.size() == 0) {
			return null;
		}
		int size = headers.size();
		Header[] header = new BasicHeader[size];
		Set<Entry<String, String>> set = headers.entrySet();
		Iterator<Entry<String, String>> it = set.iterator();
		while (it.hasNext()) {
			Entry<String, String> next = it.next();
			header[--size] = new BasicHeader(next.getKey(), next.getValue());
		}
		return header;
	}

}
