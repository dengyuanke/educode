package com.physicsfamily.http;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Lists;

/**
 * 通过池机制获取HttpClient 代码范例： 上传文件到指定的目标地址：InputStream in=postFile(url,null,new
 * File("E:/jdk-8u25-windows-x64.exe"));
 * 
 * @author 张云鹏 2014-12-09
 * 
 */
public class Https {

  private static CloseableHttpClient client = null;

  private static final PoolingHttpClientConnectionManager cm =
      new PoolingHttpClientConnectionManager();

  private static Logger log = LoggerFactory.getLogger(Https.class);

  private static final int SOCKET_TIMEOUT = 5000;
  private static final int CONNECTION_REQUEST_TIMEOUT = 5000;
  private static final int CONNECT_TIMEOUT = 5000;
  private static final boolean REDIRECTS_ENABLED = true;

  private static RequestConfig REQUEST_CONFIG =
      RequestConfig.custom().setConnectTimeout(CONNECT_TIMEOUT)
          .setRedirectsEnabled(REDIRECTS_ENABLED).setSocketTimeout(SOCKET_TIMEOUT)
          .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT).build();

  private static final int POOL_HTTPCLIENT_MAX_TOTAL = 128;

  static {
    cm.setMaxTotal(POOL_HTTPCLIENT_MAX_TOTAL);
    // 设置线路数量，此数量的大小对多线程调用httpclient的并发性影响很大，数字越大，支持的并发越高
    cm.setDefaultMaxPerRoute(20);
    client = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(REQUEST_CONFIG)
        .setConnectionManagerShared(true).build();

    if (log.isInfoEnabled()) {
      log.info("HttpClient Inited!");
    }
  }

  private static ByteArrayEntity buildByteArrayEntity(byte[] data) {
    return new ByteArrayEntity(data);
  }

  /**
   * 将参数追加至URL末尾
   * 
   * @param url
   * @param param
   * @return
   */
  private static String buildUri(String url, Map<String, String> param) {
    URIBuilder uriBuilder = null;
    String result = null;
    try {
      uriBuilder = new URIBuilder(url);
      Set<Entry<String, String>> set = param.entrySet();
      Iterator<Entry<String, String>> it = set.iterator();
      while (it.hasNext()) {
        Entry<String, String> entry = it.next();
        uriBuilder.setParameter(entry.getKey(), entry.getValue());
      }
      result = uriBuilder.build().toString();
    } catch (URISyntaxException e) {
      if (log.isErrorEnabled()) {
        log.error("buildUri: URL:[{}],Param:[{}]", url, ToStringBuilder.reflectionToString(param));
      }
    }
    return result;
  }

  /**
   * 将文件及参数封装为HttpEntity对象，用于支持HttpClient文件上传操作
   * 
   * @param file
   * @param param
   * @return
   */
  private static HttpEntity getHttpEntity(File file, Map<String, String> param,
      String formFileName) {

    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

    if (formFileName == null) {
      formFileName = "file";
    }

    if (param != null && !param.isEmpty()) {
      Iterator<Entry<String, String>> iterator = param.entrySet().iterator();
      while (iterator.hasNext()) {
        Entry<String, String> entry = iterator.next();
        StringBody stringBody = new StringBody(entry.getValue(),
            ContentType.MULTIPART_FORM_DATA.withCharset(org.apache.http.Consts.UTF_8));
        builder.addPart(entry.getKey(), stringBody);
      }
    }
    if (file.exists()) {
      FileBody fileBody = new FileBody(file);
      builder.addPart(formFileName, fileBody);
      builder.addPart("filename", new StringBody(file.getName(),
          ContentType.MULTIPART_FORM_DATA.withCharset(org.apache.http.Consts.UTF_8)));
    } else {
      throw new NullPointerException("file can not be null!");
    }
    HttpEntity httpEntity = builder.build();
    return httpEntity;
  }

  /**
   * 将NameValuePair构造为HttpEntity
   * 
   * @param nvp
   * @return
   */
  private static HttpEntity getHttpEntity(List<NameValuePair> kvp) {
    return new UrlEncodedFormEntity(kvp, org.apache.http.Consts.UTF_8);
  }

  /**
   * 将Map转换为NameValuePair
   * 
   * @param param
   * @return
   */
  private static List<NameValuePair> mapToKvp(Map<String, String> param) {
    List<NameValuePair> kvp = Lists.newArrayList();
    Iterator<String> it = param.keySet().iterator();
    while (it.hasNext()) {
      String key = it.next();
      kvp.add(new BasicNameValuePair(key, param.get(key)));
    }
    return kvp;
  }

  /**
   * 用Http post方法向指定的URL发送文件
   * 
   * @param url 目标地址
   * @param file Http post body中封装的文件
   * @return
   */
  public static InputStream postFile(String url, File file, Header... headers) {
    HttpEntity httpEntity = getHttpEntity(file, null, null);
    return sendPostHttp(url, httpEntity, headers);
  }

  /**
   * 用Http post方法向指定的URL发送文件
   * 
   * @param url
   * @param file
   * @param formFileName 用于标识form表单中的文件字段，设置formFileName后，在服务端可以通过request.getPart(
   *        formFileName);获取文件
   * @return
   */
  public static InputStream postFile(String url, File file, String formFileName,
      Header... headers) {
    HttpEntity httpEntity = getHttpEntity(file, null, formFileName);
    return sendPostHttp(url, httpEntity, headers);
  }

  /**
   * 用Http post方法向指定的URL发送文件及参数
   * 
   * @param url 目标地址
   * @param params Http post body中封装的参数
   * @param file Http post body中封装的文件
   * @return
   */
  public static InputStream postFile(String url, Map<String, String> params, File file,
      Header... headers) {
    return postFile(url, params, file, null, headers);
  }

  /**
   * 上传文件到指定的地址
   * 
   * @param url 文件上传地址
   * @param params post表单携带的参数
   * @param file 上传的文件
   * @param formFileName 文件在表单中的名字
   * @return
   */
  public static InputStream postFile(String url, Map<String, String> params, File file,
      String formFileName, Header... headers) {
    HttpEntity httpEntity = getHttpEntity(file, params, formFileName);
    return sendPostHttp(url, httpEntity, headers);
  }

  /**
   * 发送Http get请求，并返回响应字节流
   * 
   * @param url 目标地址
   * @return
   */
  public static InputStream sendGetHttp(final String url, Header... headers) {
    return sendHttp(new HttpGet(url), headers);
  }

  /**
   * 发送Http get 请求，并返回响应字节流
   * 
   * @param url 目标url
   * @param param 追加至url末尾的参数
   * @return
   */
  public static InputStream sendGetHttp(String url, Map<String, String> param, Header... headers) {
    InputStream in = null;
    try {
      in = sendGetHttp(buildUri(url, param), headers);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("IOException:", e);
      }
    }
    return in;
  }

  private static InputStream sendHttp(final HttpRequestBase httpBase, Header... headers) {
    InputStream in = null;
    if (headers != null && headers.length > 0) {
      for (int i = 0; i < headers.length; i++) {
        httpBase.setHeader(headers[i]);
      }
    }
    try {
      CloseableHttpResponse response = client.execute(httpBase);

      if (!REDIRECTS_ENABLED) {
        int code = response.getStatusLine().getStatusCode();

        if (code == HttpStatus.SC_MOVED_PERMANENTLY || code == HttpStatus.SC_MOVED_TEMPORARILY) {
          throw new Exception("Redirects not allowed");
        }
      }

      if (log.isDebugEnabled()) {
        // Header[] hds = response.getAllHeaders();
        StringBuilder sb = new StringBuilder(1024);
        sb.append("\n Send HTTP " + httpBase.getMethod() + " request:URI=[");
        sb.append(httpBase.getURI()).append("]\n");
        // sb.append(" Response Headers:\n");
        // for(int i=0;i<hds.length;i++) {
        // sb.append(" "+hds[i].getName()+":"+hds[i].getValue()+"\n");
        // }
        // sb.append(" Response Status line:\n");
        // sb.append(" "+response.getStatusLine()+"\n");
        log.debug(sb.toString());
      }
      ByteArrayOutputStream bout = new ByteArrayOutputStream();
      response.getEntity().writeTo(bout);
      in = new ByteArrayInputStream(bout.toByteArray());
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("IOException:", e);
      }
    } finally {
      httpBase.releaseConnection();
    }
    return in;

  }

  /**
   * 发送Post请求，并返回响应流
   * 
   * @param url
   * @return
   */
  public static InputStream sendPostHttp(final String url, Header... headers) {
    return sendHttp(new HttpPost(url), headers);
  }

  public static InputStream sendPostHttp(String url, byte[] body, Header... headers) {
    ByteArrayEntity entity = buildByteArrayEntity(body);
    return sendPostHttp(url, entity, headers);
  }

  /**
   * 发送Http post请求，并返回响应字节流
   * 
   * @param url 目标地址
   * @param entity 封装进Http body中的HttpEntity
   * @return
   */
  public static InputStream sendPostHttp(final String url, final HttpEntity entity,
      Header... headers) {
    HttpPost post = new HttpPost(url);
    post.setEntity(entity);
    return sendHttp(post, headers);
  }

  /**
   * 发送Http Post请求，返回响应字节流
   * 
   * @param url Http Post 目标地址
   * @param kvp Key Value 键值对集合对象，被封装进Http post body 中
   * @return
   */
  public static InputStream sendPostHttp(String url, List<NameValuePair> kvp, Header... headers) {
    return sendPostHttp(url, getHttpEntity(kvp), headers);
  }

  /**
   * 发送Http Post请求，返回响应字节流
   * 
   * @param url Http Post 目标地址
   * @param param 封装进Http Post Body 中的Key Value参数
   * @return
   */
  public static InputStream sendPostHttp(String url, Map<String, String> param, Header... headers) {
    return sendPostHttp(url, mapToKvp(param), headers);
  }

  /**
   * 发送Http Post请求，返回响应字节流
   * 
   * @param url Http Post 目标地址
   * @param param 追加至url末尾的参数
   * @param stringBody 封装进Http Post Body 中的String
   * @param charset stringBody 的编码方式，默认为UTF-8
   * @return
   */
  public static InputStream sendPostHttp(final String url, final Map<String, String> param,
      final String stringBody, final Charset charset, Header... headers) {
    return sendPostHttp(buildUri(url, param), stringBody, charset, headers);
  }

  /**
   * 发送Http Post请求，返回响应字节流
   * 
   * @param url Http Post 目标地址
   * @param stringBody 封装进Http Post Body 中的String
   * @return
   */
  public static InputStream sendPostHttp(String url, String stringBody, Header... headers) {
    return sendPostHttp(url, stringBody, org.apache.http.Consts.UTF_8, headers);
  }

  /**
   * 发送Http Post请求，返回响应字节流
   * 
   * @param url Http Post 目标地址
   * @param stringBody 封装进Http Post Body 中的String
   * @param charset stringBody 的编码方式，默认为UTF-8
   * @return
   */
  public static InputStream sendPostHttp(final String url, final String stringBody,
      final Charset charset, Header... headers) {
    Charset defaultCharSet = charset == null ? org.apache.http.Consts.UTF_8 : charset;
    try {
      StringEntity entity = new StringEntity(stringBody, defaultCharSet);
      return sendPostHttp(url, entity, headers);
    } catch (Exception e) {
      log.error("Exception:", e);
    }
    return null;
  }

}
