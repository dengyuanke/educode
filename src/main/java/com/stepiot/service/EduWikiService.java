package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduWikiMapper;
import com.stepiot.model.EduWiki;
import com.stepiot.model.EduWikiCriteria;

@Service
public class EduWikiService implements EduWikiMapper {


  @Autowired
  private EduWikiMapper mapper;

  @Override
  public long countByExample(EduWikiCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduWikiCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer wikiId) {

    return mapper.deleteByPrimaryKey(wikiId);
  }

  @Override
  public int insert(EduWiki record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduWiki record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduWiki> selectByExampleWithBLOBs(EduWikiCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduWiki> selectByExample(EduWikiCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduWiki selectByPrimaryKey(Integer wikiId) {

    return mapper.selectByPrimaryKey(wikiId);
  }

  @Override
  public int updateByExampleSelective(EduWiki record, EduWikiCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduWiki record, EduWikiCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduWiki record, EduWikiCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduWiki record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduWiki record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduWiki record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduWiki selectByExampleForOne(EduWikiCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer wikiId) {

    return mapper.selectMapByPrimaryKey(wikiId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduWikiCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduWikiCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
