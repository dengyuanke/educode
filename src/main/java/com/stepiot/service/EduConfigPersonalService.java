package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.constants.Cons;
import com.stepiot.dao.EduConfigPersonalMapper;
import com.stepiot.model.EduConfigPersonal;
import com.stepiot.model.EduConfigPersonalCriteria;
import com.stepiot.model.EduConfigPersonalCriteria.Criteria;

@Service
public class EduConfigPersonalService implements EduConfigPersonalMapper {


  @Autowired
  private EduConfigPersonalMapper mapper;

  @Override
  public long countByExample(EduConfigPersonalCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduConfigPersonalCriteria example) {

    return mapper.deleteByExample(example);
  }


  @Override
  public int insert(EduConfigPersonal record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduConfigPersonal record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduConfigPersonal> selectByExample(EduConfigPersonalCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public int updateByExampleSelective(EduConfigPersonal record, EduConfigPersonalCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduConfigPersonal record, EduConfigPersonalCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduConfigPersonal record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduConfigPersonal record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduConfigPersonal selectByExampleForOne(EduConfigPersonalCriteria example) {

    return mapper.selectByExampleForOne(example);
  }


  @Override
  public Map<String, Object> selectMapByExampleForOne(EduConfigPersonalCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  public Map<String, Object> getBlocklyThemeConfig(String username) {
    EduConfigPersonalCriteria ex = new EduConfigPersonalCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andUsernameEqualTo(username);
    criteria.andConfigItemEqualTo(Cons.CONFIG_BLOCKLY_THEME);
    return mapper.selectMapByExampleForOne(ex);
  }

  public List<EduConfigPersonal> getAllThemeConfig(String username) {
    EduConfigPersonalCriteria ex = new EduConfigPersonalCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andUsernameEqualTo(username);
    return mapper.selectByExample(ex);
  }

  public Map<String, Object> getSysThemeConfig(String username) {
    EduConfigPersonalCriteria ex = new EduConfigPersonalCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andUsernameEqualTo(username);
    criteria.andConfigItemEqualTo(Cons.CONFIG_SYS_THEME);

    return mapper.selectMapByExampleForOne(ex);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduConfigPersonalCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer configId) {

    return mapper.deleteByPrimaryKey(configId);
  }

  @Override
  public EduConfigPersonal selectByPrimaryKey(Integer configId) {

    return mapper.selectByPrimaryKey(configId);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer configId) {

    return mapper.selectMapByPrimaryKey(configId);
  }
}
