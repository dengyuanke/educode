package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduTeacherMapper;
import com.stepiot.model.EduTeacher;
import com.stepiot.model.EduTeacherCriteria;

@Service
public class EduTeacherService implements EduTeacherMapper {

  @Autowired
  private EduTeacherMapper mapper;

  @Override
  public long countByExample(EduTeacherCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduTeacherCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer no) {

    return mapper.deleteByPrimaryKey(no);
  }

  @Override
  public int insert(EduTeacher record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduTeacher record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduTeacher> selectByExample(EduTeacherCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduTeacher selectByPrimaryKey(Integer no) {

    return mapper.selectByPrimaryKey(no);
  }

  @Override
  public int updateByExampleSelective(EduTeacher record, EduTeacherCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduTeacher record, EduTeacherCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduTeacher record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduTeacher record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduTeacher selectByExampleForOne(EduTeacherCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer no) {

    return mapper.selectMapByPrimaryKey(no);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduTeacherCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduTeacherCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
