package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.stepiot.dao.EduUserCourseJuniorMapper;
import com.stepiot.dao.ext.EduCourseJuniorMapperExt;
import com.stepiot.model.EduUserCourseJunior;
import com.stepiot.model.EduUserCourseJuniorCriteria;
import com.stepiot.model.EduUserCourseJuniorWithBLOBs;

@Service
public class EduUserCourseJuniorService
    implements EduUserCourseJuniorMapper, EduCourseJuniorMapperExt {

  @Autowired
  private EduUserCourseJuniorMapper mapper;

  @Autowired
  private EduCourseJuniorMapperExt mapperExt;

  @Override
  public long countByExample(EduUserCourseJuniorCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserCourseJuniorCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String courseUid) {

    return mapper.deleteByPrimaryKey(courseUid);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int insert(EduUserCourseJuniorWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserCourseJuniorWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserCourseJuniorWithBLOBs> selectByExampleWithBLOBs(
      EduUserCourseJuniorCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserCourseJunior> selectByExample(EduUserCourseJuniorCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserCourseJuniorWithBLOBs selectByPrimaryKey(String courseUid) {

    return mapper.selectByPrimaryKey(courseUid);
  }

  @Override
  public int updateByExampleSelective(EduUserCourseJuniorWithBLOBs record,
      EduUserCourseJuniorCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int complete(EduUserCourseJuniorWithBLOBs record, EduUserCourseJuniorCriteria example,
      EduUserCourseJuniorWithBLOBs next) {
    if (next != null) {
      mapper.insert(next);
    }
    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserCourseJuniorWithBLOBs record,
      EduUserCourseJuniorCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserCourseJunior record, EduUserCourseJuniorCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserCourseJuniorWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserCourseJuniorWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserCourseJunior record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserCourseJunior selectByExampleForOne(EduUserCourseJuniorCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String courseUid) {

    return mapper.selectMapByPrimaryKey(courseUid);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserCourseJuniorCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserCourseJuniorCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }
}
