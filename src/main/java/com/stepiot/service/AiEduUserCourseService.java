package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.stepiot.dao.AiEduUserCourseMapper;
import com.stepiot.dao.ext.AiEduUserCourseMapperExt;
import com.stepiot.model.AiEduUserCourse;
import com.stepiot.model.AiEduUserCourseCriteria;
import com.stepiot.model.AiEduUserCourseWithBLOBs;

@Service
public class AiEduUserCourseService implements AiEduUserCourseMapper, AiEduUserCourseMapperExt {

  @Autowired
  private AiEduUserCourseMapper mapper;

  @Autowired
  private AiEduUserCourseMapperExt mapperExt;

  @Override
  public long countByExample(AiEduUserCourseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(AiEduUserCourseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int insert(AiEduUserCourseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(AiEduUserCourseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<AiEduUserCourseWithBLOBs> selectByExampleWithBLOBs(AiEduUserCourseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<AiEduUserCourse> selectByExample(AiEduUserCourseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public int updateByExampleSelective(AiEduUserCourseWithBLOBs record,
      AiEduUserCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(AiEduUserCourseWithBLOBs record,
      AiEduUserCourseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(AiEduUserCourse record, AiEduUserCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public AiEduUserCourse selectByExampleForOne(AiEduUserCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(AiEduUserCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(AiEduUserCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String courseUid) {

    return mapper.deleteByPrimaryKey(courseUid);
  }

  @Override
  public AiEduUserCourseWithBLOBs selectByPrimaryKey(String courseUid) {

    return mapper.selectByPrimaryKey(courseUid);
  }

  @Override
  public int updateByPrimaryKeySelective(AiEduUserCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(AiEduUserCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(AiEduUserCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String courseUid) {

    return mapper.selectMapByPrimaryKey(courseUid);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int complete(AiEduUserCourseWithBLOBs record, AiEduUserCourseCriteria example,
      AiEduUserCourseWithBLOBs next) {
    if (next != null) {
      mapper.insert(next);
    }
    return mapper.updateByExampleSelective(record, example);
  }
}
