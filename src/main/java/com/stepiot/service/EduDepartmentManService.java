package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduDepartmentMapper;
import com.stepiot.model.EduDepartment;
import com.stepiot.model.EduDepartmentCriteria;

@Service
public class EduDepartmentManService implements EduDepartmentMapper {

  @Autowired
  private EduDepartmentMapper dMapper;

  @Override
  public long countByExample(EduDepartmentCriteria example) {

    return dMapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduDepartmentCriteria example) {

    return dMapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String departmentId) {

    return dMapper.deleteByPrimaryKey(departmentId);
  }

  @Override
  public int insert(EduDepartment record) {

    return dMapper.insert(record);
  }

  @Override
  public int insertSelective(EduDepartment record) {

    return dMapper.insertSelective(record);
  }

  @Override
  public List<EduDepartment> selectByExample(EduDepartmentCriteria example) {

    return dMapper.selectByExample(example);
  }

  @Override
  public EduDepartment selectByPrimaryKey(String departmentId) {

    return dMapper.selectByPrimaryKey(departmentId);
  }

  @Override
  public int updateByExampleSelective(EduDepartment record, EduDepartmentCriteria example) {

    return dMapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduDepartment record, EduDepartmentCriteria example) {

    return dMapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduDepartment record) {

    return dMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduDepartment record) {

    return dMapper.updateByPrimaryKey(record);
  }

  @Override
  public EduDepartment selectByExampleForOne(EduDepartmentCriteria example) {

    return dMapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String departmentId) {

    return dMapper.selectMapByPrimaryKey(departmentId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduDepartmentCriteria example) {

    return dMapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduDepartmentCriteria example) {

    return dMapper.selectMapByExample(example);
  }

}
