package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserBlocklyMapper;
import com.stepiot.model.EduUserBlockly;
import com.stepiot.model.EduUserBlocklyCriteria;
import com.stepiot.model.EduUserBlocklyWithBLOBs;

@Service
public class EduUserBlocklyService implements EduUserBlocklyMapper {

  @Autowired
  private EduUserBlocklyMapper mapper;

  @Override
  public long countByExample(EduUserBlocklyCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserBlocklyCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String blocklyId) {

    return mapper.deleteByPrimaryKey(blocklyId);
  }

  @Override
  public int insert(EduUserBlocklyWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserBlocklyWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserBlocklyWithBLOBs> selectByExampleWithBLOBs(EduUserBlocklyCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserBlockly> selectByExample(EduUserBlocklyCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserBlocklyWithBLOBs selectByPrimaryKey(String blocklyId) {

    return mapper.selectByPrimaryKey(blocklyId);
  }

  @Override
  public int updateByExampleSelective(EduUserBlocklyWithBLOBs record,
      EduUserBlocklyCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserBlocklyWithBLOBs record,
      EduUserBlocklyCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserBlockly record, EduUserBlocklyCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserBlocklyWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserBlocklyWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserBlockly record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserBlockly selectByExampleForOne(EduUserBlocklyCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String blocklyId) {

    return mapper.selectMapByPrimaryKey(blocklyId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserBlocklyCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserBlocklyCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
