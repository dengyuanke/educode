package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.SysHardwaresMapper;
import com.stepiot.model.SysHardwares;
import com.stepiot.model.SysHardwaresCriteria;

@Service
public class SysHardwaresService implements SysHardwaresMapper {

  @Autowired
  private SysHardwaresMapper mapper;

  @Override
  public long countByExample(SysHardwaresCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(SysHardwaresCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer id) {

    return mapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(SysHardwares record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(SysHardwares record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<SysHardwares> selectByExample(SysHardwaresCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public SysHardwares selectByPrimaryKey(Integer id) {

    return mapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByExampleSelective(SysHardwares record, SysHardwaresCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(SysHardwares record, SysHardwaresCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(SysHardwares record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(SysHardwares record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public SysHardwares selectByExampleForOne(SysHardwaresCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer id) {

    return mapper.selectMapByPrimaryKey(id);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(SysHardwaresCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(SysHardwaresCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
