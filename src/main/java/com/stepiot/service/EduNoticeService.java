package com.stepiot.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Maps;
import com.stepiot.beans.EduNoticeBean;
import com.stepiot.constants.NoticeRecipientStatus;
import com.stepiot.dao.EduNoticeMapper;
import com.stepiot.dao.EduNoticeRecipientMapper;
import com.stepiot.dao.ext.EduNoticeMapperExt;
import com.stepiot.model.EduNotice;
import com.stepiot.model.EduNoticeCriteria;
import com.stepiot.model.EduNoticeRecipient;
import com.stepiot.model.EduNoticeRecipientCriteria;
import com.stepiot.model.EduNoticeRecipientCriteria.Criteria;
import com.stepiot.model.EduNoticeWithBLOBs;

@Service
public class EduNoticeService implements EduNoticeMapper, EduNoticeMapperExt {

  @Autowired
  private EduNoticeMapper mapper;
  @Autowired
  private EduNoticeMapperExt ext;
  @Autowired
  private EduNoticeRecipientMapper rmapper;
  @Autowired
  private DataSource ds;

  @Override
  public long countByExample(EduNoticeCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduNoticeCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer noticeId) {

    return mapper.deleteByPrimaryKey(noticeId);
  }

  @Override
  public int insert(EduNoticeWithBLOBs record) {

    return mapper.insert(record);
  }

  public int insertThenReturnPrimaryKey(EduNoticeWithBLOBs record) {
    mapper.insert(record);
    if (StringUtils.isNotEmpty(record.getUuid())) {
      String uuid = record.getUuid();
      EduNoticeCriteria ex = new EduNoticeCriteria();
      com.stepiot.model.EduNoticeCriteria.Criteria ext = ex.createCriteria();
      ext.andUuidEqualTo(uuid);
      EduNotice one = mapper.selectByExampleForOne(ex);
      if (one != null) {
        return one.getNoticeId();
      }
    }

    return 0;
  }

  @Override
  public int insertSelective(EduNoticeWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduNoticeWithBLOBs> selectByExampleWithBLOBs(EduNoticeCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduNotice> selectByExample(EduNoticeCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduNoticeWithBLOBs selectByPrimaryKey(Integer noticeId) {

    return mapper.selectByPrimaryKey(noticeId);
  }

  @Override
  public int updateByExampleSelective(EduNoticeWithBLOBs record, EduNoticeCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduNoticeWithBLOBs record, EduNoticeCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduNotice record, EduNoticeCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduNoticeWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduNoticeWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduNotice record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduNotice selectByExampleForOne(EduNoticeCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer noticeId) {

    return mapper.selectMapByPrimaryKey(noticeId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduNoticeCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduNoticeCriteria example) {

    return mapper.selectMapByExample(example);
  }

  public List<EduNoticeBean> listMyNotice(Map<String, Object> map) {
    return ext.listMyNotice(map);

  }

  public int markNoticeAsReadById(String username, Integer id) {
    EduNoticeRecipientCriteria c = new EduNoticeRecipientCriteria();
    Criteria ex = c.createCriteria();
    ex.andRecipientUserEqualTo(username).andNoticeIdEqualTo(id);
    EduNoticeRecipient recipient = new EduNoticeRecipient();
    recipient.setRecipientStatus(NoticeRecipientStatus.READED.getValue());
    return rmapper.updateByExampleSelective(recipient, c);

  }

  public int markNoticeAsReadByIds(String username, List<Integer> ids) {
    EduNoticeRecipientCriteria c = new EduNoticeRecipientCriteria();
    Criteria ex = c.createCriteria();
    ex.andRecipientUserEqualTo(username).andNoticeIdIn(ids);
    EduNoticeRecipient recipient = new EduNoticeRecipient();
    recipient.setRecipientStatus(NoticeRecipientStatus.READED.getValue());
    return rmapper.updateByExampleSelective(recipient, c);

  }

  /**
   * 获取未读通知的数量
   * 
   * @param username
   * @return
   */
  public long countUnReadedNews(String username) {
    EduNoticeRecipientCriteria c = new EduNoticeRecipientCriteria();
    Criteria ex = c.createCriteria();
    ex.andRecipientUserEqualTo(username)
        .andRecipientStatusEqualTo(NoticeRecipientStatus.UNREADED.getValue());
    long counts = rmapper.countByExample(c);
    return counts;
  }


  /**
   * 获取未读通知的数量
   * 
   * @param username
   * @return
   */
  public List<EduNoticeBean> listLatestNews(int count, String username) {

    Map<String, Object> param = Maps.newHashMap();
    param.put("limitStart", 0);
    param.put("pageSize", count);
    param.put("username", username);

    return ext.listUnReadedNews(param);
  }

  public long totalRecipient(String username) {
    EduNoticeRecipientCriteria criteria = new EduNoticeRecipientCriteria();
    Criteria ex = criteria.createCriteria();
    ex.andRecipientUserEqualTo(username);
    return rmapper.countByExample(criteria);
  }


  private static final int PER_INSERT_COUNT = 500;

  /**
   * 
   */
  public void insertNoticeRecipient(Set<String> usernames, int noticeId) {

    int size = usernames.size();
    int times = size / PER_INSERT_COUNT;
    int left = size % PER_INSERT_COUNT;
    int index = 0;

    Connection conn = null;
    PreparedStatement statement = null;
    try {
      conn = ds.getConnection();
      statement = conn.prepareStatement(
          "INSERT INTO `edu_notice_recipient`(`notice_id`,`recipient_user`,`recipient_status`)VALUES(?,?,?);");
      List<String> list = new ArrayList<String>(usernames);
      if (times > 0) {
        for (int i = 0; i < times; i++) {
          for (int j = 0; j < PER_INSERT_COUNT; j++) {
            statement.setInt(1, noticeId);
            statement.setString(2, list.get(index++));
            statement.setInt(3, 0);
            statement.addBatch();
          }
          statement.executeBatch();

        }
      }
      // 处理余数
      if (left > 0) {
        for (int i = 0; i < left; i++) {
          statement.setInt(1, noticeId);
          statement.setString(2, list.get(index++));
          statement.setInt(3, 0);
          statement.addBatch();
        }
        statement.executeBatch();

      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      if (statement != null) {
        try {
          statement.close();
        } catch (SQLException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public List<EduNoticeBean> listSentNotice(Map<String, Object> map) {
    return ext.listSentNotice(map);
  }

  public long totalSent(String username) {
    EduNoticeCriteria criteria = new EduNoticeCriteria();
    com.stepiot.model.EduNoticeCriteria.Criteria ext = criteria.createCriteria();
    ext.andCreateUserEqualTo(username);

    return mapper.countByExample(criteria);
  }

  @Override
  public List<EduNoticeBean> listUnReadedNews(Map<String, Object> map) {
    return ext.listUnReadedNews(map);
  }



}
