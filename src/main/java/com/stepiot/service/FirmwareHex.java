package com.stepiot.service;

import java.io.IOException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tika.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class FirmwareHex {

  private byte[] microbit = null;
  private byte[] nestblock = null;
  private byte[] nestcode = null;

  @Autowired
  FirmwareHex(ApplicationContext appContext) throws IOException {
    microbit =
        IOUtils.toByteArray(appContext.getResource("classpath:microbit.hex").getInputStream());
    nestblock =
        IOUtils.toByteArray(appContext.getResource("classpath:nestblock.hex").getInputStream());
    nestcode =
        IOUtils.toByteArray(appContext.getResource("classpath:nestcode.hex").getInputStream());
  }

  public byte[] toMicrobitHex(String script) {

    int[] data = new int[4 + script.length() + (16 - (4 + script.length()) % 16)];
    data[0] = 77; // 'M'
    data[1] = 80; // 'P'
    data[2] = script.length() & 0xff;
    data[3] = (script.length() >> 8) & 0xff;

    char[] py = script.toCharArray();
    for (int i = 0; i < py.length; ++i) {
      data[4 + i] = Character.codePointAt(py, i);
    }
    // check data.length < 0x2000
    if (data.length > 8192) {
      throw new java.lang.IllegalArgumentException("文件过大");
    }

    // convert to .hex format
    int addr = 0x0003E000; // magic start address in flash
    int[] chunk = new int[5 + 16];

    StringBuilder output = new StringBuilder();
    for (int i = 0; i < data.length; i += 16, addr += 16) {
      chunk[0] = 16; // length of data section
      chunk[1] = (addr >> 8) & 0xff; // high byte of 16-bit addr
      chunk[2] = addr & 0xff; // low byte of 16-bit addr
      chunk[3] = 0; // type (data)
      for (int j = 0; j < 16; ++j) {
        chunk[4 + j] = data[i + j];
      }
      int checksum = 0;
      for (int j = 0; j < 4 + 16; ++j) {
        checksum += chunk[j];
      }
      chunk[4 + 16] = (-checksum) & 0xff;
      output.append(':' + intArraytoHex(chunk).toUpperCase() + "\n");
    }
    output.append(":020000041000EA\n");
    output.append(":1010C0007CB0EE17FFFFFFFF0A0000000000E30006\n");
    output.append(":0C10D000FFFFFFFF2D6D0300000000007B\n");
    output.append(":0400000500018E2147\n");
    output.append(":00000001FF\n");
    byte[] microbitHex = ArrayUtils.addAll(microbit, output.toString().getBytes());
    return microbitHex;
  }

  public byte[] toNestblockHexBackup(String script) {

    int[] data = new int[4 + script.length() + (16 - (4 + script.length()) % 16)];
    data[0] = 77; // 'M'
    data[1] = 80; // 'P'
    data[2] = script.length() & 0xff;
    data[3] = (script.length() >> 8) & 0xff;

    char[] py = script.toCharArray();
    for (int i = 0; i < py.length; ++i) {
      data[4 + i] = Character.codePointAt(py, i);
    }
    // check data.length < 0x2000
    if (data.length > 8192) {
      throw new java.lang.IllegalArgumentException("文件过大");
    }

    // convert to .hex format
    int addr = 0x080d0000; // magic start address in flash
    int[] chunk = new int[5 + 16];

    StringBuilder output = new StringBuilder();
    for (int i = 0; i < data.length; i += 16, addr += 16) {
      chunk[0] = 16; // length of data section
      chunk[1] = (addr >> 8) & 0xff; // high byte of 16-bit addr
      chunk[2] = addr & 0xff; // low byte of 16-bit addr
      chunk[3] = 0; // type (data)
      for (int j = 0; j < 16; ++j) {
        chunk[4 + j] = data[i + j];
      }
      int checksum = 0;
      for (int j = 0; j < 4 + 16; ++j) {
        checksum += chunk[j];
      }
      chunk[4 + 16] = (-checksum) & 0xff;
      output.append(':' + intArraytoHex(chunk).toUpperCase() + "\n");
    }
    output.append(":040000050804D551C5\n");
    output.append(":00000001FF\n");
    byte[] microbitHex = ArrayUtils.addAll(nestblock, output.toString().getBytes());
    return microbitHex;
  }


  public byte[] toNestblockHex(String script) {

    int[] data = new int[4 + script.length() + (16 - (4 + script.length()) % 16)];
    data[0] = 77; // 'M'
    data[1] = 80; // 'P'
    data[2] = script.length() & 0xff;
    data[3] = (script.length() >> 8) & 0xff;

    char[] py = script.toCharArray();
    for (int i = 0; i < py.length; ++i) {
      data[4 + i] = Character.codePointAt(py, i);
    }
    // check data.length < 0x2000
    if (data.length > 8192) {
      throw new java.lang.IllegalArgumentException("文件过大");
    }

    // convert to .hex format
    int addr = 0x080d0000; // magic start address in flash
    int[] chunk = new int[5 + 16];

    StringBuilder output = new StringBuilder();
    for (int i = 0; i < data.length; i += 16, addr += 16) {
      chunk[0] = 16; // length of data section
      chunk[1] = (addr >> 8) & 0xff; // high byte of 16-bit addr
      chunk[2] = addr & 0xff; // low byte of 16-bit addr
      chunk[3] = 0; // type (data)
      for (int j = 0; j < 16; ++j) {
        chunk[4 + j] = data[i + j];
      }
      int checksum = 0;
      for (int j = 0; j < 4 + 16; ++j) {
        checksum += chunk[j];
      }
      chunk[4 + 16] = (-checksum) & 0xff;
      output.append(':' + intArraytoHex(chunk).toUpperCase() + "\n");
    }
    output.append(":040000050804B3E94F\n");
    output.append(":00000001FF\n");
    byte[] microbitHex = ArrayUtils.addAll(nestblock, output.toString().getBytes());
    return microbitHex;
  }

  public byte[] toNestblockCodeHex(String script) {

    // 前两个字节标示长度MP，第三四个字节标示script总长度; [// (16 - (4 + script.length()) % 16)] 为了与16对齐
    int[] data = new int[4 + script.length() + (16 - (4 + script.length()) % 16)];
    data[0] = 77; // 'M'
    data[1] = 80; // 'P'
    data[2] = script.length() & 0xff;
    data[3] = (script.length() >> 8) & 0xff;

    char[] py = script.toCharArray();
    for (int i = 0; i < py.length; ++i) {
      data[4 + i] = Character.codePointAt(py, i);
    }
    // check data.length < 0x2000
    if (data.length > 8192) {
      throw new java.lang.IllegalArgumentException("文件过大");
    }

    // convert to .hex format
    int addr = 0x080d0000; // magic start address in flash
    int[] chunk = new int[5 + 16];

    StringBuilder output = new StringBuilder();
    for (int i = 0; i < data.length; i += 16, addr += 16) {
      chunk[0] = 16; // length of data section
      chunk[1] = (addr >> 8) & 0xff; // high byte of 16-bit addr
      chunk[2] = addr & 0xff; // low byte of 16-bit addr
      chunk[3] = 0; // type (data)
      for (int j = 0; j < 16; ++j) {
        chunk[4 + j] = data[i + j];
      }
      int checksum = 0;
      for (int j = 0; j < 4 + 16; ++j) {
        checksum += chunk[j];
      }
      chunk[4 + 16] = (-checksum) & 0xff;
      output.append(':' + intArraytoHex(chunk).toUpperCase() + "\n");
    }
    // output.append(":040000050804B3E94F\n");
    output.append(":00000001FF\n");
    byte[] codes = ArrayUtils.addAll(nestcode, output.toString().getBytes());
    return codes;
  }


  public byte[] toNestblockHex_backup(String script) {

    int[] data = new int[4 + script.length() + (16 - (4 + script.length()) % 16)];
    data[0] = 77; // 'M'
    data[1] = 80; // 'P'
    data[2] = script.length() & 0xff;
    data[3] = (script.length() >> 8) & 0xff;

    char[] py = script.toCharArray();
    for (int i = 0; i < py.length; ++i) {
      data[4 + i] = Character.codePointAt(py, i);
    }
    // check data.length < 0x2000
    if (data.length > 8192) {
      throw new java.lang.IllegalArgumentException("文件过大");
    }

    // convert to .hex format
    int addr = 0x080d0000; // magic start address in flash
    int[] chunk = new int[5 + 16];

    StringBuilder output = new StringBuilder();
    for (int i = 0; i < data.length; i += 16, addr += 16) {
      chunk[0] = 16; // length of data section
      chunk[1] = (addr >> 8) & 0xff; // high byte of 16-bit addr
      chunk[2] = addr & 0xff; // low byte of 16-bit addr
      chunk[3] = 0; // type (data)
      for (int j = 0; j < 16; ++j) {
        chunk[4 + j] = data[i + j];
      }
      int checksum = 0;
      for (int j = 0; j < 4 + 16; ++j) {
        checksum += chunk[j];
      }
      chunk[4 + 16] = (-checksum) & 0xff;
      output.append(':' + intArraytoHex(chunk).toUpperCase() + "\n");
    }
    output.append(":040000050804DA61B0\n");
    output.append(":00000001FF\n");
    byte[] microbitHex = ArrayUtils.addAll(nestblock, output.toString().getBytes());
    return microbitHex;
  }



  private static String intArraytoHex(int[] iarray) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < iarray.length; ++i) {
      if (iarray[i] < 16) {
        result.append('0');
      }
      result.append(Integer.toHexString(iarray[i]));
    }
    return result.toString();

  }

}

