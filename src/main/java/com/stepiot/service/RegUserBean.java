package com.stepiot.service;

public class RegUserBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8078399630084946968L;
  private static final int MAX_TRY_TIMES = 5;
  private String username;
  private String password;
  private String telephone;
  private String vcode;
  private String captcha;
  private String captchaBase64;
  private int tryTimes;


  public boolean exceedMaxTimes() {
    return this.getTryTimes() >= MAX_TRY_TIMES;
  }

  public int getTryTimes() {
    return tryTimes;
  }

  public void increateTryTimes() {
    this.tryTimes += 1;
  }

  public void resetTryTimes() {
    this.tryTimes = 0;
  }


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getTelephone() {
    return this.telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }



  @Override
  public String toString() {
    return "RegUserBean [username=" + username + ", password=" + password + ", telephone="
        + telephone + ", vcode=" + vcode + ", captcha=" + captcha + ", captchaBase64="
        + captchaBase64 + ", tryTimes=" + tryTimes + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((captcha == null) ? 0 : captcha.hashCode());
    result = prime * result + ((captchaBase64 == null) ? 0 : captchaBase64.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
    result = prime * result + tryTimes;
    result = prime * result + ((username == null) ? 0 : username.hashCode());
    result = prime * result + ((vcode == null) ? 0 : vcode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RegUserBean other = (RegUserBean) obj;
    if (captcha == null) {
      if (other.captcha != null)
        return false;
    } else if (!captcha.equals(other.captcha))
      return false;
    if (captchaBase64 == null) {
      if (other.captchaBase64 != null)
        return false;
    } else if (!captchaBase64.equals(other.captchaBase64))
      return false;
    if (password == null) {
      if (other.password != null)
        return false;
    } else if (!password.equals(other.password))
      return false;
    if (telephone == null) {
      if (other.telephone != null)
        return false;
    } else if (!telephone.equals(other.telephone))
      return false;
    if (tryTimes != other.tryTimes)
      return false;
    if (username == null) {
      if (other.username != null)
        return false;
    } else if (!username.equals(other.username))
      return false;
    if (vcode == null) {
      if (other.vcode != null)
        return false;
    } else if (!vcode.equals(other.vcode))
      return false;
    return true;
  }

  public String getCaptcha() {
    return captcha;
  }

  public void setCaptcha(String captcha) {
    this.captcha = captcha;
  }

  public String getCaptchaBase64() {
    return captchaBase64;
  }

  public void setCaptchaBase64(String captchaBase64) {
    this.captchaBase64 = captchaBase64;
  }

  public String getVcode() {
    return vcode;
  }

  public void setVcode(String vcode) {
    this.vcode = vcode;
  }

}
