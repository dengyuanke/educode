package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduPubDownMapper;
import com.stepiot.model.EduPubDown;
import com.stepiot.model.EduPubDownCriteria;

@Service
public class EduPubDownService implements EduPubDownMapper {

  @Autowired
  private EduPubDownMapper mapper;

  @Override
  public long countByExample(EduPubDownCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduPubDownCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer id) {

    return mapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(EduPubDown record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduPubDown record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduPubDown> selectByExample(EduPubDownCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduPubDown selectByPrimaryKey(Integer id) {

    return mapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByExampleSelective(EduPubDown record, EduPubDownCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduPubDown record, EduPubDownCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduPubDown record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduPubDown record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduPubDown selectByExampleForOne(EduPubDownCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer id) {

    return mapper.selectMapByPrimaryKey(id);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduPubDownCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduPubDownCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
