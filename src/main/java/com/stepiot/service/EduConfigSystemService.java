package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduConfigSystemMapper;
import com.stepiot.model.EduConfigSystem;
import com.stepiot.model.EduConfigSystemCriteria;
import com.stepiot.model.EduConfigSystemCriteria.Criteria;

@Service
public class EduConfigSystemService implements EduConfigSystemMapper {


  @Autowired
  private EduConfigSystemMapper sys;

  @Override
  public long countByExample(EduConfigSystemCriteria example) {

    return sys.countByExample(example);
  }

  @Override
  public int deleteByExample(EduConfigSystemCriteria example) {

    return sys.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String configItem) {

    return sys.deleteByPrimaryKey(configItem);
  }

  @Override
  public int insert(EduConfigSystem record) {

    return sys.insert(record);
  }

  @Override
  public int insertSelective(EduConfigSystem record) {

    return sys.insertSelective(record);
  }

  @Override
  public List<EduConfigSystem> selectByExample(EduConfigSystemCriteria example) {

    return sys.selectByExample(example);
  }

  @Override
  public EduConfigSystem selectByPrimaryKey(String configItem) {

    return sys.selectByPrimaryKey(configItem);
  }

  @Override
  public int updateByExampleSelective(EduConfigSystem record, EduConfigSystemCriteria example) {

    return sys.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduConfigSystem record, EduConfigSystemCriteria example) {

    return sys.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduConfigSystem record) {

    return sys.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduConfigSystem record) {

    return sys.updateByPrimaryKey(record);
  }

  @Override
  public EduConfigSystem selectByExampleForOne(EduConfigSystemCriteria example) {

    return sys.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String configItem) {

    return sys.selectMapByPrimaryKey(configItem);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduConfigSystemCriteria example) {

    return sys.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduConfigSystemCriteria example) {

    return sys.selectMapByExample(example);
  }

  public Map<String, Object> getConfigByKey(String key) {
    EduConfigSystemCriteria ex = new EduConfigSystemCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andConfigItemEqualTo(key);
    return sys.selectMapByExampleForOne(ex);
  }
}
