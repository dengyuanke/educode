package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserMediaMapper;
import com.stepiot.model.EduUserMedia;
import com.stepiot.model.EduUserMediaCriteria;

@Service
public class EduUserMediaService implements EduUserMediaMapper {

  @Autowired
  private EduUserMediaMapper mapper;

  @Override
  public long countByExample(EduUserMediaCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserMediaCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String mediaUid) {

    return mapper.deleteByPrimaryKey(mediaUid);
  }

  @Override
  public int insert(EduUserMedia record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserMedia record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserMedia> selectByExample(EduUserMediaCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserMedia selectByPrimaryKey(String mediaUid) {

    return mapper.selectByPrimaryKey(mediaUid);
  }

  @Override
  public int updateByExampleSelective(EduUserMedia record, EduUserMediaCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduUserMedia record, EduUserMediaCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserMedia record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserMedia record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserMedia selectByExampleForOne(EduUserMediaCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String mediaUid) {

    return mapper.selectMapByPrimaryKey(mediaUid);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserMediaCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserMediaCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
