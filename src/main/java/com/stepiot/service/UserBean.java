package com.stepiot.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.google.common.collect.Lists;
import com.stepiot.model.EduClass;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserRole;

public class UserBean implements UserDetails {

  private EduUser eduUser = null;

  private List<EduUserRole> roles = null;

  private EduClass clazzInfo = null;

  public UserBean(EduUser eduUser, List<EduUserRole> roles) {
    this.eduUser = eduUser;
    this.roles = roles;
  }

  public void setClazzInfo(EduClass clazzInfo) {
    this.clazzInfo = clazzInfo;
  }

  public String getClassName() {
    if (clazzInfo == null) {
      return null;
    }
    return clazzInfo.getClassName();
  }

  public String getClassId() {
    if (clazzInfo == null) {
      return null;
    }
    return clazzInfo.getClassId();
  }

  public Date getPasswordLut() {
    return eduUser.getPasswordLut();
  }

  /**
   * 
   */
  private static final long serialVersionUID = 5981965926099681676L;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    ArrayList<GrantedAuthority> list = Lists.newArrayList();
    for (int i = 0; i < roles.size(); i++) {
      EduUserRole role = roles.get(i);
      list.add(new SimpleGrantedAuthority(role.getRole()));
    }
    return list;
  }

  @Override
  public String getPassword() {

    return eduUser.getPassword();
  }

  @Override
  public String getUsername() {

    return eduUser.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {

    return true;
  }

  @Override
  public boolean isAccountNonLocked() {

    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {

    return true;
  }

  @Override
  public boolean isEnabled() {
    if (eduUser == null) {
      return false;
    }
    return eduUser.getEnabled().byteValue() > (byte) 0;
  }

}
