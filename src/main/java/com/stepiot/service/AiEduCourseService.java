package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.AiEduCourseMapper;
import com.stepiot.model.AiEduCourse;
import com.stepiot.model.AiEduCourseCriteria;
import com.stepiot.model.AiEduCourseWithBLOBs;

@Service
public class AiEduCourseService implements AiEduCourseMapper {

  @Autowired
  private AiEduCourseMapper mapper;

  @Override
  public long countByExample(AiEduCourseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(AiEduCourseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int insert(AiEduCourseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(AiEduCourseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<AiEduCourseWithBLOBs> selectByExampleWithBLOBs(AiEduCourseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<AiEduCourse> selectByExample(AiEduCourseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public int updateByExampleSelective(AiEduCourseWithBLOBs record, AiEduCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(AiEduCourseWithBLOBs record, AiEduCourseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(AiEduCourse record, AiEduCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public AiEduCourse selectByExampleForOne(AiEduCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(AiEduCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(AiEduCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {

    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public AiEduCourseWithBLOBs selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByPrimaryKeySelective(AiEduCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(AiEduCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(AiEduCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {

    return mapper.selectMapByPrimaryKey(courseId);
  }
}
