package com.stepiot.service;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.stepiot.proto.HttpProto;
import com.stepiot.util.ProtoUtils;

@Service
public class ProtoResponseService {

  @Autowired
  ApplicationContext appContext;

  public ImmutableMap<String, String> of(String k, String v) {
    String message = appContext.getMessage(v, null, LocaleContextHolder.getLocale());
    return ImmutableBiMap.of(k, message);
  }

  public ImmutableMap<String, String> of(String k, String v, String k1, String v1) {
    String message = appContext.getMessage(v, null, LocaleContextHolder.getLocale());
    String message1 = appContext.getMessage(v1, null, LocaleContextHolder.getLocale());
    return ImmutableBiMap.of(k, message, k1, message1);
  }

  public ImmutableMap<String, String> of(String k, String v, String k1, String v1, String k2,
      String v2) {
    String message = appContext.getMessage(v, null, LocaleContextHolder.getLocale());
    String message1 = appContext.getMessage(v1, null, LocaleContextHolder.getLocale());
    String message2 = appContext.getMessage(v2, null, LocaleContextHolder.getLocale());
    return ImmutableBiMap.of(k, message, k1, message1, k2, message2);
  }

  public ImmutableMap<String, String> of(String k, String v, String k1, String v1, String k2,
      String v2, String k3, String v3) {
    String message = appContext.getMessage(v, null, LocaleContextHolder.getLocale());
    String message1 = appContext.getMessage(v1, null, LocaleContextHolder.getLocale());
    String message2 = appContext.getMessage(v2, null, LocaleContextHolder.getLocale());
    String message3 = appContext.getMessage(v3, null, LocaleContextHolder.getLocale());
    return ImmutableBiMap.of(k, message, k1, message1, k2, message2, k3, message3);
  }

  public ImmutableMap<String, String> of(String k, String v, String k1, String v1, String k2,
      String v2, String k3, String v3, String k4, String v4) {
    String message = appContext.getMessage(v, null, LocaleContextHolder.getLocale());
    String message1 = appContext.getMessage(v1, null, LocaleContextHolder.getLocale());
    String message2 = appContext.getMessage(v2, null, LocaleContextHolder.getLocale());
    String message3 = appContext.getMessage(v3, null, LocaleContextHolder.getLocale());
    String message4 = appContext.getMessage(v4, null, LocaleContextHolder.getLocale());
    return ImmutableBiMap.of(k, message, k1, message1, k2, message2, k3, message3, k4, message4);
  }

  public String response(HttpStatus status, String nextUrl, String messageKey,
      Map<String, String> msgMap) {
    com.stepiot.proto.HttpProto.Res.Builder builder = HttpProto.Res.newBuilder();
    builder.setStatusCode(status.value());
    if (StringUtils.isNoneBlank(messageKey)) {
      String message;
      try {
        message = appContext.getMessage(messageKey, null, LocaleContextHolder.getLocale());
        builder.setMessage(message);
      } catch (NoSuchMessageException e) {
        builder.setMessage(messageKey);
      }

    }
    if (StringUtils.isNoneBlank(nextUrl)) {
      builder.setNextUrl(nextUrl);
    }
    if (msgMap != null && msgMap.size() > 0) {
      builder.putAllMsgMap(msgMap);
    }
    return ProtoUtils.buildResponse(builder.build());
  }

  public String response(HttpStatus status, String messageKey, Map<String, String> msgMap) {
    return this.response(status, null, messageKey, msgMap);
  }

  public String response(HttpStatus status, Map<String, String> msgMap) {
    return this.response(status, null, null, msgMap);
  }

  public String response(HttpStatus status, String nextUrl, String messageKey) {
    return this.response(status, nextUrl, messageKey, null);
  }

}
