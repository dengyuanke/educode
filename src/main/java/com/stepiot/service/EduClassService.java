package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.beans.AssignmentDataBean;
import com.stepiot.beans.EduChartDataBean;
import com.stepiot.beans.EduClassCourseProgressBean;
import com.stepiot.beans.EduClassScoreBean;
import com.stepiot.dao.EduClassMapper;
import com.stepiot.dao.ext.EduClassMapperExt;
import com.stepiot.model.EduClass;
import com.stepiot.model.EduClassCriteria;
import com.stepiot.model.EduStudentClassRefCriteria;
import com.stepiot.model.EduTeacherClassRefCriteria;

@Service
public class EduClassService implements EduClassMapper, EduClassMapperExt {

  @Autowired
  private EduClassMapper mapper;

  @Autowired
  private EduTeacherRefService tservice;

  @Autowired
  private EduStudentRefService smapper;

  @Autowired
  private EduClassMapperExt ext;

  @Override
  public List<Map<String, Object>> listClasses(Map<String, Object> map) {
    return ext.listClasses(map);
  }

  public long countClasses(Map<String, Object> map) {
    long result = 0;
    List<Map<String, Object>> list = ext.listClasses(map);
    if (list == null) {
      return result;
    } else {
      result = list.size();
    }
    return result;
  }

  @Override
  public long countByExample(EduClassCriteria example) {

    return mapper.countByExample(example);
  }

  public long countClassByTeacherId(String username) {
    EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
    com.stepiot.model.EduTeacherClassRefCriteria.Criteria exam = ex.createCriteria();
    exam.andTearchIdEqualTo(username);

    return tservice.countByExample(ex);
  }

  public long countStudentByClassId(String classId) {
    EduStudentClassRefCriteria ex = new EduStudentClassRefCriteria();
    com.stepiot.model.EduStudentClassRefCriteria.Criteria exam = ex.createCriteria();
    exam.andClassIdEqualTo(classId);

    return smapper.countByExample(ex);
  }


  @Override
  public int deleteByExample(EduClassCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String classId) {

    return mapper.deleteByPrimaryKey(classId);
  }

  @Override
  public int insert(EduClass record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduClass record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduClass> selectByExample(EduClassCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduClass selectByPrimaryKey(String classId) {

    return mapper.selectByPrimaryKey(classId);
  }

  @Override
  public int updateByExampleSelective(EduClass record, EduClassCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduClass record, EduClassCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduClass record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduClass record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduClass selectByExampleForOne(EduClassCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String classId) {

    return mapper.selectMapByPrimaryKey(classId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduClassCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduClassCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listMyClasses(Map<String, Object> map) {
    return ext.listMyClasses(map);
  }

  @Override
  public List<Map<String, Object>> feedDepSelector(Map<String, Object> map) {
    return ext.feedDepSelector(map);
  }

  @Override
  public List<EduClassScoreBean> listClassScores(Map<String, Object> map) {
    return ext.listClassScores(map);
  }

  @Override
  public List<EduChartDataBean> chartClassCode(Map<String, Object> map) {

    return ext.chartClassCode(map);
  }

  @Override
  public List<AssignmentDataBean> queryAssignment(Map<String, Object> map) {
    return ext.queryAssignment(map);
  }

  @Override
  public List<EduClassCourseProgressBean> listClassCourseProgress(Map<String, Object> map) {
    return ext.listClassCourseProgress(map);
  }

}
