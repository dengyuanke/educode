package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduPythonCourseMapper;
import com.stepiot.dao.EduPythonCourseOutlineAnswerMapper;
import com.stepiot.model.EduPythonCourse;
import com.stepiot.model.EduPythonCourseCriteria;
import com.stepiot.model.EduPythonCourseOutlineAnswerCriteria;

@Service
public class EduPythonCourseService implements EduPythonCourseMapper {

  @Autowired
  private EduPythonCourseMapper mapper;

  @Autowired
  private EduPythonCourseOutlineAnswerMapper anser;

  private EduPythonCourseOutlineAnswerCriteria ex = new EduPythonCourseOutlineAnswerCriteria();

  public long countAllCourseLevels() {
    return anser.countByExample(ex);
  }

  @Override
  public long countByExample(EduPythonCourseCriteria example) {
    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduPythonCourseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {

    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public int insert(EduPythonCourse record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduPythonCourse record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduPythonCourse> selectByExample(EduPythonCourseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduPythonCourse selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByExampleSelective(EduPythonCourse record, EduPythonCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduPythonCourse record, EduPythonCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduPythonCourse record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduPythonCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduPythonCourse selectByExampleForOne(EduPythonCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {

    return mapper.selectMapByPrimaryKey(courseId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduPythonCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduPythonCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
