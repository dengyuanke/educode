package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.stepiot.model.EduClass;
import com.stepiot.model.EduClassCriteria;
import com.stepiot.model.EduDepartment;
import com.stepiot.model.EduDepartmentCriteria;
import com.stepiot.model.EduDiscipline;
import com.stepiot.model.EduDisciplineCriteria;
import com.stepiot.model.EduDisciplineCriteria.Criteria;
import com.stepiot.model.EduPythonCourseOutline;
import com.stepiot.model.EduPythonCourseOutlineCriteria;

@Service
public class SelectService {

  @Value("${stepiot.static.dns}")
  private String dnstatic;

  @Autowired
  private EduClassService cService;

  @Autowired
  private EduDepartmentManService dService;

  @Autowired
  private EduDisciplineService disService;

  @Autowired
  private EduPythonCourseOutlineService oservice;

  public void courseTypeSelect(Model model, boolean useBlank, String username, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(
          ImmutableMap.of("value", "0", "text", "请选择课程阶段", "icon", dnstatic + "/images/0.png"));
    }
    data.add(ImmutableMap.of("value", "1", "text", "认知阶段", "icon", dnstatic + "/images/1.png"));
    data.add(ImmutableMap.of("value", "2", "text", "初级阶段", "icon", dnstatic + "/images/2.png"));
    data.add(ImmutableMap.of("value", "3", "text", "高级阶段", "icon", dnstatic + "/images/3.png"));

    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    model.addAttribute("topSelector", bean);
  }

  public void genderSelector(Model model, boolean useBlank, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "0", "text", "请选择性别", "icon", dnstatic + "/images/0.png"));
    }
    data.add(ImmutableMap.of("value", "1", "text", "男", "icon", dnstatic + "/images/1.png"));
    data.add(ImmutableMap.of("value", "2", "text", "女", "icon", dnstatic + "/images/2.png"));

    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    model.addAttribute("genderSelector", bean);
  }

  /**
   * 院校下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void departmentSelect(Model model, boolean useBlank, String username, String selectedKey) {
    SelectBean bean = this.selectDepartment(useBlank, username, selectedKey);
    model.addAttribute("departmentSelector", bean);
  }

  /**
   * 院校下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void allDepartmentSelect(Model model, boolean useBlank, String selectedKey) {
    SelectBean bean = this.allSelectDepartment(useBlank, selectedKey);
    model.addAttribute("allDepartmentSelector", bean);
  }

  /**
   * 院校下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  private SelectBean allSelectDepartment(boolean useBlank, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择院校", "icon", dnstatic + "/images/0.png"));
    }
    Map<String, Object> param = Maps.newHashMap();
    param.put("groupByClause", "department_id");

    List<EduDepartment> list = dService.selectByExample(new EduDepartmentCriteria());
    for (int i = 0; i < list.size(); i++) {
      EduDepartment department = list.get(i);
      data.add(ImmutableMap.of("value", department.getDepartmentId(), "text",
          department.getDepartmentName(), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    return bean;
  }

  /**
   * 院校下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public SelectBean selectDepartment(boolean useBlank, String username, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择院校", "icon", dnstatic + "/images/0.png"));
    }
    Map<String, Object> param = Maps.newHashMap();
    param.put("tearchId", username);
    param.put("groupByClause", "department_id");

    List<Map<String, Object>> list = cService.feedDepSelector(param);
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> map = list.get(i);
      data.add(ImmutableMap.of("value", (String) map.get("departmentId"), "text",
          (String) map.get("departmentName"), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    return bean;
  }


  /**
   * 专业下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void disciplineSelect(Model model, boolean useBlank, String username, String departmentId,
      String selectedKey) {

    SelectBean bean = this.selectDiscipline(useBlank, username, departmentId, selectedKey);
    model.addAttribute("disciplineSelector", bean);
  }

  /**
   * 专业下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void allDisciplineSelect(Model model, boolean useBlank, String departmentId,
      String selectedKey) {

    SelectBean bean = this.allSelectDiscipline(useBlank, departmentId, selectedKey);
    model.addAttribute("allDisciplineSelector", bean);
  }


  /**
   * 专业下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public SelectBean allSelectDiscipline(boolean useBlank, String departmentId, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择专业", "icon", dnstatic + "/images/0.png"));
    }

    EduDisciplineCriteria ex = new EduDisciplineCriteria();
    Criteria criteria = ex.createCriteria();
    if (StringUtils.isNotEmpty(departmentId)) {
      criteria.andDepartmentIdEqualTo(departmentId);
    }

    List<EduDiscipline> list = disService.selectByExample(ex);
    for (int i = 0; i < list.size(); i++) {
      EduDiscipline discipline = list.get(i);
      data.add(ImmutableMap.of("value", discipline.getDisciplineId(), "text",
          discipline.getDisciplineName(), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);

    return bean;
  }

  /**
   * 专业下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public SelectBean selectDiscipline(boolean useBlank, String username, String departmentId,
      String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择专业", "icon", dnstatic + "/images/0.png"));
    }
    Map<String, Object> param = Maps.newHashMap();
    param.put("tearchId", username);
    param.put("departmentId", departmentId);
    param.put("groupByClause", "discipline_id");

    List<Map<String, Object>> list = cService.feedDepSelector(param);
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> map = list.get(i);
      data.add(ImmutableMap.of("value", (String) map.get("disciplineId"), "text",
          (String) map.get("disciplineName"), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);

    return bean;
  }

  /**
   * 班级下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void classSelect(Model model, boolean useBlank, String username, String disciplineId,
      String departmentId, String selectedKey) {
    SelectBean bean = this.selectClass(useBlank, username, disciplineId, departmentId, selectedKey);
    model.addAttribute("classSelector", bean);
  }

  public void allClassSelect(Model model, boolean useBlank, String disciplineId,
      String departmentId, String selectedKey) {
    SelectBean bean = this.allSelectClass(useBlank, disciplineId, departmentId, selectedKey);
    model.addAttribute("allClassSelector", bean);
  }

  /**
   * 班级下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public SelectBean allSelectClass(boolean useBlank, String disciplineId, String departmentId,
      String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择班级", "icon", dnstatic + "/images/0.png"));
    }
    // Map<String, Object> param = Maps.newHashMap();
    // param.put("disciplineId", disciplineId);
    // param.put("departmentId", departmentId);
    // param.put("groupByClause", "class_id");

    // List<Map<String, Object>> list = cService.feedDepSelector(param);

    EduClassCriteria ex = new EduClassCriteria();
    com.stepiot.model.EduClassCriteria.Criteria criteria = ex.createCriteria();
    if (StringUtils.isNotEmpty(disciplineId)) {
      criteria.andDisciplineIdEqualTo(disciplineId);
    } else {
      if (StringUtils.isNotEmpty(departmentId)) {
        EduDisciplineCriteria exx = new EduDisciplineCriteria();
        Criteria criteria2 = exx.createCriteria();
        criteria2.andDepartmentIdEqualTo(departmentId);
        List<EduDiscipline> list = disService.selectByExample(exx);
        List<String> disList = Lists.newArrayList();
        for (int i = 0; i < list.size(); i++) {
          disList.add(list.get(i).getDisciplineId());
        }
        criteria.andDisciplineIdIn(disList);
      }
    }

    List<EduClass> list = cService.selectByExample(ex);
    for (int i = 0; i < list.size(); i++) {
      EduClass eduClass = list.get(i);
      data.add(ImmutableMap.of("value", eduClass.getClassId(), "text", eduClass.getClassName(),
          "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    return bean;
  }

  /**
   * 班级下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public SelectBean selectClass(boolean useBlank, String username, String disciplineId,
      String departmentId, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择班级", "icon", dnstatic + "/images/0.png"));
    }
    Map<String, Object> param = Maps.newHashMap();
    param.put("tearchId", username);
    param.put("disciplineId", disciplineId);
    param.put("departmentId", departmentId);
    param.put("groupByClause", "class_id");

    List<Map<String, Object>> list = cService.feedDepSelector(param);
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> map = list.get(i);
      data.add(ImmutableMap.of("value", (String) map.get("classId"), "text",
          (String) map.get("className"), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    return bean;
  }


  /**
   * 章节下拉框
   * 
   * @param useBlank
   * @param selectedOutlineId
   * @return
   */
  public void outlineSelect(Model model, boolean useBlank, int selectedOutlineId) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择章节", "icon", dnstatic + "/images/0.png"));
    }
    List<EduPythonCourseOutline> list =
        oservice.selectByExample(new EduPythonCourseOutlineCriteria());
    for (int i = 0; i < list.size(); i++) {
      EduPythonCourseOutline outline = list.get(i);
      data.add(ImmutableMap.of("value", outline.getOutlineId() + "", "text",
          outline.getOutlineName(), "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedOutlineId + "");
    model.addAttribute("outlineSelector", bean);
  }


  /**
   * 班级下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  private SelectBean singleClassSelect(boolean useBlank, String username, String selectedKey) {
    List<Map<String, String>> data = Lists.newArrayList();
    if (useBlank) {
      data.add(ImmutableMap.of("value", "", "text", "请选择班级", "icon", dnstatic + "/images/0.png"));
    }
    Map<String, Object> param = Maps.newHashMap();
    param.put("tearchId", username);
    param.put("groupByClause", "class_id");

    List<Map<String, Object>> list = cService.feedDepSelector(param);
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> map = list.get(i);
      data.add(ImmutableMap.of(
          "value", (String) map.get("classId"), "text", (String) map.get("departmentName") + " ⇨ "
              + (String) map.get("disciplineName") + " ⇨ " + (String) map.get("className"),
          "icon", dnstatic + "/images/1.png"));
    }
    SelectBean bean = new SelectBean();
    bean.setData(data);
    bean.setSelectedKey(selectedKey);
    return bean;
  }


  /**
   * 班级下拉框
   * 
   * @param model
   * @param useBlank
   * @param username
   * @param selectedKey
   */
  public void singleClassSelect(Model model, boolean useBlank, String username,
      String selectedKey) {
    SelectBean bean = this.singleClassSelect(useBlank, username, selectedKey);
    model.addAttribute("singleClassSelect", bean);
  }
}
