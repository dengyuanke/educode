package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.MicrobitCourseMapper;
import com.stepiot.model.MicrobitCourse;
import com.stepiot.model.MicrobitCourseCriteria;
import com.stepiot.model.MicrobitCourseWithBLOBs;

@Service
public class MicrobitCourseService implements MicrobitCourseMapper {

  @Autowired
  private MicrobitCourseMapper mapper;

  @Override
  public long countByExample(MicrobitCourseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(MicrobitCourseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int insert(MicrobitCourseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(MicrobitCourseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<MicrobitCourseWithBLOBs> selectByExampleWithBLOBs(MicrobitCourseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<MicrobitCourse> selectByExample(MicrobitCourseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public int updateByExampleSelective(MicrobitCourseWithBLOBs record,
      MicrobitCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(MicrobitCourseWithBLOBs record,
      MicrobitCourseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(MicrobitCourse record, MicrobitCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public MicrobitCourse selectByExampleForOne(MicrobitCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(MicrobitCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(MicrobitCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {

    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public MicrobitCourseWithBLOBs selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByPrimaryKeySelective(MicrobitCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(MicrobitCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(MicrobitCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {

    return mapper.selectMapByPrimaryKey(courseId);
  }
}
