package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduPythonCourseOutlineAnswerMapper;
import com.stepiot.dao.EduPythonCourseOutlineMapper;
import com.stepiot.model.EduPythonCourseOutline;
import com.stepiot.model.EduPythonCourseOutlineAnswerCriteria;
import com.stepiot.model.EduPythonCourseOutlineAnswerCriteria.Criteria;
import com.stepiot.model.EduPythonCourseOutlineAnswerWithBLOBs;
import com.stepiot.model.EduPythonCourseOutlineCriteria;
import com.stepiot.model.EduPythonCourseOutlineWithBLOBs;

@Service
public class EduPythonCourseOutlineService implements EduPythonCourseOutlineMapper {

  @Autowired
  private EduPythonCourseOutlineMapper mapper;

  @Autowired
  private EduPythonCourseOutlineAnswerMapper amapper;


  @Override
  public long countByExample(EduPythonCourseOutlineCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduPythonCourseOutlineCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer outlineId) {

    return mapper.deleteByPrimaryKey(outlineId);
  }

  @Override
  public int insert(EduPythonCourseOutlineWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduPythonCourseOutlineWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduPythonCourseOutlineWithBLOBs> selectByExampleWithBLOBs(
      EduPythonCourseOutlineCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduPythonCourseOutline> selectByExample(EduPythonCourseOutlineCriteria example) {

    return mapper.selectByExample(example);
  }

  public EduPythonCourseOutlineAnswerWithBLOBs selectAnswerByIdAndLevel(Integer outlineId,
      Integer level) {
    EduPythonCourseOutlineAnswerCriteria example = new EduPythonCourseOutlineAnswerCriteria();
    Criteria ex = example.createCriteria();
    ex.andOutlineIdEqualTo(outlineId);
    ex.andLevelEqualTo(level);
    List<EduPythonCourseOutlineAnswerWithBLOBs> list = amapper.selectByExampleWithBLOBs(example);
    if (list != null && list.size() > 0) {
      return list.get(0);
    }
    return null;
  }

  @Override
  public EduPythonCourseOutlineWithBLOBs selectByPrimaryKey(Integer outlineId) {

    return mapper.selectByPrimaryKey(outlineId);
  }

  @Override
  public int updateByExampleSelective(EduPythonCourseOutlineWithBLOBs record,
      EduPythonCourseOutlineCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduPythonCourseOutlineWithBLOBs record,
      EduPythonCourseOutlineCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduPythonCourseOutline record,
      EduPythonCourseOutlineCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduPythonCourseOutlineWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduPythonCourseOutlineWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduPythonCourseOutline record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduPythonCourseOutline selectByExampleForOne(EduPythonCourseOutlineCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer outlineId) {

    return mapper.selectMapByPrimaryKey(outlineId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduPythonCourseOutlineCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduPythonCourseOutlineCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
