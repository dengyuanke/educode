package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduSysMediaMapper;
import com.stepiot.model.EduSysMedia;
import com.stepiot.model.EduSysMediaCriteria;
import com.stepiot.model.EduSysMediaCriteria.Criteria;

@Service
public class EduSysMediaService implements EduSysMediaMapper {

  @Autowired
  private EduSysMediaMapper mapper;

  @Override
  public long countByExample(EduSysMediaCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduSysMediaCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer meidaId) {

    return mapper.deleteByPrimaryKey(meidaId);
  }

  @Override
  public int insert(EduSysMedia record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduSysMedia record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduSysMedia> selectByExample(EduSysMediaCriteria example) {

    return mapper.selectByExample(example);
  }


  public List<EduSysMedia> selectByS3Paths(List<String> s3paths) {
    EduSysMediaCriteria ex = new EduSysMediaCriteria();
    Criteria ext = ex.createCriteria();
    ext.andS3PathIn(s3paths);

    return mapper.selectByExample(ex);
  }


  @Override
  public EduSysMedia selectByPrimaryKey(Integer meidaId) {

    return mapper.selectByPrimaryKey(meidaId);
  }

  @Override
  public int updateByExampleSelective(EduSysMedia record, EduSysMediaCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduSysMedia record, EduSysMediaCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduSysMedia record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduSysMedia record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduSysMedia selectByExampleForOne(EduSysMediaCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer meidaId) {

    return mapper.selectMapByPrimaryKey(meidaId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduSysMediaCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduSysMediaCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
