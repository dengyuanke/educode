package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.MicrobitUserCourseMapper;
import com.stepiot.dao.ext.MicrobitUserCourseMapperExt;
import com.stepiot.model.MicrobitUserCourse;
import com.stepiot.model.MicrobitUserCourseCriteria;
import com.stepiot.model.MicrobitUserCourseWithBLOBs;

@Service
public class MicrobitUserCourseService
    implements MicrobitUserCourseMapper, MicrobitUserCourseMapperExt {

  @Autowired
  private MicrobitUserCourseMapper mapper;

  @Autowired
  private MicrobitUserCourseMapperExt mapperExt;

  @Override
  public long countByExample(MicrobitUserCourseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(MicrobitUserCourseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int insert(MicrobitUserCourseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(MicrobitUserCourseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<MicrobitUserCourseWithBLOBs> selectByExampleWithBLOBs(
      MicrobitUserCourseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<MicrobitUserCourse> selectByExample(MicrobitUserCourseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public int updateByExampleSelective(MicrobitUserCourseWithBLOBs record,
      MicrobitUserCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(MicrobitUserCourseWithBLOBs record,
      MicrobitUserCourseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(MicrobitUserCourse record, MicrobitUserCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public MicrobitUserCourse selectByExampleForOne(MicrobitUserCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(MicrobitUserCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(MicrobitUserCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String courseUid) {

    return mapper.deleteByPrimaryKey(courseUid);
  }

  @Override
  public MicrobitUserCourseWithBLOBs selectByPrimaryKey(String courseUid) {

    return mapper.selectByPrimaryKey(courseUid);
  }

  @Override
  public int updateByPrimaryKeySelective(MicrobitUserCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(MicrobitUserCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(MicrobitUserCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String courseUid) {

    return mapper.selectMapByPrimaryKey(courseUid);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }

}
