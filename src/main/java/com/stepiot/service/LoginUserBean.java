package com.stepiot.service;

public class LoginUserBean implements java.io.Serializable {

  public static final String SESSION_KEY = "LOGINUSERBEAN";
  private static final long serialVersionUID = 3008279836198785540L;
  private String username;
  private String password;
  private String telephone;
  private String error;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }


}
