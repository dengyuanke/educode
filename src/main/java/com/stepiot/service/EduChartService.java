package com.stepiot.service;

import java.awt.Color;
import java.util.List;
import java.util.Random;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.proto.ChartProto.Chart;

@Service
public class EduChartService {

  private static final List<Color> clist = Lists.newArrayList();
  static {
    clist.add(new Color(229, 115, 115));
    clist.add(new Color(240, 98, 146));
    clist.add(new Color(186, 104, 200));
    clist.add(new Color(149, 117, 205));
    clist.add(new Color(121, 134, 203));
    clist.add(new Color(100, 181, 246));
    clist.add(new Color(79, 195, 247));
    clist.add(new Color(77, 208, 225));
    clist.add(new Color(77, 182, 172));
    clist.add(new Color(129, 199, 132));
    clist.add(new Color(220, 231, 117));
    clist.add(new Color(255, 241, 118));
    clist.add(new Color(255, 213, 79));
    clist.add(new Color(255, 183, 77));
    clist.add(new Color(255, 138, 101));
    clist.add(new Color(161, 136, 127));
    clist.add(new Color(224, 224, 224));
    clist.add(new Color(144, 164, 174));
    clist.add(new Color(140, 116, 179));
    clist.add(new Color(223, 97, 138));
    clist.add(new Color(49, 158, 147));
    clist.add(new Color(217, 113, 113));
    clist.add(new Color(117, 130, 191));
    clist.add(new Color(244, 172, 49));
    clist.add(new Color(159, 135, 127));
    clist.add(new Color(79, 156, 214));
    clist.add(new Color(223, 84, 82));
    clist.add(new Color(49, 158, 147));
    clist.add(new Color(241, 139, 45));
    clist.add(new Color(58, 141, 205));
    clist.add(new Color(81, 165, 83));
    clist.add(new Color(117, 130, 191));
    clist.add(new Color(238, 163, 58));
    clist.add(new Color(30, 130, 191));
    clist.add(new Color(217, 67, 117));
    clist.add(new Color(166, 105, 169));
    clist.add(new Color(118, 85, 73));
    clist.add(new Color(35, 163, 181));
    clist.add(new Color(228, 150, 152));
    clist.add(new Color(87, 104, 176));
    clist.add(new Color(15, 117, 104));
    clist.add(new Color(114, 89, 163));
    clist.add(new Color(48, 63, 145));
    clist.add(new Color(147, 78, 155));
    clist.add(new Color(138, 109, 99));
    clist.add(new Color(69, 89, 99));
    clist.add(new Color(111, 47, 141));
    clist.add(new Color(181, 33, 88));
    clist.add(new Color(66, 65, 66));
    clist.add(new Color(11, 102, 89));
    clist.add(new Color(33, 98, 174));
    clist.add(new Color(24, 126, 136));
    clist.add(new Color(186, 43, 46));
    clist.add(new Color(48, 120, 56));
    clist.add(new Color(48, 63, 145));
    clist.add(new Color(200, 38, 93));
    clist.add(new Color(38, 113, 185));
    clist.add(new Color(106, 75, 65));
    clist.add(new Color(162, 27, 84));
    clist.add(new Color(203, 69, 36));


  }
  static Random random = new Random();

  public String randomColor() {
    Color color = clist.get(random.nextInt(clist.size()));
    return "rgb(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ")";
  }

  public String buildChartJson(String chartType, List<String> labels,
      List<com.stepiot.proto.ChartProto.Chart.Data.Dataset> dataSetBuilder) {
    com.stepiot.proto.ChartProto.Chart.Options.Builder optionBuider = Chart.Options.newBuilder();
    com.stepiot.proto.ChartProto.Chart.Builder chartBuider = Chart.newBuilder();
    com.stepiot.proto.ChartProto.Chart.Data.Builder dataBuilder = chartBuider.getDataBuilder();
    dataBuilder.addAllLabels(labels);
    dataBuilder.addAllDatasets(dataSetBuilder);
    chartBuider.setType(chartType);
    chartBuider.setData(dataBuilder);
    chartBuider.setOptions(optionBuider);
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(chartBuider);

    } catch (InvalidProtocolBufferException e) {
    }
    return json;
  }
}
