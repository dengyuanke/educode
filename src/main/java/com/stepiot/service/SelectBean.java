package com.stepiot.service;

import java.util.List;

public class SelectBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3011727883123376793L;

  private List<?> data;

  private String selectedKey;

  private Integer selectedId;

  public List<?> getData() {
    return data;
  }

  public void setData(List<?> data) {
    this.data = data;
  }

  public String getSelectedKey() {
    return selectedKey;
  }

  public void setSelectedKey(String selectedKey) {
    this.selectedKey = selectedKey;
  }

  public Integer getSelectedId() {
    return selectedId;
  }

  public void setSelectedId(Integer selectedId) {
    this.selectedId = selectedId;
  }
}
