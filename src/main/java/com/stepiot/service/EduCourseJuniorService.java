package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduCourseJuniorMapper;
import com.stepiot.model.EduCourseJunior;
import com.stepiot.model.EduCourseJuniorCriteria;
import com.stepiot.model.EduCourseJuniorWithBLOBs;

@Service
public class EduCourseJuniorService implements EduCourseJuniorMapper {

  @Autowired
  private EduCourseJuniorMapper mapper;

  @Override
  public long countByExample(EduCourseJuniorCriteria example) {
    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduCourseJuniorCriteria example) {
    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {
    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public int insert(EduCourseJuniorWithBLOBs record) {
    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduCourseJuniorWithBLOBs record) {
    return mapper.insertSelective(record);
  }

  @Override
  public List<EduCourseJuniorWithBLOBs> selectByExampleWithBLOBs(EduCourseJuniorCriteria example) {
    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduCourseJunior> selectByExample(EduCourseJuniorCriteria example) {
    return mapper.selectByExample(example);
  }

  @Override
  public EduCourseJuniorWithBLOBs selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByExampleSelective(EduCourseJuniorWithBLOBs record,
      EduCourseJuniorCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduCourseJuniorWithBLOBs record,
      EduCourseJuniorCriteria example) {
    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduCourseJunior record, EduCourseJuniorCriteria example) {
    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduCourseJuniorWithBLOBs record) {
    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduCourseJuniorWithBLOBs record) {
    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduCourseJunior record) {
    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduCourseJunior selectByExampleForOne(EduCourseJuniorCriteria example) {
    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {
    return mapper.selectMapByPrimaryKey(courseId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduCourseJuniorCriteria example) {
    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduCourseJuniorCriteria example) {
    return mapper.selectMapByExample(example);
  }

}
