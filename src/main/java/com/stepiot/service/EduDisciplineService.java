package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduDisciplineMapper;
import com.stepiot.dao.ext.EduDisciplineMapperExt;
import com.stepiot.model.EduDiscipline;
import com.stepiot.model.EduDisciplineCriteria;

@Service
public class EduDisciplineService implements EduDisciplineMapper {

  @Autowired
  private EduDisciplineMapper dMapper;

  @Autowired
  private EduDisciplineMapperExt ext;

  public List<Map<String, Object>> listAllDiscipline(Map<String, Object> map) {
    return ext.listAllDiscipline(map);
  }

  @Override
  public long countByExample(EduDisciplineCriteria example) {

    return dMapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduDisciplineCriteria example) {

    return dMapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String disciplineId) {

    return dMapper.deleteByPrimaryKey(disciplineId);
  }

  @Override
  public int insert(EduDiscipline record) {

    return dMapper.insert(record);
  }

  @Override
  public int insertSelective(EduDiscipline record) {

    return dMapper.insertSelective(record);
  }

  @Override
  public List<EduDiscipline> selectByExample(EduDisciplineCriteria example) {

    return dMapper.selectByExample(example);
  }

  @Override
  public EduDiscipline selectByPrimaryKey(String disciplineId) {

    return dMapper.selectByPrimaryKey(disciplineId);
  }

  @Override
  public int updateByExampleSelective(EduDiscipline record, EduDisciplineCriteria example) {

    return dMapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduDiscipline record, EduDisciplineCriteria example) {

    return dMapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduDiscipline record) {

    return dMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduDiscipline record) {

    return dMapper.updateByPrimaryKey(record);
  }

  @Override
  public EduDiscipline selectByExampleForOne(EduDisciplineCriteria example) {

    return dMapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String disciplineId) {

    return dMapper.selectMapByPrimaryKey(disciplineId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduDisciplineCriteria example) {

    return dMapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduDisciplineCriteria example) {

    return dMapper.selectMapByExample(example);
  }

}
