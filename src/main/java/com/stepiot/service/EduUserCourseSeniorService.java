package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.stepiot.dao.EduUserCourseSeniorMapper;
import com.stepiot.dao.ext.EduCourseSeniorMapperExt;
import com.stepiot.model.EduUserCourseSenior;
import com.stepiot.model.EduUserCourseSeniorCriteria;
import com.stepiot.model.EduUserCourseSeniorWithBLOBs;

@Service
public class EduUserCourseSeniorService
    implements EduUserCourseSeniorMapper, EduCourseSeniorMapperExt {

  @Autowired
  private EduUserCourseSeniorMapper mapper;

  @Autowired
  private EduCourseSeniorMapperExt mapperExt;

  @Override
  public long countByExample(EduUserCourseSeniorCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserCourseSeniorCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String courseUid) {

    return mapper.deleteByPrimaryKey(courseUid);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int insert(EduUserCourseSeniorWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserCourseSeniorWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserCourseSeniorWithBLOBs> selectByExampleWithBLOBs(
      EduUserCourseSeniorCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserCourseSenior> selectByExample(EduUserCourseSeniorCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserCourseSeniorWithBLOBs selectByPrimaryKey(String courseUid) {

    return mapper.selectByPrimaryKey(courseUid);
  }

  @Override
  public int updateByExampleSelective(EduUserCourseSeniorWithBLOBs record,
      EduUserCourseSeniorCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int complete(EduUserCourseSeniorWithBLOBs record, EduUserCourseSeniorCriteria example,
      EduUserCourseSeniorWithBLOBs next) {
    if (next != null) {
      mapper.insert(next);
    }
    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserCourseSeniorWithBLOBs record,
      EduUserCourseSeniorCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserCourseSenior record, EduUserCourseSeniorCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserCourseSeniorWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserCourseSeniorWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserCourseSenior record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserCourseSenior selectByExampleForOne(EduUserCourseSeniorCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String courseUid) {

    return mapper.selectMapByPrimaryKey(courseUid);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserCourseSeniorCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserCourseSeniorCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }
}
