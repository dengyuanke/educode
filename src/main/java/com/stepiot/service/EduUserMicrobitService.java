package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserMicrobitMapper;
import com.stepiot.model.EduUserMicrobit;
import com.stepiot.model.EduUserMicrobitCriteria;
import com.stepiot.model.EduUserMicrobitWithBLOBs;

@Service
public class EduUserMicrobitService implements EduUserMicrobitMapper {

  @Autowired
  private EduUserMicrobitMapper mapper;

  @Override
  public long countByExample(EduUserMicrobitCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserMicrobitCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String blocklyId) {

    return mapper.deleteByPrimaryKey(blocklyId);
  }

  @Override
  public int insert(EduUserMicrobitWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserMicrobitWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserMicrobitWithBLOBs> selectByExampleWithBLOBs(EduUserMicrobitCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserMicrobit> selectByExample(EduUserMicrobitCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserMicrobitWithBLOBs selectByPrimaryKey(String blocklyId) {

    return mapper.selectByPrimaryKey(blocklyId);
  }

  @Override
  public int updateByExampleSelective(EduUserMicrobitWithBLOBs record,
      EduUserMicrobitCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserMicrobitWithBLOBs record,
      EduUserMicrobitCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserMicrobit record, EduUserMicrobitCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserMicrobitWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserMicrobitWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserMicrobit record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserMicrobit selectByExampleForOne(EduUserMicrobitCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String blocklyId) {

    return mapper.selectMapByPrimaryKey(blocklyId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserMicrobitCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserMicrobitCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
