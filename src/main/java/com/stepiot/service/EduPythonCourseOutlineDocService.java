package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduPythonCourseOutlineDocMapper;
import com.stepiot.model.EduPythonCourseOutlineDoc;
import com.stepiot.model.EduPythonCourseOutlineDocCriteria;
import com.stepiot.model.EduPythonCourseOutlineDocWithBLOBs;

@Service
public class EduPythonCourseOutlineDocService implements EduPythonCourseOutlineDocMapper {


  @Autowired
  private EduPythonCourseOutlineDocMapper mapper;

  @Override
  public long countByExample(EduPythonCourseOutlineDocCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduPythonCourseOutlineDocCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer outlineId) {

    return mapper.deleteByPrimaryKey(outlineId);
  }

  @Override
  public int insert(EduPythonCourseOutlineDocWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduPythonCourseOutlineDocWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduPythonCourseOutlineDocWithBLOBs> selectByExampleWithBLOBs(
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduPythonCourseOutlineDoc> selectByExample(
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduPythonCourseOutlineDocWithBLOBs selectByPrimaryKey(Integer outlineId) {

    return mapper.selectByPrimaryKey(outlineId);
  }

  @Override
  public int updateByExampleSelective(EduPythonCourseOutlineDocWithBLOBs record,
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduPythonCourseOutlineDocWithBLOBs record,
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduPythonCourseOutlineDoc record,
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduPythonCourseOutlineDocWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduPythonCourseOutlineDocWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduPythonCourseOutlineDoc record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduPythonCourseOutlineDoc selectByExampleForOne(
      EduPythonCourseOutlineDocCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer outlineId) {

    return mapper.selectMapByPrimaryKey(outlineId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduPythonCourseOutlineDocCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduPythonCourseOutlineDocCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
