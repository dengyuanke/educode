package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserTortoiseMapper;
import com.stepiot.model.EduUserTortoise;
import com.stepiot.model.EduUserTortoiseCriteria;
import com.stepiot.model.EduUserTortoiseWithBLOBs;

@Service
public class EduUserTortoiseService implements EduUserTortoiseMapper {

  @Autowired
  private EduUserTortoiseMapper mapper;

  @Override
  public long countByExample(EduUserTortoiseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserTortoiseCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String blocklyId) {

    return mapper.deleteByPrimaryKey(blocklyId);
  }

  @Override
  public int insert(EduUserTortoiseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserTortoiseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserTortoiseWithBLOBs> selectByExampleWithBLOBs(EduUserTortoiseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserTortoise> selectByExample(EduUserTortoiseCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserTortoiseWithBLOBs selectByPrimaryKey(String blocklyId) {

    return mapper.selectByPrimaryKey(blocklyId);
  }

  @Override
  public int updateByExampleSelective(EduUserTortoiseWithBLOBs record,
      EduUserTortoiseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserTortoiseWithBLOBs record,
      EduUserTortoiseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserTortoise record, EduUserTortoiseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserTortoiseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserTortoiseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserTortoise record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserTortoise selectByExampleForOne(EduUserTortoiseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String blocklyId) {

    return mapper.selectMapByPrimaryKey(blocklyId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserTortoiseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserTortoiseCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
