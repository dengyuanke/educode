package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.google.common.collect.Maps;
import com.stepiot.constants.Cons;
import com.stepiot.model.EduClass;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserRole;
import com.stepiot.model.EduUserRoleCriteria;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  private EduUserService service;

  @Autowired
  private EduUserRoleService roleService;

  @Autowired
  private LoginAttemptService loginAttemptService;

  @Autowired
  private EduStudentRefService ref;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    if (loginAttemptService.isBlocked(username)) {
      throw new RuntimeException(Cons.LOGIN_BLOCKED_MESSAGE);
    }
    EduUser eduUser = service.selectByPrimaryKey(username);

    EduUserRoleCriteria ex = new EduUserRoleCriteria();
    ex.createCriteria().andUsernameEqualTo(username);

    Map<String, Object> map = Maps.newHashMap();
    map.put("username", username);
    List<Map<String, Object>> clist = ref.selectClassInfoById(map);

    List<EduUserRole> roles = roleService.selectByExample(ex);
    UserBean bean = new UserBean(eduUser, roles);
    if (clist != null && clist.size() > 0) {
      Map<String, Object> clz = clist.get(0);
      EduClass eduClass = new EduClass();
      eduClass.setClassId((String) clz.get("classId"));
      eduClass.setClassName((String) clz.get("className"));
      bean.setClazzInfo(eduClass);
    }
    return bean;
  }

}
