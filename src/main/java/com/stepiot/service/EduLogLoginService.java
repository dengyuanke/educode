package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduLogLoginMapper;
import com.stepiot.model.EduLogLogin;
import com.stepiot.model.EduLogLoginCriteria;

@Service
public class EduLogLoginService implements EduLogLoginMapper {


  @Autowired
  private EduLogLoginMapper mapper;

  @Override
  public long countByExample(EduLogLoginCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduLogLoginCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer id) {

    return mapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(EduLogLogin record) {

    return mapper.insert(record);
  }



  @Override
  public int insertSelective(EduLogLogin record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduLogLogin> selectByExample(EduLogLoginCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduLogLogin selectByPrimaryKey(Integer id) {

    return mapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByExampleSelective(EduLogLogin record, EduLogLoginCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduLogLogin record, EduLogLoginCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduLogLogin record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduLogLogin record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduLogLogin selectByExampleForOne(EduLogLoginCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer id) {

    return mapper.selectMapByPrimaryKey(id);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduLogLoginCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduLogLoginCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
