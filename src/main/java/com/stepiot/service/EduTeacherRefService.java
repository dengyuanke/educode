package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.beans.EduTeacherQueryBean;
import com.stepiot.dao.EduTeacherClassRefMapper;
import com.stepiot.dao.ext.EduTeacherClassRefMapperExt;
import com.stepiot.model.EduTeacherClassRef;
import com.stepiot.model.EduTeacherClassRefCriteria;
import com.stepiot.model.EduTeacherClassRefCriteria.Criteria;

@Service
public class EduTeacherRefService implements EduTeacherClassRefMapper, EduTeacherClassRefMapperExt {

  @Autowired
  private EduTeacherClassRefMapper mapper;

  @Autowired
  private EduTeacherClassRefMapperExt ext;

  @Override
  public long countByExample(EduTeacherClassRefCriteria example) {

    return mapper.countByExample(example);
  }

  public long countByClassId(String classId) {
    EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
    Criteria ee = ex.createCriteria();
    ee.andClassIdEqualTo(classId);
    return mapper.countByExample(ex);
  }

  @Override
  public int deleteByExample(EduTeacherClassRefCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer no) {

    return mapper.deleteByPrimaryKey(no);
  }

  @Override
  public int insert(EduTeacherClassRef record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduTeacherClassRef record) {
    return mapper.insertSelective(record);
  }

  @Override
  public List<EduTeacherClassRef> selectByExample(EduTeacherClassRefCriteria example) {
    return mapper.selectByExample(example);
  }

  public List<EduTeacherClassRef> selectByClassId(String classId) {
    EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
    Criteria ext = ex.createCriteria();
    ext.andClassIdEqualTo(classId);
    return mapper.selectByExample(ex);
  }

  public void deleteByTeacherId(String teacherId) {
    EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
    Criteria ext = ex.createCriteria();
    ext.andTearchIdEqualTo(teacherId);
    mapper.deleteByExample(ex);
  }

  public List<EduTeacherClassRef> selectByTeacherId(String teacherId) {
    EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
    Criteria ext = ex.createCriteria();
    ext.andTearchIdEqualTo(teacherId);
    return mapper.selectByExample(ex);
  }



  @Override
  public EduTeacherClassRef selectByPrimaryKey(Integer no) {
    return mapper.selectByPrimaryKey(no);
  }

  @Override
  public int updateByExampleSelective(EduTeacherClassRef record,
      EduTeacherClassRefCriteria example) {
    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduTeacherClassRef record, EduTeacherClassRefCriteria example) {
    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduTeacherClassRef record) {
    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduTeacherClassRef record) {
    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduTeacherClassRef selectByExampleForOne(EduTeacherClassRefCriteria example) {
    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer no) {
    return mapper.selectMapByPrimaryKey(no);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduTeacherClassRefCriteria example) {
    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduTeacherClassRefCriteria example) {
    return mapper.selectMapByExample(example);
  }

  @Override
  public List<EduTeacherQueryBean> listAllTeachers(Map<String, Object> map) {
    return ext.listAllTeachers(map);
  }

}
