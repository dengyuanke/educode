package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.beans.EduUserCourseProgressBean;
import com.stepiot.dao.EduUserPythonCourseMapper;
import com.stepiot.dao.ext.EduUserPythonCourseMapperExt;
import com.stepiot.model.EduUserPythonCourse;
import com.stepiot.model.EduUserPythonCourseCriteria;
import com.stepiot.model.EduUserPythonCourseCriteria.Criteria;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;

@Service
public class EduUserPythonCourseService
    implements EduUserPythonCourseMapper, EduUserPythonCourseMapperExt {

  @Autowired
  private EduUserPythonCourseMapper mapper;

  @Autowired
  private EduUserPythonCourseMapperExt mapperExt;

  @Override
  public long countByExample(EduUserPythonCourseCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserPythonCourseCriteria example) {

    return mapper.deleteByExample(example);
  }


  @Override
  public int insert(EduUserPythonCourseWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserPythonCourseWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserPythonCourseWithBLOBs> selectByExampleWithBLOBs(
      EduUserPythonCourseCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }



  public EduUserPythonCourseWithBLOBs selectByOutlineIdAndLevel(Integer outlineId, Integer level) {
    EduUserPythonCourseCriteria up = new EduUserPythonCourseCriteria();
    Criteria ex = up.createCriteria();
    ex.andOutlineIdEqualTo(outlineId).andLevelEqualTo(level);
    List<EduUserPythonCourseWithBLOBs> bs = mapper.selectByExampleWithBLOBs(up);
    if (bs != null && bs.size() > 0) {
      return bs.get(0);
    }
    return null;
  }

  @Override
  public List<EduUserPythonCourse> selectByExample(EduUserPythonCourseCriteria example) {

    return mapper.selectByExample(example);
  }


  @Override
  public int updateByExampleSelective(EduUserPythonCourseWithBLOBs record,
      EduUserPythonCourseCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserPythonCourseWithBLOBs record,
      EduUserPythonCourseCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserPythonCourse record, EduUserPythonCourseCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserPythonCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserPythonCourseWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserPythonCourse record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserPythonCourse selectByExampleForOne(EduUserPythonCourseCriteria example) {

    return mapper.selectByExampleForOne(example);
  }


  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserPythonCourseCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserPythonCourseCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }

  @Override
  public Map<String, Object> selectByUserCourseId(Map<String, Object> map) {

    return mapperExt.selectByUserCourseId(map);
  }

  @Override
  public int deleteByPrimaryKey(Integer userCourseId) {
    return mapper.deleteByPrimaryKey(userCourseId);
  }

  @Override
  public EduUserPythonCourseWithBLOBs selectByPrimaryKey(Integer userCourseId) {

    return mapper.selectByPrimaryKey(userCourseId);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer userCourseId) {

    return mapper.selectMapByPrimaryKey(userCourseId);
  }

  @Override
  public List<EduUserCourseProgressBean> listMyCourseProgress(Map<String, Object> map) {
    return mapperExt.listMyCourseProgress(map);
  }
}
