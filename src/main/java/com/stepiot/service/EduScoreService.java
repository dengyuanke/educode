package com.stepiot.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;
import com.stepiot.constants.CourseType;
import com.stepiot.model.EduPythonCourseOutlineAnswerWithBLOBs;
import com.stepiot.model.EduPythonCourseOutlineWithBLOBs;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;
import com.stepiot.util.GsonUtils;

@Service
public class EduScoreService {

  @Autowired
  private EduPythonCourseOutlineService outline;

  @Autowired
  private EduUserPythonCourseService uservice;

  private static final TypeToken<List<String>> tlist = new TypeToken<List<String>>() {};


  public void updateUserCourseScore(int outlineId, int level) {
    EduUserPythonCourseWithBLOBs userCourse = uservice.selectByOutlineIdAndLevel(outlineId, level);
    BigDecimal score = caculateScore(userCourse);
    userCourse.setScore(score.intValue());
    uservice.updateByPrimaryKey(userCourse);
  }

  private BigDecimal caculateScore(EduUserPythonCourseWithBLOBs userCourse) {
    EduPythonCourseOutlineAnswerWithBLOBs answer =
        outline.selectAnswerByIdAndLevel(userCourse.getOutlineId(), userCourse.getLevel());
    EduPythonCourseOutlineWithBLOBs outlines =
        outline.selectByPrimaryKey(userCourse.getOutlineId());
    CourseType courseType = CourseType.valueOf(outlines.getCourseType());
    switch (courseType) {
      case NESTBLOCK:
        return nestScore(userCourse, answer);
      case NULL:
        return new BigDecimal(answer.getWeights());
      case PYTHON:
        return pyScore(userCourse, answer);
      case SPACE:
        return new BigDecimal(answer.getWeights());
      case TURTLE:
        return new BigDecimal(answer.getWeights());
      default:
        return new BigDecimal(0);
    }
  }

  private BigDecimal nestScore(EduUserPythonCourseWithBLOBs uc,
      EduPythonCourseOutlineAnswerWithBLOBs answer) {
    BigDecimal scores = null;
    Set<String> uset = Sets.newHashSet();
    String utypes = uc.getUserBlocklyTypes();
    if (StringUtils.isNotEmpty(utypes)) {
      List<String> ulist = GsonUtils.toTypedList(utypes, tlist);
      uset.addAll(ulist);
    }

    List<String> list = GsonUtils.toTypedList(answer.getMustInclude(), tlist);
    if (list == null) {
      list = Lists.newArrayList();
    }
    Set<String> set = new HashSet<String>(list);
    double score = 0.0;
    if (!set.isEmpty()) {
      score = score + (answer.getWeights() * 0.3);
      if (set.containsAll(uset)) {
        score = score + (answer.getWeights() * 0.3);
      }
      if (uset.containsAll(set)) {
        score = score + (answer.getWeights() * 0.4);
      }
      scores = new BigDecimal(score);
    } else {
      scores = new BigDecimal(0);
    }
    return scores;
  }

  private BigDecimal pyScore(EduUserPythonCourseWithBLOBs uc,
      EduPythonCourseOutlineAnswerWithBLOBs answer) {
    String userCode = uc.getUserCode();
    if (StringUtils.isEmpty(userCode)) {
      return new BigDecimal(0);
    }
    String answers = answer.getDefaultAnswer();
    if (StringUtils.isEmpty(answers)) {
      answers = "";
    }
    double ulen = (double) userCode.length();
    double alen = (double) answers.length();
    double x = Math.abs(ulen - alen) / alen;
    double logv = Math.log(x + 1);
    if (logv <= 1) {
      return new BigDecimal(answer.getWeights());
    } else {
      return new BigDecimal(answer.getWeights() * 0.75);
    }
  }

}
