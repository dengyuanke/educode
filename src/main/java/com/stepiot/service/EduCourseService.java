package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduCoursePrimaryMapper;
import com.stepiot.model.EduCoursePrimary;
import com.stepiot.model.EduCoursePrimaryCriteria;
import com.stepiot.model.EduCoursePrimaryWithBLOBs;

@Service
public class EduCourseService implements EduCoursePrimaryMapper {

  @Autowired
  private EduCoursePrimaryMapper mapper;

  @Override
  public long countByExample(EduCoursePrimaryCriteria example) {
    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduCoursePrimaryCriteria example) {
    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {
    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public int insert(EduCoursePrimaryWithBLOBs record) {
    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduCoursePrimaryWithBLOBs record) {
    return mapper.insertSelective(record);
  }

  @Override
  public List<EduCoursePrimaryWithBLOBs> selectByExampleWithBLOBs(
      EduCoursePrimaryCriteria example) {
    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduCoursePrimary> selectByExample(EduCoursePrimaryCriteria example) {
    return mapper.selectByExample(example);
  }

  @Override
  public EduCoursePrimaryWithBLOBs selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByExampleSelective(EduCoursePrimaryWithBLOBs record,
      EduCoursePrimaryCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduCoursePrimaryWithBLOBs record,
      EduCoursePrimaryCriteria example) {
    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduCoursePrimary record, EduCoursePrimaryCriteria example) {
    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduCoursePrimaryWithBLOBs record) {
    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduCoursePrimaryWithBLOBs record) {
    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduCoursePrimary record) {
    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduCoursePrimary selectByExampleForOne(EduCoursePrimaryCriteria example) {
    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {
    return mapper.selectMapByPrimaryKey(courseId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduCoursePrimaryCriteria example) {
    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduCoursePrimaryCriteria example) {
    return mapper.selectMapByExample(example);
  }

}
