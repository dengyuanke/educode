package com.stepiot.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.physicsfamily.util.SignatureUtils;

@Service
public class EduHmacService {

  @Value("${stepiot.hmac.key}")
  private String hmacKey;

  public String hmacSha1(Map<String, String> map) {
    return SignatureUtils.hmacSha1(map, null, hmacKey);
  }
}
