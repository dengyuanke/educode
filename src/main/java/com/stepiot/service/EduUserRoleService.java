package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserRoleMapper;
import com.stepiot.model.EduUserRole;
import com.stepiot.model.EduUserRoleCriteria;

@Service
public class EduUserRoleService implements EduUserRoleMapper {

  @Autowired
  private EduUserRoleMapper mapper;

  @Override
  public long countByExample(EduUserRoleCriteria example) {

    return mapper.countByExample(example);

  }

  @Override
  public int deleteByExample(EduUserRoleCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer id) {

    return mapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(EduUserRole record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserRole record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserRole> selectByExample(EduUserRoleCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserRole selectByPrimaryKey(Integer id) {

    return mapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByExampleSelective(EduUserRole record, EduUserRoleCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduUserRole record, EduUserRoleCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserRole record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserRole record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserRole selectByExampleForOne(EduUserRoleCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer id) {

    return mapper.selectMapByPrimaryKey(id);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserRoleCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserRoleCriteria example) {

    return mapper.selectMapByExample(example);
  }
}
