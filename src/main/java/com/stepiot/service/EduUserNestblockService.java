package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduUserNestblockMapper;
import com.stepiot.model.EduUserNestblock;
import com.stepiot.model.EduUserNestblockCriteria;
import com.stepiot.model.EduUserNestblockWithBLOBs;

@Service
public class EduUserNestblockService implements EduUserNestblockMapper {

  @Autowired
  private EduUserNestblockMapper mapper;

  @Override
  public long countByExample(EduUserNestblockCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserNestblockCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String blocklyId) {

    return mapper.deleteByPrimaryKey(blocklyId);
  }

  @Override
  public int insert(EduUserNestblockWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserNestblockWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserNestblockWithBLOBs> selectByExampleWithBLOBs(
      EduUserNestblockCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserNestblock> selectByExample(EduUserNestblockCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserNestblockWithBLOBs selectByPrimaryKey(String blocklyId) {

    return mapper.selectByPrimaryKey(blocklyId);
  }

  @Override
  public int updateByExampleSelective(EduUserNestblockWithBLOBs record,
      EduUserNestblockCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserNestblockWithBLOBs record,
      EduUserNestblockCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserNestblock record, EduUserNestblockCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserNestblockWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserNestblockWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserNestblock record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserNestblock selectByExampleForOne(EduUserNestblockCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String blocklyId) {

    return mapper.selectMapByPrimaryKey(blocklyId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserNestblockCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserNestblockCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
