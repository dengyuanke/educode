package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.stepiot.constants.RoleEnum;
import com.stepiot.dao.EduUserMapper;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import com.stepiot.model.EduUserRole;
import com.stepiot.model.EduUserRoleCriteria;
import com.stepiot.model.EduUserRoleCriteria.Criteria;

@Service
public class EduUserService implements EduUserMapper {

  @Autowired
  private EduUserMapper mapper;

  @Autowired
  private EduUserRoleService roleMapper;


  @Override
  public long countByExample(EduUserCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String username) {

    return mapper.deleteByPrimaryKey(username);
  }

  @Transactional
  public int signUpUser(EduUser record, EduUserRole userRole) {
    roleMapper.insert(userRole);
    return mapper.insert(record);
  }

  @Override
  public int insert(EduUser record) {
    return mapper.insert(record);
  }

  public int insertUserAndRole(EduUser record, RoleEnum roleEnum) {
    mapper.insert(record);
    EduUserRole role = new EduUserRole();
    role.setUsername(record.getUsername());
    role.setRole(roleEnum.getValue());
    EduUserRoleCriteria ex = new EduUserRoleCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andUsernameEqualTo(record.getUsername());
    roleMapper.deleteByExample(ex);
    return roleMapper.insert(role);
  }

  public int updateUserAndRole(EduUser record, RoleEnum roleEnum) {
    mapper.updateByPrimaryKeySelective(record);
    EduUserRole role = new EduUserRole();
    role.setUsername(record.getUsername());
    role.setRole(roleEnum.getValue());
    EduUserRoleCriteria ex = new EduUserRoleCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andUsernameEqualTo(record.getUsername());
    roleMapper.deleteByExample(ex);
    return roleMapper.insert(role);
  }


  @Override
  public int insertSelective(EduUser record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUser> selectByExample(EduUserCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUser selectByPrimaryKey(String username) {

    return mapper.selectByPrimaryKey(username);
  }

  @Override
  public int updateByExampleSelective(EduUser record, EduUserCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduUser record, EduUserCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUser record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduUser record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUser selectByExampleForOne(EduUserCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String username) {

    return mapper.selectMapByPrimaryKey(username);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserCriteria example) {

    return mapper.selectMapByExample(example);
  }

}
