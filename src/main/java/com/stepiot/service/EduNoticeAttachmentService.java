package com.stepiot.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduNoticeAttachmentMapper;
import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.model.EduNoticeAttachmentCriteria;

@Service
public class EduNoticeAttachmentService implements EduNoticeAttachmentMapper {


  @Autowired
  private EduNoticeAttachmentMapper mapper;

  @Override
  public long countByExample(EduNoticeAttachmentCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduNoticeAttachmentCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer attachmentId) {

    return mapper.deleteByPrimaryKey(attachmentId);
  }

  @Override
  public int insert(EduNoticeAttachment record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduNoticeAttachment record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduNoticeAttachment> selectByExample(EduNoticeAttachmentCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduNoticeAttachment selectByPrimaryKey(Integer attachmentId) {

    return mapper.selectByPrimaryKey(attachmentId);
  }

  @Override
  public int updateByExampleSelective(EduNoticeAttachment record,
      EduNoticeAttachmentCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduNoticeAttachment record, EduNoticeAttachmentCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduNoticeAttachment record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduNoticeAttachment record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduNoticeAttachment selectByExampleForOne(EduNoticeAttachmentCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer attachmentId) {

    return mapper.selectMapByPrimaryKey(attachmentId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduNoticeAttachmentCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduNoticeAttachmentCriteria example) {

    return mapper.selectMapByExample(example);
  }

  public static void mainxx(String args[]) {
    Path p = Paths.get("C:\\Hello\\AnotherFolder\\The File Name.PDF");
    System.out.print(p.getFileName());
  }
}
