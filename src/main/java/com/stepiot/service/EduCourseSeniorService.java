package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduCourseSeniorMapper;
import com.stepiot.model.EduCourseSenior;
import com.stepiot.model.EduCourseSeniorCriteria;
import com.stepiot.model.EduCourseSeniorWithBLOBs;

@Service
public class EduCourseSeniorService implements EduCourseSeniorMapper {

  @Autowired
  private EduCourseSeniorMapper mapper;

  @Override
  public long countByExample(EduCourseSeniorCriteria example) {
    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduCourseSeniorCriteria example) {
    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer courseId) {
    return mapper.deleteByPrimaryKey(courseId);
  }

  @Override
  public int insert(EduCourseSeniorWithBLOBs record) {
    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduCourseSeniorWithBLOBs record) {
    return mapper.insertSelective(record);
  }

  @Override
  public List<EduCourseSeniorWithBLOBs> selectByExampleWithBLOBs(EduCourseSeniorCriteria example) {
    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduCourseSenior> selectByExample(EduCourseSeniorCriteria example) {
    return mapper.selectByExample(example);
  }

  @Override
  public EduCourseSeniorWithBLOBs selectByPrimaryKey(Integer courseId) {

    return mapper.selectByPrimaryKey(courseId);
  }

  @Override
  public int updateByExampleSelective(EduCourseSeniorWithBLOBs record,
      EduCourseSeniorCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduCourseSeniorWithBLOBs record,
      EduCourseSeniorCriteria example) {
    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduCourseSenior record, EduCourseSeniorCriteria example) {
    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduCourseSeniorWithBLOBs record) {
    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduCourseSeniorWithBLOBs record) {
    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduCourseSenior record) {
    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduCourseSenior selectByExampleForOne(EduCourseSeniorCriteria example) {
    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer courseId) {
    return mapper.selectMapByPrimaryKey(courseId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduCourseSeniorCriteria example) {
    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduCourseSeniorCriteria example) {
    return mapper.selectMapByExample(example);
  }

}
