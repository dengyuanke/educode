package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduStudentClassRefMapper;
import com.stepiot.dao.ext.EduStudentMapperExt;
import com.stepiot.model.EduStudentClassRef;
import com.stepiot.model.EduStudentClassRefCriteria;
import com.stepiot.model.EduStudentClassRefCriteria.Criteria;

@Service
public class EduStudentRefService implements EduStudentClassRefMapper, EduStudentMapperExt {

  @Autowired
  private EduStudentClassRefMapper mapper;

  @Autowired
  private EduStudentMapperExt ext;

  @Override
  public long countByExample(EduStudentClassRefCriteria example) {

    return mapper.countByExample(example);
  }

  public long countByClassId(String classId) {
    EduStudentClassRefCriteria ex = new EduStudentClassRefCriteria();
    Criteria ee = ex.createCriteria();
    ee.andClassIdEqualTo(classId);
    return mapper.countByExample(ex);
  }

  @Override
  public int deleteByExample(EduStudentClassRefCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer no) {

    return mapper.deleteByPrimaryKey(no);
  }

  @Override
  public int insert(EduStudentClassRef record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduStudentClassRef record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduStudentClassRef> selectByExample(EduStudentClassRefCriteria example) {

    return mapper.selectByExample(example);
  }

  public List<EduStudentClassRef> selectByClassId(String classId) {
    EduStudentClassRefCriteria ex = new EduStudentClassRefCriteria();
    Criteria ext = ex.createCriteria();
    ext.andClassIdEqualTo(classId);
    return mapper.selectByExample(ex);
  }

  public EduStudentClassRef selectByStudentId(String studentId) {
    EduStudentClassRefCriteria ex = new EduStudentClassRefCriteria();
    Criteria ext = ex.createCriteria();
    ext.andStudentIdEqualTo(studentId);
    return mapper.selectByExampleForOne(ex);
  }

  @Override
  public EduStudentClassRef selectByPrimaryKey(Integer no) {

    return mapper.selectByPrimaryKey(no);
  }

  @Override
  public int updateByExampleSelective(EduStudentClassRef record,
      EduStudentClassRefCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduStudentClassRef record, EduStudentClassRefCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduStudentClassRef record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduStudentClassRef record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduStudentClassRef selectByExampleForOne(EduStudentClassRefCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer no) {

    return mapper.selectMapByPrimaryKey(no);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduStudentClassRefCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduStudentClassRefCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listStudentsByMap(Map<String, Object> map) {
    return ext.listStudentsByMap(map);
  }

  public long countStudents(Map<String, Object> map) {
    return ext.countStudents(map);
  }

  public long countAllStudents(Map<String, Object> map) {
    return ext.countAllStudents(map);
  }

  @Override
  public List<Map<String, Object>> selectStudentsById(Map<String, Object> map) {
    return ext.selectStudentsById(map);
  }

  @Override
  public List<Map<String, Object>> listAllStudentsByMap(Map<String, Object> map) {
    return ext.listAllStudentsByMap(map);
  }

  @Override
  public List<Map<String, Object>> selectClassInfoById(Map<String, Object> map) {
    return ext.selectClassInfoById(map);
  }

}
