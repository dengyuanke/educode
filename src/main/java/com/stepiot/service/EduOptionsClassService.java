package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stepiot.dao.EduOptionsClassMapper;
import com.stepiot.dao.ext.EduOptionsClassMapperExt;
import com.stepiot.model.EduOptionsClass;
import com.stepiot.model.EduOptionsClassCriteria;

@Service
public class EduOptionsClassService implements EduOptionsClassMapper, EduOptionsClassMapperExt {

  @Autowired
  private EduOptionsClassMapper mapper;

  @Autowired
  private EduOptionsClassMapperExt ext;

  @Override
  public long countByExample(EduOptionsClassCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduOptionsClassCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(Integer optId) {

    return mapper.deleteByPrimaryKey(optId);
  }

  @Override
  public int insert(EduOptionsClass record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduOptionsClass record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduOptionsClass> selectByExample(EduOptionsClassCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduOptionsClass selectByPrimaryKey(Integer optId) {

    return mapper.selectByPrimaryKey(optId);
  }

  @Override
  public int updateByExampleSelective(EduOptionsClass record, EduOptionsClassCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExample(EduOptionsClass record, EduOptionsClassCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduOptionsClass record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(EduOptionsClass record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduOptionsClass selectByExampleForOne(EduOptionsClassCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(Integer optId) {

    return mapper.selectMapByPrimaryKey(optId);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduOptionsClassCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduOptionsClassCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public Map<String, Object> loadClassOptionsByStudentId(Map<String, Object> param) {
    return ext.loadClassOptionsByStudentId(param);
  }

}
