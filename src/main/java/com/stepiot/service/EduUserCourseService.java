package com.stepiot.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.stepiot.dao.EduUserCoursePrimaryMapper;
import com.stepiot.dao.ext.EduCoursePrimaryMapperExt;
import com.stepiot.model.EduUserCoursePrimary;
import com.stepiot.model.EduUserCoursePrimaryCriteria;
import com.stepiot.model.EduUserCoursePrimaryWithBLOBs;

@Service
public class EduUserCourseService implements EduUserCoursePrimaryMapper, EduCoursePrimaryMapperExt {

  @Autowired
  private EduUserCoursePrimaryMapper mapper;

  @Autowired
  private EduCoursePrimaryMapperExt mapperExt;

  @Override
  public long countByExample(EduUserCoursePrimaryCriteria example) {

    return mapper.countByExample(example);
  }

  @Override
  public int deleteByExample(EduUserCoursePrimaryCriteria example) {

    return mapper.deleteByExample(example);
  }

  @Override
  public int deleteByPrimaryKey(String courseUid) {

    return mapper.deleteByPrimaryKey(courseUid);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int insert(EduUserCoursePrimaryWithBLOBs record) {

    return mapper.insert(record);
  }

  @Override
  public int insertSelective(EduUserCoursePrimaryWithBLOBs record) {

    return mapper.insertSelective(record);
  }

  @Override
  public List<EduUserCoursePrimaryWithBLOBs> selectByExampleWithBLOBs(
      EduUserCoursePrimaryCriteria example) {

    return mapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<EduUserCoursePrimary> selectByExample(EduUserCoursePrimaryCriteria example) {

    return mapper.selectByExample(example);
  }

  @Override
  public EduUserCoursePrimaryWithBLOBs selectByPrimaryKey(String courseUid) {

    return mapper.selectByPrimaryKey(courseUid);
  }

  @Override
  public int updateByExampleSelective(EduUserCoursePrimaryWithBLOBs record,
      EduUserCoursePrimaryCriteria example) {

    return mapper.updateByExampleSelective(record, example);
  }

  @Transactional(isolation = Isolation.READ_COMMITTED)
  public int complete(EduUserCoursePrimaryWithBLOBs record, EduUserCoursePrimaryCriteria example,
      EduUserCoursePrimaryWithBLOBs next) {
    if (next != null) {
      mapper.insert(next);
    }
    return mapper.updateByExampleSelective(record, example);
  }

  @Override
  public int updateByExampleWithBLOBs(EduUserCoursePrimaryWithBLOBs record,
      EduUserCoursePrimaryCriteria example) {

    return mapper.updateByExampleWithBLOBs(record, example);
  }

  @Override
  public int updateByExample(EduUserCoursePrimary record, EduUserCoursePrimaryCriteria example) {

    return mapper.updateByExample(record, example);
  }

  @Override
  public int updateByPrimaryKeySelective(EduUserCoursePrimaryWithBLOBs record) {

    return mapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKeyWithBLOBs(EduUserCoursePrimaryWithBLOBs record) {

    return mapper.updateByPrimaryKeyWithBLOBs(record);
  }

  @Override
  public int updateByPrimaryKey(EduUserCoursePrimary record) {

    return mapper.updateByPrimaryKey(record);
  }

  @Override
  public EduUserCoursePrimary selectByExampleForOne(EduUserCoursePrimaryCriteria example) {

    return mapper.selectByExampleForOne(example);
  }

  @Override
  public Map<String, Object> selectMapByPrimaryKey(String courseUid) {

    return mapper.selectMapByPrimaryKey(courseUid);
  }

  @Override
  public Map<String, Object> selectMapByExampleForOne(EduUserCoursePrimaryCriteria example) {

    return mapper.selectMapByExampleForOne(example);
  }

  @Override
  public List<Map<String, Object>> selectMapByExample(EduUserCoursePrimaryCriteria example) {

    return mapper.selectMapByExample(example);
  }

  @Override
  public List<Map<String, Object>> listMyCourse(Map<String, Object> map) {
    return mapperExt.listMyCourse(map);
  }
}
