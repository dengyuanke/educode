package com.stepiot.iot;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.physicsfamily.http.HeaderCreator;
import com.physicsfamily.http.Https;
import com.physicsfamily.util.RandomUtils;
import com.physicsfamily.util.SignatureUtils;
import com.physicsfamily.util.StreamUtils;
import com.physicsfamily.util.URIComposer;
import com.stepiot.util.GsonUtils;

public class IotCore {
  // UserKey
  private static final String HC_USER_KEY = "JJgMoVmz";
  // User Auth. Key
  private static final String HC_USER_AUTH_KEY = "ddaDEEpX";

  // User Auth. Password
  private static final String USER_AUTH_SECRET = "WSgw2wbHjV2ctmio";


  private static final String BASE = "https://api.hanclouds.com/api/v1/";


  static final TypeToken<List<IotResponseBean>> iotBean = new TypeToken<List<IotResponseBean>>() {};
  static final Map<String, Boolean> interruptMap = Maps.newConcurrentMap();


  private static Header[] httpHeaders = null;
  static {
    Map<String, String> header = Maps.newHashMap();
    header.put("HC-USER-KEY", HC_USER_KEY);
    header.put("HC-USER-AUTH-KEY", HC_USER_AUTH_KEY);
    header.put("Content-Type", "application/json");
    httpHeaders = HeaderCreator.createHeaders(header);
  }

  public static String ts() {
    return System.currentTimeMillis() + "";
  }

  private static String nonce() {
    return RandomUtils.genPass(16);
  }

  private static Map<String, String> genBaseMap() {
    Map<String, String> param = Maps.newHashMap();
    param.put("ts", ts());
    param.put("nonce", nonce());
    return param;
  }

  /**
   * 获取产品列表
   * 
   * @param otherParam
   * @return
   */
  public static Object getProducts(Map<String, String> otherParam) {
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, null, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer.uriBuilder(BASE + "products", param);
    InputStream stream = Https.sendGetHttp(url, httpHeaders);
    System.out.println(StreamUtils.streamToStr(stream));
    return stream;
  }

  /**
   * 获取产品下的设备列表
   * 
   * @param otherParam
   * @param productKey
   * @return
   */
  public static Object getProductDevices(Map<String, String> otherParam, String productKey) {
    if (StringUtils.isEmpty(productKey)) {
      throw new NullPointerException("productKey can not be null");
    }
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, null, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer.uriBuilder(BASE + "products/" + productKey + "/devices", param);
    InputStream stream = Https.sendGetHttp(url, httpHeaders);
    System.out.println(StreamUtils.streamToStr(stream));
    return stream;
  }


  /**
   * 
   * @param otherParam
   * @param deviceKey b9157a45596e493aac029c920bd65b40
   * @param dataName abc
   * @return
   */
  public static Object sendDataFromDevice(Map<String, String> otherParam, String deviceKey,
      String dataName, String data) {
    if (StringUtils.isAnyEmpty(dataName, deviceKey)) {
      throw new NullPointerException("deviceKey and dataName can not be null");
    }

    ///
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, data, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer
        .uriBuilder(BASE + "devices/" + deviceKey + "/datastreams/" + dataName + "/data", param);
    InputStream stream = Https.sendPostHttp(url, data, httpHeaders);
    System.out.println(StreamUtils.streamToStr(stream));
    return stream;
  }

  public static String getDataFromDevice(Map<String, String> otherParam, String deviceKey,
      String dataName) {
    if (StringUtils.isAnyEmpty(dataName, deviceKey)) {
      throw new NullPointerException("deviceKey and dataName can not be null");
    }

    ///
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, null, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer.uriBuilder(
        BASE + "devices/" + deviceKey + "/datastreams/" + dataName + "/latestData", param);
    InputStream stream = Https.sendGetHttp(url, httpHeaders);

    return stream == null ? null : StreamUtils.streamToStr(stream);
  }

  // otherParam中的 dataType参数必填 Integer 是 命令数据类型
  public static Object sendCommandToDevice(Map<String, String> otherParam, String deviceKey,
      String cmd) {
    if (StringUtils.isAnyEmpty(deviceKey)) {
      throw new NullPointerException("deviceKey can not be null");
    }

    ///
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, cmd, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer.uriBuilder(BASE + "devices/" + deviceKey + "/commands", param);
    InputStream stream = Https.sendPostHttp(url, cmd, httpHeaders);
    return stream == null ? null : StreamUtils.streamToStr(stream);
  }

  public static Object getDeviceData(Map<String, String> otherParam, String deviceKey) {
    if (StringUtils.isAnyEmpty(deviceKey)) {
      throw new NullPointerException("deviceKey and dataName can not be null");
    }

    ///
    Map<String, String> param = genBaseMap();
    if (otherParam != null && otherParam.size() > 0) {
      param.putAll(otherParam);
    }
    String hmacSha1Base64 = SignatureUtils.hmacSha1(param, null, USER_AUTH_SECRET);
    param.put("signature", hmacSha1Base64);
    String url = URIComposer.uriBuilder(BASE + "devices/" + deviceKey + "/datastreams", param);
    InputStream stream = Https.sendGetHttp(url, httpHeaders);
    System.out.println(StreamUtils.streamToStr(stream));
    return stream;
  }


  // private static String deviceKey = "b9157a45596e493aac029c920bd65b40";
  private static String deviceKey = "c14124e4836e48348decfdc40f5041d0";

  /**
   * 控制继电器
   */
  public static void turnRelay(int command) {
    System.err.println("turnRelay" + Thread.currentThread().getName());
    String cmd = "relay_off";
    if (command == 1) {
      cmd = "relay_on";
    }
    Map<String, String> param = Maps.newHashMap();
    param.put("dataType", "3");
    sendCommandToDevice(param, deviceKey, cmd);
  }

  /**
   * 控制伺服电机
   */
  public static void turnStepperotor(int command) {
    System.err.println("turnRelay" + Thread.currentThread().getName());
    String cmd = "stop";
    if (command == 1) {
      cmd = "run";
    }
    Map<String, String> param = Maps.newHashMap();
    param.put("dataType", "3");
    sendCommandToDevice(param, deviceKey, cmd);
  }

  /**
   * 读取人体红外传感器数据
   */
  public static Long readAd() {
    IotResponseBean bean = readLatestData(deviceKey, "pirModule");
    if (bean != null && bean.getData() != null) {
      return bean.getData().getPirModuleAD();
    }
    return 0L;

  }

  /**
   * 读取超声波传感器数据
   */
  public static Long readDistance() {
    IotResponseBean bean = readLatestData(deviceKey, "ultrasonicModule");
    if (bean != null && bean.getData() != null) {
      return bean.getData().getDistance();
    }
    return 0L;
  }

  /**
   * 读取光照强度传感器数据
   */
  public static Long readIntensity() {
    IotResponseBean bean = readLatestData(deviceKey, "humidityModule");
    if (bean != null && bean.getData() != null) {
      return bean.getData().getLightIntensity();
    }
    return 0L;
  }

  /**
   * 读取湿度值
   */
  public static Double readHumidity() {
    IotResponseBean bean = readLatestData(deviceKey, "humidityModule");
    if (bean != null && bean.getData() != null) {
      return bean.getData().getHumiHM();
    }
    return 0d;
  }

  /**
   * 读取温度值
   */
  public static Double readTemp() {
    IotResponseBean bean = readLatestData(deviceKey, "humidityModule");
    if (bean != null && bean.getData() != null) {
      return bean.getData().getTempHM();
    }
    return 0d;

  }

  public static boolean interrupted(String username) {
    Boolean v = interruptMap.get(username);
    if (v == null) {
      return false;
    }
    return interruptMap.get(username);
  }

  public static void setInterrupted(String username, boolean v) {
    interruptMap.put(username, v);
  }



  private static IotResponseBean readLatestData(String deviceKey, String dataName) {
    System.err.println("readLatestData" + Thread.currentThread().getName());
    String json = getDataFromDevice(null, deviceKey, dataName);
    if (json == null) {
      return null;
    }
    List<IotResponseBean> list = GsonUtils.toTypedList(json, iotBean);
    if (list != null && !list.isEmpty()) {
      IotResponseBean bean = list.get(0);
      return bean;
    }
    return null;
  }

}
