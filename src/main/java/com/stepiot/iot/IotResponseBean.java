package com.stepiot.iot;

public class IotResponseBean implements java.io.Serializable {



  static final class Data implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2657912062666952409L;
    private Long lightIntensity = 0L;
    private Long packedID = 0L;
    private Long moduleAddr = 0L;
    private Double humiHM = 0d;
    private Double tempHM = 0d;
    private Long distance = 0L;
    private Long pirModuleAD = 0L;



    public Long getDistance() {
      return distance;
    }

    public void setDistance(Long distance) {
      this.distance = distance;
    }

    public Long getPirModuleAD() {
      return pirModuleAD;
    }

    public void setPirModuleAD(Long pirModuleAD) {
      this.pirModuleAD = pirModuleAD;
    }

    @Override
    public String toString() {
      return "Data [lightIntensity=" + lightIntensity + ", packedID=" + packedID + ", moduleAddr="
          + moduleAddr + ", humiHM=" + humiHM + ", tempHM=" + tempHM + ", distance=" + distance
          + ", pirModuleAD=" + pirModuleAD + "]";
    }

    public Long getLightIntensity() {
      return lightIntensity;
    }

    public void setLightIntensity(Long lightIntensity) {
      this.lightIntensity = lightIntensity;
    }

    public Long getPackedID() {
      return packedID;
    }

    public void setPackedID(Long packedID) {
      this.packedID = packedID;
    }

    public Long getModuleAddr() {
      return moduleAddr;
    }

    public void setModuleAddr(Long moduleAddr) {
      this.moduleAddr = moduleAddr;
    }

    public Double getHumiHM() {
      return humiHM;
    }

    public void setHumiHM(Double humiHM) {
      this.humiHM = humiHM;
    }

    public Double getTempHM() {
      return tempHM;
    }

    public void setTempHM(Double tempHM) {
      this.tempHM = tempHM;
    }
  }

  @Override
  public String toString() {
    return "IotResponseBean [userKey=" + userKey + ", productKey=" + productKey + ", deviceKey="
        + deviceKey + ", time=" + time + ", stream=" + stream + ", type=" + type + ", data=" + data
        + "]";
  }

  /**
   * 
   */
  private static final long serialVersionUID = 7993015033477597015L;
  private String userKey;
  private String productKey;
  private String deviceKey;
  private Long time;

  public String getUserKey() {
    return userKey;
  }

  public void setUserKey(String userKey) {
    this.userKey = userKey;
  }

  public String getProductKey() {
    return productKey;
  }

  public void setProductKey(String productKey) {
    this.productKey = productKey;
  }

  public String getDeviceKey() {
    return deviceKey;
  }

  public void setDeviceKey(String deviceKey) {
    this.deviceKey = deviceKey;
  }

  public Long getTime() {
    return time;
  }

  public void setTime(Long time) {
    this.time = time;
  }

  public String getStream() {
    return stream;
  }

  public void setStream(String stream) {
    this.stream = stream;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  private String stream;
  private Integer type;
  private Data data;


}
