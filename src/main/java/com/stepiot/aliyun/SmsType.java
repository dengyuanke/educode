package com.stepiot.aliyun;

public enum SmsType {
  REGISTRATION("REGISTRATION", "SMS_142480148"), RECOVERY("RECOVERY", "SMS_142480147");
  private String name;
  private String typeCode;

  SmsType(String name, String typeCode) {
    this.name = name;
    this.typeCode = typeCode;
  }

  public String getName() {
    return this.name;
  }

  public String getCode() {
    return this.typeCode;
  }
}
