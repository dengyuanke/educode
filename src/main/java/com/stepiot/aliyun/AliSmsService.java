package com.stepiot.aliyun;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

@Service
public class AliSmsService {

  private static final String product = "Dysmsapi";
  private static final String domain = "dysmsapi.aliyuncs.com";

  private static final String accessKeyId = "LTAIUo9r2azmasKp";
  private static final String accessKeySecret = "KBO3bfntNQtO19SK2bSf4h5ChIbzsq";

  IAcsClient acsClient;

  AliSmsService() {
    // 可自助调整超时时间
    System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
    System.setProperty("sun.net.client.defaultReadTimeout", "10000");

    // 初始化acsClient,暂不支持region化
    IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
    DefaultProfile.addEndpoint("cn-hangzhou", product, domain);

    acsClient = new DefaultAcsClient(profile);
  }

  @Async
  public SendSmsResponse sendSms(String phone, String code, SmsType type) {

    // 组装请求对象-具体描述见控制台-文档部分内容
    SendSmsRequest request = new SendSmsRequest();
    // 必填:待发送手机号
    request.setPhoneNumbers(phone);
    // 必填:短信签名-可在短信控制台中找到
    request.setSignName("八城科技");
    // 必填:短信模板-可在短信控制台中找到
    request.setTemplateCode(type.getCode());
    // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
    request.setTemplateParam("{\"code\":\"" + code + "\"}");

    SendSmsResponse sendSmsResponse = null;
    try {

      sendSmsResponse = acsClient.getAcsResponse(request);
    } catch (ServerException e) {
      e.printStackTrace();
    } catch (ClientException e) {
      e.printStackTrace();
    }

    return sendSmsResponse;
  }
}
