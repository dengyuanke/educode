package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduUserBlockly implements Serializable {
    /**
     * blocklyId
     */
    public static final String BLOCKLYID = "blocklyId";

    /**
     * blocklyId
     */
    private String blocklyId;

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    /**
     * titleName
     */
    public static final String TITLENAME = "titleName";

    /**
     * titleName
     */
    private String titleName;

    /**
     * createDate
     */
    public static final String CREATEDATE = "createDate";

    /**
     * createDate
     */
    private Date createDate;

    /**
     * lastEdit
     */
    public static final String LASTEDIT = "lastEdit";

    /**
     * lastEdit
     */
    private Date lastEdit;

    private static final long serialVersionUID = 1L;

    /**
     * @return blocklyId
     *         blocklyId
     */
    public String getBlocklyId() {
        return blocklyId;
    }

    /**
     * @param blocklyId
     *         blocklyId
     */
    public void setBlocklyId(String blocklyId) {
        this.blocklyId = blocklyId == null ? null : blocklyId.trim();
    }

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return titleName
     *         titleName
     */
    public String getTitleName() {
        return titleName;
    }

    /**
     * @param titleName
     *         titleName
     */
    public void setTitleName(String titleName) {
        this.titleName = titleName == null ? null : titleName.trim();
    }

    /**
     * @return createDate
     *         createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *         createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return lastEdit
     *         lastEdit
     */
    public Date getLastEdit() {
        return lastEdit;
    }

    /**
     * @param lastEdit
     *         lastEdit
     */
    public void setLastEdit(Date lastEdit) {
        this.lastEdit = lastEdit;
    }
}