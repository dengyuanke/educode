package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduUserPythonCourse implements Serializable {
    /**
     * userCourseId
     */
    public static final String USERCOURSEID = "userCourseId";

    /**
     * userCourseId
     */
    private Integer userCourseId;

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    /**
     * 对应大纲编号
     */
    public static final String OUTLINEID = "outlineId";

    /**
     * 对应大纲编号
     */
    private Integer outlineId;

    /**
     * 代码类型：python c 等
     */
    public static final String CODETYPE = "codeType";

    /**
     * 代码类型：python c 等
     */
    private String codeType;

    /**
     * 状态
     * 0:saved
     * 1:submitted
     * 2:passed
     * -1:rejected
     */
    public static final String STATUS = "status";

    /**
     * 状态
     * 0:saved
     * 1:submitted
     * 2:passed
     * -1:rejected
     */
    private Integer status;

    /**
     * 创建时间
     */
    public static final String CREATETIME = "createTime";

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    public static final String LASTUPDATETIME = "lastUpdateTime";

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;

    /**
     * level
     */
    public static final String LEVEL = "level";

    /**
     * level
     */
    private Integer level;

    /**
     * 累加时长
     */
    public static final String ACCUMULATEDSECONDS = "accumulatedSeconds";

    /**
     * 累加时长
     */
    private Integer accumulatedSeconds;

    /**
     * 分数
     */
    public static final String SCORE = "score";

    /**
     * 分数
     */
    private Integer score;

    private static final long serialVersionUID = 1L;

    /**
     * @return userCourseId
     *         userCourseId
     */
    public Integer getUserCourseId() {
        return userCourseId;
    }

    /**
     * @param userCourseId
     *         userCourseId
     */
    public void setUserCourseId(Integer userCourseId) {
        this.userCourseId = userCourseId;
    }

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return outlineId
     *         对应大纲编号
     */
    public Integer getOutlineId() {
        return outlineId;
    }

    /**
     * @param outlineId
     *         对应大纲编号
     */
    public void setOutlineId(Integer outlineId) {
        this.outlineId = outlineId;
    }

    /**
     * @return codeType
     *         代码类型：python c 等
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     * @param codeType
     *         代码类型：python c 等
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType == null ? null : codeType.trim();
    }

    /**
     * @return status
     *         状态
     *         0:saved
     *         1:submitted
     *         2:passed
     *         -1:rejected
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     *         状态
     *         0:saved
     *         1:submitted
     *         2:passed
     *         -1:rejected
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return createTime
     *         创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     *         创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return lastUpdateTime
     *         最后更新时间
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime
     *         最后更新时间
     */
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * @return level
     *         level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level
     *         level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return accumulatedSeconds
     *         累加时长
     */
    public Integer getAccumulatedSeconds() {
        return accumulatedSeconds;
    }

    /**
     * @param accumulatedSeconds
     *         累加时长
     */
    public void setAccumulatedSeconds(Integer accumulatedSeconds) {
        this.accumulatedSeconds = accumulatedSeconds;
    }

    /**
     * @return score
     *         分数
     */
    public Integer getScore() {
        return score;
    }

    /**
     * @param score
     *         分数
     */
    public void setScore(Integer score) {
        this.score = score;
    }
}