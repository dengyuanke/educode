package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class SysHardwaresCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public SysHardwaresCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHardwareNameIsNull() {
            addCriterion("hardware_name is null");
            return (Criteria) this;
        }

        public Criteria andHardwareNameIsNotNull() {
            addCriterion("hardware_name is not null");
            return (Criteria) this;
        }

        public Criteria andHardwareNameEqualTo(String value) {
            addCriterion("hardware_name =", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameNotEqualTo(String value) {
            addCriterion("hardware_name <>", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameGreaterThan(String value) {
            addCriterion("hardware_name >", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameGreaterThanOrEqualTo(String value) {
            addCriterion("hardware_name >=", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameLessThan(String value) {
            addCriterion("hardware_name <", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameLessThanOrEqualTo(String value) {
            addCriterion("hardware_name <=", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameLike(String value) {
            addCriterion("hardware_name like", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameNotLike(String value) {
            addCriterion("hardware_name not like", value, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameIn(List<String> values) {
            addCriterion("hardware_name in", values, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameNotIn(List<String> values) {
            addCriterion("hardware_name not in", values, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameBetween(String value1, String value2) {
            addCriterion("hardware_name between", value1, value2, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andHardwareNameNotBetween(String value1, String value2) {
            addCriterion("hardware_name not between", value1, value2, "hardwareName");
            return (Criteria) this;
        }

        public Criteria andPhotoIsNull() {
            addCriterion("photo is null");
            return (Criteria) this;
        }

        public Criteria andPhotoIsNotNull() {
            addCriterion("photo is not null");
            return (Criteria) this;
        }

        public Criteria andPhotoEqualTo(String value) {
            addCriterion("photo =", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoNotEqualTo(String value) {
            addCriterion("photo <>", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoGreaterThan(String value) {
            addCriterion("photo >", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoGreaterThanOrEqualTo(String value) {
            addCriterion("photo >=", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoLessThan(String value) {
            addCriterion("photo <", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoLessThanOrEqualTo(String value) {
            addCriterion("photo <=", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoLike(String value) {
            addCriterion("photo like", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoNotLike(String value) {
            addCriterion("photo not like", value, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoIn(List<String> values) {
            addCriterion("photo in", values, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoNotIn(List<String> values) {
            addCriterion("photo not in", values, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoBetween(String value1, String value2) {
            addCriterion("photo between", value1, value2, "photo");
            return (Criteria) this;
        }

        public Criteria andPhotoNotBetween(String value1, String value2) {
            addCriterion("photo not between", value1, value2, "photo");
            return (Criteria) this;
        }

        public Criteria andActionLinkIsNull() {
            addCriterion("action_link is null");
            return (Criteria) this;
        }

        public Criteria andActionLinkIsNotNull() {
            addCriterion("action_link is not null");
            return (Criteria) this;
        }

        public Criteria andActionLinkEqualTo(String value) {
            addCriterion("action_link =", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkNotEqualTo(String value) {
            addCriterion("action_link <>", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkGreaterThan(String value) {
            addCriterion("action_link >", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkGreaterThanOrEqualTo(String value) {
            addCriterion("action_link >=", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkLessThan(String value) {
            addCriterion("action_link <", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkLessThanOrEqualTo(String value) {
            addCriterion("action_link <=", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkLike(String value) {
            addCriterion("action_link like", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkNotLike(String value) {
            addCriterion("action_link not like", value, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkIn(List<String> values) {
            addCriterion("action_link in", values, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkNotIn(List<String> values) {
            addCriterion("action_link not in", values, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkBetween(String value1, String value2) {
            addCriterion("action_link between", value1, value2, "actionLink");
            return (Criteria) this;
        }

        public Criteria andActionLinkNotBetween(String value1, String value2) {
            addCriterion("action_link not between", value1, value2, "actionLink");
            return (Criteria) this;
        }

        public Criteria andTypesIsNull() {
            addCriterion("types is null");
            return (Criteria) this;
        }

        public Criteria andTypesIsNotNull() {
            addCriterion("types is not null");
            return (Criteria) this;
        }

        public Criteria andTypesEqualTo(String value) {
            addCriterion("types =", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotEqualTo(String value) {
            addCriterion("types <>", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesGreaterThan(String value) {
            addCriterion("types >", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesGreaterThanOrEqualTo(String value) {
            addCriterion("types >=", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLessThan(String value) {
            addCriterion("types <", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLessThanOrEqualTo(String value) {
            addCriterion("types <=", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesLike(String value) {
            addCriterion("types like", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotLike(String value) {
            addCriterion("types not like", value, "types");
            return (Criteria) this;
        }

        public Criteria andTypesIn(List<String> values) {
            addCriterion("types in", values, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotIn(List<String> values) {
            addCriterion("types not in", values, "types");
            return (Criteria) this;
        }

        public Criteria andTypesBetween(String value1, String value2) {
            addCriterion("types between", value1, value2, "types");
            return (Criteria) this;
        }

        public Criteria andTypesNotBetween(String value1, String value2) {
            addCriterion("types not between", value1, value2, "types");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("version is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("version is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(String value) {
            addCriterion("version =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(String value) {
            addCriterion("version <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(String value) {
            addCriterion("version >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(String value) {
            addCriterion("version >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(String value) {
            addCriterion("version <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(String value) {
            addCriterion("version <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLike(String value) {
            addCriterion("version like", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotLike(String value) {
            addCriterion("version not like", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<String> values) {
            addCriterion("version in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<String> values) {
            addCriterion("version not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(String value1, String value2) {
            addCriterion("version between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(String value1, String value2) {
            addCriterion("version not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDocLinkIsNull() {
            addCriterion("doc_link is null");
            return (Criteria) this;
        }

        public Criteria andDocLinkIsNotNull() {
            addCriterion("doc_link is not null");
            return (Criteria) this;
        }

        public Criteria andDocLinkEqualTo(String value) {
            addCriterion("doc_link =", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkNotEqualTo(String value) {
            addCriterion("doc_link <>", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkGreaterThan(String value) {
            addCriterion("doc_link >", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkGreaterThanOrEqualTo(String value) {
            addCriterion("doc_link >=", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkLessThan(String value) {
            addCriterion("doc_link <", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkLessThanOrEqualTo(String value) {
            addCriterion("doc_link <=", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkLike(String value) {
            addCriterion("doc_link like", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkNotLike(String value) {
            addCriterion("doc_link not like", value, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkIn(List<String> values) {
            addCriterion("doc_link in", values, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkNotIn(List<String> values) {
            addCriterion("doc_link not in", values, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkBetween(String value1, String value2) {
            addCriterion("doc_link between", value1, value2, "docLink");
            return (Criteria) this;
        }

        public Criteria andDocLinkNotBetween(String value1, String value2) {
            addCriterion("doc_link not between", value1, value2, "docLink");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}