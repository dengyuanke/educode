package com.stepiot.model;

import java.io.Serializable;

public class MicrobitCourse implements Serializable {
    /**
     * 课程ID
     */
    public static final String COURSEID = "courseId";

    /**
     * 课程ID
     */
    private Integer courseId;

    /**
     * 课程标题
     */
    public static final String COURSENAME = "courseName";

    /**
     * 课程标题
     */
    private String courseName;

    /**
     * 课程目标
     */
    public static final String COURSEGOAL = "courseGoal";

    /**
     * 课程目标
     */
    private String courseGoal;

    /**
     * courseStage
     */
    public static final String COURSESTAGE = "courseStage";

    /**
     * courseStage
     */
    private Integer courseStage;

    private static final long serialVersionUID = 1L;

    /**
     * @return courseId
     *         课程ID
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         课程ID
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return courseName
     *         课程标题
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName
     *         课程标题
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    /**
     * @return courseGoal
     *         课程目标
     */
    public String getCourseGoal() {
        return courseGoal;
    }

    /**
     * @param courseGoal
     *         课程目标
     */
    public void setCourseGoal(String courseGoal) {
        this.courseGoal = courseGoal == null ? null : courseGoal.trim();
    }

    /**
     * @return courseStage
     *         courseStage
     */
    public Integer getCourseStage() {
        return courseStage;
    }

    /**
     * @param courseStage
     *         courseStage
     */
    public void setCourseStage(Integer courseStage) {
        this.courseStage = courseStage;
    }
}