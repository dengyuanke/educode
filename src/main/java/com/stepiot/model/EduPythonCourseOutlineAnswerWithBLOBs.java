package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutlineAnswerWithBLOBs extends EduPythonCourseOutlineAnswer implements Serializable {
    /**
     * 代码必须包含的内容
     */
    public static final String MUSTINCLUDE = "mustInclude";

    /**
     * 代码必须包含的内容
     */
    private String mustInclude;

    /**
     * 代码最好包含的内容
     */
    public static final String PREFERINCLUDE = "preferInclude";

    /**
     * 代码最好包含的内容
     */
    private String preferInclude;

    /**
     * 代码一定不可以包含的内容
     */
    public static final String MUSTNOTINCLUDE = "mustNotInclude";

    /**
     * 代码一定不可以包含的内容
     */
    private String mustNotInclude;

    /**
     * defaultAnswer
     */
    public static final String DEFAULTANSWER = "defaultAnswer";

    /**
     * defaultAnswer
     */
    private String defaultAnswer;

    /**
     * 积木提示
     */
    public static final String TUTORIALBLOCKLY = "tutorialBlockly";

    /**
     * 积木提示
     */
    private String tutorialBlockly;

    /**
     * 代码提示
     */
    public static final String TUTORIALCODE = "tutorialCode";

    /**
     * 代码提示
     */
    private String tutorialCode;

    private static final long serialVersionUID = 1L;

    /**
     * @return mustInclude
     *         代码必须包含的内容
     */
    public String getMustInclude() {
        return mustInclude;
    }

    /**
     * @param mustInclude
     *         代码必须包含的内容
     */
    public void setMustInclude(String mustInclude) {
        this.mustInclude = mustInclude == null ? null : mustInclude.trim();
    }

    /**
     * @return preferInclude
     *         代码最好包含的内容
     */
    public String getPreferInclude() {
        return preferInclude;
    }

    /**
     * @param preferInclude
     *         代码最好包含的内容
     */
    public void setPreferInclude(String preferInclude) {
        this.preferInclude = preferInclude == null ? null : preferInclude.trim();
    }

    /**
     * @return mustNotInclude
     *         代码一定不可以包含的内容
     */
    public String getMustNotInclude() {
        return mustNotInclude;
    }

    /**
     * @param mustNotInclude
     *         代码一定不可以包含的内容
     */
    public void setMustNotInclude(String mustNotInclude) {
        this.mustNotInclude = mustNotInclude == null ? null : mustNotInclude.trim();
    }

    /**
     * @return defaultAnswer
     *         defaultAnswer
     */
    public String getDefaultAnswer() {
        return defaultAnswer;
    }

    /**
     * @param defaultAnswer
     *         defaultAnswer
     */
    public void setDefaultAnswer(String defaultAnswer) {
        this.defaultAnswer = defaultAnswer == null ? null : defaultAnswer.trim();
    }

    /**
     * @return tutorialBlockly
     *         积木提示
     */
    public String getTutorialBlockly() {
        return tutorialBlockly;
    }

    /**
     * @param tutorialBlockly
     *         积木提示
     */
    public void setTutorialBlockly(String tutorialBlockly) {
        this.tutorialBlockly = tutorialBlockly == null ? null : tutorialBlockly.trim();
    }

    /**
     * @return tutorialCode
     *         代码提示
     */
    public String getTutorialCode() {
        return tutorialCode;
    }

    /**
     * @param tutorialCode
     *         代码提示
     */
    public void setTutorialCode(String tutorialCode) {
        this.tutorialCode = tutorialCode == null ? null : tutorialCode.trim();
    }
}