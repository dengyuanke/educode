package com.stepiot.model;

import java.io.Serializable;

public class EduCourseOutline implements Serializable {
    /**
     * outlineId
     */
    public static final String OUTLINEID = "outlineId";

    /**
     * outlineId
     */
    private Integer outlineId;

    /**
     * outlineName
     */
    public static final String OUTLINENAME = "outlineName";

    /**
     * outlineName
     */
    private String outlineName;

    /**
     * courseId
     */
    public static final String COURSEID = "courseId";

    /**
     * courseId
     */
    private Integer courseId;

    private static final long serialVersionUID = 1L;

    /**
     * @return outlineId
     *         outlineId
     */
    public Integer getOutlineId() {
        return outlineId;
    }

    /**
     * @param outlineId
     *         outlineId
     */
    public void setOutlineId(Integer outlineId) {
        this.outlineId = outlineId;
    }

    /**
     * @return outlineName
     *         outlineName
     */
    public String getOutlineName() {
        return outlineName;
    }

    /**
     * @param outlineName
     *         outlineName
     */
    public void setOutlineName(String outlineName) {
        this.outlineName = outlineName == null ? null : outlineName.trim();
    }

    /**
     * @return courseId
     *         courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         courseId
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}