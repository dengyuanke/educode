package com.stepiot.model;

import java.io.Serializable;

public class EduStudentClassRef implements Serializable {
    /**
     * refId
     */
    public static final String REFID = "refId";

    /**
     * refId
     */
    private Integer refId;

    /**
     * studentId
     */
    public static final String STUDENTID = "studentId";

    /**
     * studentId
     */
    private String studentId;

    /**
     * classId
     */
    public static final String CLASSID = "classId";

    /**
     * classId
     */
    private String classId;

    /**
     * className
     */
    public static final String CLASSNAME = "className";

    /**
     * className
     */
    private String className;

    private static final long serialVersionUID = 1L;

    /**
     * @return refId
     *         refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId
     *         refId
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return studentId
     *         studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId
     *         studentId
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId == null ? null : studentId.trim();
    }

    /**
     * @return classId
     *         classId
     */
    public String getClassId() {
        return classId;
    }

    /**
     * @param classId
     *         classId
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * @return className
     *         className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className
     *         className
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }
}