package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduTeacher implements Serializable {
    /**
     * teacherId
     */
    public static final String TEACHERID = "teacherId";

    /**
     * teacherId
     */
    private Integer teacherId;

    /**
     * teacherName
     */
    public static final String TEACHERNAME = "teacherName";

    /**
     * teacherName
     */
    private String teacherName;

    /**
     * age
     */
    public static final String AGE = "age";

    /**
     * age
     */
    private Integer age;

    /**
     * 1:male
     * 2:female
     */
    public static final String GENDER = "gender";

    /**
     * 1:male
     * 2:female
     */
    private Integer gender;

    /**
     * regDate
     */
    public static final String REGDATE = "regDate";

    /**
     * regDate
     */
    private Date regDate;

    /**
     * avatar
     */
    public static final String AVATAR = "avatar";

    /**
     * avatar
     */
    private String avatar;

    private static final long serialVersionUID = 1L;

    /**
     * @return teacherId
     *         teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId
     *         teacherId
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return teacherName
     *         teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName
     *         teacherName
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    /**
     * @return age
     *         age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     *         age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return gender
     *         1:male
     *         2:female
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * @param gender
     *         1:male
     *         2:female
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * @return regDate
     *         regDate
     */
    public Date getRegDate() {
        return regDate;
    }

    /**
     * @param regDate
     *         regDate
     */
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    /**
     * @return avatar
     *         avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar
     *         avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }
}