package com.stepiot.model;

import java.io.Serializable;

public class EduUserPythonCourseWithBLOBs extends EduUserPythonCourse implements Serializable {
    /**
     * 用户创建的Blockly代码
     */
    public static final String USERBLOCKLY = "userBlockly";

    /**
     * 用户创建的Blockly代码
     */
    private String userBlockly;

    /**
     * 用户编写的代码
     */
    public static final String USERCODE = "userCode";

    /**
     * 用户编写的代码
     */
    private String userCode;

    /**
     * userBlocklyTypes
     */
    public static final String USERBLOCKLYTYPES = "userBlocklyTypes";

    /**
     * userBlocklyTypes
     */
    private String userBlocklyTypes;

    private static final long serialVersionUID = 1L;

    /**
     * @return userBlockly
     *         用户创建的Blockly代码
     */
    public String getUserBlockly() {
        return userBlockly;
    }

    /**
     * @param userBlockly
     *         用户创建的Blockly代码
     */
    public void setUserBlockly(String userBlockly) {
        this.userBlockly = userBlockly == null ? null : userBlockly.trim();
    }

    /**
     * @return userCode
     *         用户编写的代码
     */
    public String getUserCode() {
        return userCode;
    }

    /**
     * @param userCode
     *         用户编写的代码
     */
    public void setUserCode(String userCode) {
        this.userCode = userCode == null ? null : userCode.trim();
    }

    /**
     * @return userBlocklyTypes
     *         userBlocklyTypes
     */
    public String getUserBlocklyTypes() {
        return userBlocklyTypes;
    }

    /**
     * @param userBlocklyTypes
     *         userBlocklyTypes
     */
    public void setUserBlocklyTypes(String userBlocklyTypes) {
        this.userBlocklyTypes = userBlocklyTypes == null ? null : userBlocklyTypes.trim();
    }
}