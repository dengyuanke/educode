package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class EduCourseOutlineCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduCourseOutlineCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOutlineIdIsNull() {
            addCriterion("outline_id is null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIsNotNull() {
            addCriterion("outline_id is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdEqualTo(Integer value) {
            addCriterion("outline_id =", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotEqualTo(Integer value) {
            addCriterion("outline_id <>", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThan(Integer value) {
            addCriterion("outline_id >", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("outline_id >=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThan(Integer value) {
            addCriterion("outline_id <", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThanOrEqualTo(Integer value) {
            addCriterion("outline_id <=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIn(List<Integer> values) {
            addCriterion("outline_id in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotIn(List<Integer> values) {
            addCriterion("outline_id not in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdBetween(Integer value1, Integer value2) {
            addCriterion("outline_id between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("outline_id not between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIsNull() {
            addCriterion("outline_name is null");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIsNotNull() {
            addCriterion("outline_name is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineNameEqualTo(String value) {
            addCriterion("outline_name =", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotEqualTo(String value) {
            addCriterion("outline_name <>", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameGreaterThan(String value) {
            addCriterion("outline_name >", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameGreaterThanOrEqualTo(String value) {
            addCriterion("outline_name >=", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLessThan(String value) {
            addCriterion("outline_name <", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLessThanOrEqualTo(String value) {
            addCriterion("outline_name <=", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLike(String value) {
            addCriterion("outline_name like", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotLike(String value) {
            addCriterion("outline_name not like", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIn(List<String> values) {
            addCriterion("outline_name in", values, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotIn(List<String> values) {
            addCriterion("outline_name not in", values, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameBetween(String value1, String value2) {
            addCriterion("outline_name between", value1, value2, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotBetween(String value1, String value2) {
            addCriterion("outline_name not between", value1, value2, "outlineName");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Integer value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Integer value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Integer value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Integer value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Integer value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Integer> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Integer> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Integer value1, Integer value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}