package com.stepiot.model;

import java.io.Serializable;

public class EduOptionsClass implements Serializable {
    /**
     * optId
     */
    public static final String OPTID = "optId";

    /**
     * optId
     */
    private Integer optId;

    /**
     * clazzId
     */
    public static final String CLAZZID = "clazzId";

    /**
     * clazzId
     */
    private String clazzId;

    /**
     * openVideo
     */
    public static final String OPENVIDEO = "openVideo";

    /**
     * openVideo
     */
    private Integer openVideo;

    private static final long serialVersionUID = 1L;

    /**
     * @return optId
     *         optId
     */
    public Integer getOptId() {
        return optId;
    }

    /**
     * @param optId
     *         optId
     */
    public void setOptId(Integer optId) {
        this.optId = optId;
    }

    /**
     * @return clazzId
     *         clazzId
     */
    public String getClazzId() {
        return clazzId;
    }

    /**
     * @param clazzId
     *         clazzId
     */
    public void setClazzId(String clazzId) {
        this.clazzId = clazzId == null ? null : clazzId.trim();
    }

    /**
     * @return openVideo
     *         openVideo
     */
    public Integer getOpenVideo() {
        return openVideo;
    }

    /**
     * @param openVideo
     *         openVideo
     */
    public void setOpenVideo(Integer openVideo) {
        this.openVideo = openVideo;
    }
}