package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduClass implements Serializable {
    /**
     * classId
     */
    public static final String CLASSID = "classId";

    /**
     * classId
     */
    private String classId;

    /**
     * className
     */
    public static final String CLASSNAME = "className";

    /**
     * className
     */
    private String className;

    /**
     * createDate
     */
    public static final String CREATEDATE = "createDate";

    /**
     * createDate
     */
    private Date createDate;

    /**
     * createUser
     */
    public static final String CREATEUSER = "createUser";

    /**
     * createUser
     */
    private String createUser;

    /**
     * updateDate
     */
    public static final String UPDATEDATE = "updateDate";

    /**
     * updateDate
     */
    private Date updateDate;

    /**
     * updateUser
     */
    public static final String UPDATEUSER = "updateUser";

    /**
     * updateUser
     */
    private String updateUser;

    /**
     * roomDesc
     */
    public static final String ROOMDESC = "roomDesc";

    /**
     * roomDesc
     */
    private String roomDesc;

    /**
     * 专业编号
     */
    public static final String DISCIPLINEID = "disciplineId";

    /**
     * 专业编号
     */
    private String disciplineId;

    private static final long serialVersionUID = 1L;

    /**
     * @return classId
     *         classId
     */
    public String getClassId() {
        return classId;
    }

    /**
     * @param classId
     *         classId
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * @return className
     *         className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className
     *         className
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    /**
     * @return createDate
     *         createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *         createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return createUser
     *         createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     *         createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return updateDate
     *         updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     *         updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return updateUser
     *         updateUser
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     *         updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * @return roomDesc
     *         roomDesc
     */
    public String getRoomDesc() {
        return roomDesc;
    }

    /**
     * @param roomDesc
     *         roomDesc
     */
    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc == null ? null : roomDesc.trim();
    }

    /**
     * @return disciplineId
     *         专业编号
     */
    public String getDisciplineId() {
        return disciplineId;
    }

    /**
     * @param disciplineId
     *         专业编号
     */
    public void setDisciplineId(String disciplineId) {
        this.disciplineId = disciplineId == null ? null : disciplineId.trim();
    }
}