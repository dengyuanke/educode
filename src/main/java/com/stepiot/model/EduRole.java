package com.stepiot.model;

import java.io.Serializable;

public class EduRole implements Serializable {
    /**
     * 角色名称
     */
    public static final String ROLE = "role";

    /**
     * 角色名称
     */
    private String role;

    /**
     * 角色描述
     */
    public static final String DESC = "desc";

    /**
     * 角色描述
     */
    private String desc;

    private static final long serialVersionUID = 1L;

    /**
     * @return role
     *         角色名称
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     *         角色名称
     */
    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    /**
     * @return desc
     *         角色描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc
     *         角色描述
     */
    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }
}