package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutlineAnswer implements Serializable {
    /**
     * answerId
     */
    public static final String ANSWERID = "answerId";

    /**
     * answerId
     */
    private Integer answerId;

    /**
     * outlineId
     */
    public static final String OUTLINEID = "outlineId";

    /**
     * outlineId
     */
    private Integer outlineId;

    /**
     * courseId
     */
    public static final String COURSEID = "courseId";

    /**
     * courseId
     */
    private Integer courseId;

    /**
     * level
     */
    public static final String LEVEL = "level";

    /**
     * level
     */
    private Integer level;

    /**
     * courseType
     */
    public static final String COURSETYPE = "courseType";

    /**
     * courseType
     */
    private String courseType;

    /**
     * 分值权重，总值为1024
     */
    public static final String WEIGHTS = "weights";

    /**
     * 分值权重，总值为1024
     */
    private Integer weights;

    /**
     * videoSrc
     */
    public static final String VIDEOSRC = "videoSrc";

    /**
     * videoSrc
     */
    private String videoSrc;

    /**
     * videoType
     */
    public static final String VIDEOTYPE = "videoType";

    /**
     * videoType
     */
    private String videoType;

    /**
     * coveragetc
     */
    public static final String COVERAGETC = "coveragetc";

    /**
     * coveragetc
     */
    private String coveragetc;

    /**
     * videoCoverage
     */
    public static final String VIDEOCOVERAGE = "videoCoverage";

    /**
     * videoCoverage
     */
    private String videoCoverage;

    /**
     * videoSrcAlt
     */
    public static final String VIDEOSRCALT = "videoSrcAlt";

    /**
     * videoSrcAlt
     */
    private String videoSrcAlt;

    private static final long serialVersionUID = 1L;

    /**
     * @return answerId
     *         answerId
     */
    public Integer getAnswerId() {
        return answerId;
    }

    /**
     * @param answerId
     *         answerId
     */
    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    /**
     * @return outlineId
     *         outlineId
     */
    public Integer getOutlineId() {
        return outlineId;
    }

    /**
     * @param outlineId
     *         outlineId
     */
    public void setOutlineId(Integer outlineId) {
        this.outlineId = outlineId;
    }

    /**
     * @return courseId
     *         courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         courseId
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return level
     *         level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level
     *         level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return courseType
     *         courseType
     */
    public String getCourseType() {
        return courseType;
    }

    /**
     * @param courseType
     *         courseType
     */
    public void setCourseType(String courseType) {
        this.courseType = courseType == null ? null : courseType.trim();
    }

    /**
     * @return weights
     *         分值权重，总值为1024
     */
    public Integer getWeights() {
        return weights;
    }

    /**
     * @param weights
     *         分值权重，总值为1024
     */
    public void setWeights(Integer weights) {
        this.weights = weights;
    }

    /**
     * @return videoSrc
     *         videoSrc
     */
    public String getVideoSrc() {
        return videoSrc;
    }

    /**
     * @param videoSrc
     *         videoSrc
     */
    public void setVideoSrc(String videoSrc) {
        this.videoSrc = videoSrc == null ? null : videoSrc.trim();
    }

    /**
     * @return videoType
     *         videoType
     */
    public String getVideoType() {
        return videoType;
    }

    /**
     * @param videoType
     *         videoType
     */
    public void setVideoType(String videoType) {
        this.videoType = videoType == null ? null : videoType.trim();
    }

    /**
     * @return coveragetc
     *         coveragetc
     */
    public String getCoveragetc() {
        return coveragetc;
    }

    /**
     * @param coveragetc
     *         coveragetc
     */
    public void setCoveragetc(String coveragetc) {
        this.coveragetc = coveragetc == null ? null : coveragetc.trim();
    }

    /**
     * @return videoCoverage
     *         videoCoverage
     */
    public String getVideoCoverage() {
        return videoCoverage;
    }

    /**
     * @param videoCoverage
     *         videoCoverage
     */
    public void setVideoCoverage(String videoCoverage) {
        this.videoCoverage = videoCoverage == null ? null : videoCoverage.trim();
    }

    /**
     * @return videoSrcAlt
     *         videoSrcAlt
     */
    public String getVideoSrcAlt() {
        return videoSrcAlt;
    }

    /**
     * @param videoSrcAlt
     *         videoSrcAlt
     */
    public void setVideoSrcAlt(String videoSrcAlt) {
        this.videoSrcAlt = videoSrcAlt == null ? null : videoSrcAlt.trim();
    }
}