package com.stepiot.model;

import java.io.Serializable;

public class EduDepartment implements Serializable {
    /**
     * 院系编号
     */
    public static final String DEPARTMENTID = "departmentId";

    /**
     * 院系编号
     */
    private String departmentId;

    /**
     * 院系名称
     */
    public static final String DEPARTMENTNAME = "departmentName";

    /**
     * 院系名称
     */
    private String departmentName;

    private static final long serialVersionUID = 1L;

    /**
     * @return departmentId
     *         院系编号
     */
    public String getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId
     *         院系编号
     */
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId == null ? null : departmentId.trim();
    }

    /**
     * @return departmentName
     *         院系名称
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName
     *         院系名称
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName == null ? null : departmentName.trim();
    }
}