package com.stepiot.model;

import java.io.Serializable;

public class EduTeacherCourseRef implements Serializable {
    /**
     * 关联ID
     */
    public static final String REFID = "refId";

    /**
     * 关联ID
     */
    private Integer refId;

    /**
     * 教师编号
     */
    public static final String TEACHERID = "teacherId";

    /**
     * 教师编号
     */
    private String teacherId;

    /**
     * 课程编号
     */
    public static final String COURSEID = "courseId";

    /**
     * 课程编号
     */
    private String courseId;

    private static final long serialVersionUID = 1L;

    /**
     * @return refId
     *         关联ID
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId
     *         关联ID
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return teacherId
     *         教师编号
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId
     *         教师编号
     */
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId == null ? null : teacherId.trim();
    }

    /**
     * @return courseId
     *         课程编号
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         课程编号
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId == null ? null : courseId.trim();
    }
}