package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class EduWikiCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduWikiCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andWikiIdIsNull() {
            addCriterion("wiki_id is null");
            return (Criteria) this;
        }

        public Criteria andWikiIdIsNotNull() {
            addCriterion("wiki_id is not null");
            return (Criteria) this;
        }

        public Criteria andWikiIdEqualTo(Integer value) {
            addCriterion("wiki_id =", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdNotEqualTo(Integer value) {
            addCriterion("wiki_id <>", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdGreaterThan(Integer value) {
            addCriterion("wiki_id >", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("wiki_id >=", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdLessThan(Integer value) {
            addCriterion("wiki_id <", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdLessThanOrEqualTo(Integer value) {
            addCriterion("wiki_id <=", value, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdIn(List<Integer> values) {
            addCriterion("wiki_id in", values, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdNotIn(List<Integer> values) {
            addCriterion("wiki_id not in", values, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdBetween(Integer value1, Integer value2) {
            addCriterion("wiki_id between", value1, value2, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiIdNotBetween(Integer value1, Integer value2) {
            addCriterion("wiki_id not between", value1, value2, "wikiId");
            return (Criteria) this;
        }

        public Criteria andWikiTitleIsNull() {
            addCriterion("wiki_title is null");
            return (Criteria) this;
        }

        public Criteria andWikiTitleIsNotNull() {
            addCriterion("wiki_title is not null");
            return (Criteria) this;
        }

        public Criteria andWikiTitleEqualTo(String value) {
            addCriterion("wiki_title =", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleNotEqualTo(String value) {
            addCriterion("wiki_title <>", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleGreaterThan(String value) {
            addCriterion("wiki_title >", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleGreaterThanOrEqualTo(String value) {
            addCriterion("wiki_title >=", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleLessThan(String value) {
            addCriterion("wiki_title <", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleLessThanOrEqualTo(String value) {
            addCriterion("wiki_title <=", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleLike(String value) {
            addCriterion("wiki_title like", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleNotLike(String value) {
            addCriterion("wiki_title not like", value, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleIn(List<String> values) {
            addCriterion("wiki_title in", values, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleNotIn(List<String> values) {
            addCriterion("wiki_title not in", values, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleBetween(String value1, String value2) {
            addCriterion("wiki_title between", value1, value2, "wikiTitle");
            return (Criteria) this;
        }

        public Criteria andWikiTitleNotBetween(String value1, String value2) {
            addCriterion("wiki_title not between", value1, value2, "wikiTitle");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}