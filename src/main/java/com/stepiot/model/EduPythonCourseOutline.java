package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutline implements Serializable {
    /**
     * outlineId
     */
    public static final String OUTLINEID = "outlineId";

    /**
     * outlineId
     */
    private Integer outlineId;

    /**
     * 大纲名称
     */
    public static final String OUTLINENAME = "outlineName";

    /**
     * 大纲名称
     */
    private String outlineName;

    /**
     * 关联到edu_python_course表的course_id
     */
    public static final String COURSEID = "courseId";

    /**
     * 关联到edu_python_course表的course_id
     */
    private Integer courseId;

    /**
     * /img/sys/2018/11/12/19/06/15/XxKV/1.png
     */
    public static final String COVERAGE = "coverage";

    /**
     * /img/sys/2018/11/12/19/06/15/XxKV/1.png
     */
    private String coverage;

    /**
     * codeType
     */
    public static final String CODETYPE = "codeType";

    /**
     * codeType
     */
    private String codeType;

    /**
     * 课程类型：
     * 1: 迷失太空
     * 2: 乌龟编程
     * 3: 硬件编程
     * 4: 代码编程
     */
    public static final String COURSETYPE = "courseType";

    /**
     * 课程类型：
     * 1: 迷失太空
     * 2: 乌龟编程
     * 3: 硬件编程
     * 4: 代码编程
     */
    private Integer courseType;

    /**
     * 关联该章节的起始关卡
     */
    public static final String LEVELFROM = "levelFrom";

    /**
     * 关联该章节的起始关卡
     */
    private Integer levelFrom;

    /**
     * 关联该章节的结束关卡
     */
    public static final String LEVELTO = "levelTo";

    /**
     * 关联该章节的结束关卡
     */
    private Integer levelTo;

    /**
     * levelTitles
     */
    public static final String LEVELTITLES = "levelTitles";

    /**
     * levelTitles
     */
    private String levelTitles;

    /**
     * 教程视频绝对路径
     */
    public static final String VIDEOSRC = "videoSrc";

    /**
     * 教程视频绝对路径
     */
    private String videoSrc;

    /**
     * 视频格式
     */
    public static final String VIDEOTYPE = "videoType";

    /**
     * 视频格式
     */
    private String videoType;

    /**
     * coveragetc
     */
    public static final String COVERAGETC = "coveragetc";

    /**
     * coveragetc
     */
    private String coveragetc;

    private static final long serialVersionUID = 1L;

    /**
     * @return outlineId
     *         outlineId
     */
    public Integer getOutlineId() {
        return outlineId;
    }

    /**
     * @param outlineId
     *         outlineId
     */
    public void setOutlineId(Integer outlineId) {
        this.outlineId = outlineId;
    }

    /**
     * @return outlineName
     *         大纲名称
     */
    public String getOutlineName() {
        return outlineName;
    }

    /**
     * @param outlineName
     *         大纲名称
     */
    public void setOutlineName(String outlineName) {
        this.outlineName = outlineName == null ? null : outlineName.trim();
    }

    /**
     * @return courseId
     *         关联到edu_python_course表的course_id
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         关联到edu_python_course表的course_id
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return coverage
     *         /img/sys/2018/11/12/19/06/15/XxKV/1.png
     */
    public String getCoverage() {
        return coverage;
    }

    /**
     * @param coverage
     *         /img/sys/2018/11/12/19/06/15/XxKV/1.png
     */
    public void setCoverage(String coverage) {
        this.coverage = coverage == null ? null : coverage.trim();
    }

    /**
     * @return codeType
     *         codeType
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     * @param codeType
     *         codeType
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType == null ? null : codeType.trim();
    }

    /**
     * @return courseType
     *         课程类型：
     *         1: 迷失太空
     *         2: 乌龟编程
     *         3: 硬件编程
     *         4: 代码编程
     */
    public Integer getCourseType() {
        return courseType;
    }

    /**
     * @param courseType
     *         课程类型：
     *         1: 迷失太空
     *         2: 乌龟编程
     *         3: 硬件编程
     *         4: 代码编程
     */
    public void setCourseType(Integer courseType) {
        this.courseType = courseType;
    }

    /**
     * @return levelFrom
     *         关联该章节的起始关卡
     */
    public Integer getLevelFrom() {
        return levelFrom;
    }

    /**
     * @param levelFrom
     *         关联该章节的起始关卡
     */
    public void setLevelFrom(Integer levelFrom) {
        this.levelFrom = levelFrom;
    }

    /**
     * @return levelTo
     *         关联该章节的结束关卡
     */
    public Integer getLevelTo() {
        return levelTo;
    }

    /**
     * @param levelTo
     *         关联该章节的结束关卡
     */
    public void setLevelTo(Integer levelTo) {
        this.levelTo = levelTo;
    }

    /**
     * @return levelTitles
     *         levelTitles
     */
    public String getLevelTitles() {
        return levelTitles;
    }

    /**
     * @param levelTitles
     *         levelTitles
     */
    public void setLevelTitles(String levelTitles) {
        this.levelTitles = levelTitles == null ? null : levelTitles.trim();
    }

    /**
     * @return videoSrc
     *         教程视频绝对路径
     */
    public String getVideoSrc() {
        return videoSrc;
    }

    /**
     * @param videoSrc
     *         教程视频绝对路径
     */
    public void setVideoSrc(String videoSrc) {
        this.videoSrc = videoSrc == null ? null : videoSrc.trim();
    }

    /**
     * @return videoType
     *         视频格式
     */
    public String getVideoType() {
        return videoType;
    }

    /**
     * @param videoType
     *         视频格式
     */
    public void setVideoType(String videoType) {
        this.videoType = videoType == null ? null : videoType.trim();
    }

    /**
     * @return coveragetc
     *         coveragetc
     */
    public String getCoveragetc() {
        return coveragetc;
    }

    /**
     * @param coveragetc
     *         coveragetc
     */
    public void setCoveragetc(String coveragetc) {
        this.coveragetc = coveragetc == null ? null : coveragetc.trim();
    }
}