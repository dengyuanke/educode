package com.stepiot.model;

import java.io.Serializable;

public class EduPubDown implements Serializable {
    /**
     * id
     */
    public static final String ID = "id";

    /**
     * id
     */
    private Integer id;

    /**
     * filename
     */
    public static final String FILENAME = "filename";

    /**
     * filename
     */
    private String filename;

    /**
     * s3Path
     */
    public static final String S3PATH = "s3Path";

    /**
     * s3Path
     */
    private String s3Path;

    /**
     * filesize
     */
    public static final String FILESIZE = "filesize";

    /**
     * filesize
     */
    private Integer filesize;

    /**
     * version
     */
    public static final String VERSION = "version";

    /**
     * version
     */
    private String version;

    /**
     * description
     */
    public static final String DESCRIPTION = "description";

    /**
     * description
     */
    private String description;

    /**
     * os
     */
    public static final String OS = "os";

    /**
     * os
     */
    private String os;

    /**
     * osbit
     */
    public static final String OSBIT = "osbit";

    /**
     * osbit
     */
    private String osbit;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     *         id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *         id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return filename
     *         filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename
     *         filename
     */
    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    /**
     * @return s3Path
     *         s3Path
     */
    public String getS3Path() {
        return s3Path;
    }

    /**
     * @param s3Path
     *         s3Path
     */
    public void setS3Path(String s3Path) {
        this.s3Path = s3Path == null ? null : s3Path.trim();
    }

    /**
     * @return filesize
     *         filesize
     */
    public Integer getFilesize() {
        return filesize;
    }

    /**
     * @param filesize
     *         filesize
     */
    public void setFilesize(Integer filesize) {
        this.filesize = filesize;
    }

    /**
     * @return version
     *         version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *         version
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * @return description
     *         description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *         description
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * @return os
     *         os
     */
    public String getOs() {
        return os;
    }

    /**
     * @param os
     *         os
     */
    public void setOs(String os) {
        this.os = os == null ? null : os.trim();
    }

    /**
     * @return osbit
     *         osbit
     */
    public String getOsbit() {
        return osbit;
    }

    /**
     * @param osbit
     *         osbit
     */
    public void setOsbit(String osbit) {
        this.osbit = osbit == null ? null : osbit.trim();
    }
}