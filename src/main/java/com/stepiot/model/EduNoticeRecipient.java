package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduNoticeRecipient implements Serializable {
    /**
     * recipientId
     */
    public static final String RECIPIENTID = "recipientId";

    /**
     * recipientId
     */
    private Integer recipientId;

    /**
     * noticeId
     */
    public static final String NOTICEID = "noticeId";

    /**
     * noticeId
     */
    private Integer noticeId;

    /**
     * recipientUser
     */
    public static final String RECIPIENTUSER = "recipientUser";

    /**
     * recipientUser
     */
    private String recipientUser;

    /**
     * recipientStatus
     */
    public static final String RECIPIENTSTATUS = "recipientStatus";

    /**
     * recipientStatus
     */
    private Integer recipientStatus;

    /**
     * recipientTime
     */
    public static final String RECIPIENTTIME = "recipientTime";

    /**
     * recipientTime
     */
    private Date recipientTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return recipientId
     *         recipientId
     */
    public Integer getRecipientId() {
        return recipientId;
    }

    /**
     * @param recipientId
     *         recipientId
     */
    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    /**
     * @return noticeId
     *         noticeId
     */
    public Integer getNoticeId() {
        return noticeId;
    }

    /**
     * @param noticeId
     *         noticeId
     */
    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    /**
     * @return recipientUser
     *         recipientUser
     */
    public String getRecipientUser() {
        return recipientUser;
    }

    /**
     * @param recipientUser
     *         recipientUser
     */
    public void setRecipientUser(String recipientUser) {
        this.recipientUser = recipientUser == null ? null : recipientUser.trim();
    }

    /**
     * @return recipientStatus
     *         recipientStatus
     */
    public Integer getRecipientStatus() {
        return recipientStatus;
    }

    /**
     * @param recipientStatus
     *         recipientStatus
     */
    public void setRecipientStatus(Integer recipientStatus) {
        this.recipientStatus = recipientStatus;
    }

    /**
     * @return recipientTime
     *         recipientTime
     */
    public Date getRecipientTime() {
        return recipientTime;
    }

    /**
     * @param recipientTime
     *         recipientTime
     */
    public void setRecipientTime(Date recipientTime) {
        this.recipientTime = recipientTime;
    }
}