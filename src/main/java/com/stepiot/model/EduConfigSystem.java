package com.stepiot.model;

import java.io.Serializable;

public class EduConfigSystem implements Serializable {
    /**
     * 系统默认配置项目
     */
    public static final String CONFIGITEM = "configItem";

    /**
     * 系统默认配置项目
     */
    private String configItem;

    /**
     * 系统默认配置值
     */
    public static final String CONFIGVALUE = "configValue";

    /**
     * 系统默认配置值
     */
    private String configValue;

    private static final long serialVersionUID = 1L;

    /**
     * @return configItem
     *         系统默认配置项目
     */
    public String getConfigItem() {
        return configItem;
    }

    /**
     * @param configItem
     *         系统默认配置项目
     */
    public void setConfigItem(String configItem) {
        this.configItem = configItem == null ? null : configItem.trim();
    }

    /**
     * @return configValue
     *         系统默认配置值
     */
    public String getConfigValue() {
        return configValue;
    }

    /**
     * @param configValue
     *         系统默认配置值
     */
    public void setConfigValue(String configValue) {
        this.configValue = configValue == null ? null : configValue.trim();
    }
}