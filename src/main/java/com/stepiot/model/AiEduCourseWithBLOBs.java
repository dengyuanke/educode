package com.stepiot.model;

import java.io.Serializable;

public class AiEduCourseWithBLOBs extends AiEduCourse implements Serializable {
    /**
     * 课程图片
     */
    public static final String COURSEIMAGE = "courseImage";

    /**
     * 课程图片
     */
    private String courseImage;

    /**
     * Blockly工具箱
     */
    public static final String BLOCKLYTOOLBOX = "blocklyToolbox";

    /**
     * Blockly工具箱
     */
    private String blocklyToolbox;

    /**
     * Blockly选项
     */
    public static final String BLOCKLYOPTIONS = "blocklyOptions";

    /**
     * Blockly选项
     */
    private String blocklyOptions;

    /**
     * 初始化Blockly内容
     */
    public static final String BLOCKLYXML = "blocklyXml";

    /**
     * 初始化Blockly内容
     */
    private String blocklyXml;

    private static final long serialVersionUID = 1L;

    /**
     * @return courseImage
     *         课程图片
     */
    public String getCourseImage() {
        return courseImage;
    }

    /**
     * @param courseImage
     *         课程图片
     */
    public void setCourseImage(String courseImage) {
        this.courseImage = courseImage == null ? null : courseImage.trim();
    }

    /**
     * @return blocklyToolbox
     *         Blockly工具箱
     */
    public String getBlocklyToolbox() {
        return blocklyToolbox;
    }

    /**
     * @param blocklyToolbox
     *         Blockly工具箱
     */
    public void setBlocklyToolbox(String blocklyToolbox) {
        this.blocklyToolbox = blocklyToolbox == null ? null : blocklyToolbox.trim();
    }

    /**
     * @return blocklyOptions
     *         Blockly选项
     */
    public String getBlocklyOptions() {
        return blocklyOptions;
    }

    /**
     * @param blocklyOptions
     *         Blockly选项
     */
    public void setBlocklyOptions(String blocklyOptions) {
        this.blocklyOptions = blocklyOptions == null ? null : blocklyOptions.trim();
    }

    /**
     * @return blocklyXml
     *         初始化Blockly内容
     */
    public String getBlocklyXml() {
        return blocklyXml;
    }

    /**
     * @param blocklyXml
     *         初始化Blockly内容
     */
    public void setBlocklyXml(String blocklyXml) {
        this.blocklyXml = blocklyXml == null ? null : blocklyXml.trim();
    }
}