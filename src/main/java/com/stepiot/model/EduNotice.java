package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduNotice implements Serializable {
    /**
     * noticeId
     */
    public static final String NOTICEID = "noticeId";

    /**
     * noticeId
     */
    private Integer noticeId;

    /**
     * noticeSubject
     */
    public static final String NOTICESUBJECT = "noticeSubject";

    /**
     * noticeSubject
     */
    private String noticeSubject;

    /**
     * createTime
     */
    public static final String CREATETIME = "createTime";

    /**
     * createTime
     */
    private Date createTime;

    /**
     * createUser
     */
    public static final String CREATEUSER = "createUser";

    /**
     * createUser
     */
    private String createUser;

    /**
     * uuid
     */
    public static final String UUID = "uuid";

    /**
     * uuid
     */
    private String uuid;

    private static final long serialVersionUID = 1L;

    /**
     * @return noticeId
     *         noticeId
     */
    public Integer getNoticeId() {
        return noticeId;
    }

    /**
     * @param noticeId
     *         noticeId
     */
    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    /**
     * @return noticeSubject
     *         noticeSubject
     */
    public String getNoticeSubject() {
        return noticeSubject;
    }

    /**
     * @param noticeSubject
     *         noticeSubject
     */
    public void setNoticeSubject(String noticeSubject) {
        this.noticeSubject = noticeSubject == null ? null : noticeSubject.trim();
    }

    /**
     * @return createTime
     *         createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     *         createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createUser
     *         createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     *         createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return uuid
     *         uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     *         uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }
}