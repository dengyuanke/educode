package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduUser implements Serializable {
    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    /**
     * email
     */
    public static final String EMAIL = "email";

    /**
     * email
     */
    private String email;

    /**
     * telephone
     */
    public static final String TELEPHONE = "telephone";

    /**
     * telephone
     */
    private String telephone;

    /**
     * 注册日期
     */
    public static final String REGDATE = "regDate";

    /**
     * 注册日期
     */
    private Date regDate;

    /**
     * 是否启用 1启用 0 禁用
     */
    public static final String ENABLED = "enabled";

    /**
     * 是否启用 1启用 0 禁用
     */
    private Byte enabled;

    /**
     * password
     */
    public static final String PASSWORD = "password";

    /**
     * password
     */
    private String password;

    /**
     * 0:超级用户
     * 1:普通用户
     */
    public static final String SUPERUSER = "superuser";

    /**
     * 0:超级用户
     * 1:普通用户
     */
    private Integer superuser;

    /**
     * avatar
     */
    public static final String AVATAR = "avatar";

    /**
     * avatar
     */
    private String avatar;

    /**
     * name
     */
    public static final String NAME = "name";

    /**
     * name
     */
    private String name;

    /**
     * age
     */
    public static final String AGE = "age";

    /**
     * age
     */
    private Integer age;

    /**
     * gender
     */
    public static final String GENDER = "gender";

    /**
     * gender
     */
    private Integer gender;

    /**
     * 密码最后更新时间
     */
    public static final String PASSWORDLUT = "passwordLut";

    /**
     * 密码最后更新时间
     */
    private Date passwordLut;

    private static final long serialVersionUID = 1L;

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return email
     *         email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *         email
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * @return telephone
     *         telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone
     *         telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    /**
     * @return regDate
     *         注册日期
     */
    public Date getRegDate() {
        return regDate;
    }

    /**
     * @param regDate
     *         注册日期
     */
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    /**
     * @return enabled
     *         是否启用 1启用 0 禁用
     */
    public Byte getEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *         是否启用 1启用 0 禁用
     */
    public void setEnabled(Byte enabled) {
        this.enabled = enabled;
    }

    /**
     * @return password
     *         password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *         password
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * @return superuser
     *         0:超级用户
     *         1:普通用户
     */
    public Integer getSuperuser() {
        return superuser;
    }

    /**
     * @param superuser
     *         0:超级用户
     *         1:普通用户
     */
    public void setSuperuser(Integer superuser) {
        this.superuser = superuser;
    }

    /**
     * @return avatar
     *         avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar
     *         avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    /**
     * @return name
     *         name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *         name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return age
     *         age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     *         age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return gender
     *         gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * @param gender
     *         gender
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * @return passwordLut
     *         密码最后更新时间
     */
    public Date getPasswordLut() {
        return passwordLut;
    }

    /**
     * @param passwordLut
     *         密码最后更新时间
     */
    public void setPasswordLut(Date passwordLut) {
        this.passwordLut = passwordLut;
    }
}