package com.stepiot.model;

import java.io.Serializable;

public class EduUserTortoiseWithBLOBs extends EduUserTortoise implements Serializable {
    /**
     * blocklyXml
     */
    public static final String BLOCKLYXML = "blocklyXml";

    /**
     * blocklyXml
     */
    private String blocklyXml;

    /**
     * blocklyCode
     */
    public static final String BLOCKLYCODE = "blocklyCode";

    /**
     * blocklyCode
     */
    private String blocklyCode;

    private static final long serialVersionUID = 1L;

    /**
     * @return blocklyXml
     *         blocklyXml
     */
    public String getBlocklyXml() {
        return blocklyXml;
    }

    /**
     * @param blocklyXml
     *         blocklyXml
     */
    public void setBlocklyXml(String blocklyXml) {
        this.blocklyXml = blocklyXml == null ? null : blocklyXml.trim();
    }

    /**
     * @return blocklyCode
     *         blocklyCode
     */
    public String getBlocklyCode() {
        return blocklyCode;
    }

    /**
     * @param blocklyCode
     *         blocklyCode
     */
    public void setBlocklyCode(String blocklyCode) {
        this.blocklyCode = blocklyCode == null ? null : blocklyCode.trim();
    }
}