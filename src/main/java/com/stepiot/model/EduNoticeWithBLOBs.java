package com.stepiot.model;

import java.io.Serializable;

public class EduNoticeWithBLOBs extends EduNotice implements Serializable {
    /**
     * noticeBody
     */
    public static final String NOTICEBODY = "noticeBody";

    /**
     * noticeBody
     */
    private String noticeBody;

    /**
     * toClasses
     */
    public static final String TOCLASSES = "toClasses";

    /**
     * toClasses
     */
    private String toClasses;

    private static final long serialVersionUID = 1L;

    /**
     * @return noticeBody
     *         noticeBody
     */
    public String getNoticeBody() {
        return noticeBody;
    }

    /**
     * @param noticeBody
     *         noticeBody
     */
    public void setNoticeBody(String noticeBody) {
        this.noticeBody = noticeBody == null ? null : noticeBody.trim();
    }

    /**
     * @return toClasses
     *         toClasses
     */
    public String getToClasses() {
        return toClasses;
    }

    /**
     * @param toClasses
     *         toClasses
     */
    public void setToClasses(String toClasses) {
        this.toClasses = toClasses == null ? null : toClasses.trim();
    }
}