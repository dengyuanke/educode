package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class EduPythonCourseOutlineCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduPythonCourseOutlineCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOutlineIdIsNull() {
            addCriterion("outline_id is null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIsNotNull() {
            addCriterion("outline_id is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdEqualTo(Integer value) {
            addCriterion("outline_id =", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotEqualTo(Integer value) {
            addCriterion("outline_id <>", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThan(Integer value) {
            addCriterion("outline_id >", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("outline_id >=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThan(Integer value) {
            addCriterion("outline_id <", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThanOrEqualTo(Integer value) {
            addCriterion("outline_id <=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIn(List<Integer> values) {
            addCriterion("outline_id in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotIn(List<Integer> values) {
            addCriterion("outline_id not in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdBetween(Integer value1, Integer value2) {
            addCriterion("outline_id between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("outline_id not between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIsNull() {
            addCriterion("outline_name is null");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIsNotNull() {
            addCriterion("outline_name is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineNameEqualTo(String value) {
            addCriterion("outline_name =", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotEqualTo(String value) {
            addCriterion("outline_name <>", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameGreaterThan(String value) {
            addCriterion("outline_name >", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameGreaterThanOrEqualTo(String value) {
            addCriterion("outline_name >=", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLessThan(String value) {
            addCriterion("outline_name <", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLessThanOrEqualTo(String value) {
            addCriterion("outline_name <=", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameLike(String value) {
            addCriterion("outline_name like", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotLike(String value) {
            addCriterion("outline_name not like", value, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameIn(List<String> values) {
            addCriterion("outline_name in", values, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotIn(List<String> values) {
            addCriterion("outline_name not in", values, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameBetween(String value1, String value2) {
            addCriterion("outline_name between", value1, value2, "outlineName");
            return (Criteria) this;
        }

        public Criteria andOutlineNameNotBetween(String value1, String value2) {
            addCriterion("outline_name not between", value1, value2, "outlineName");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Integer value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Integer value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Integer value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Integer value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Integer value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Integer> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Integer> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Integer value1, Integer value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCoverageIsNull() {
            addCriterion("coverage is null");
            return (Criteria) this;
        }

        public Criteria andCoverageIsNotNull() {
            addCriterion("coverage is not null");
            return (Criteria) this;
        }

        public Criteria andCoverageEqualTo(String value) {
            addCriterion("coverage =", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageNotEqualTo(String value) {
            addCriterion("coverage <>", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageGreaterThan(String value) {
            addCriterion("coverage >", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageGreaterThanOrEqualTo(String value) {
            addCriterion("coverage >=", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageLessThan(String value) {
            addCriterion("coverage <", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageLessThanOrEqualTo(String value) {
            addCriterion("coverage <=", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageLike(String value) {
            addCriterion("coverage like", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageNotLike(String value) {
            addCriterion("coverage not like", value, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageIn(List<String> values) {
            addCriterion("coverage in", values, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageNotIn(List<String> values) {
            addCriterion("coverage not in", values, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageBetween(String value1, String value2) {
            addCriterion("coverage between", value1, value2, "coverage");
            return (Criteria) this;
        }

        public Criteria andCoverageNotBetween(String value1, String value2) {
            addCriterion("coverage not between", value1, value2, "coverage");
            return (Criteria) this;
        }

        public Criteria andCodeTypeIsNull() {
            addCriterion("code_type is null");
            return (Criteria) this;
        }

        public Criteria andCodeTypeIsNotNull() {
            addCriterion("code_type is not null");
            return (Criteria) this;
        }

        public Criteria andCodeTypeEqualTo(String value) {
            addCriterion("code_type =", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeNotEqualTo(String value) {
            addCriterion("code_type <>", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeGreaterThan(String value) {
            addCriterion("code_type >", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("code_type >=", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeLessThan(String value) {
            addCriterion("code_type <", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeLessThanOrEqualTo(String value) {
            addCriterion("code_type <=", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeLike(String value) {
            addCriterion("code_type like", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeNotLike(String value) {
            addCriterion("code_type not like", value, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeIn(List<String> values) {
            addCriterion("code_type in", values, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeNotIn(List<String> values) {
            addCriterion("code_type not in", values, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeBetween(String value1, String value2) {
            addCriterion("code_type between", value1, value2, "codeType");
            return (Criteria) this;
        }

        public Criteria andCodeTypeNotBetween(String value1, String value2) {
            addCriterion("code_type not between", value1, value2, "codeType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNull() {
            addCriterion("course_type is null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNotNull() {
            addCriterion("course_type is not null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeEqualTo(Integer value) {
            addCriterion("course_type =", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotEqualTo(Integer value) {
            addCriterion("course_type <>", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThan(Integer value) {
            addCriterion("course_type >", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_type >=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThan(Integer value) {
            addCriterion("course_type <", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThanOrEqualTo(Integer value) {
            addCriterion("course_type <=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIn(List<Integer> values) {
            addCriterion("course_type in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotIn(List<Integer> values) {
            addCriterion("course_type not in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeBetween(Integer value1, Integer value2) {
            addCriterion("course_type between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("course_type not between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andLevelFromIsNull() {
            addCriterion("level_from is null");
            return (Criteria) this;
        }

        public Criteria andLevelFromIsNotNull() {
            addCriterion("level_from is not null");
            return (Criteria) this;
        }

        public Criteria andLevelFromEqualTo(Integer value) {
            addCriterion("level_from =", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromNotEqualTo(Integer value) {
            addCriterion("level_from <>", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromGreaterThan(Integer value) {
            addCriterion("level_from >", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromGreaterThanOrEqualTo(Integer value) {
            addCriterion("level_from >=", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromLessThan(Integer value) {
            addCriterion("level_from <", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromLessThanOrEqualTo(Integer value) {
            addCriterion("level_from <=", value, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromIn(List<Integer> values) {
            addCriterion("level_from in", values, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromNotIn(List<Integer> values) {
            addCriterion("level_from not in", values, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromBetween(Integer value1, Integer value2) {
            addCriterion("level_from between", value1, value2, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelFromNotBetween(Integer value1, Integer value2) {
            addCriterion("level_from not between", value1, value2, "levelFrom");
            return (Criteria) this;
        }

        public Criteria andLevelToIsNull() {
            addCriterion("level_to is null");
            return (Criteria) this;
        }

        public Criteria andLevelToIsNotNull() {
            addCriterion("level_to is not null");
            return (Criteria) this;
        }

        public Criteria andLevelToEqualTo(Integer value) {
            addCriterion("level_to =", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToNotEqualTo(Integer value) {
            addCriterion("level_to <>", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToGreaterThan(Integer value) {
            addCriterion("level_to >", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToGreaterThanOrEqualTo(Integer value) {
            addCriterion("level_to >=", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToLessThan(Integer value) {
            addCriterion("level_to <", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToLessThanOrEqualTo(Integer value) {
            addCriterion("level_to <=", value, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToIn(List<Integer> values) {
            addCriterion("level_to in", values, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToNotIn(List<Integer> values) {
            addCriterion("level_to not in", values, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToBetween(Integer value1, Integer value2) {
            addCriterion("level_to between", value1, value2, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelToNotBetween(Integer value1, Integer value2) {
            addCriterion("level_to not between", value1, value2, "levelTo");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesIsNull() {
            addCriterion("level_titles is null");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesIsNotNull() {
            addCriterion("level_titles is not null");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesEqualTo(String value) {
            addCriterion("level_titles =", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesNotEqualTo(String value) {
            addCriterion("level_titles <>", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesGreaterThan(String value) {
            addCriterion("level_titles >", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesGreaterThanOrEqualTo(String value) {
            addCriterion("level_titles >=", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesLessThan(String value) {
            addCriterion("level_titles <", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesLessThanOrEqualTo(String value) {
            addCriterion("level_titles <=", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesLike(String value) {
            addCriterion("level_titles like", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesNotLike(String value) {
            addCriterion("level_titles not like", value, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesIn(List<String> values) {
            addCriterion("level_titles in", values, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesNotIn(List<String> values) {
            addCriterion("level_titles not in", values, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesBetween(String value1, String value2) {
            addCriterion("level_titles between", value1, value2, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andLevelTitlesNotBetween(String value1, String value2) {
            addCriterion("level_titles not between", value1, value2, "levelTitles");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIsNull() {
            addCriterion("video_src is null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIsNotNull() {
            addCriterion("video_src is not null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcEqualTo(String value) {
            addCriterion("video_src =", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotEqualTo(String value) {
            addCriterion("video_src <>", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcGreaterThan(String value) {
            addCriterion("video_src >", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcGreaterThanOrEqualTo(String value) {
            addCriterion("video_src >=", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLessThan(String value) {
            addCriterion("video_src <", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLessThanOrEqualTo(String value) {
            addCriterion("video_src <=", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLike(String value) {
            addCriterion("video_src like", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotLike(String value) {
            addCriterion("video_src not like", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIn(List<String> values) {
            addCriterion("video_src in", values, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotIn(List<String> values) {
            addCriterion("video_src not in", values, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcBetween(String value1, String value2) {
            addCriterion("video_src between", value1, value2, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotBetween(String value1, String value2) {
            addCriterion("video_src not between", value1, value2, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNull() {
            addCriterion("video_type is null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNotNull() {
            addCriterion("video_type is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeEqualTo(String value) {
            addCriterion("video_type =", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotEqualTo(String value) {
            addCriterion("video_type <>", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThan(String value) {
            addCriterion("video_type >", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("video_type >=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThan(String value) {
            addCriterion("video_type <", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThanOrEqualTo(String value) {
            addCriterion("video_type <=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLike(String value) {
            addCriterion("video_type like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotLike(String value) {
            addCriterion("video_type not like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIn(List<String> values) {
            addCriterion("video_type in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotIn(List<String> values) {
            addCriterion("video_type not in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeBetween(String value1, String value2) {
            addCriterion("video_type between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotBetween(String value1, String value2) {
            addCriterion("video_type not between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIsNull() {
            addCriterion("coveragetc is null");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIsNotNull() {
            addCriterion("coveragetc is not null");
            return (Criteria) this;
        }

        public Criteria andCoveragetcEqualTo(String value) {
            addCriterion("coveragetc =", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotEqualTo(String value) {
            addCriterion("coveragetc <>", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcGreaterThan(String value) {
            addCriterion("coveragetc >", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcGreaterThanOrEqualTo(String value) {
            addCriterion("coveragetc >=", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLessThan(String value) {
            addCriterion("coveragetc <", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLessThanOrEqualTo(String value) {
            addCriterion("coveragetc <=", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLike(String value) {
            addCriterion("coveragetc like", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotLike(String value) {
            addCriterion("coveragetc not like", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIn(List<String> values) {
            addCriterion("coveragetc in", values, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotIn(List<String> values) {
            addCriterion("coveragetc not in", values, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcBetween(String value1, String value2) {
            addCriterion("coveragetc between", value1, value2, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotBetween(String value1, String value2) {
            addCriterion("coveragetc not between", value1, value2, "coveragetc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}