package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class EduPythonCourseOutlineAnswerCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduPythonCourseOutlineAnswerCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAnswerIdIsNull() {
            addCriterion("answer_id is null");
            return (Criteria) this;
        }

        public Criteria andAnswerIdIsNotNull() {
            addCriterion("answer_id is not null");
            return (Criteria) this;
        }

        public Criteria andAnswerIdEqualTo(Integer value) {
            addCriterion("answer_id =", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdNotEqualTo(Integer value) {
            addCriterion("answer_id <>", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdGreaterThan(Integer value) {
            addCriterion("answer_id >", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("answer_id >=", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdLessThan(Integer value) {
            addCriterion("answer_id <", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdLessThanOrEqualTo(Integer value) {
            addCriterion("answer_id <=", value, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdIn(List<Integer> values) {
            addCriterion("answer_id in", values, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdNotIn(List<Integer> values) {
            addCriterion("answer_id not in", values, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdBetween(Integer value1, Integer value2) {
            addCriterion("answer_id between", value1, value2, "answerId");
            return (Criteria) this;
        }

        public Criteria andAnswerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("answer_id not between", value1, value2, "answerId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIsNull() {
            addCriterion("outline_id is null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIsNotNull() {
            addCriterion("outline_id is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdEqualTo(Integer value) {
            addCriterion("outline_id =", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotEqualTo(Integer value) {
            addCriterion("outline_id <>", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThan(Integer value) {
            addCriterion("outline_id >", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("outline_id >=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThan(Integer value) {
            addCriterion("outline_id <", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThanOrEqualTo(Integer value) {
            addCriterion("outline_id <=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIn(List<Integer> values) {
            addCriterion("outline_id in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotIn(List<Integer> values) {
            addCriterion("outline_id not in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdBetween(Integer value1, Integer value2) {
            addCriterion("outline_id between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("outline_id not between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Integer value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Integer value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Integer value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Integer value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Integer value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Integer> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Integer> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Integer value1, Integer value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Integer value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Integer value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Integer value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Integer value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Integer value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Integer> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Integer> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Integer value1, Integer value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNull() {
            addCriterion("course_type is null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNotNull() {
            addCriterion("course_type is not null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeEqualTo(String value) {
            addCriterion("course_type =", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotEqualTo(String value) {
            addCriterion("course_type <>", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThan(String value) {
            addCriterion("course_type >", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThanOrEqualTo(String value) {
            addCriterion("course_type >=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThan(String value) {
            addCriterion("course_type <", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThanOrEqualTo(String value) {
            addCriterion("course_type <=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLike(String value) {
            addCriterion("course_type like", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotLike(String value) {
            addCriterion("course_type not like", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIn(List<String> values) {
            addCriterion("course_type in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotIn(List<String> values) {
            addCriterion("course_type not in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeBetween(String value1, String value2) {
            addCriterion("course_type between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotBetween(String value1, String value2) {
            addCriterion("course_type not between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andWeightsIsNull() {
            addCriterion("weights is null");
            return (Criteria) this;
        }

        public Criteria andWeightsIsNotNull() {
            addCriterion("weights is not null");
            return (Criteria) this;
        }

        public Criteria andWeightsEqualTo(Integer value) {
            addCriterion("weights =", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsNotEqualTo(Integer value) {
            addCriterion("weights <>", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsGreaterThan(Integer value) {
            addCriterion("weights >", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsGreaterThanOrEqualTo(Integer value) {
            addCriterion("weights >=", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsLessThan(Integer value) {
            addCriterion("weights <", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsLessThanOrEqualTo(Integer value) {
            addCriterion("weights <=", value, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsIn(List<Integer> values) {
            addCriterion("weights in", values, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsNotIn(List<Integer> values) {
            addCriterion("weights not in", values, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsBetween(Integer value1, Integer value2) {
            addCriterion("weights between", value1, value2, "weights");
            return (Criteria) this;
        }

        public Criteria andWeightsNotBetween(Integer value1, Integer value2) {
            addCriterion("weights not between", value1, value2, "weights");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIsNull() {
            addCriterion("video_src is null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIsNotNull() {
            addCriterion("video_src is not null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcEqualTo(String value) {
            addCriterion("video_src =", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotEqualTo(String value) {
            addCriterion("video_src <>", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcGreaterThan(String value) {
            addCriterion("video_src >", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcGreaterThanOrEqualTo(String value) {
            addCriterion("video_src >=", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLessThan(String value) {
            addCriterion("video_src <", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLessThanOrEqualTo(String value) {
            addCriterion("video_src <=", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcLike(String value) {
            addCriterion("video_src like", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotLike(String value) {
            addCriterion("video_src not like", value, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcIn(List<String> values) {
            addCriterion("video_src in", values, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotIn(List<String> values) {
            addCriterion("video_src not in", values, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcBetween(String value1, String value2) {
            addCriterion("video_src between", value1, value2, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoSrcNotBetween(String value1, String value2) {
            addCriterion("video_src not between", value1, value2, "videoSrc");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNull() {
            addCriterion("video_type is null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNotNull() {
            addCriterion("video_type is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeEqualTo(String value) {
            addCriterion("video_type =", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotEqualTo(String value) {
            addCriterion("video_type <>", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThan(String value) {
            addCriterion("video_type >", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("video_type >=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThan(String value) {
            addCriterion("video_type <", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThanOrEqualTo(String value) {
            addCriterion("video_type <=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLike(String value) {
            addCriterion("video_type like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotLike(String value) {
            addCriterion("video_type not like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIn(List<String> values) {
            addCriterion("video_type in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotIn(List<String> values) {
            addCriterion("video_type not in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeBetween(String value1, String value2) {
            addCriterion("video_type between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotBetween(String value1, String value2) {
            addCriterion("video_type not between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIsNull() {
            addCriterion("coveragetc is null");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIsNotNull() {
            addCriterion("coveragetc is not null");
            return (Criteria) this;
        }

        public Criteria andCoveragetcEqualTo(String value) {
            addCriterion("coveragetc =", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotEqualTo(String value) {
            addCriterion("coveragetc <>", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcGreaterThan(String value) {
            addCriterion("coveragetc >", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcGreaterThanOrEqualTo(String value) {
            addCriterion("coveragetc >=", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLessThan(String value) {
            addCriterion("coveragetc <", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLessThanOrEqualTo(String value) {
            addCriterion("coveragetc <=", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcLike(String value) {
            addCriterion("coveragetc like", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotLike(String value) {
            addCriterion("coveragetc not like", value, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcIn(List<String> values) {
            addCriterion("coveragetc in", values, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotIn(List<String> values) {
            addCriterion("coveragetc not in", values, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcBetween(String value1, String value2) {
            addCriterion("coveragetc between", value1, value2, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andCoveragetcNotBetween(String value1, String value2) {
            addCriterion("coveragetc not between", value1, value2, "coveragetc");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageIsNull() {
            addCriterion("video_coverage is null");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageIsNotNull() {
            addCriterion("video_coverage is not null");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageEqualTo(String value) {
            addCriterion("video_coverage =", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageNotEqualTo(String value) {
            addCriterion("video_coverage <>", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageGreaterThan(String value) {
            addCriterion("video_coverage >", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageGreaterThanOrEqualTo(String value) {
            addCriterion("video_coverage >=", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageLessThan(String value) {
            addCriterion("video_coverage <", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageLessThanOrEqualTo(String value) {
            addCriterion("video_coverage <=", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageLike(String value) {
            addCriterion("video_coverage like", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageNotLike(String value) {
            addCriterion("video_coverage not like", value, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageIn(List<String> values) {
            addCriterion("video_coverage in", values, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageNotIn(List<String> values) {
            addCriterion("video_coverage not in", values, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageBetween(String value1, String value2) {
            addCriterion("video_coverage between", value1, value2, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoCoverageNotBetween(String value1, String value2) {
            addCriterion("video_coverage not between", value1, value2, "videoCoverage");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltIsNull() {
            addCriterion("video_src_alt is null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltIsNotNull() {
            addCriterion("video_src_alt is not null");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltEqualTo(String value) {
            addCriterion("video_src_alt =", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltNotEqualTo(String value) {
            addCriterion("video_src_alt <>", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltGreaterThan(String value) {
            addCriterion("video_src_alt >", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltGreaterThanOrEqualTo(String value) {
            addCriterion("video_src_alt >=", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltLessThan(String value) {
            addCriterion("video_src_alt <", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltLessThanOrEqualTo(String value) {
            addCriterion("video_src_alt <=", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltLike(String value) {
            addCriterion("video_src_alt like", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltNotLike(String value) {
            addCriterion("video_src_alt not like", value, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltIn(List<String> values) {
            addCriterion("video_src_alt in", values, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltNotIn(List<String> values) {
            addCriterion("video_src_alt not in", values, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltBetween(String value1, String value2) {
            addCriterion("video_src_alt between", value1, value2, "videoSrcAlt");
            return (Criteria) this;
        }

        public Criteria andVideoSrcAltNotBetween(String value1, String value2) {
            addCriterion("video_src_alt not between", value1, value2, "videoSrcAlt");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}