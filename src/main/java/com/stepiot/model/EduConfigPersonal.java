package com.stepiot.model;

import java.io.Serializable;

public class EduConfigPersonal implements Serializable {
    /**
     * configId
     */
    public static final String CONFIGID = "configId";

    /**
     * configId
     */
    private Integer configId;

    /**
     * 配置项目
     */
    public static final String CONFIGITEM = "configItem";

    /**
     * 配置项目
     */
    private String configItem;

    /**
     * 配置内容
     */
    public static final String CONFIGVALUE = "configValue";

    /**
     * 配置内容
     */
    private String configValue;

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    private static final long serialVersionUID = 1L;

    /**
     * @return configId
     *         configId
     */
    public Integer getConfigId() {
        return configId;
    }

    /**
     * @param configId
     *         configId
     */
    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    /**
     * @return configItem
     *         配置项目
     */
    public String getConfigItem() {
        return configItem;
    }

    /**
     * @param configItem
     *         配置项目
     */
    public void setConfigItem(String configItem) {
        this.configItem = configItem == null ? null : configItem.trim();
    }

    /**
     * @return configValue
     *         配置内容
     */
    public String getConfigValue() {
        return configValue;
    }

    /**
     * @param configValue
     *         配置内容
     */
    public void setConfigValue(String configValue) {
        this.configValue = configValue == null ? null : configValue.trim();
    }

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }
}