package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutlineDoc implements Serializable {
    /**
     * 大纲编号
     */
    public static final String OUTLINEID = "outlineId";

    /**
     * 大纲编号
     */
    private Integer outlineId;

    /**
     * 课程编号
     */
    public static final String COURSEID = "courseId";

    /**
     * 课程编号
     */
    private Integer courseId;

    /**
     * pptSrc
     */
    public static final String PPTSRC = "pptSrc";

    /**
     * pptSrc
     */
    private String pptSrc;

    /**
     * pptFilename
     */
    public static final String PPTFILENAME = "pptFilename";

    /**
     * pptFilename
     */
    private String pptFilename;

    private static final long serialVersionUID = 1L;

    /**
     * @return outlineId
     *         大纲编号
     */
    public Integer getOutlineId() {
        return outlineId;
    }

    /**
     * @param outlineId
     *         大纲编号
     */
    public void setOutlineId(Integer outlineId) {
        this.outlineId = outlineId;
    }

    /**
     * @return courseId
     *         课程编号
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         课程编号
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return pptSrc
     *         pptSrc
     */
    public String getPptSrc() {
        return pptSrc;
    }

    /**
     * @param pptSrc
     *         pptSrc
     */
    public void setPptSrc(String pptSrc) {
        this.pptSrc = pptSrc == null ? null : pptSrc.trim();
    }

    /**
     * @return pptFilename
     *         pptFilename
     */
    public String getPptFilename() {
        return pptFilename;
    }

    /**
     * @param pptFilename
     *         pptFilename
     */
    public void setPptFilename(String pptFilename) {
        this.pptFilename = pptFilename == null ? null : pptFilename.trim();
    }
}