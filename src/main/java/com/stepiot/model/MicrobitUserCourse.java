package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class MicrobitUserCourse implements Serializable {
    /**
     * courseUid
     */
    public static final String COURSEUID = "courseUid";

    /**
     * courseUid
     */
    private String courseUid;

    /**
     * 课程名称
     */
    public static final String COURSENAME = "courseName";

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 课程完成状态 : 
     * 0 未完成 
     * 1 待下发 
     * 2 已下发 
     * 3 已完成
     */
    public static final String COURSESTATUS = "courseStatus";

    /**
     * 课程完成状态 : 
     * 0 未完成 
     * 1 待下发 
     * 2 已下发 
     * 3 已完成
     */
    private Integer courseStatus;

    /**
     * 用户名
     */
    public static final String USERNAME = "username";

    /**
     * 用户名
     */
    private String username;

    /**
     * 创建日期
     */
    public static final String CREATEDATE = "createDate";

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 最后更新日期
     */
    public static final String LASTUPDATE = "lastUpdate";

    /**
     * 最后更新日期
     */
    private Date lastUpdate;

    /**
     * 课程代码 关联到iot_course表中的course_id
     */
    public static final String COURSEID = "courseId";

    /**
     * 课程代码 关联到iot_course表中的course_id
     */
    private Integer courseId;

    private static final long serialVersionUID = 1L;

    /**
     * @return courseUid
     *         courseUid
     */
    public String getCourseUid() {
        return courseUid;
    }

    /**
     * @param courseUid
     *         courseUid
     */
    public void setCourseUid(String courseUid) {
        this.courseUid = courseUid == null ? null : courseUid.trim();
    }

    /**
     * @return courseName
     *         课程名称
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName
     *         课程名称
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    /**
     * @return courseStatus
     *         课程完成状态 : 
     *         0 未完成 
     *         1 待下发 
     *         2 已下发 
     *         3 已完成
     */
    public Integer getCourseStatus() {
        return courseStatus;
    }

    /**
     * @param courseStatus
     *         课程完成状态 : 
     *         0 未完成 
     *         1 待下发 
     *         2 已下发 
     *         3 已完成
     */
    public void setCourseStatus(Integer courseStatus) {
        this.courseStatus = courseStatus;
    }

    /**
     * @return username
     *         用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         用户名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return createDate
     *         创建日期
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *         创建日期
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return lastUpdate
     *         最后更新日期
     */
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate
     *         最后更新日期
     */
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return courseId
     *         课程代码 关联到iot_course表中的course_id
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         课程代码 关联到iot_course表中的course_id
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}