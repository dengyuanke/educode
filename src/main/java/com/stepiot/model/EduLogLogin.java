package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduLogLogin implements Serializable {
    /**
     * id
     */
    public static final String ID = "id";

    /**
     * id
     */
    private Integer id;

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    /**
     * loginTime
     */
    public static final String LOGINTIME = "loginTime";

    /**
     * loginTime
     */
    private Date loginTime;

    /**
     * ipaddress
     */
    public static final String IPADDRESS = "ipaddress";

    /**
     * ipaddress
     */
    private String ipaddress;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     *         id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *         id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return loginTime
     *         loginTime
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * @param loginTime
     *         loginTime
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * @return ipaddress
     *         ipaddress
     */
    public String getIpaddress() {
        return ipaddress;
    }

    /**
     * @param ipaddress
     *         ipaddress
     */
    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress == null ? null : ipaddress.trim();
    }
}