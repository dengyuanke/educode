package com.stepiot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EduUserMediaCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduUserMediaCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMediaUidIsNull() {
            addCriterion("media_uid is null");
            return (Criteria) this;
        }

        public Criteria andMediaUidIsNotNull() {
            addCriterion("media_uid is not null");
            return (Criteria) this;
        }

        public Criteria andMediaUidEqualTo(String value) {
            addCriterion("media_uid =", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidNotEqualTo(String value) {
            addCriterion("media_uid <>", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidGreaterThan(String value) {
            addCriterion("media_uid >", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidGreaterThanOrEqualTo(String value) {
            addCriterion("media_uid >=", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidLessThan(String value) {
            addCriterion("media_uid <", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidLessThanOrEqualTo(String value) {
            addCriterion("media_uid <=", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidLike(String value) {
            addCriterion("media_uid like", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidNotLike(String value) {
            addCriterion("media_uid not like", value, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidIn(List<String> values) {
            addCriterion("media_uid in", values, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidNotIn(List<String> values) {
            addCriterion("media_uid not in", values, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidBetween(String value1, String value2) {
            addCriterion("media_uid between", value1, value2, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaUidNotBetween(String value1, String value2) {
            addCriterion("media_uid not between", value1, value2, "mediaUid");
            return (Criteria) this;
        }

        public Criteria andMediaNameIsNull() {
            addCriterion("media_name is null");
            return (Criteria) this;
        }

        public Criteria andMediaNameIsNotNull() {
            addCriterion("media_name is not null");
            return (Criteria) this;
        }

        public Criteria andMediaNameEqualTo(String value) {
            addCriterion("media_name =", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameNotEqualTo(String value) {
            addCriterion("media_name <>", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameGreaterThan(String value) {
            addCriterion("media_name >", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameGreaterThanOrEqualTo(String value) {
            addCriterion("media_name >=", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameLessThan(String value) {
            addCriterion("media_name <", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameLessThanOrEqualTo(String value) {
            addCriterion("media_name <=", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameLike(String value) {
            addCriterion("media_name like", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameNotLike(String value) {
            addCriterion("media_name not like", value, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameIn(List<String> values) {
            addCriterion("media_name in", values, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameNotIn(List<String> values) {
            addCriterion("media_name not in", values, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameBetween(String value1, String value2) {
            addCriterion("media_name between", value1, value2, "mediaName");
            return (Criteria) this;
        }

        public Criteria andMediaNameNotBetween(String value1, String value2) {
            addCriterion("media_name not between", value1, value2, "mediaName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andMimeTypeIsNull() {
            addCriterion("mime_type is null");
            return (Criteria) this;
        }

        public Criteria andMimeTypeIsNotNull() {
            addCriterion("mime_type is not null");
            return (Criteria) this;
        }

        public Criteria andMimeTypeEqualTo(String value) {
            addCriterion("mime_type =", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeNotEqualTo(String value) {
            addCriterion("mime_type <>", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeGreaterThan(String value) {
            addCriterion("mime_type >", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("mime_type >=", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeLessThan(String value) {
            addCriterion("mime_type <", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeLessThanOrEqualTo(String value) {
            addCriterion("mime_type <=", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeLike(String value) {
            addCriterion("mime_type like", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeNotLike(String value) {
            addCriterion("mime_type not like", value, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeIn(List<String> values) {
            addCriterion("mime_type in", values, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeNotIn(List<String> values) {
            addCriterion("mime_type not in", values, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeBetween(String value1, String value2) {
            addCriterion("mime_type between", value1, value2, "mimeType");
            return (Criteria) this;
        }

        public Criteria andMimeTypeNotBetween(String value1, String value2) {
            addCriterion("mime_type not between", value1, value2, "mimeType");
            return (Criteria) this;
        }

        public Criteria andS3PathIsNull() {
            addCriterion("s3_path is null");
            return (Criteria) this;
        }

        public Criteria andS3PathIsNotNull() {
            addCriterion("s3_path is not null");
            return (Criteria) this;
        }

        public Criteria andS3PathEqualTo(String value) {
            addCriterion("s3_path =", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathNotEqualTo(String value) {
            addCriterion("s3_path <>", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathGreaterThan(String value) {
            addCriterion("s3_path >", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathGreaterThanOrEqualTo(String value) {
            addCriterion("s3_path >=", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathLessThan(String value) {
            addCriterion("s3_path <", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathLessThanOrEqualTo(String value) {
            addCriterion("s3_path <=", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathLike(String value) {
            addCriterion("s3_path like", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathNotLike(String value) {
            addCriterion("s3_path not like", value, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathIn(List<String> values) {
            addCriterion("s3_path in", values, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathNotIn(List<String> values) {
            addCriterion("s3_path not in", values, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathBetween(String value1, String value2) {
            addCriterion("s3_path between", value1, value2, "s3Path");
            return (Criteria) this;
        }

        public Criteria andS3PathNotBetween(String value1, String value2) {
            addCriterion("s3_path not between", value1, value2, "s3Path");
            return (Criteria) this;
        }

        public Criteria andSizeIsNull() {
            addCriterion("size is null");
            return (Criteria) this;
        }

        public Criteria andSizeIsNotNull() {
            addCriterion("size is not null");
            return (Criteria) this;
        }

        public Criteria andSizeEqualTo(Integer value) {
            addCriterion("size =", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotEqualTo(Integer value) {
            addCriterion("size <>", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThan(Integer value) {
            addCriterion("size >", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThanOrEqualTo(Integer value) {
            addCriterion("size >=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThan(Integer value) {
            addCriterion("size <", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThanOrEqualTo(Integer value) {
            addCriterion("size <=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeIn(List<Integer> values) {
            addCriterion("size in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotIn(List<Integer> values) {
            addCriterion("size not in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeBetween(Integer value1, Integer value2) {
            addCriterion("size between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotBetween(Integer value1, Integer value2) {
            addCriterion("size not between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("username is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("username is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("username =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("username <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("username >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("username >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("username <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("username <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("username like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("username not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("username in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("username not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("username between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("username not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andMediaSrcIsNull() {
            addCriterion("media_src is null");
            return (Criteria) this;
        }

        public Criteria andMediaSrcIsNotNull() {
            addCriterion("media_src is not null");
            return (Criteria) this;
        }

        public Criteria andMediaSrcEqualTo(String value) {
            addCriterion("media_src =", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcNotEqualTo(String value) {
            addCriterion("media_src <>", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcGreaterThan(String value) {
            addCriterion("media_src >", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcGreaterThanOrEqualTo(String value) {
            addCriterion("media_src >=", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcLessThan(String value) {
            addCriterion("media_src <", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcLessThanOrEqualTo(String value) {
            addCriterion("media_src <=", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcLike(String value) {
            addCriterion("media_src like", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcNotLike(String value) {
            addCriterion("media_src not like", value, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcIn(List<String> values) {
            addCriterion("media_src in", values, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcNotIn(List<String> values) {
            addCriterion("media_src not in", values, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcBetween(String value1, String value2) {
            addCriterion("media_src between", value1, value2, "mediaSrc");
            return (Criteria) this;
        }

        public Criteria andMediaSrcNotBetween(String value1, String value2) {
            addCriterion("media_src not between", value1, value2, "mediaSrc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}