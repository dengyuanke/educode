package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourse implements Serializable {
    /**
     * 课程编号
     */
    public static final String COURSEID = "courseId";

    /**
     * 课程编号
     */
    private Integer courseId;

    /**
     * 课程名称
     */
    public static final String COURSENAME = "courseName";

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 学习时长
     */
    public static final String LENGTH = "length";

    /**
     * 学习时长
     */
    private Integer length;

    /**
     * 努力
     */
    public static final String EFFORT = "effort";

    /**
     * 努力
     */
    private String effort;

    /**
     * 价格
     */
    public static final String PRICE = "price";

    /**
     * 价格
     */
    private String price;

    /**
     * 主题
     */
    public static final String SUBJECT = "subject";

    /**
     * 主题
     */
    private String subject;

    /**
     * 级别
     */
    public static final String LEVEL = "level";

    /**
     * 级别
     */
    private String level;

    /**
     * 证书
     */
    public static final String PROGRAMS = "programs";

    /**
     * 证书
     */
    private String programs;

    /**
     * 学分
     */
    public static final String CREDIT = "credit";

    /**
     * 学分
     */
    private Integer credit;

    /**
     * 课程描述Wi-Ki编号
     */
    public static final String WIKIID = "wikiId";

    /**
     * 课程描述Wi-Ki编号
     */
    private Integer wikiId;

    /**
     * 课程简单描述
     */
    public static final String DESCRIPTION = "description";

    /**
     * 课程简单描述
     */
    private String description;

    /**
     * 课程封面图片
     */
    public static final String COVERAGE = "coverage";

    /**
     * 课程封面图片
     */
    private String coverage;

    private static final long serialVersionUID = 1L;

    /**
     * @return courseId
     *         课程编号
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId
     *         课程编号
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return courseName
     *         课程名称
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName
     *         课程名称
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    /**
     * @return length
     *         学习时长
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length
     *         学习时长
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return effort
     *         努力
     */
    public String getEffort() {
        return effort;
    }

    /**
     * @param effort
     *         努力
     */
    public void setEffort(String effort) {
        this.effort = effort == null ? null : effort.trim();
    }

    /**
     * @return price
     *         价格
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     *         价格
     */
    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    /**
     * @return subject
     *         主题
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject
     *         主题
     */
    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    /**
     * @return level
     *         级别
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level
     *         级别
     */
    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    /**
     * @return programs
     *         证书
     */
    public String getPrograms() {
        return programs;
    }

    /**
     * @param programs
     *         证书
     */
    public void setPrograms(String programs) {
        this.programs = programs == null ? null : programs.trim();
    }

    /**
     * @return credit
     *         学分
     */
    public Integer getCredit() {
        return credit;
    }

    /**
     * @param credit
     *         学分
     */
    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    /**
     * @return wikiId
     *         课程描述Wi-Ki编号
     */
    public Integer getWikiId() {
        return wikiId;
    }

    /**
     * @param wikiId
     *         课程描述Wi-Ki编号
     */
    public void setWikiId(Integer wikiId) {
        this.wikiId = wikiId;
    }

    /**
     * @return description
     *         课程简单描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *         课程简单描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * @return coverage
     *         课程封面图片
     */
    public String getCoverage() {
        return coverage;
    }

    /**
     * @param coverage
     *         课程封面图片
     */
    public void setCoverage(String coverage) {
        this.coverage = coverage == null ? null : coverage.trim();
    }
}