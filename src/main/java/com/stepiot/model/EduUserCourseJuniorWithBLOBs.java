package com.stepiot.model;

import java.io.Serializable;

public class EduUserCourseJuniorWithBLOBs extends EduUserCourseJunior implements Serializable {
    /**
     * 课程对应的Blockly代码
     */
    public static final String COURSEBLOCKLY = "courseBlockly";

    /**
     * 课程对应的Blockly代码
     */
    private String courseBlockly;

    /**
     * 对应的Python代码
     */
    public static final String BLOCKLYCODE = "blocklyCode";

    /**
     * 对应的Python代码
     */
    private String blocklyCode;

    private static final long serialVersionUID = 1L;

    /**
     * @return courseBlockly
     *         课程对应的Blockly代码
     */
    public String getCourseBlockly() {
        return courseBlockly;
    }

    /**
     * @param courseBlockly
     *         课程对应的Blockly代码
     */
    public void setCourseBlockly(String courseBlockly) {
        this.courseBlockly = courseBlockly == null ? null : courseBlockly.trim();
    }

    /**
     * @return blocklyCode
     *         对应的Python代码
     */
    public String getBlocklyCode() {
        return blocklyCode;
    }

    /**
     * @param blocklyCode
     *         对应的Python代码
     */
    public void setBlocklyCode(String blocklyCode) {
        this.blocklyCode = blocklyCode == null ? null : blocklyCode.trim();
    }
}