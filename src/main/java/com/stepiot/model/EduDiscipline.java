package com.stepiot.model;

import java.io.Serializable;

public class EduDiscipline implements Serializable {
    /**
     * 专业编号
     */
    public static final String DISCIPLINEID = "disciplineId";

    /**
     * 专业编号
     */
    private String disciplineId;

    /**
     * 专业名称
     */
    public static final String DISCIPLINENAME = "disciplineName";

    /**
     * 专业名称
     */
    private String disciplineName;

    /**
     * 所属院系编号
     */
    public static final String DEPARTMENTID = "departmentId";

    /**
     * 所属院系编号
     */
    private String departmentId;

    private static final long serialVersionUID = 1L;

    /**
     * @return disciplineId
     *         专业编号
     */
    public String getDisciplineId() {
        return disciplineId;
    }

    /**
     * @param disciplineId
     *         专业编号
     */
    public void setDisciplineId(String disciplineId) {
        this.disciplineId = disciplineId == null ? null : disciplineId.trim();
    }

    /**
     * @return disciplineName
     *         专业名称
     */
    public String getDisciplineName() {
        return disciplineName;
    }

    /**
     * @param disciplineName
     *         专业名称
     */
    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName == null ? null : disciplineName.trim();
    }

    /**
     * @return departmentId
     *         所属院系编号
     */
    public String getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId
     *         所属院系编号
     */
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId == null ? null : departmentId.trim();
    }
}