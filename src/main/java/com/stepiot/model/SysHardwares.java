package com.stepiot.model;

import java.io.Serializable;

public class SysHardwares implements Serializable {
    /**
     * 编号
     */
    public static final String ID = "id";

    /**
     * 编号
     */
    private Integer id;

    /**
     * 硬件名称
     */
    public static final String HARDWARENAME = "hardwareName";

    /**
     * 硬件名称
     */
    private String hardwareName;

    /**
     * 图片，用于选择页面的展示
     */
    public static final String PHOTO = "photo";

    /**
     * 图片，用于选择页面的展示
     */
    private String photo;

    /**
     * 链接地址
     */
    public static final String ACTIONLINK = "actionLink";

    /**
     * 链接地址
     */
    private String actionLink;

    /**
     * 类型编码
     */
    public static final String TYPES = "types";

    /**
     * 类型编码
     */
    private String types;

    /**
     * 硬件版本
     */
    public static final String VERSION = "version";

    /**
     * 硬件版本
     */
    private String version;

    /**
     * description
     */
    public static final String DESCRIPTION = "description";

    /**
     * description
     */
    private String description;

    /**
     * docLink
     */
    public static final String DOCLINK = "docLink";

    /**
     * docLink
     */
    private String docLink;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     *         编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *         编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return hardwareName
     *         硬件名称
     */
    public String getHardwareName() {
        return hardwareName;
    }

    /**
     * @param hardwareName
     *         硬件名称
     */
    public void setHardwareName(String hardwareName) {
        this.hardwareName = hardwareName == null ? null : hardwareName.trim();
    }

    /**
     * @return photo
     *         图片，用于选择页面的展示
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo
     *         图片，用于选择页面的展示
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * @return actionLink
     *         链接地址
     */
    public String getActionLink() {
        return actionLink;
    }

    /**
     * @param actionLink
     *         链接地址
     */
    public void setActionLink(String actionLink) {
        this.actionLink = actionLink == null ? null : actionLink.trim();
    }

    /**
     * @return types
     *         类型编码
     */
    public String getTypes() {
        return types;
    }

    /**
     * @param types
     *         类型编码
     */
    public void setTypes(String types) {
        this.types = types == null ? null : types.trim();
    }

    /**
     * @return version
     *         硬件版本
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *         硬件版本
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * @return description
     *         description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *         description
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * @return docLink
     *         docLink
     */
    public String getDocLink() {
        return docLink;
    }

    /**
     * @param docLink
     *         docLink
     */
    public void setDocLink(String docLink) {
        this.docLink = docLink == null ? null : docLink.trim();
    }
}