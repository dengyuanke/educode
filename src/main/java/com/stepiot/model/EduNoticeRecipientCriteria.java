package com.stepiot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EduNoticeRecipientCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduNoticeRecipientCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecipientIdIsNull() {
            addCriterion("recipient_id is null");
            return (Criteria) this;
        }

        public Criteria andRecipientIdIsNotNull() {
            addCriterion("recipient_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecipientIdEqualTo(Integer value) {
            addCriterion("recipient_id =", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdNotEqualTo(Integer value) {
            addCriterion("recipient_id <>", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdGreaterThan(Integer value) {
            addCriterion("recipient_id >", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("recipient_id >=", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdLessThan(Integer value) {
            addCriterion("recipient_id <", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdLessThanOrEqualTo(Integer value) {
            addCriterion("recipient_id <=", value, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdIn(List<Integer> values) {
            addCriterion("recipient_id in", values, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdNotIn(List<Integer> values) {
            addCriterion("recipient_id not in", values, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdBetween(Integer value1, Integer value2) {
            addCriterion("recipient_id between", value1, value2, "recipientId");
            return (Criteria) this;
        }

        public Criteria andRecipientIdNotBetween(Integer value1, Integer value2) {
            addCriterion("recipient_id not between", value1, value2, "recipientId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdIsNull() {
            addCriterion("notice_id is null");
            return (Criteria) this;
        }

        public Criteria andNoticeIdIsNotNull() {
            addCriterion("notice_id is not null");
            return (Criteria) this;
        }

        public Criteria andNoticeIdEqualTo(Integer value) {
            addCriterion("notice_id =", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdNotEqualTo(Integer value) {
            addCriterion("notice_id <>", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdGreaterThan(Integer value) {
            addCriterion("notice_id >", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("notice_id >=", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdLessThan(Integer value) {
            addCriterion("notice_id <", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdLessThanOrEqualTo(Integer value) {
            addCriterion("notice_id <=", value, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdIn(List<Integer> values) {
            addCriterion("notice_id in", values, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdNotIn(List<Integer> values) {
            addCriterion("notice_id not in", values, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdBetween(Integer value1, Integer value2) {
            addCriterion("notice_id between", value1, value2, "noticeId");
            return (Criteria) this;
        }

        public Criteria andNoticeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("notice_id not between", value1, value2, "noticeId");
            return (Criteria) this;
        }

        public Criteria andRecipientUserIsNull() {
            addCriterion("recipient_user is null");
            return (Criteria) this;
        }

        public Criteria andRecipientUserIsNotNull() {
            addCriterion("recipient_user is not null");
            return (Criteria) this;
        }

        public Criteria andRecipientUserEqualTo(String value) {
            addCriterion("recipient_user =", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserNotEqualTo(String value) {
            addCriterion("recipient_user <>", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserGreaterThan(String value) {
            addCriterion("recipient_user >", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserGreaterThanOrEqualTo(String value) {
            addCriterion("recipient_user >=", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserLessThan(String value) {
            addCriterion("recipient_user <", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserLessThanOrEqualTo(String value) {
            addCriterion("recipient_user <=", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserLike(String value) {
            addCriterion("recipient_user like", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserNotLike(String value) {
            addCriterion("recipient_user not like", value, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserIn(List<String> values) {
            addCriterion("recipient_user in", values, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserNotIn(List<String> values) {
            addCriterion("recipient_user not in", values, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserBetween(String value1, String value2) {
            addCriterion("recipient_user between", value1, value2, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientUserNotBetween(String value1, String value2) {
            addCriterion("recipient_user not between", value1, value2, "recipientUser");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusIsNull() {
            addCriterion("recipient_status is null");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusIsNotNull() {
            addCriterion("recipient_status is not null");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusEqualTo(Integer value) {
            addCriterion("recipient_status =", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusNotEqualTo(Integer value) {
            addCriterion("recipient_status <>", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusGreaterThan(Integer value) {
            addCriterion("recipient_status >", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("recipient_status >=", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusLessThan(Integer value) {
            addCriterion("recipient_status <", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusLessThanOrEqualTo(Integer value) {
            addCriterion("recipient_status <=", value, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusIn(List<Integer> values) {
            addCriterion("recipient_status in", values, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusNotIn(List<Integer> values) {
            addCriterion("recipient_status not in", values, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusBetween(Integer value1, Integer value2) {
            addCriterion("recipient_status between", value1, value2, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("recipient_status not between", value1, value2, "recipientStatus");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeIsNull() {
            addCriterion("recipient_time is null");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeIsNotNull() {
            addCriterion("recipient_time is not null");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeEqualTo(Date value) {
            addCriterion("recipient_time =", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeNotEqualTo(Date value) {
            addCriterion("recipient_time <>", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeGreaterThan(Date value) {
            addCriterion("recipient_time >", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("recipient_time >=", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeLessThan(Date value) {
            addCriterion("recipient_time <", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeLessThanOrEqualTo(Date value) {
            addCriterion("recipient_time <=", value, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeIn(List<Date> values) {
            addCriterion("recipient_time in", values, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeNotIn(List<Date> values) {
            addCriterion("recipient_time not in", values, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeBetween(Date value1, Date value2) {
            addCriterion("recipient_time between", value1, value2, "recipientTime");
            return (Criteria) this;
        }

        public Criteria andRecipientTimeNotBetween(Date value1, Date value2) {
            addCriterion("recipient_time not between", value1, value2, "recipientTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}