package com.stepiot.model;

import java.io.Serializable;

public class EduUserRole implements Serializable {
    /**
     * id
     */
    public static final String ID = "id";

    /**
     * id
     */
    private Integer id;

    /**
     * 用户名
     */
    public static final String USERNAME = "username";

    /**
     * 用户名
     */
    private String username;

    /**
     * 角色ID
     */
    public static final String ROLE = "role";

    /**
     * 角色ID
     */
    private String role;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     *         id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *         id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return username
     *         用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         用户名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return role
     *         角色ID
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     *         角色ID
     */
    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }
}