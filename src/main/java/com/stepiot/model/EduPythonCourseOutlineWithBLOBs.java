package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutlineWithBLOBs extends EduPythonCourseOutline implements Serializable {
    /**
     * 编辑器工具栏
     */
    public static final String TOOLBOX = "toolbox";

    /**
     * 编辑器工具栏
     */
    private String toolbox;

    /**
     * 工具栏选项
     */
    public static final String OPTIONS = "options";

    /**
     * 工具栏选项
     */
    private String options;

    /**
     * 编辑器默认积木
     */
    public static final String BLOCKLY = "blockly";

    /**
     * 编辑器默认积木
     */
    private String blockly;

    /**
     * code
     */
    public static final String CODE = "code";

    /**
     * code
     */
    private String code;

    private static final long serialVersionUID = 1L;

    /**
     * @return toolbox
     *         编辑器工具栏
     */
    public String getToolbox() {
        return toolbox;
    }

    /**
     * @param toolbox
     *         编辑器工具栏
     */
    public void setToolbox(String toolbox) {
        this.toolbox = toolbox == null ? null : toolbox.trim();
    }

    /**
     * @return options
     *         工具栏选项
     */
    public String getOptions() {
        return options;
    }

    /**
     * @param options
     *         工具栏选项
     */
    public void setOptions(String options) {
        this.options = options == null ? null : options.trim();
    }

    /**
     * @return blockly
     *         编辑器默认积木
     */
    public String getBlockly() {
        return blockly;
    }

    /**
     * @param blockly
     *         编辑器默认积木
     */
    public void setBlockly(String blockly) {
        this.blockly = blockly == null ? null : blockly.trim();
    }

    /**
     * @return code
     *         code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *         code
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }
}