package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduSysMedia implements Serializable {
    /**
     * mediaId
     */
    public static final String MEDIAID = "mediaId";

    /**
     * mediaId
     */
    private Integer mediaId;

    /**
     * mediaName
     */
    public static final String MEDIANAME = "mediaName";

    /**
     * mediaName
     */
    private String mediaName;

    /**
     * createTime
     */
    public static final String CREATETIME = "createTime";

    /**
     * createTime
     */
    private Date createTime;

    /**
     * mimeType
     */
    public static final String MIMETYPE = "mimeType";

    /**
     * mimeType
     */
    private String mimeType;

    /**
     * s3Path
     */
    public static final String S3PATH = "s3Path";

    /**
     * s3Path
     */
    private String s3Path;

    /**
     * mediaSize
     */
    public static final String MEDIASIZE = "mediaSize";

    /**
     * mediaSize
     */
    private Integer mediaSize;

    /**
     * mediaSrc
     */
    public static final String MEDIASRC = "mediaSrc";

    /**
     * mediaSrc
     */
    private String mediaSrc;

    private static final long serialVersionUID = 1L;

    /**
     * @return mediaId
     *         mediaId
     */
    public Integer getMediaId() {
        return mediaId;
    }

    /**
     * @param mediaId
     *         mediaId
     */
    public void setMediaId(Integer mediaId) {
        this.mediaId = mediaId;
    }

    /**
     * @return mediaName
     *         mediaName
     */
    public String getMediaName() {
        return mediaName;
    }

    /**
     * @param mediaName
     *         mediaName
     */
    public void setMediaName(String mediaName) {
        this.mediaName = mediaName == null ? null : mediaName.trim();
    }

    /**
     * @return createTime
     *         createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     *         createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return mimeType
     *         mimeType
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * @param mimeType
     *         mimeType
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType == null ? null : mimeType.trim();
    }

    /**
     * @return s3Path
     *         s3Path
     */
    public String getS3Path() {
        return s3Path;
    }

    /**
     * @param s3Path
     *         s3Path
     */
    public void setS3Path(String s3Path) {
        this.s3Path = s3Path == null ? null : s3Path.trim();
    }

    /**
     * @return mediaSize
     *         mediaSize
     */
    public Integer getMediaSize() {
        return mediaSize;
    }

    /**
     * @param mediaSize
     *         mediaSize
     */
    public void setMediaSize(Integer mediaSize) {
        this.mediaSize = mediaSize;
    }

    /**
     * @return mediaSrc
     *         mediaSrc
     */
    public String getMediaSrc() {
        return mediaSrc;
    }

    /**
     * @param mediaSrc
     *         mediaSrc
     */
    public void setMediaSrc(String mediaSrc) {
        this.mediaSrc = mediaSrc == null ? null : mediaSrc.trim();
    }
}