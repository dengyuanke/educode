package com.stepiot.model;

import java.io.Serializable;

public class EduTeacherClassRef implements Serializable {
    /**
     * 教师班级关联表
     */
    public static final String REFID = "refId";

    /**
     * 教师班级关联表
     */
    private Integer refId;

    /**
     * 教师编号
     */
    public static final String TEARCHID = "tearchId";

    /**
     * 教师编号
     */
    private String tearchId;

    /**
     * 班级编号
     */
    public static final String CLASSID = "classId";

    /**
     * 班级编号
     */
    private String classId;

    /**
     * className
     */
    public static final String CLASSNAME = "className";

    /**
     * className
     */
    private String className;

    private static final long serialVersionUID = 1L;

    /**
     * @return refId
     *         教师班级关联表
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId
     *         教师班级关联表
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return tearchId
     *         教师编号
     */
    public String getTearchId() {
        return tearchId;
    }

    /**
     * @param tearchId
     *         教师编号
     */
    public void setTearchId(String tearchId) {
        this.tearchId = tearchId == null ? null : tearchId.trim();
    }

    /**
     * @return classId
     *         班级编号
     */
    public String getClassId() {
        return classId;
    }

    /**
     * @param classId
     *         班级编号
     */
    public void setClassId(String classId) {
        this.classId = classId == null ? null : classId.trim();
    }

    /**
     * @return className
     *         className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className
     *         className
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }
}