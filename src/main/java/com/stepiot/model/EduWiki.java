package com.stepiot.model;

import java.io.Serializable;

public class EduWiki implements Serializable {
    /**
     * wikiId
     */
    public static final String WIKIID = "wikiId";

    /**
     * wikiId
     */
    private Integer wikiId;

    /**
     * wikiTitle
     */
    public static final String WIKITITLE = "wikiTitle";

    /**
     * wikiTitle
     */
    private String wikiTitle;

    /**
     * wikiBody
     */
    public static final String WIKIBODY = "wikiBody";

    /**
     * wikiBody
     */
    private String wikiBody;

    private static final long serialVersionUID = 1L;

    /**
     * @return wikiId
     *         wikiId
     */
    public Integer getWikiId() {
        return wikiId;
    }

    /**
     * @param wikiId
     *         wikiId
     */
    public void setWikiId(Integer wikiId) {
        this.wikiId = wikiId;
    }

    /**
     * @return wikiTitle
     *         wikiTitle
     */
    public String getWikiTitle() {
        return wikiTitle;
    }

    /**
     * @param wikiTitle
     *         wikiTitle
     */
    public void setWikiTitle(String wikiTitle) {
        this.wikiTitle = wikiTitle == null ? null : wikiTitle.trim();
    }

    /**
     * @return wikiBody
     *         wikiBody
     */
    public String getWikiBody() {
        return wikiBody;
    }

    /**
     * @param wikiBody
     *         wikiBody
     */
    public void setWikiBody(String wikiBody) {
        this.wikiBody = wikiBody == null ? null : wikiBody.trim();
    }
}