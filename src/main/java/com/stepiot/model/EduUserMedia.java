package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduUserMedia implements Serializable {
    /**
     * mediaUid
     */
    public static final String MEDIAUID = "mediaUid";

    /**
     * mediaUid
     */
    private String mediaUid;

    /**
     * mediaName
     */
    public static final String MEDIANAME = "mediaName";

    /**
     * mediaName
     */
    private String mediaName;

    /**
     * createTime
     */
    public static final String CREATETIME = "createTime";

    /**
     * createTime
     */
    private Date createTime;

    /**
     * mimeType
     */
    public static final String MIMETYPE = "mimeType";

    /**
     * mimeType
     */
    private String mimeType;

    /**
     * s3Path
     */
    public static final String S3PATH = "s3Path";

    /**
     * s3Path
     */
    private String s3Path;

    /**
     * size
     */
    public static final String SIZE = "size";

    /**
     * size
     */
    private Integer size;

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * username
     */
    private String username;

    /**
     * mediaSrc
     */
    public static final String MEDIASRC = "mediaSrc";

    /**
     * mediaSrc
     */
    private String mediaSrc;

    private static final long serialVersionUID = 1L;

    /**
     * @return mediaUid
     *         mediaUid
     */
    public String getMediaUid() {
        return mediaUid;
    }

    /**
     * @param mediaUid
     *         mediaUid
     */
    public void setMediaUid(String mediaUid) {
        this.mediaUid = mediaUid == null ? null : mediaUid.trim();
    }

    /**
     * @return mediaName
     *         mediaName
     */
    public String getMediaName() {
        return mediaName;
    }

    /**
     * @param mediaName
     *         mediaName
     */
    public void setMediaName(String mediaName) {
        this.mediaName = mediaName == null ? null : mediaName.trim();
    }

    /**
     * @return createTime
     *         createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     *         createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return mimeType
     *         mimeType
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * @param mimeType
     *         mimeType
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType == null ? null : mimeType.trim();
    }

    /**
     * @return s3Path
     *         s3Path
     */
    public String getS3Path() {
        return s3Path;
    }

    /**
     * @param s3Path
     *         s3Path
     */
    public void setS3Path(String s3Path) {
        this.s3Path = s3Path == null ? null : s3Path.trim();
    }

    /**
     * @return size
     *         size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size
     *         size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * @return username
     *         username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *         username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return mediaSrc
     *         mediaSrc
     */
    public String getMediaSrc() {
        return mediaSrc;
    }

    /**
     * @param mediaSrc
     *         mediaSrc
     */
    public void setMediaSrc(String mediaSrc) {
        this.mediaSrc = mediaSrc == null ? null : mediaSrc.trim();
    }
}