package com.stepiot.model;

import java.util.ArrayList;
import java.util.List;

public class EduPythonCourseOutlineDocCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer pageSize;

    public EduPythonCourseOutlineDocCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize=pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOutlineIdIsNull() {
            addCriterion("outline_id is null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIsNotNull() {
            addCriterion("outline_id is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineIdEqualTo(Integer value) {
            addCriterion("outline_id =", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotEqualTo(Integer value) {
            addCriterion("outline_id <>", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThan(Integer value) {
            addCriterion("outline_id >", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("outline_id >=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThan(Integer value) {
            addCriterion("outline_id <", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdLessThanOrEqualTo(Integer value) {
            addCriterion("outline_id <=", value, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdIn(List<Integer> values) {
            addCriterion("outline_id in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotIn(List<Integer> values) {
            addCriterion("outline_id not in", values, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdBetween(Integer value1, Integer value2) {
            addCriterion("outline_id between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andOutlineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("outline_id not between", value1, value2, "outlineId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Integer value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Integer value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Integer value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Integer value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Integer value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Integer> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Integer> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Integer value1, Integer value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andPptSrcIsNull() {
            addCriterion("ppt_src is null");
            return (Criteria) this;
        }

        public Criteria andPptSrcIsNotNull() {
            addCriterion("ppt_src is not null");
            return (Criteria) this;
        }

        public Criteria andPptSrcEqualTo(String value) {
            addCriterion("ppt_src =", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcNotEqualTo(String value) {
            addCriterion("ppt_src <>", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcGreaterThan(String value) {
            addCriterion("ppt_src >", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_src >=", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcLessThan(String value) {
            addCriterion("ppt_src <", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcLessThanOrEqualTo(String value) {
            addCriterion("ppt_src <=", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcLike(String value) {
            addCriterion("ppt_src like", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcNotLike(String value) {
            addCriterion("ppt_src not like", value, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcIn(List<String> values) {
            addCriterion("ppt_src in", values, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcNotIn(List<String> values) {
            addCriterion("ppt_src not in", values, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcBetween(String value1, String value2) {
            addCriterion("ppt_src between", value1, value2, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptSrcNotBetween(String value1, String value2) {
            addCriterion("ppt_src not between", value1, value2, "pptSrc");
            return (Criteria) this;
        }

        public Criteria andPptFilenameIsNull() {
            addCriterion("ppt_filename is null");
            return (Criteria) this;
        }

        public Criteria andPptFilenameIsNotNull() {
            addCriterion("ppt_filename is not null");
            return (Criteria) this;
        }

        public Criteria andPptFilenameEqualTo(String value) {
            addCriterion("ppt_filename =", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameNotEqualTo(String value) {
            addCriterion("ppt_filename <>", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameGreaterThan(String value) {
            addCriterion("ppt_filename >", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_filename >=", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameLessThan(String value) {
            addCriterion("ppt_filename <", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameLessThanOrEqualTo(String value) {
            addCriterion("ppt_filename <=", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameLike(String value) {
            addCriterion("ppt_filename like", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameNotLike(String value) {
            addCriterion("ppt_filename not like", value, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameIn(List<String> values) {
            addCriterion("ppt_filename in", values, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameNotIn(List<String> values) {
            addCriterion("ppt_filename not in", values, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameBetween(String value1, String value2) {
            addCriterion("ppt_filename between", value1, value2, "pptFilename");
            return (Criteria) this;
        }

        public Criteria andPptFilenameNotBetween(String value1, String value2) {
            addCriterion("ppt_filename not between", value1, value2, "pptFilename");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}