package com.stepiot.model;

import java.io.Serializable;

public class EduPythonCourseOutlineDocWithBLOBs extends EduPythonCourseOutlineDoc implements Serializable {
    /**
     * markdown格式的文档
     */
    public static final String MDOC = "mdoc";

    /**
     * markdown格式的文档
     */
    private String mdoc;

    /**
     * mdocs
     */
    public static final String MDOCS = "mdocs";

    /**
     * mdocs
     */
    private String mdocs;

    private static final long serialVersionUID = 1L;

    /**
     * @return mdoc
     *         markdown格式的文档
     */
    public String getMdoc() {
        return mdoc;
    }

    /**
     * @param mdoc
     *         markdown格式的文档
     */
    public void setMdoc(String mdoc) {
        this.mdoc = mdoc == null ? null : mdoc.trim();
    }

    /**
     * @return mdocs
     *         mdocs
     */
    public String getMdocs() {
        return mdocs;
    }

    /**
     * @param mdocs
     *         mdocs
     */
    public void setMdocs(String mdocs) {
        this.mdocs = mdocs == null ? null : mdocs.trim();
    }
}