package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduNoticeAttachment implements Serializable {
    /**
     * attachmentId
     */
    public static final String ATTACHMENTID = "attachmentId";

    /**
     * attachmentId
     */
    private Integer attachmentId;

    /**
     * noticeId
     */
    public static final String NOTICEID = "noticeId";

    /**
     * noticeId
     */
    private Integer noticeId;

    /**
     * fileType
     */
    public static final String FILETYPE = "fileType";

    /**
     * fileType
     */
    private String fileType;

    /**
     * fileSize
     */
    public static final String FILESIZE = "fileSize";

    /**
     * fileSize
     */
    private Integer fileSize;

    /**
     * location
     */
    public static final String LOCATION = "location";

    /**
     * location
     */
    private String location;

    /**
     * createTime
     */
    public static final String CREATETIME = "createTime";

    /**
     * createTime
     */
    private Date createTime;

    /**
     * cssClass
     */
    public static final String CSSCLASS = "cssClass";

    /**
     * cssClass
     */
    private String cssClass;

    /**
     * attaName
     */
    public static final String ATTANAME = "attaName";

    /**
     * attaName
     */
    private String attaName;

    private static final long serialVersionUID = 1L;

    /**
     * @return attachmentId
     *         attachmentId
     */
    public Integer getAttachmentId() {
        return attachmentId;
    }

    /**
     * @param attachmentId
     *         attachmentId
     */
    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    /**
     * @return noticeId
     *         noticeId
     */
    public Integer getNoticeId() {
        return noticeId;
    }

    /**
     * @param noticeId
     *         noticeId
     */
    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    /**
     * @return fileType
     *         fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType
     *         fileType
     */
    public void setFileType(String fileType) {
        this.fileType = fileType == null ? null : fileType.trim();
    }

    /**
     * @return fileSize
     *         fileSize
     */
    public Integer getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize
     *         fileSize
     */
    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return location
     *         location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location
     *         location
     */
    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    /**
     * @return createTime
     *         createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     *         createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return cssClass
     *         cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @param cssClass
     *         cssClass
     */
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass == null ? null : cssClass.trim();
    }

    /**
     * @return attaName
     *         attaName
     */
    public String getAttaName() {
        return attaName;
    }

    /**
     * @param attaName
     *         attaName
     */
    public void setAttaName(String attaName) {
        this.attaName = attaName == null ? null : attaName.trim();
    }
}