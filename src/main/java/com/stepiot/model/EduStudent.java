package com.stepiot.model;

import java.io.Serializable;
import java.util.Date;

public class EduStudent implements Serializable {
    /**
     * studentId
     */
    public static final String STUDENTID = "studentId";

    /**
     * studentId
     */
    private Integer studentId;

    /**
     * studentName
     */
    public static final String STUDENTNAME = "studentName";

    /**
     * studentName
     */
    private String studentName;

    /**
     * age
     */
    public static final String AGE = "age";

    /**
     * age
     */
    private Integer age;

    /**
     * gender
     */
    public static final String GENDER = "gender";

    /**
     * gender
     */
    private Integer gender;

    /**
     * regDate
     */
    public static final String REGDATE = "regDate";

    /**
     * regDate
     */
    private Date regDate;

    /**
     * 头像
     */
    public static final String AVATAR = "avatar";

    /**
     * 头像
     */
    private String avatar;

    private static final long serialVersionUID = 1L;

    /**
     * @return studentId
     *         studentId
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * @param studentId
     *         studentId
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * @return studentName
     *         studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName
     *         studentName
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName == null ? null : studentName.trim();
    }

    /**
     * @return age
     *         age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     *         age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return gender
     *         gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * @param gender
     *         gender
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * @return regDate
     *         regDate
     */
    public Date getRegDate() {
        return regDate;
    }

    /**
     * @param regDate
     *         regDate
     */
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    /**
     * @return avatar
     *         头像
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar
     *         头像
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }
}