
package com.stepiot.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.stepiot.aliyun.AliSmsService;
import com.stepiot.aliyun.SmsType;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import com.stepiot.model.EduUserRole;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduUserService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.RegUserBean;
import com.stepiot.util.PassWordUtils;
import com.stepiot.util.RandomUtils;
import com.stepiot.util.RecaptchaUtils;
import com.stepiot.util.SessionUtils;
import com.stepiot.util.ValUtils;

@Controller
public class AccountController extends BaseController {

  private static final String SESSION_KEY_REGUSER = "reg";

  @Autowired
  private EduUserService userService;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private AliSmsService sms;


  @GetMapping("/login**")
  public ModelAndView loginPage(Authentication authentication) {
    ModelAndView model = new ModelAndView();
    model.addObject("title", "Spring Security Hello World");
    model.addObject("message", "This is protected page - Admin Page!");
    model.setViewName("login");
    return model;

  }

  @GetMapping("/logout**")
  public ModelAndView logoutPage(Authentication authentication) {
    ModelAndView model = new ModelAndView();
    model.addObject("title", "Spring Security Hello World");
    model.addObject("message", "This is protected page - Admin Page!");
    model.setViewName("login");
    return model;

  }

  @GetMapping("/done")
  public String getDone(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "signup";
    }
    model.addAttribute("data", reg);
    return "done";
  }

  @GetMapping("/recovery")
  public String recovery(HttpSession session, Model model) {
    session.removeAttribute(SESSION_KEY_REGUSER);
    RegUserBean rbean = new RegUserBean();
    String captcha = RandomUtils.genPass(5).toUpperCase();
    byte[] captchas = RecaptchaUtils.genRecaptcha(captcha);

    rbean.setCaptcha(captcha);
    rbean.setCaptchaBase64("data:image/jpeg;base64," + Base64.encodeBase64String(captchas));
    rbean.resetTryTimes();
    session.setAttribute(SESSION_KEY_REGUSER, rbean);
    model.addAttribute("data", rbean);
    return "recovery";
  }

  @ResponseBody
  @PostMapping("/recovery")
  public String doRecovery(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    RegUserBean rbean = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (rbean == null) {
      return "recovery";
    }
    Req body = map.getBody();
    String telephone = body.getTelephone();
    String captcha = body.getCaptcha();


    if (!rbean.getCaptcha().equalsIgnoreCase(captcha)) {

      rbean.increateTryTimes();
      session.setAttribute(SESSION_KEY_REGUSER, rbean);

      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("captcha", "validate.captcha.wrong"));
    }
    EduUserCriteria ex = new EduUserCriteria();
    ex.createCriteria().andTelephoneEqualTo(telephone);
    List<EduUser> list = userService.selectByExample(ex);

    if (list == null || list.size() == 0) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("telephone", "validate.telephone.not.exsits"));
    }

    rbean.setUsername(list.get(0).getUsername());
    int vcode = RandomUtils.randomNumberBetween(100000, 999999);
    rbean.setVcode(vcode + "");
    rbean.setTelephone(telephone);
    sms.sendSms(rbean.getTelephone(), rbean.getVcode(), SmsType.RECOVERY);
    // 发送验证码，并将验证码设置到RegUserBean中

    // 发送完验证码后，清理掉内存中的验证码信息
    rbean.setCaptcha(null);
    rbean.setCaptchaBase64(null);
    rbean.resetTryTimes();

    session.setAttribute(SESSION_KEY_REGUSER, rbean);
    model.addAttribute("data", rbean);
    return reservice.response(HttpStatus.OK, "/vcode", "");

  }


  @GetMapping("/tphone")
  public String getTphone(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "signup";
    }
    String captcha = RandomUtils.genPass(5).toUpperCase();
    byte[] captchas = RecaptchaUtils.genRecaptcha(captcha);

    reg.setCaptcha(captcha);
    reg.setCaptchaBase64("data:image/jpeg;base64," + Base64.encodeBase64String(captchas));
    reg.resetTryTimes();
    session.setAttribute(SESSION_KEY_REGUSER, reg);
    model.addAttribute("data", reg);
    return "tphone";
  }



  @GetMapping("/vphone")
  public String getVphone(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "signup";
    }
    model.addAttribute("data", reg);
    return "vphone";
  }


  @ResponseBody
  @PostMapping("/recaptcha")
  public String captcha(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {

    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "signup";
    }
    String captcha = RandomUtils.genPass(5).toUpperCase();
    byte[] captchas = RecaptchaUtils.genRecaptcha(captcha);

    reg.setCaptcha(captcha);
    reg.setCaptchaBase64("data:image/jpeg;base64," + Base64.encodeBase64String(captchas));
    reg.resetTryTimes();
    session.setAttribute(SESSION_KEY_REGUSER, reg);
    model.addAttribute("data", reg);

    return reservice.response(HttpStatus.OK,
        ImmutableBiMap.of("captchabase64", reg.getCaptchaBase64()));
  }

  @ResponseBody
  @PostMapping("/signup")
  public String signup(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    String username = body.getUsername();
    String password = body.getPassword();
    String confirmPassword = body.getConfirmPassword();
    RegUserBean bean = new RegUserBean();
    bean.setUsername(body.getUsername());
    bean.setPassword(body.getPassword());
    HashMap<String, String> maap = Maps.newHashMap();
    if (!ValUtils.isValidUsername(6, 32, username)) {
      maap.putAll(reservice.of("username", "validate.username.wrong"));
    }
    if (!ValUtils.isValidPassword(8, 64, password)) {
      maap.putAll(reservice.of("password", "validate.password.wrong"));
    }
    if (!password.equals(confirmPassword)) {
      maap.putAll(reservice.of("confirmPassword", "validate.password.not.match"));
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }

    EduUser user = userService.selectByPrimaryKey(body.getUsername());
    if (user != null) {
      ImmutableMap<String, String> maps = reservice.of("username", "validate.username.exsits",
          "password", "validate.password.wrong");
      return reservice.response(HttpStatus.CONFLICT, maps);
    }

    session.setAttribute(SESSION_KEY_REGUSER, bean);

    model.addAttribute("data", bean);
    return reservice.response(HttpStatus.OK, "/tphone", "");
  }

  @GetMapping("/signup")
  public String toSignup() {
    return "signup";
  }


  @ResponseBody
  @PostMapping("/tphone")
  public String tphone(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    RegUserBean bean = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (bean == null) {
      return reservice.response(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "/signup", "");
    }
    String telephone = body.getTelephone();
    if (!ValUtils.isValidTelephone(telephone)) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("telephone", "validate.telephone.wrong"));
    }

    String captcha = body.getCaptcha();
    if (bean.exceedMaxTimes()) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("captcha", "validate.captcha.exceed"));
    }

    if (!bean.getCaptcha().equalsIgnoreCase(captcha)) {

      bean.increateTryTimes();
      session.setAttribute(SESSION_KEY_REGUSER, bean);

      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("captcha", "validate.captcha.wrong"));
    }

    EduUserCriteria ex = new EduUserCriteria();
    ex.createCriteria().andTelephoneEqualTo(telephone);
    List<EduUser> list = userService.selectByExample(ex);

    if (list != null && list.size() > 0) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("telephone", "validate.telephone.exsits"));
    }

    int vcode = RandomUtils.randomNumberBetween(100000, 999999);
    bean.setVcode(vcode + "");
    bean.setTelephone(telephone);
    sms.sendSms(bean.getTelephone(), bean.getVcode(), SmsType.REGISTRATION);
    // 发送验证码，并将验证码设置到RegUserBean中

    // 发送完验证码后，清理掉内存中的验证码信息
    bean.setCaptcha(null);
    bean.setCaptchaBase64(null);
    bean.resetTryTimes();

    session.setAttribute(SESSION_KEY_REGUSER, bean);
    model.addAttribute("data", bean);
    return reservice.response(HttpStatus.OK, "/vphone", "");
  }

  @ResponseBody
  @PostMapping("/vphone")
  public String vphone(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    RegUserBean bean = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (bean == null) {
      return reservice.response(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "/signup", "");
    }
    Req body = map.getBody();
    String vcode = body.getVcode();
    if (!vcode.equals(bean.getVcode())) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("vcode", "validate.vcode.wrong"));
    }
    // 插入数据库
    EduUser user = new EduUser();
    user.setUsername(bean.getUsername());
    user.setPassword(PassWordUtils.encodePassword(bean.getPassword()));
    user.setTelephone(bean.getTelephone());
    user.setRegDate(new Date());
    user.setEnabled((byte) 1);

    EduUserRole role = new EduUserRole();
    role.setRole("ROLE_USER");
    role.setUsername(user.getUsername());
    userService.signUpUser(user, role);
    model.addAttribute("data", bean);
    return reservice.response(HttpStatus.OK, "/done", "");
  }

  @GetMapping("/vcode")
  public String vcode(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "recovery";
    }
    model.addAttribute("data", reg);
    return "vcode";
  }

  @ResponseBody
  @PostMapping("/vcode")
  public String vcode(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    RegUserBean bean = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (bean == null) {
      return reservice.response(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "/recovery", "");
    }
    Req body = map.getBody();
    String vcode = body.getVcode();
    if (!vcode.equals(bean.getVcode())) {
      return reservice.response(HttpStatus.BAD_REQUEST,
          reservice.of("vcode", "validate.vcode.wrong"));
    }

    model.addAttribute("data", bean);
    return reservice.response(HttpStatus.OK, "/newpassword", "");
  }

  @GetMapping("/newpassword")
  public String newpassword(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "recovery";
    }
    model.addAttribute("data", reg);
    return "newpassword";
  }

  @GetMapping("/recoveried")
  public String recoveried(HttpSession session, Model model) {
    RegUserBean reg = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (reg == null) {
      return "recovery";
    }
    model.addAttribute("data", reg);
    return "recoveried";
  }

  @ResponseBody
  @PostMapping("/newpassword")
  public String newpassword(HttpSession session, @ModelAttribute ProtoRequest map, Model model) {
    RegUserBean bean = SessionUtils.getObj(session, SESSION_KEY_REGUSER, RegUserBean.class);
    if (bean == null) {
      return reservice.response(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "/recovery", "");
    }
    Req body = map.getBody();
    String password = body.getPassword();
    String confirmPassword = body.getConfirmPassword();
    HashMap<String, String> maap = Maps.newHashMap();

    if (!ValUtils.isValidPassword(8, 64, password)) {
      maap.putAll(reservice.of("password", "validate.password.wrong"));
    }
    if (!password.equals(confirmPassword)) {
      maap.putAll(reservice.of("confirmPassword", "validate.password.not.match"));
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    EduUser user = new EduUser();
    user.setUsername(bean.getUsername());
    user.setPassword(PassWordUtils.encodePassword(password));

    userService.updateByPrimaryKeySelective(user);

    session.setAttribute(SESSION_KEY_REGUSER, bean);

    model.addAttribute("data", bean);
    return reservice.response(HttpStatus.OK, "/recoveried", "");
  }
  @ResponseBody
  @PostMapping("/resetpwd")
  public String resetpwd( @ModelAttribute ProtoRequest map) {
    Req body = map.getBody();
    String username = body.getUsername();
    EduUser user = new EduUser();
    user.setUsername(username);
    user.setPassword(PassWordUtils.encodePassword(username));
    int i = userService.updateByPrimaryKeySelective(user);
    if(i>0){
      return reservice.response(HttpStatus.OK, "", "密码重置成功");
    }else {
      return reservice.response(HttpStatus.CONFLICT, "", "密码重置失败");
    }
  }
}
