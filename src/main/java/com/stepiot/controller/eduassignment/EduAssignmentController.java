package com.stepiot.controller.eduassignment;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.AssignmentDataBean;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.constants.CourseType;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduPythonCourseOutlineCriteria;
import com.stepiot.model.EduPythonCourseOutlineWithBLOBs;
import com.stepiot.model.EduUserCourseJuniorCriteria;
import com.stepiot.model.EduUserCourseJuniorWithBLOBs;
import com.stepiot.model.EduUserPythonCourseCriteria;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduClassService;
import com.stepiot.service.EduPythonCourseOutlineService;
import com.stepiot.service.EduScoreService;
import com.stepiot.service.EduStudentRefService;
import com.stepiot.service.EduUserCourseJuniorService;
import com.stepiot.service.EduUserPythonCourseService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduAssignmentController extends BaseController {

  private static final int PAGE_SIZE = 18;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduPythonCourseOutlineService oservice;

  @Autowired
  private EduUserPythonCourseService uservice;

  @Autowired
  private EduClassService cService;

  @Autowired
  private EduUserCourseJuniorService ucService;

  @Autowired
  private SelectService sis;

  @Autowired
  private EduScoreService scoreService;

  @Autowired
  private EduStudentRefService sservice;

  @RequestMapping("/eduassignment")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
    return "eduassignment/cards";
  }

  @RequestMapping(value = "/eduassignment/edit")
  public String eduassignmentEdit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    // String teacherId = authentication.getName();
    Req body = map.getBody();
    Builder builder = getBcbBuilder(request);
    Integer outlineId = body.getOutlineId();
    Integer currentLevel = body.getCurrentLevel();
    String username = body.getUsername();
    // 用于区分level和openlevel
    String type = body.getType();

    if (currentLevel == 0) {
      EduPythonCourseOutlineWithBLOBs outline = oservice.selectByPrimaryKey(outlineId);
      currentLevel = outline.getLevelFrom();
    }


    HashMap<String, Object> param = Maps.newHashMap();
    param.put("level", currentLevel);
    param.put("username", username);
    param.put("outlineId", outlineId);
    Map<String, Object> mycourse = uservice.selectByUserCourseId(param);
    Integer courseType = (Integer) mycourse.get("courseType");
    CourseType t = CourseType.valueOf(courseType);
    mycourse.put("username", username);

    List<Map<String, Object>> slist =
        sservice.selectStudentsById(ImmutableBiMap.of("username", username));
    if (slist != null && slist.size() > 0) {
      Map<String, Object> student = slist.get(0);
      String name = student.get("name").toString();
      String className = student.get("className").toString();
      builder.setItem(name + "【" + className + "】");
      builder.setSubitem(mycourse.get("outlineName").toString());
    }

    model.addAttribute("data", mycourse);

    model.addAttribute("bcb", BcbCreator.create(builder,
        BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.COMPLETE));

    int from = (Integer) mycourse.get("levelFrom");
    int to = (Integer) mycourse.get("levelTo");
    if (currentLevel == 0) {
      currentLevel = from;
    } else {
      // 对于openlevel,不进行校验
      if (type == null || !type.equals("open")) {
        currentLevel = Math.min(Math.max(currentLevel, from), to);
      }
    }

    mycourse.put("currentLevel", currentLevel);
    buildLevels(builder, currentLevel, from, to);
    int button = 0;
    if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      button = BcbCreator.ANSWER;
    }

    switch (t) {
      case NESTBLOCK:
        model.addAttribute("bcb", BcbCreator.create(builder,
            button | BcbCreator.CODE | BcbCreator.SETTODEFAULT | BcbCreator.DOWNLOAD));
        return "eduassignment/editnestblock";
      case WABC:
        model.addAttribute("bcb",
            BcbCreator.create(builder, button | BcbCreator.CODE | BcbCreator.DOWNLOAD));
        return "eduassignment/editwabc";
      case NULL:
        return "eduassignment/editnestblock";
      case PYTHON:
        model.addAttribute("bcb",
            BcbCreator.create(builder, button | BcbCreator.RUNBUTTON | BcbCreator.COLOR));
        return "eduassignment/editcode";
      case SPACE:
        model.addAttribute("bcb", BcbCreator.create(builder, button | BcbCreator.CODE
            | BcbCreator.RUNBUTTON | BcbCreator.SETTODEFAULT | BcbCreator.RESETBUTTON));
        return "eduassignment/editspace";
      case TURTLE:
        model.addAttribute("bcb",
            BcbCreator.create(builder, button | BcbCreator.CODE | BcbCreator.RUNBUTTON
                | BcbCreator.RESETBUTTON | BcbCreator.SETTODEFAULT | BcbCreator.SPEED));
        return "eduassignment/editturtle";
      case IOT:
        model.addAttribute("bcb",
            BcbCreator.create(builder, button | BcbCreator.CODE | BcbCreator.RUNBUTTON));
        return "eduassignment/editiot";
      default:
        return null;
    }


  }


  @RequestMapping(value = "/eduassignment/clazz")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    Builder builder = getBcbBuilder(request);
    EduPythonCourseOutlineWithBLOBs outline = null;
    int outlineId = 2;
    int level = 0;
    if (map == null || map.getBody() == null) {
      sis.outlineSelect(model, false, 0);
      // ss.departmentSelect(model, true, name, "");
      // ss.disciplineSelect(model, true, name, "", "");
      sis.singleClassSelect(model, true, name, "");
      outline = oservice.selectByPrimaryKey(outlineId);
    } else {
      Req body = map.getBody();

      String levelTop = null;
      String levelSecond = null;
      String levelThird = null;
      StudentQueryBean bean = new StudentQueryBean();

      levelTop = body.getLevelTop();
      levelSecond = body.getLevelSecond();
      levelThird = body.getLevelThird();

      outlineId = body.getOutlineId();
      level = body.getCurrentLevel();

      bean.setClassId(levelThird);
      bean.setDisciplineId(levelSecond);
      bean.setDepartmentId(levelTop);
      outline = oservice.selectByPrimaryKey(outlineId);
      if (level == 0) {
        level = outline.getLevelFrom();
      }

      sis.outlineSelect(model, false, outlineId);
      // ss.departmentSelect(model, true, name, bean.getDepartmentId());
      // ss.disciplineSelect(model, true, name, bean.getDepartmentId(), bean.getDisciplineId());
      sis.singleClassSelect(model, true, name, bean.getClassId());
      Map<String, Object> param = Maps.newHashMap();
      param.put("classId", bean.getClassId());
      param.put("outlineId", outlineId);
      param.put("level", level);

      buildLevels(builder, level, outline.getLevelFrom(), outline.getLevelTo());
      List<AssignmentDataBean> assignments = cService.queryAssignment(param);
      model.addAttribute("data", assignments);
    }
    builder.setItem(outline.getOutlineName());
    if (level == 0) {
      level = outline.getLevelFrom();
    }
    String levelTitles = outline.getLevelTitles();
    if (StringUtils.isNotEmpty(levelTitles)) {
      Map<String, Object> titles = GsonUtils.toMap(levelTitles);
      String key = ((level - outline.getLevelFrom()) + 1) + "";
      String _ltitle = (String) titles.get(key);
      if (StringUtils.isNotBlank(_ltitle)) {
        builder.setSubitem(key + ". " + _ltitle);
      }
    }

    model.addAttribute("bcb", BcbCreator.create(builder, BcbCreator.NOOP));
    CourseType t = CourseType.valueOf(outline.getCourseType());
    String returnPage = null;
    switch (t) {
      case IOT:
        returnPage = "eduassignment/iotlist";
        break;
      case NESTBLOCK:
        returnPage = "eduassignment/nestlist";
        break;
      case NULL:
        returnPage = "eduassignment/hellolist";
        break;
      case PYTHON:
        returnPage = "eduassignment/pylist";
        break;
      case SPACE:
        returnPage = "eduassignment/spacelist";
        break;
      case TURTLE:
        returnPage = "eduassignment/ttlist";
        break;
      case WABC:
        returnPage = "eduassignment/nestlist";
        break;
      default:
        break;
    }

    return returnPage;
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);
    List<Map<String, Object>> myCourse = uservice.listMyCourse(map);
    model.addAttribute("data", myCourse);
    long total = oservice.countByExample(new EduPythonCourseOutlineCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  @ResponseBody
  @PostMapping(value = "/eduassignment/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    String blockly = body.getBlockly();
    String code = body.getCode();
    int level = body.getCurrentLevel();
    String name = authentication.getName();

    EduUserPythonCourseCriteria ex = new EduUserPythonCourseCriteria();
    ex.createCriteria().andOutlineIdEqualTo(outlineId).andLevelEqualTo(level)
        .andUsernameEqualTo(name);
    List<EduUserPythonCourseWithBLOBs> list = uservice.selectByExampleWithBLOBs(ex);

    EduUserPythonCourseWithBLOBs userCourse = null;

    // 更新操作
    if (list != null && list.size() > 0) {
      userCourse = list.get(0);
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      if (uservice.updateByPrimaryKeySelective(userCourse) > 0) {
        scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
    // 新增操作
    else {
      userCourse = new EduUserPythonCourseWithBLOBs();
      userCourse.setCreateTime(new Date());
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      userCourse.setUsername(name);
      if (uservice.insertSelective(userCourse) > 0) {
        scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
  }

  private void buildLevels(com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      int current, int from, int to) {
    com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.Builder lb =
        com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.newBuilder();
    int i = 1;
    for (int x = from; x <= to; x++) {
      lb.setPage("" + i++);
      lb.setLevel("" + x);
      lb.setTip("ddddd");
      lb.setEnabled(true);
      if (current == x) {
        lb.setActive(true);
      } else {
        lb.setActive(false);
      }
      builder.addLevels(lb.build());
      lb.clear();
    }
  }

  @SuppressWarnings("unused")
  private void buildOpenLevel(com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      int level) {
    com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.Builder lb =
        com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.newBuilder();

    lb.setPage("Free");
    lb.setLevel(level + "");
    lb.setTip("ddddd");
    lb.setEnabled(true);
    lb.setActive(true);
    builder.addOpenlevels(lb);

  }

  @ResponseBody
  @PostMapping(value = "/eduassignment/flash")
  public String flash(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    // String name = authentication.getName();

    EduUserCourseJuniorCriteria iex = new EduUserCourseJuniorCriteria();
    iex.createCriteria().andCourseUidEqualTo(courseUid);
    EduUserCourseJuniorWithBLOBs course = new EduUserCourseJuniorWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(1);
    course.setLastUpdate(new Date());

    if (ucService.updateByExampleSelective(course, iex) == 1) {
      return reservice.response(HttpStatus.OK, "", "info.device.flashed");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "info.device.flashed.failed");
    }

  }

}
