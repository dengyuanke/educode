
package com.stepiot.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.stepiot.controller.base.BaseController;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectBean;
import com.stepiot.service.SelectService;
import com.stepiot.util.GsonUtils;

@Controller
public class SelectController extends BaseController {

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private SelectService ss;

  @ResponseBody
  @PostMapping(value = "/select/subItems")
  public String subItems(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    Req body = map.getBody();
    String levelSecond = body.getLevelSecond();
    String levelTop = body.getLevelTop();
    SelectBean bean = null;
    if (StringUtils.isNotEmpty(levelTop)) {
      bean = ss.selectDiscipline(true, name, levelTop, "");
    } else if (StringUtils.isNotEmpty(levelSecond)) {
      bean = ss.selectClass(true, name, levelSecond, null, "");
    }


    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("items", GsonUtils.fromObj(bean)));

  }


  @ResponseBody
  @PostMapping(value = "/select/allSubItems")
  public String allSubItems(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    Req body = map.getBody();
    String levelSecond = body.getLevelSecond();
    String levelTop = body.getLevelTop();
    SelectBean bean = null;
    if (StringUtils.isNotEmpty(levelTop)) {
      bean = ss.allSelectDiscipline(true, levelTop, "");
    } else if (StringUtils.isNotEmpty(levelSecond)) {
      bean = ss.allSelectClass(true, levelSecond, null, "");
    }


    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("items", GsonUtils.fromObj(bean)));

  }

}
