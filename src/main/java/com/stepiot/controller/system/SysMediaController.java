
package com.stepiot.controller.system;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.aws.AmazonS3Service;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduSysMedia;
import com.stepiot.model.EduSysMediaCriteria;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduSysMediaService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.PathUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class SysMediaController extends BaseController {

  private static final int PAGE_SIZE = 32;

  private static final String IMAGE = "code/img/";

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduSysMediaService mService;

  @Autowired
  private AmazonS3Service s3;

  @RequestMapping("/sysmedia")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.ADD));
    return "media/sysmedialist";
  }

  @PostMapping(value = "sysmedia/delen")
  public String delen(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Integer mediaId = Integer.parseInt(map.getBody().getPid());
    EduSysMediaCriteria ex = new EduSysMediaCriteria();
    com.stepiot.model.EduSysMediaCriteria.Criteria ext = ex.createCriteria();
    ext.andMediaIdEqualTo(mediaId);
    mService.deleteByExample(ex);
    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "media/sysmedialist";
  }

  @PostMapping(value = "sysmedia/edit")
  public String edit(Authentication authentication, @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    String mediaId = body.getId(0);

    EduSysMediaCriteria ex = new EduSysMediaCriteria();
    com.stepiot.model.EduSysMediaCriteria.Criteria ext = ex.createCriteria();
    ext.andMediaIdEqualTo(Integer.parseInt(mediaId));
    EduSysMedia sysMedia = mService.selectByExampleForOne(ex);
    model.addAttribute("data", sysMedia);

    return "media/sysmediaedit";
  }

  @RequestMapping(value = "sysmedia/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.UPLOAD));
    return "media/sysmediaadd";
  }

  @ResponseBody
  @PostMapping(value = "/sysmedia/upload", consumes = "multipart/form-data")
  public String handleFileUpload(@RequestParam("fileUpload") MultipartFile[] files)
      throws IOException, InterruptedException, ExecutionException {
    List<String> keys = Lists.newArrayList();
    for (int i = 0; i < files.length; i++) {
      keys.add(uploadToS3(files[i]));
    }

    Map<String, String> msgMap = Maps.newHashMap();
    msgMap.put("keys", GsonUtils.fromObj(keys));
    return reservice.response(HttpStatus.OK, "global.message.insert.success", msgMap);
  }

  private String uploadToS3(MultipartFile file)
      throws IOException, InterruptedException, ExecutionException {
    String contentType = file.getContentType();
    String filename = file.getOriginalFilename();

    String key =
        IMAGE + "sys" + "/" + PathUtils.curTime() + "/" + RandomUtils.genPass(4) + "/" + filename;

    byte[] data = file.getBytes();
    EduSysMedia sysmedia = new EduSysMedia();
    sysmedia.setCreateTime(new Date());
    sysmedia.setMediaName(filename);
    sysmedia.setMediaSize(data.length);
    sysmedia.setMimeType(contentType);
    sysmedia.setS3Path(key);
    sysmedia.setMediaSrc("/" + sysmedia.getS3Path());
    mService.insert(sysmedia);
    s3.pubObject(data, key);
//     future.get();
    return key;
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "course_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    EduSysMediaCriteria ex = new EduSysMediaCriteria();
    ex.setPageSize(PAGE_SIZE);
    ex.setLimitStart(Math.max(current - 1, 0) * PAGE_SIZE);
    ex.setOrderByClause("create_time desc");

    List<EduSysMedia> list = mService.selectByExample(ex);

    model.addAttribute("data", list);

    long total = mService.countByExample(new EduSysMediaCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

}
