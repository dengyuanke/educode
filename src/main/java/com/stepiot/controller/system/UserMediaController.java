
package com.stepiot.controller.system;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduCourseSeniorCriteria;
import com.stepiot.model.EduCourseSeniorWithBLOBs;
import com.stepiot.model.EduUserCourseSenior;
import com.stepiot.model.EduUserCourseSeniorCriteria;
import com.stepiot.model.EduUserCourseSeniorWithBLOBs;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduCourseSeniorService;
import com.stepiot.service.EduUserCourseSeniorService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class UserMediaController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduUserCourseSeniorService ucService;

  @Autowired
  private EduCourseSeniorService cService;

  @GetMapping("/usermedia")
  public String myblockly(HttpServletRequest request, Authentication authentication, Model model) {
    String name = authentication.getName();
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.ADD));

    fillPagination(model, 1, name);
    return "course/courselist";
  }

  @PostMapping("/usermedia")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    fillPagination(model, map.getBody().getCurrentPage(), name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "media/usermedia";
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "course_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<Map<String, Object>> myCourse = ucService.listMyCourse(map);
    model.addAttribute("data", myCourse);

    // 判断是否存在第一课，如果没有，创建

    EduCourseSeniorWithBLOBs nextCourse = cService.selectByPrimaryKey(1);// 修改为常量
    EduUserCourseSeniorCriteria ex = new EduUserCourseSeniorCriteria();
    ex.createCriteria().andUsernameEqualTo(name).andCourseIdEqualTo(nextCourse.getCourseId());

    long havefirst = ucService.countByExample(ex);
    if (havefirst == 0) {
      EduUserCourseSeniorWithBLOBs firstCourse = new EduUserCourseSeniorWithBLOBs();
      firstCourse.setCourseId(nextCourse.getCourseId());
      firstCourse.setCourseName(nextCourse.getCourseName());
      firstCourse.setCourseUid(RandomUtils.uuid());
      firstCourse.setCourseStatus(0);// TODO 修改为常量
      firstCourse.setCreateDate(new Date());
      firstCourse.setLastUpdate(firstCourse.getCreateDate());
      firstCourse.setUsername(name);
      ucService.insert(firstCourse);
    }

    long total = cService.countByExample(new EduCourseSeniorCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  @PostMapping(value = "usermedia/delen")
  public String delen(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Integer courseId = Integer.parseInt(map.getBody().getPid());
    EduUserCourseSeniorCriteria ex = new EduUserCourseSeniorCriteria();
    com.stepiot.model.EduUserCourseSeniorCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andCourseIdEqualTo(courseId);
    ucService.deleteByExample(ex);
    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "media/usermedia";
  }

  @PostMapping(value = "usermedia/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String courseUid = body.getId(0);

    EduUserCourseSeniorCriteria ex = new EduUserCourseSeniorCriteria();
    com.stepiot.model.EduUserCourseSeniorCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    EduUserCourseSenior userCourse = ucService.selectByExampleForOne(ex);
    model.addAttribute("data", userCourse);

    if (body.getCourseId() != 0) {
      EduCourseSeniorWithBLOBs course = cService.selectByPrimaryKey(body.getCourseId());
      model.addAttribute("course", course);
      model.addAttribute("bcb",
          BcbCreator.create(super.getBcbBuilder(request), BcbCreator.CODE | BcbCreator.DOWNLOAD
              | BcbCreator.SAVEBLOCKLY | BcbCreator.COMPLETE | BcbCreator.HISTORY));
      return "media/usermedia";
    }

    EduCourseSeniorWithBLOBs course = cService.selectByPrimaryKey(userCourse.getCourseId());
    model.addAttribute("course", course);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.DOACTION));
    return "media/usermedia";
  }

  @ResponseBody
  @PostMapping(value = "usermedia/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    String name = authentication.getName();

    EduUserCourseSeniorCriteria iex = new EduUserCourseSeniorCriteria();
    iex.createCriteria().andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    EduUserCourseSeniorWithBLOBs course = new EduUserCourseSeniorWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(0);
    course.setLastUpdate(new Date());

    if (ucService.updateByExampleSelective(course, iex) == 1) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }

}
