
package com.stepiot.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.stepiot.constants.Cons;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduConfigPersonal;
import com.stepiot.model.EduPubDown;
import com.stepiot.model.EduPubDownCriteria;
import com.stepiot.model.EduWiki;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduConfigPersonalService;
import com.stepiot.service.EduPubDownService;
import com.stepiot.service.EduWikiService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;

@Controller
public class SysController extends BaseController {

  @Autowired
  private EduConfigPersonalService service;

  @Autowired
  private EduWikiService wiki;

  @Autowired
  private EduPubDownService downService;

  @Autowired
  private ProtoResponseService reservice;

  @GetMapping("/settingbase")
  public String settings(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "sys/settings";
  }

  @GetMapping("/settheme")
  public String settheme(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    return "sys/settheme";
  }

  @ResponseBody
  @PostMapping(value = "/settheme/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String theme = body.getValue();
    String name = authentication.getName();

    if (StringUtils.isEmpty(theme)) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }
    String configItem = null;
    Map<String, Object> com = null;
    // 系统主题设置
    if (theme.indexOf(Cons.CONFIG_BLOCKLY_THEME) == 0) {
      configItem = Cons.CONFIG_BLOCKLY_THEME;
      theme = StringUtils.substringAfter(theme, Cons.CONFIG_BLOCKLY_THEME);
      com = service.getBlocklyThemeConfig(name);
    } else if (theme.indexOf(Cons.CONFIG_SYS_THEME) == 0) {
      configItem = Cons.CONFIG_SYS_THEME;
      theme = StringUtils.substringAfter(theme, Cons.CONFIG_SYS_THEME);
      com = service.getSysThemeConfig(name);
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    EduConfigPersonal config = new EduConfigPersonal();
    config.setConfigItem(configItem);
    config.setConfigValue(theme);
    config.setUsername(name);
    if (com == null) {
      if (service.insert(config) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    } else {
      config.setConfigId((Integer) com.get("configId"));

      if (service.updateByPrimaryKey(config) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }

    }
  }

  @GetMapping("/downloads")
  public String downloads(Authentication authentication, @ModelAttribute ProtoRequest map,
      HttpServletRequest request, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    List<EduPubDown> downs = downService.selectByExample(new EduPubDownCriteria());
    model.addAttribute("data", downs);
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
    return "sys/downloads";
  }



  @GetMapping("/faq")
  public String faq(Authentication authentication, HttpServletRequest request, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));

    Integer wikiId = Cons.STUDENT_FAQ_WIKI_ID;

    if (containsRole(RoleEnum.ROLE_STUDENT.getValue())) {
      wikiId = Cons.STUDENT_FAQ_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      wikiId = Cons.TEACHER_FAQ_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_COADMIN.getValue())) {
      wikiId = Cons.TEACHER_FAQ_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_DEV.getValue())) {
      wikiId = Cons.STUDENT_FAQ_WIKI_ID;
    }
    EduWiki wk = wiki.selectByPrimaryKey(wikiId);
    if (wk == null) {
      wk = new EduWiki();
    }
    ImmutableMap<String, String> md = ImmutableBiMap.of("mdoc", wk.getWikiBody());
    model.addAttribute("md", md);
    return "sys/faq";
  }


  @GetMapping("/help")
  public String help(Authentication authentication, HttpServletRequest request, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));

    Integer wikiId = Cons.STUDENT_HELP_WIKI_ID;

    if (containsRole(RoleEnum.ROLE_STUDENT.getValue())) {
      wikiId = Cons.STUDENT_HELP_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      wikiId = Cons.TEACHER_HELP_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_COADMIN.getValue())) {
      wikiId = Cons.TEACHER_HELP_WIKI_ID;
    } else if (containsRole(RoleEnum.ROLE_DEV.getValue())) {
      wikiId = Cons.STUDENT_HELP_WIKI_ID;
    }
    EduWiki wk = wiki.selectByPrimaryKey(wikiId);
    if (wk == null) {
      wk = new EduWiki();
    }
    ImmutableMap<String, String> md = ImmutableBiMap.of("mdoc", wk.getWikiBody());
    model.addAttribute("md", md);
    return "sys/faq";
  }

  @ResponseBody
  @RequestMapping(value = "/health")
  public String aws(Authentication authentication, @ModelAttribute ProtoRequest map, Model model) {
    wiki.selectByPrimaryKey(1);
    System.nanoTime();
    return reservice.response(HttpStatus.OK, "", "global.message.update.success");
  }
}
