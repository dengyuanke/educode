package com.stepiot.controller.eduwiki;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduWiki;
import com.stepiot.model.EduWikiCriteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduWikiService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduWikiController extends BaseController {

  private static final int PAGE_SIZE = 32;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduWikiService service;


  @RequestMapping("/eduwiki")
  public String eduwiki(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
    return "eduwiki/wikilist";
  }

  @RequestMapping(value = "eduwiki/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    Integer wikiId = Integer.parseInt(body.getId(0));
    EduWiki wiki = service.selectByPrimaryKey(wikiId);
    Builder builder = getBcbBuilder(request);
    builder.setItem(wiki.getWikiTitle());
    model.addAttribute("data", wiki);
    model.addAttribute("bcb", BcbCreator.create(builder, BcbCreator.SAVE));
    return "eduwiki/wikiedit";
  }


  @ResponseBody
  @PostMapping(value = "/eduwiki/loadwiki")
  public String loadWiki(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    Integer wikiId = Integer.parseInt(body.getId(0));
    EduWiki wiki = service.selectByPrimaryKey(wikiId);

    ImmutableMap<String, String> md = ImmutableBiMap.of("wikiBody", wiki.getWikiBody());

    return reservice.response(HttpStatus.OK, md);

  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    EduWikiCriteria ex = new EduWikiCriteria();
    ex.setPageSize(PAGE_SIZE);
    ex.setLimitStart(Math.max(current - 1, 0) * PAGE_SIZE);
    List<Map<String, Object>> wikis = service.selectMapByExample(ex);
    model.addAttribute("data", wikis);
    long total = service.countByExample(new EduWikiCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  @ResponseBody
  @PostMapping(value = "/eduwiki/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();

    Integer wikiId = Integer.parseInt(body.getId(0));
    String contents = body.getValue();

    EduWiki wiki = service.selectByPrimaryKey(wikiId);


    // 更新操作
    if (wiki != null) {
      wiki.setWikiBody(contents);
      if (service.updateByPrimaryKeySelective(wiki) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
    // 新增操作
    else {
      // TOOD denny
      return null;
    }
  }

}
