
package com.stepiot.controller.mycreation;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduUserMicrobit;
import com.stepiot.model.EduUserMicrobitCriteria;
import com.stepiot.model.EduUserMicrobitWithBLOBs;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.EduUserMicrobitService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class MyCreationMicrobitController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduUserMicrobitService bservice;


  @GetMapping("/mycodemicrobit")
  public String myblockly(HttpServletRequest request, Authentication authentication, Model model) {
    String name = authentication.getName();
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.ADD));

    fillPagination(model, 1, name);
    return "mycode/mycodemicrobits";
  }

  @PostMapping("/mycodemicrobit")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    fillPagination(model, map.getBody().getCurrentPage(), name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "mycode/mycodemicrobits";
  }

  @PostMapping(value = "/mycodemicrobit/delen")
  public String delen(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    String blocklyId = map.getBody().getPid();
    EduUserMicrobitCriteria ex = new EduUserMicrobitCriteria();
    com.stepiot.model.EduUserMicrobitCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);
    bservice.deleteByExample(ex);

    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "mycode/mycodemicrobits";

  }

  @PostMapping(value = "/mycodemicrobit/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    String blocklyId = map.getBody().getId(0);

    EduUserMicrobitCriteria ex = new EduUserMicrobitCriteria();
    com.stepiot.model.EduUserMicrobitCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);
    EduUserMicrobit bit = bservice.selectByExampleForOne(ex);
    if (bit != null) {
      model.addAttribute("data", bit);
    }

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request),
        BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.SHARE));
    return "mycode/newmicrobit";
  }

  private void fillPagination(Model model, int current, String name) {
    EduUserMicrobitCriteria ex = new EduUserMicrobitCriteria();
    ex.setPageSize(PAGE_SIZE);

    ex.setLimitStart(Math.max(current - 1, 0) * ex.getPageSize());
    com.stepiot.model.EduUserMicrobitCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name);
    List<EduUserMicrobitWithBLOBs> data = bservice.selectByExampleWithBLOBs(ex);
    model.addAttribute("data", data);

    long total = bservice.countByExample(ex);
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> map = GsonUtils.toMap(json);
    model.addAttribute("pagination", map);
  }

  @GetMapping(value = "/mycodemicrobit/add")
  public String create(HttpServletRequest request, Authentication authentication, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "menu.code.microbit");
    }


    EduUserMicrobitWithBLOBs userCourse = new EduUserMicrobitWithBLOBs();

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request),
        BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.SHARE));

    model.addAttribute("data", userCourse);
    return "mycode/newmicrobit";
  }

  @ResponseBody
  @PostMapping(value = "/mycodemicrobit/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String blocklyId = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String titleName = body.getTitleName();
    String name = authentication.getName();
    EduUserMicrobitWithBLOBs blockly = new EduUserMicrobitWithBLOBs();
    blockly.setBlocklyXml(blocklyXml);
    blockly.setLastEdit(new Date());
    blockly.setTitleName(titleName);
    blockly.setUsername(name);
    blockly.setBlocklyId(blocklyId);
    // 新增
    if ("-1".equals(blocklyId)) {
      blocklyId = RandomUtils.genPass(12);
      blockly.setBlocklyId(blocklyId);
      bservice.insert(blockly);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success",
          ImmutableBiMap.of("blocklyId", blocklyId, "titleName", titleName));
    } else {
      EduUserMicrobitCriteria ex = new EduUserMicrobitCriteria();
      ex.createCriteria().andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);

      if (bservice.updateByExampleSelective(blockly, ex) == 1) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success",
            ImmutableBiMap.of("blocklyId", blocklyId, "titleName", titleName));
      } else {
        bservice.insert(blockly);
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }


  }

}
