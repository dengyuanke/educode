package com.stepiot.controller.edunotice;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ProtocolStringList;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.EduNoticeBean;
import com.stepiot.beans.EduTreeNodeBean;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.model.EduNoticeAttachmentCriteria;
import com.stepiot.model.EduNoticeAttachmentCriteria.Criteria;
import com.stepiot.model.EduNoticeWithBLOBs;
import com.stepiot.model.EduStudentClassRef;
import com.stepiot.model.EduStudentClassRefCriteria;
import com.stepiot.model.EduSysMedia;
import com.stepiot.model.EduTeacherClassRef;
import com.stepiot.model.EduTeacherClassRefCriteria;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduClassService;
import com.stepiot.service.EduNoticeAttachmentService;
import com.stepiot.service.EduNoticeService;
import com.stepiot.service.EduStudentRefService;
import com.stepiot.service.EduSysMediaService;
import com.stepiot.service.EduTeacherRefService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.ListUtils;
import com.stepiot.util.MimeUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class EduNoticeManController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduNoticeService nservice;

  @Autowired
  private EduNoticeAttachmentService aService;

  @Autowired
  private EduSysMediaService mService;



  @Autowired
  private EduClassService cService;

  @Autowired
  private EduStudentRefService stuService;
  @Autowired
  private EduTeacherRefService teaService;


  @RequestMapping("/edunoticeman")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.ADD));
    return "edunoticeman/list";
  }


  @PostMapping(value = "edunoticeman/markAsRead")
  public String markAsRead(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    ProtocolStringList list = map.getBody().getIdList();

    nservice.markNoticeAsReadByIds(name, ListUtils.stringListToIntegerList(list));
    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.ADD));
    return "edunoticeman/list";

  }


  @Transactional
  @ResponseBody
  @PostMapping(value = "edunoticeman/send")
  public String send(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    Req body = map.getBody();
    ProtocolStringList attaIdList = body.getAttaIdList();
    ProtocolStringList clazzIdList = body.getClazzIdList();
    String noticeBody = body.getProjectDesc();
    String titleName = body.getTitleName();


    EduNoticeWithBLOBs notice = new EduNoticeWithBLOBs();
    notice.setCreateTime(new Date());
    notice.setCreateUser(name);
    notice.setNoticeBody(noticeBody);
    notice.setNoticeSubject(titleName);
    notice.setToClasses(GsonUtils.toJson(clazzIdList));
    notice.setUuid(RandomUtils.uuid());
    int noticeId = nservice.insertThenReturnPrimaryKey(notice);

    if (attaIdList != null && attaIdList.size() > 0) {

      List<EduSysMedia> medias = mService.selectByS3Paths(attaIdList);
      if (medias != null && medias.size() > 0) {
        for (int i = 0; i < medias.size(); i++) {
          EduSysMedia sysMedia = medias.get(i);
          EduNoticeAttachment att = new EduNoticeAttachment();
          att.setNoticeId(noticeId);
          att.setCreateTime(new Date());
          att.setLocation(sysMedia.getS3Path());
          att.setAttaName(sysMedia.getMediaName());
          att.setCssClass(MimeUtils.getMaterialcssByMimetype(sysMedia.getMimeType()));
          att.setFileSize(sysMedia.getMediaSize());
          att.setFileType(sysMedia.getMimeType());
          aService.insert(att);
        }
      }

    }

    if (containsRole(RoleEnum.ROLE_COADMIN.getValue())) {
      // 值发送给老师（10）；只发送给学生（01）；发送全部（11）
      String type = body.getType();
      if (type.equals("11")) {
        sendToTeachers(clazzIdList, noticeId);
        sendToStudents(clazzIdList, noticeId);
      } else if (type.equals("10")) {
        sendToTeachers(clazzIdList, noticeId);
      } else if (type.equals("01")) {
        sendToStudents(clazzIdList, noticeId);
      } else {
        return reservice.response(HttpStatus.BAD_REQUEST, "/edunoticeman/",
            "global.message.update.fail");
      }
    } else if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      sendToStudents(clazzIdList, noticeId);
    }

    return reservice.response(HttpStatus.OK, "/edunoticeman/", "global.message.update.success");

  }


  @Async
  private void sendToStudents(ProtocolStringList clazzIdList, int noticeId) {
    Set<String> studentIds = Sets.newHashSet();
    if (clazzIdList != null && clazzIdList.size() > 0) {
      EduStudentClassRefCriteria ex = new EduStudentClassRefCriteria();
      com.stepiot.model.EduStudentClassRefCriteria.Criteria ext = ex.createCriteria();
      ext.andClassIdIn(clazzIdList);

      List<EduStudentClassRef> sList = stuService.selectByExample(ex);

      if (sList != null && sList.size() > 0) {
        for (int x = 0; x < sList.size(); x++) {
          EduStudentClassRef ref = sList.get(x);
          String username = ref.getStudentId();
          studentIds.add(username);
        }
      }
      nservice.insertNoticeRecipient(studentIds, noticeId);
    }
  }

  @Async
  private void sendToTeachers(ProtocolStringList clazzIdList, int noticeId) {
    Set<String> teacherIds = Sets.newHashSet();
    if (clazzIdList != null && clazzIdList.size() > 0) {
      EduTeacherClassRefCriteria ex = new EduTeacherClassRefCriteria();
      com.stepiot.model.EduTeacherClassRefCriteria.Criteria criteria = ex.createCriteria();
      criteria.andClassIdIn(clazzIdList);

      List<EduTeacherClassRef> tlist = teaService.selectByExample(ex);

      if (tlist != null && tlist.size() > 0) {
        for (int x = 0; x < tlist.size(); x++) {
          EduTeacherClassRef ref = tlist.get(x);
          String username = ref.getTearchId();
          teacherIds.add(username);
        }
      }
      nservice.insertNoticeRecipient(teacherIds, noticeId);
    }
  }

  @ResponseBody
  @PostMapping(value = "edunoticeman/countNews")
  public String countNews(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    long unreaded = nservice.countUnReadedNews(name);
    model.addAttribute("unReadedCounts", unreaded);
    if (unreaded > 0) {
      int showItemCounts = (int) Math.min(5, unreaded);
      List<EduNoticeBean> items = nservice.listLatestNews(showItemCounts, name);
      model.addAttribute("unReadedNews", items);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success",
          ImmutableBiMap.of("news", GsonUtils.fromObj(items), "counts", unreaded + ""));
    }
    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("counts", unreaded + ""));

  }

  @RequestMapping(value = "edunoticeman/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String noticeId = body.getId(0);
    Map<String, Object> maps = Maps.newHashMap();
    maps.put("username", name);
    maps.put("noticeId", noticeId);

    List<EduNoticeBean> myNotice = nservice.listSentNotice(maps);
    EduNoticeBean noticeBean = myNotice.get(0);
    List<Integer> ids = noticeBean.getAttachmentIds();
    if (ids != null && ids.size() > 0) {
      EduNoticeAttachmentCriteria ex = new EduNoticeAttachmentCriteria();
      Criteria exam = ex.createCriteria();
      exam.andAttachmentIdIn(ids);
      List<EduNoticeAttachment> atts = aService.selectByExample(ex);
      model.addAttribute("attachments", atts);
    }

    String classes = noticeBean.getToClasses();
    TypeToken<List<String>> tlist = new TypeToken<List<String>>() {};
    List<String> clist = GsonUtils.toTypedList(classes, tlist);
    Set<String> set = new HashSet<String>(clist);
    this.buildTree(model, name, true, set, false);

    model.addAttribute("data", myNotice.get(0));

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    return "edunoticeman/view";

  }



  @RequestMapping(value = "edunoticeman/add")
  public String composeNotice(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    this.buildTree(model, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SEND));
    return "edunoticeman/compose";

  }

  private void buildTree(Model model, String name) {
    this.buildTree(model, name, false, null, false);
  }

  private void buildTree(Model model, String name, boolean opened, Set<String> selectedSet,
      boolean disabled) {
    if (selectedSet == null) {
      selectedSet = Sets.newHashSet();
    }
    Map<String, Object> param = Maps.newHashMap();
    List<Map<String, Object>> list = Lists.newArrayList();
    if (containsRole(RoleEnum.ROLE_COADMIN.getValue())) {
      list.addAll(cService.listClasses(param));
    } else if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      param.put("tearchId", name);
      list.addAll(cService.feedDepSelector(param));

    } else {
      return;
    }


    Set<String> classes = Sets.newHashSet();
    Set<EduTreeNodeBean> classSet = Sets.newHashSet();
    Set<EduTreeNodeBean> disSet = Sets.newHashSet();
    Set<EduTreeNodeBean> depSet = Sets.newHashSet();
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> item = list.get(i);
      EduTreeNodeBean bean = new EduTreeNodeBean();
      bean.setId((String) item.get("classId"));
      bean.setText((String) item.get("className"));
      bean.setParent((String) item.get("disciplineId"));
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }

      classSet.add(bean);
      classes.add("\"" + (String) item.get("classId") + "\"");

      bean = new EduTreeNodeBean();
      bean.setId((String) item.get("disciplineId"));
      bean.setText((String) item.get("disciplineName"));
      bean.setParent((String) item.get("departmentId"));
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }
      disSet.add(bean);

      bean = new EduTreeNodeBean();
      bean.setId((String) item.get("departmentId"));
      bean.setText((String) item.get("departmentName"));
      bean.setParent("#");
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }
      depSet.add(bean);
    }

    classSet.addAll(disSet);
    classSet.addAll(depSet);


    model.addAttribute("classes", classes);
    model.addAttribute("classTreeStr", GsonUtils.fromObj(classSet));
  }


  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<EduNoticeBean> myNotice = nservice.listSentNotice(map);
    model.addAttribute("data", myNotice);

    long total = nservice.totalSent(name);
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

}
