package com.stepiot.controller.edunotice;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ProtocolStringList;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.EduNoticeBean;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.model.EduNoticeAttachmentCriteria;
import com.stepiot.model.EduNoticeAttachmentCriteria.Criteria;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduNoticeAttachmentService;
import com.stepiot.service.EduNoticeService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.ListUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduNoticeController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduNoticeService nservice;

  @Autowired
  private EduNoticeAttachmentService aService;

  @RequestMapping("/edunotice")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.MARKASREAD));
    return "edunotice/list";
  }


  @PostMapping(value = "edunotice/markAsRead")
  public String markAsRead(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    ProtocolStringList list = map.getBody().getIdList();

    nservice.markNoticeAsReadByIds(name, ListUtils.stringListToIntegerList(list));
    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.MARKASREAD));
    return "edunotice/list";

  }

  @ResponseBody
  @PostMapping(value = "edunotice/countNews")
  public String countNews(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    long unreaded = nservice.countUnReadedNews(name);
    model.addAttribute("unReadedCounts", unreaded);
    if (unreaded > 0) {
      int showItemCounts = (int) Math.min(5, unreaded);
      List<EduNoticeBean> items = nservice.listLatestNews(showItemCounts, name);
      model.addAttribute("unReadedNews", items);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success",
          ImmutableBiMap.of("news", GsonUtils.fromObj(items), "counts", unreaded + ""));
    }
    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("counts", unreaded + ""));

  }

  @RequestMapping(value = "edunotice/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String noticeId = body.getId(0);
    Map<String, Object> maps = Maps.newHashMap();
    maps.put("username", name);
    maps.put("noticeId", noticeId);
    nservice.markNoticeAsReadById(name, Integer.parseInt(noticeId));
    List<EduNoticeBean> myNotice = nservice.listMyNotice(maps);
    EduNoticeBean noticeBean = myNotice.get(0);
    List<Integer> ids = noticeBean.getAttachmentIds();
    if (ids != null && ids.size() > 0) {
      EduNoticeAttachmentCriteria ex = new EduNoticeAttachmentCriteria();
      Criteria exam = ex.createCriteria();
      exam.andAttachmentIdIn(ids);
      List<EduNoticeAttachment> atts = aService.selectByExample(ex);
      model.addAttribute("attachments", atts);
    }
    model.addAttribute("data", myNotice.get(0));

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    return "edunotice/edit";
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<EduNoticeBean> myNotice = nservice.listMyNotice(map);
    model.addAttribute("data", myNotice);

    long total = nservice.totalRecipient(name);
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

}
