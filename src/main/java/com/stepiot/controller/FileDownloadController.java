package com.stepiot.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;

import com.stepiot.aws.AmazonS3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduNoticeAttachmentService;

@Controller
public class FileDownloadController extends BaseController {


  @Autowired
  private AmazonS3Service s3;

  @Autowired
  private EduNoticeAttachmentService aService;

  @RequestMapping(path = "/edunotice/downlood", method = RequestMethod.GET)
  public ResponseEntity<Resource> download(HttpServletRequest request,
      Authentication authentication, @ModelAttribute ProtoRequest map, Model model)
      throws IOException {
    Req body = map.getBody();
    String attachId = body.getPid();
    EduNoticeAttachment attachement = aService.selectByPrimaryKey(Integer.parseInt(attachId));
    String s3location = attachement.getLocation();
    byte[] data = s3.getS3Object(s3location);
    Path p = Paths.get(s3location);

    ByteArrayResource resource = new ByteArrayResource(data);
    HttpHeaders headers = new HttpHeaders();
    headers.setCacheControl(CacheControl.noCache());

    String fileName = URLEncoder.encode(p.getFileName().toString(), "UTF-8");
    headers.setContentDisposition(
        ContentDisposition.parse("attachment; filename=\"" + fileName + "\""));
    headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
    headers.setPragma("no-cache");
    headers.setExpires(0);
    return ResponseEntity.ok().headers(headers).contentLength(data.length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
  }
}
