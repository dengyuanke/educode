
package com.stepiot.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.constants.Cons;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.AiEduCourseCriteria;
import com.stepiot.model.AiEduCourseWithBLOBs;
import com.stepiot.model.AiEduUserCourse;
import com.stepiot.model.AiEduUserCourseCriteria;
import com.stepiot.model.AiEduUserCourseWithBLOBs;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.AiEduCourseService;
import com.stepiot.service.AiEduUserCourseService;
import com.stepiot.service.FirmwareHex;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class AiEduController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private AiEduUserCourseService ucService;

  @Autowired
  private AiEduCourseService cService;

  @Autowired
  FirmwareHex hex = null;
  HttpHeaders headers = new HttpHeaders();
  {
    headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=microbit.hex");
  }

  @GetMapping("/aiedu")
  public String myblockly(Authentication authentication, Model model, HttpServletRequest request) {
    String name = authentication.getName();
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));

    fillPagination(model, 1, name);
    return "aiedu/aiedulist";
  }

  @PostMapping("/aiedu")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    fillPagination(model, map.getBody().getCurrentPage(), name);
    return "aiedu/aiedulist";
  }

  @PostMapping(value = "aiedu/delen")
  public String delen(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Integer courseId = Integer.parseInt(map.getBody().getPid());
    AiEduUserCourseCriteria ex = new AiEduUserCourseCriteria();
    com.stepiot.model.AiEduUserCourseCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andCourseIdEqualTo(courseId);
    ucService.deleteByExample(ex);
    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "aiedu/aiedulist";
  }

  @PostMapping(value = "aiedu/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String courseUid = body.getId(0);

    AiEduUserCourseCriteria ex = new AiEduUserCourseCriteria();
    com.stepiot.model.AiEduUserCourseCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    AiEduUserCourse userCourse = ucService.selectByExampleForOne(ex);
    model.addAttribute("data", userCourse);

    if (body.getCourseId() != 0) {
      AiEduCourseWithBLOBs course = cService.selectByPrimaryKey(body.getCourseId());
      model.addAttribute("course", course);
      model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request),
          BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.COMPLETE));
      return "aiedu/aieduedit";
    }

    AiEduCourseWithBLOBs course = cService.selectByPrimaryKey(userCourse.getCourseId());
    model.addAttribute("course", course);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.DOACTION));
    return "aiedu/aiedudoc";
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "course_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<Map<String, Object>> myCourse = ucService.listMyCourse(map);
    model.addAttribute("data", myCourse);

    // 判断是否存在第一课，如果没有，创建

    AiEduCourseWithBLOBs nextCourse = cService.selectByPrimaryKey(1);// 修改为常量
    AiEduUserCourseCriteria ex = new AiEduUserCourseCriteria();
    ex.createCriteria().andUsernameEqualTo(name).andCourseIdEqualTo(nextCourse.getCourseId());

    long havefirst = ucService.countByExample(ex);
    if (havefirst == 0) {
      AiEduUserCourseWithBLOBs firstCourse = new AiEduUserCourseWithBLOBs();
      firstCourse.setCourseId(nextCourse.getCourseId());
      firstCourse.setCourseName(nextCourse.getCourseName());
      firstCourse.setCourseUid(RandomUtils.uuid());
      firstCourse.setCourseStatus(0);// TODO 修改为常量
      firstCourse.setCreateDate(new Date());
      firstCourse.setLastUpdate(firstCourse.getCreateDate());
      firstCourse.setUsername(name);
      firstCourse.setCourseBlockly(Cons.BOOTBLOCKLY);
      ucService.insert(firstCourse);
    }

    long total = cService.countByExample(new AiEduCourseCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  @ResponseBody
  @PostMapping(value = "aiedu/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    String name = authentication.getName();

    AiEduUserCourseCriteria iex = new AiEduUserCourseCriteria();
    iex.createCriteria().andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    AiEduUserCourseWithBLOBs course = new AiEduUserCourseWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(0);
    course.setLastUpdate(new Date());

    if (ucService.updateByExampleSelective(course, iex) == 1) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }


  @ResponseBody
  @PostMapping(value = "aiedu/complete")
  public String complete(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    int courseId = body.getCourseId();
    String name = authentication.getName();

    AiEduUserCourseCriteria iex = new AiEduUserCourseCriteria();
    iex.createCriteria().andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    AiEduUserCourseWithBLOBs course = new AiEduUserCourseWithBLOBs();

    course.setCourseStatus(3);
    course.setLastUpdate(new Date());

    AiEduUserCourseWithBLOBs next = null;
    // TODO 修改为数据库获取，课程总数
    if (courseId < 19) {
      AiEduCourseWithBLOBs nextCourse = cService.selectByPrimaryKey(courseId + 1);

      AiEduUserCourseCriteria ex = new AiEduUserCourseCriteria();
      ex.createCriteria().andUsernameEqualTo(name).andCourseIdEqualTo(nextCourse.getCourseId());

      long haveNext = ucService.countByExample(ex);
      if (haveNext == 0) {
        next = new AiEduUserCourseWithBLOBs();
        next.setCourseId(nextCourse.getCourseId());
        next.setCourseName(nextCourse.getCourseName());
        next.setCourseUid(RandomUtils.uuid());
        next.setCourseStatus(0);// TODO 修改为常量
        next.setCreateDate(new Date());
        next.setLastUpdate(next.getCreateDate());
        next.setCourseBlockly(Cons.BOOTBLOCKLY);
        next.setUsername(name);
      }

    }

    if (ucService.complete(course, iex, next) == 1) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }


  @ResponseBody
  @PostMapping(value = "aiedu/flash")
  public String flash(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    String name = authentication.getName();

    AiEduUserCourseCriteria iex = new AiEduUserCourseCriteria();
    iex.createCriteria().andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    AiEduUserCourseWithBLOBs course = new AiEduUserCourseWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(1);
    course.setLastUpdate(new Date());

    if (ucService.updateByExampleSelective(course, iex) == 1) {
      return reservice.response(HttpStatus.OK, "", "info.device.flashed");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "info.device.flashed.failed");
    }

  }

  @PostMapping(value = "aiedu/download")
  public ResponseEntity<ByteArrayResource> download(Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    String name = authentication.getName();

    AiEduUserCourseCriteria iex = new AiEduUserCourseCriteria();
    iex.createCriteria().andUsernameEqualTo(name).andCourseUidEqualTo(courseUid);
    AiEduUserCourseWithBLOBs course = new AiEduUserCourseWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(1);
    course.setLastUpdate(new Date());

    ucService.updateByExampleSelective(course, iex);

    ByteArrayResource resource = new ByteArrayResource(hex.toMicrobitHex(blocklyCode));

    return ResponseEntity.ok().headers(headers).contentLength(resource.contentLength())
        .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
  }

}
