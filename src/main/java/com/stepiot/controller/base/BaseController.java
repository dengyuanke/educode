package com.stepiot.controller.base;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import com.google.common.collect.Sets;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.constants.Cons;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.UserBean;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

public abstract class BaseController {

  protected UserBean getUserBean() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return null;
    }
    Object principal = authentication.getPrincipal();
    if (principal == null) {
      return null;
    }
    if (!(principal instanceof UserBean)) {
      return null;
    }
    return (UserBean) principal;
  }

  protected com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder getBcbBuilder(
      HttpServletRequest request) {


    Object object = request.getAttribute(Cons.REQUEST_ATTR_BREADCRUMB_BUILDER);

    if (object == null) {
      Builder builder = com.stepiot.proto.BreadcrumbProto.Breadcrumb.newBuilder();
      request.setAttribute(Cons.REQUEST_ATTR_BREADCRUMB_BUILDER, builder);
    }


    return (com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder) request
        .getAttribute(Cons.REQUEST_ATTR_BREADCRUMB_BUILDER);
  }


  protected void genPager(Model model, int current, long total, int pageSize) {
    Pagination pagination = PaginationUtils.genPagination(current, total, pageSize);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  protected Set<String> getUserRolesName() {
    Set<String> roleSet = Sets.newHashSet();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return roleSet;
    }
    Object principal = authentication.getPrincipal();
    if (principal == null) {
      return roleSet;
    }
    if (!(principal instanceof UserDetails)) {
      return roleSet;
    }

    UserDetails details = (UserDetails) authentication.getPrincipal();
    if (details == null) {
      return roleSet;
    }
    Collection<? extends GrantedAuthority> roles = details.getAuthorities();

    Iterator<? extends GrantedAuthority> it = roles.iterator();
    while (it.hasNext()) {
      roleSet.add(it.next().toString());
    }
    return roleSet;
  }

  /**
   * 判断当前登录用户是否包含指定的角色
   * 
   * @param rolename
   * @return
   */
  protected boolean containsRole(String rolename) {
    return getUserRolesName().contains(rolename);
  }

}
