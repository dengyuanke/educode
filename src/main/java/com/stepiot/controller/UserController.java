
package com.stepiot.controller;

import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import com.stepiot.model.EduUserCriteria.Criteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduUserService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.PassWordUtils;
import com.stepiot.util.ValUtils;

@Controller
public class UserController extends BaseController {

  @Autowired
  private EduUserService service;

  @Autowired
  private EmailValidator emailValidator;

  @Autowired
  private ProtoResponseService reservice;

  @GetMapping("/userprofile")
  public String userprofile(HttpServletRequest request, Authentication authentication,
      Model model) {
    String name = authentication.getName();
    EduUser user = service.selectByPrimaryKey(name);
    Builder builder = super.getBcbBuilder(request);
    if (user.getPasswordLut() == null) {
      builder.setItem("首次登录，请完善个人信息，并修改初始密码!");
    }

    model.addAttribute("data", user);

    model.addAttribute("bcb", BcbCreator.create(builder, BcbCreator.UPDATE));
    return "user/userprofile";
  }

  @ResponseBody
  @PostMapping(value = "/userprofile/update")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String username = authentication.getName();

    EduUser eduUser = service.selectByPrimaryKey(username);

    String telephone = body.getTelephone();
    String email = body.getEmail();

    String origPassword = body.getValue();
    String confirmNewPassword = body.getConfirmPassword();
    String newPassword = body.getPassword();

    @SuppressWarnings("unused")
    int age = body.getAge();
    @SuppressWarnings("unused")
    int gender = body.getGender();
    @SuppressWarnings("unused")
    String name = body.getName();


    HashMap<String, String> maap = Maps.newHashMap();
    if (!emailValidator.isValid(email)) {
      maap.putAll(reservice.of("email", "validate.email.wrong"));
    }
    if (!ValUtils.isValidTelephone(telephone)) {
      maap.putAll(reservice.of("telephone", "validate.telephone.wrong"));
    }

    // 如果密码为初始密码，则强制用户修改密码
    if (eduUser.getPasswordLut() == null) {
      if (StringUtils.isAnyEmpty(origPassword, newPassword, confirmNewPassword)) {
        return reservice.response(HttpStatus.CONFLICT, "", "validate.password.need.set");
      }
    }
    String redirectUrl = "";
    // 如果密码相关字段均不为空，则校验密码
    if (!StringUtils.isAnyEmpty(origPassword, newPassword, confirmNewPassword)) {

      if (!ValUtils.isValidPassword(8, 64, newPassword)) {
        maap.putAll(reservice.of("password", "validate.password.wrong"));
      }
      if (!newPassword.equals(confirmNewPassword)) {
        maap.putAll(reservice.of("confirmPassword", "validate.password.not.match"));
      }

      if (!PassWordUtils.matches(origPassword, eduUser.getPassword())) {
        maap.putAll(reservice.of("password", "validate.invalid.password"));
      }
      eduUser.setPassword(PassWordUtils.encodePassword(newPassword));
      eduUser.setPasswordLut(new Date());
      redirectUrl = "/logout";
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }

    EduUserCriteria ex = new EduUserCriteria();
    Criteria ext = ex.createCriteria();
    ext.andTelephoneEqualTo(telephone).andUsernameNotEqualTo(username);
    long counts = service.countByExample(ex);

    if (counts > 0) {
      maap.putAll(reservice.of("telephone", "validate.telephone.exsits"));
    }

    ex.clear();
    ext = ex.createCriteria();
    ext.andEmailEqualTo(email).andUsernameNotEqualTo(username);
    counts = service.countByExample(ex);

    if (counts > 0) {
      maap.putAll(reservice.of("email", "validate.email.exsits"));
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }

    if (StringUtils.isEmpty(email)) {
      email = null;
    }
    if (StringUtils.isEmpty(telephone)) {
      telephone = null;
    }
    eduUser.setEmail(email);
    eduUser.setTelephone(telephone);

    ex.clear();
    ext = ex.createCriteria();
    ext.andUsernameEqualTo(username);
    if (service.updateByExampleSelective(eduUser, ex) == 1) {
      return reservice.response(HttpStatus.OK, redirectUrl, "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.BAD_REQUEST, redirectUrl, "global.message.update.fail");
    }

  }

}
