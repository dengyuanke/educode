package com.stepiot.controller.educourse;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ProtocolStringList;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.aws.AmazonS3Service;
import com.stepiot.constants.CourseType;
import com.stepiot.constants.RoleEnum;
import com.stepiot.context.RunnerContext;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.iot.IotCore;
import com.stepiot.iot.IotUserCore;
import com.stepiot.model.EduPythonCourse;
import com.stepiot.model.EduPythonCourseOutlineAnswerWithBLOBs;
import com.stepiot.model.EduPythonCourseOutlineCriteria;
import com.stepiot.model.EduPythonCourseOutlineDocWithBLOBs;
import com.stepiot.model.EduPythonCourseOutlineWithBLOBs;
import com.stepiot.model.EduUserCourseJuniorCriteria;
import com.stepiot.model.EduUserCourseJuniorWithBLOBs;
import com.stepiot.model.EduUserPythonCourseCriteria;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;
import com.stepiot.model.EduWiki;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduOptionsClassService;
import com.stepiot.service.EduPythonCourseOutlineDocService;
import com.stepiot.service.EduPythonCourseOutlineService;
import com.stepiot.service.EduPythonCourseService;
import com.stepiot.service.EduScoreService;
import com.stepiot.service.EduUserCourseJuniorService;
import com.stepiot.service.EduUserPythonCourseService;
import com.stepiot.service.EduWikiService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.ProtoUtils;

@Controller
public class EduPythonCourseController extends BaseController {

  private static final int PAGE_SIZE = 18;

  @Autowired
  private AmazonS3Service s3;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduPythonCourseOutlineService oservice;

  @Autowired
  private EduUserPythonCourseService uservice;

  @Autowired
  private EduScoreService scoreService;

  @Autowired
  private EduPythonCourseOutlineDocService mdService;

  @Autowired
  private EduUserCourseJuniorService ucService;

  @Autowired
  private EduOptionsClassService optService;

  @Autowired
  private EduPythonCourseService courseService;

  @Autowired
  private EduWikiService wikiService;


  private boolean courseVideoOpened(String studentId) {
    Map<String, Object> param = Maps.newHashMap();
    param.put("username", studentId);
    Map<String, Object> options = optService.loadClassOptionsByStudentId(param);
    if (options != null) {
      Integer openVideo = (Integer) options.get("openVideo");
      if (openVideo != null && openVideo.equals(-1)) {
        return false;
      }
    }
    return true;
  }

  @RequestMapping("/pythoncourse")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    EduPythonCourse course = courseService.selectByPrimaryKey(1);
    Integer wikiId = course.getWikiId();
    EduWiki wiki = wikiService.selectByPrimaryKey(wikiId);

    model.addAttribute("course", course);
    model.addAttribute("wiki", wiki);

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
    return "edupythoncourse/profile";
  }

  @RequestMapping("/pythoncourse/outline")
  public String pythoncourses(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
    return "edupythoncourse/cards";
  }

  @RequestMapping(value = "pythoncourse/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    Integer outlineId = Integer.parseInt(body.getId(0));
    int currentLevel = body.getCurrentLevel();
    int userCourseId = body.getUserCourseId();
    // 用于区分level和openlevel
    String type = body.getType();

    // TODO 物联网演示用的模块只给一个用户 teacher开放
    if (outlineId == 19) {
      if (!name.equals("teacher")) {
        outlineId = 18;
      }
    }

    return toEditPage(request, model, name, outlineId, currentLevel, userCourseId, type);
  }

  private String toEditPage(HttpServletRequest request, Model model, String name, Integer outlineId,
      int currentLevel, int userCourseId, String type) {
    HashMap<String, Object> param = Maps.newHashMap();
    param.put("level", currentLevel);
    param.put("username", name);
    param.put("outlineId", outlineId);

    Map<String, Object> mycourse = uservice.selectByUserCourseId(param);
    Integer courseType = (Integer) mycourse.get("courseType");
    Integer playButton = 0;
    String videoSrc = (String) mycourse.get("videoSrc");
    if (StringUtils.isNotEmpty(videoSrc)) {
      if (courseVideoOpened(name)) {
        playButton = BcbCreator.PLAY;
      }
    }
    CourseType t = CourseType.valueOf(courseType);
    model.addAttribute("data", mycourse);
    Builder builder = getBcbBuilder(request);
    builder.setItem(mycourse.get("outlineName").toString());
    // 进入编辑页面
    if (userCourseId > 0) {

      model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request),
          BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.COMPLETE));

      int from = (Integer) mycourse.get("levelFrom");
      int to = (Integer) mycourse.get("levelTo");
      if (currentLevel == 0) {
        currentLevel = from;
      } else {
        // 对于openlevel,不进行校验
        if (type == null || !type.equals("open")) {
          currentLevel = Math.min(Math.max(currentLevel, from), to);
        }
      }
      String levelTitles = (String) mycourse.get("levelTitles");
      if (StringUtils.isNoneBlank(levelTitles)) {
        Map<String, Object> titles = GsonUtils.toMap(levelTitles);
        String key = ((currentLevel - from) + 1) + "";
        String _ltitle = (String) titles.get(key);
        if (StringUtils.isNotBlank(_ltitle)) {
          builder.setSubitem(key + ". " + _ltitle);
        }
      }
      mycourse.put("currentLevel", currentLevel);
      buildLevels(builder, currentLevel, from, to);
      int button = 0;
      if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
        button = BcbCreator.ANSWER;
      }
      EduPythonCourseOutlineWithBLOBs pre = oservice.selectByPrimaryKey(outlineId - 1);
      EduPythonCourseOutlineWithBLOBs next = oservice.selectByPrimaryKey(outlineId + 1);

      if (pre != null) {
        button = button | BcbCreator.PREVIOUS;
        com.stepiot.proto.HttpProto.Req.Builder reqb = HttpProto.Req.newBuilder();
        reqb.addId(pre.getOutlineId() + "");
        reqb.setCurrentLevel(pre.getLevelFrom());
        reqb.setUserCourseId(0);
        model.addAttribute("previousLink", ProtoUtils.buildRequest(reqb.build()));
      }
      if (next != null) {
        button = button | BcbCreator.NEXT;
        com.stepiot.proto.HttpProto.Req.Builder nextb = HttpProto.Req.newBuilder();
        nextb.addId(next.getOutlineId() + "");
        nextb.setCurrentLevel(next.getLevelFrom());
        nextb.setUserCourseId(0);
        model.addAttribute("nextLink", ProtoUtils.buildRequest(nextb.build()));
      }
      switch (t) {
        case NESTBLOCK:
          model.addAttribute("bcb", BcbCreator.create(builder, button | BcbCreator.CODE
              | BcbCreator.DOWNLOAD | BcbCreator.SAVE | BcbCreator.SETTODEFAULT | playButton));
          return "edupythoncourse/editnestblock";
        case WABC:
          model.addAttribute("bcb", BcbCreator.create(builder,
              button | BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVE | playButton));
          return "edupythoncourse/editwabc";
        case NULL:
          return "edupythoncourse/editnestblock";
        case PYTHON:
          model.addAttribute("bcb", BcbCreator.create(builder,
              button | BcbCreator.RUNBUTTON | BcbCreator.SAVE | BcbCreator.COLOR | playButton));
          return "edupythoncourse/editcode";
        case SPACE:
          model.addAttribute("bcb",
              BcbCreator.create(builder, button | BcbCreator.CODE | BcbCreator.RUNBUTTON
                  | BcbCreator.RESETBUTTON | BcbCreator.SETTODEFAULT | playButton));
          return "edupythoncourse/editspace";
        case TURTLE:
          model.addAttribute("bcb",
              BcbCreator.create(builder,
                  button | BcbCreator.CODE | BcbCreator.RUNBUTTON | BcbCreator.RESETBUTTON
                      | BcbCreator.SETTODEFAULT | BcbCreator.SPEED | playButton));
          return "edupythoncourse/editturtle";
        case IOT:
          model.addAttribute("bcb", BcbCreator.create(builder,
              button | BcbCreator.CODE | BcbCreator.SAVE | BcbCreator.RUNBUTTON | playButton));
          return "edupythoncourse/editiot";
        default:
          break;
      }
    }
    // load doc from db
    EduPythonCourseOutlineDocWithBLOBs md = mdService.selectByPrimaryKey(outlineId);
    if (md == null) {
      md = new EduPythonCourseOutlineDocWithBLOBs();
      md.setMdoc("# .");
    }
    model.addAttribute("md", md);
    switch (t) {
      case NULL:
        this.insertOrUpdate(outlineId, "", new ArrayList<String>(), "mdi-check-decagram", 1, name);
        return extracted(request, model, builder, md, BcbCreator.NOOP);
      default:
        return extracted(request, model, builder, md, BcbCreator.DOACTION);
    }
  }

  private String extracted(HttpServletRequest request, Model model,
      com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      EduPythonCourseOutlineDocWithBLOBs md, int but) {

    Set<String> roles = getUserRolesName();
    if (StringUtils.isNotEmpty(md.getPptSrc())) {
      if (roles.contains(RoleEnum.ROLE_TEACHER.getValue())
          || roles.contains(RoleEnum.ROLE_COADMIN.getValue())
          || roles.contains(RoleEnum.ROLE_DEV.getValue())) {
        but = but | BcbCreator.DOWNPPT;
      }
    }
    if (getUserRolesName().contains(RoleEnum.ROLE_DEV.getValue())) {
      but = but | BcbCreator.SAVE;
      model.addAttribute("bcb", BcbCreator.create(builder, but));
      return "edupythoncourse/docedit";
    } else {
      model.addAttribute("bcb", BcbCreator.create(builder, but));
      return "edupythoncourse/doc";
    }
  }

  @RequestMapping(value = "/pythoncourse/downppt")
  public ResponseEntity<Resource> downppt(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) throws IOException {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    EduPythonCourseOutlineDocWithBLOBs md = mdService.selectByPrimaryKey(outlineId);
    if (md == null) {
      return null;
    }
    String s3location = md.getPptSrc();
    byte[] data = s3.getS3Object(s3location);
    String filename = md.getPptFilename();
    if (StringUtils.isEmpty(filename)) {
      filename = Paths.get(s3location).getFileName().toString();
    }

    ByteArrayResource resource = new ByteArrayResource(data);
    HttpHeaders headers = new HttpHeaders();
    headers.setCacheControl(CacheControl.noCache());

    String fileName = URLEncoder.encode(filename, "UTF-8");
    headers.setContentDisposition(
        ContentDisposition.parse("attachment; filename=\"" + fileName + "\""));
    headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
    headers.setPragma("no-cache");
    headers.setExpires(0);
    return ResponseEntity.ok().headers(headers).contentLength(data.length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);
    List<Map<String, Object>> myCourse = uservice.listMyCourse(map);
    model.addAttribute("data", myCourse);
    long total = oservice.countByExample(new EduPythonCourseOutlineCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

  @Autowired
  private RedissonClient rediss;

  @Transactional
  @ResponseBody
  @PostMapping(value = "/pythoncourse/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    String blockly = body.getBlockly();
    String code = body.getCode();
    ProtocolStringList blockTypes = body.getClazzIdList();
    int level = body.getCurrentLevel();
    String name = authentication.getName();
    RLock lock = rediss.getLock(name);
    boolean tryLock = lock.tryLock();
    String result = "";
    if (tryLock) {
      try {
        result = insertOrUpdate(outlineId, blockly, blockTypes, code, level, name);
      } finally {
        lock.unlock();
      }
    }
    return result;
  }

  private String insertOrUpdate(int outlineId, String blockly, List<String> blockTypes, String code,
      int level, String name) {
    EduUserPythonCourseCriteria ex = new EduUserPythonCourseCriteria();
    ex.createCriteria().andOutlineIdEqualTo(outlineId).andLevelEqualTo(level)
        .andUsernameEqualTo(name);
    List<EduUserPythonCourseWithBLOBs> list = uservice.selectByExampleWithBLOBs(ex);

    EduUserPythonCourseWithBLOBs userCourse = null;

    String blocksArray = GsonUtils.toJson(blockTypes);

    // 更新操作
    if (list != null && list.size() > 0) {
      userCourse = list.get(0);
      userCourse.setLastUpdateTime(new Date());
      // userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      // userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserBlocklyTypes(blocksArray);
      userCourse.setUserCode(code);
      if (uservice.updateByPrimaryKeySelective(userCourse) > 0) {
        scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
    // 新增操作
    else {
      userCourse = new EduUserPythonCourseWithBLOBs();
      userCourse.setCreateTime(new Date());
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserBlocklyTypes(blocksArray);
      userCourse.setUserCode(code);
      userCourse.setUsername(name);

      if (uservice.insertSelective(userCourse) > 0) {
        scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
  }


  @ResponseBody
  @PostMapping(value = "/pythoncourse/answer")
  public String answer(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    int level = body.getCurrentLevel();

    if (!containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.unauthorized.request");
    } else {
      Map<String, String> msgmap = Maps.newHashMap();
      EduPythonCourseOutlineAnswerWithBLOBs answer =
          oservice.selectAnswerByIdAndLevel(outlineId, level);
      if (answer != null) {
        String anscode = answer.getDefaultAnswer();
        if (StringUtils.isNoneEmpty(anscode)) {
          msgmap.put("answer", anscode);
        }
      }
      return reservice.response(HttpStatus.OK, msgmap);
    }
  }


  @ResponseBody
  @PostMapping(value = "/pythoncourse/run")
  public String execute(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    String blockly = body.getBlockly();
    String code = body.getCode();
    int level = body.getCurrentLevel();
    String name = authentication.getName();

    EduUserPythonCourseCriteria ex = new EduUserPythonCourseCriteria();
    ex.createCriteria().andOutlineIdEqualTo(outlineId).andLevelEqualTo(level)
        .andUsernameEqualTo(name);
    List<EduUserPythonCourseWithBLOBs> list = uservice.selectByExampleWithBLOBs(ex);

    EduUserPythonCourseWithBLOBs userCourse = null;

    // 更新操作
    if (list != null && list.size() > 0) {
      userCourse = list.get(0);
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      uservice.updateByPrimaryKeySelective(userCourse);
      scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
      // if (uservice.updateByPrimaryKeySelective(userCourse) > 0) {
      // return reservice.response(HttpStatus.OK, "", "global.message.deploy.success");
      // } else {
      // return reservice.response(HttpStatus.CONFLICT, "", "global.message.deploy.fail");
      // }
    }
    // 新增操作
    else {
      userCourse = new EduUserPythonCourseWithBLOBs();
      userCourse.setCreateTime(new Date());
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      userCourse.setUsername(name);
      uservice.insertSelective(userCourse);
      scoreService.updateUserCourseScore(userCourse.getOutlineId(), userCourse.getLevel());
      // if (uservice.insertSelective(userCourse) > 0) {
      // return reservice.response(HttpStatus.OK, "", "global.message.deploy.success");
      // } else {
      // return reservice.response(HttpStatus.CONFLICT, "", "global.message.deploy.fail");
      // }
    }
    ExecutorService exe = RunnerContext.getUserExecutorService(name);
    exe.execute(new Runnable() {
      ScriptEngineManager manager = new ScriptEngineManager();
      ScriptEngine engine = manager.getEngineByName("nashorn");

      @Override
      public void run() {
        try {
          IotUserCore iot = IotUserCore.getInstance(name);
          iot.setHasJob(true);
          iot.setInterrupt(0);
          StringBuilder sb = new StringBuilder();
          sb.append("(function() {");
          sb.append("try {");
          String coder =
              StringUtils.replace(code, "getInstance();", "getInstance(\"" + name + "\");");
          sb.append(coder);
          sb.append("} catch (error) {");
          sb.append("print(error);");
          sb.append("return;");
          sb.append("}");
          sb.append("})();");
          System.err.println(sb.toString());
          engine.eval(sb.toString());
        } catch (ScriptException e) {
          e.printStackTrace();
        }
      }
    });
    return reservice.response(HttpStatus.OK, "", "global.message.deploy.success");
  }

  @ResponseBody
  @PostMapping(value = "/pythoncourse/docapdate")
  public String updatedoc(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {

    // 非系统DEV角色不允许进行该操作
    if (!getUserRolesName().contains(RoleEnum.ROLE_DEV.getValue())) {// TODO 用枚举替换
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");

    }

    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    String mdcontent = body.getValue();

    EduPythonCourseOutlineDocWithBLOBs doc = mdService.selectByPrimaryKey(outlineId);


    // 更新操作
    if (doc != null) {
      doc.setMdoc(mdcontent);
      if (mdService.updateByPrimaryKeyWithBLOBs(doc) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
    // 新增操作
    else {
      doc = new EduPythonCourseOutlineDocWithBLOBs();
      doc.setCourseId(1);// TODO 用枚举替换
      doc.setOutlineId(outlineId);
      doc.setMdoc(mdcontent);
      if (mdService.insert(doc) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
  }


  private void buildLevels(com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      int current, int from, int to) {
    com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.Builder lb =
        com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.newBuilder();
    int i = 1;
    for (int x = from; x <= to; x++) {
      lb.setPage("" + i++);
      lb.setLevel("" + x);
      lb.setTip("ddddd");
      lb.setEnabled(true);
      if (current == x) {
        lb.setActive(true);
      } else {
        lb.setActive(false);
      }
      builder.addLevels(lb.build());
      lb.clear();
    }
  }

  @SuppressWarnings("unused")
  private void buildOpenLevel(com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      int level) {
    com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.Builder lb =
        com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.newBuilder();

    lb.setPage("Free");
    lb.setLevel(level + "");
    lb.setTip("ddddd");
    lb.setEnabled(true);
    lb.setActive(true);
    builder.addOpenlevels(lb);

  }



  @ResponseBody
  @PostMapping(value = "/pythoncourse/getDeviceData")
  public String getDeviceData(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    IotCore.getDataFromDevice(null, "b9157a45596e493aac029c920bd65b40", "abc");
    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("temp", "1234"));
  }

  @ResponseBody
  @PostMapping(value = "/pythoncourse/sendCmdToDevice")
  public String sendCmdToDevice(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Map<String, String> param = Maps.newHashMap();
    param.put("dataType", "3");
    IotCore.sendCommandToDevice(param, "b9157a45596e493aac029c920bd65b40",
        "sendCmdToDeviceTestData");
    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("result", "true"));
  }

  @ResponseBody
  @PostMapping(value = "/pythoncourse/flash")
  public String flash(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String courseUid = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String blocklyCode = body.getBlocklyCode();
    // String name = authentication.getName();

    EduUserCourseJuniorCriteria iex = new EduUserCourseJuniorCriteria();
    iex.createCriteria().andCourseUidEqualTo(courseUid);
    EduUserCourseJuniorWithBLOBs course = new EduUserCourseJuniorWithBLOBs();

    course.setCourseBlockly(blocklyXml);
    course.setBlocklyCode(blocklyCode);
    course.setCourseStatus(1);
    course.setLastUpdate(new Date());

    if (ucService.updateByExampleSelective(course, iex) == 1) {
      return reservice.response(HttpStatus.OK, "", "info.device.flashed");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "info.device.flashed.failed");
    }

  }

}
