package com.stepiot.controller.educourse;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.stepiot.beans.EduUserCourseProgressBean;
import com.stepiot.constants.CourseType;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduPythonCourseOutlineCriteria;
import com.stepiot.model.EduUserPythonCourseCriteria;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduPythonCourseOutlineService;
import com.stepiot.service.EduStudentRefService;
import com.stepiot.service.EduUserPythonCourseService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;

@Controller
public class EduCourseProgressController extends BaseController {

  private static final int PAGE_SIZE = 18;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduPythonCourseOutlineService oservice;

  @Autowired
  private EduUserPythonCourseService uservice;

  @Autowired
  private EduStudentRefService sservice;

  @RequestMapping("/courseprogress")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String username = null;
    boolean fromTeacher = false;
    if (containsRole(RoleEnum.ROLE_TEACHER.getValue())) {
      if (map != null) {
        Req body = map.getBody();
        if (body != null) {
          username = body.getUsername();
          model.addAttribute("studentId", username);
          fromTeacher = true;
        }
      }
    }


    if (username == null) {
      username = authentication.getName();
    }

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, username);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), username);
    }

    Builder builder = getBcbBuilder(request);

    if (fromTeacher) {
      List<Map<String, Object>> slist =
          sservice.selectStudentsById(ImmutableBiMap.of("username", username));
      if (slist != null && slist.size() > 0) {
        Map<String, Object> student = slist.get(0);
        String name = student.get("name").toString();
        String className = student.get("className").toString();
        builder.setItem(name + "【" + className + "】");
      }
    }


    model.addAttribute("bcb", BcbCreator.create(builder, BcbCreator.NOOP));
    return "educourseprogress/list";
  }

  @RequestMapping(value = "courseprogress/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String outlineId = body.getId(0);
    int currentLevel = body.getCurrentLevel();
    int userCourseId = body.getUserCourseId();


    HashMap<String, Object> param = Maps.newHashMap();
    param.put("level", currentLevel);
    param.put("username", name);
    param.put("outlineId", Integer.parseInt(outlineId));
    Map<String, Object> mycourse = uservice.selectByUserCourseId(param);
    Integer courseType = (Integer) mycourse.get("courseType");
    CourseType t = CourseType.valueOf(courseType);
    model.addAttribute("data", mycourse);
    // 进入编辑页面
    if (userCourseId > 0) {

      model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request),
          BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVEBLOCKLY | BcbCreator.COMPLETE));

      Builder builder = getBcbBuilder(request);
      int from = (Integer) mycourse.get("levelFrom");
      int to = (Integer) mycourse.get("levelTo");
      if (currentLevel == 0) {
        currentLevel = from;
      } else {
        currentLevel = Math.min(Math.max(currentLevel, from), to);
      }

      mycourse.put("currentLevel", currentLevel);

      switch (t) {
        case NESTBLOCK:
          buildLevels(builder, currentLevel, from, to);
          builder.setItem(mycourse.get("outlineName").toString());
          model.addAttribute("bcb",
              BcbCreator.create(builder, BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SAVE));
          return "educourseprogress/editnestblock";
        case NULL:
          return "educourseprogress/editnestblock";
        case PYTHON:

          buildLevels(builder, currentLevel, from, to);
          builder.setItem(mycourse.get("outlineName").toString());
          model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request),
              BcbCreator.RUNBUTTON | BcbCreator.SAVE | BcbCreator.COLOR));
          return "educourseprogress/editcode";
        case SPACE:


          buildLevels(builder, currentLevel, from, to);
          builder.setItem(mycourse.get("outlineName").toString());
          model.addAttribute("bcb", BcbCreator.create(builder,
              BcbCreator.CODE | BcbCreator.RUNBUTTON | BcbCreator.RESETBUTTON));
          return "educourseprogress/editspace";
        case TURTLE:


          buildLevels(builder, currentLevel, from, to);
          builder.setItem(mycourse.get("outlineName").toString());
          model.addAttribute("bcb", BcbCreator.create(builder,
              BcbCreator.CODE | BcbCreator.RUNBUTTON | BcbCreator.RESETBUTTON));
          return "educourseprogress/editturtle";
        default:
          break;
      }
    }

    switch (t) {
      case NULL:
        // 进入文档页面
        model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.NOOP));
        return "educourseprogress/doc";
      default:
        model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.DOACTION));
        return "educourseprogress/doc";


    }


  }

  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<EduUserCourseProgressBean> plist = uservice.listMyCourseProgress(map);

    for (int i = 0; i < plist.size(); i++) {
      plist.get(i).setPro();
    }

    model.addAttribute("data", plist);

    long total = oservice.countByExample(new EduPythonCourseOutlineCriteria());
    this.genPager(model, current, total, PAGE_SIZE);
  }

  @ResponseBody
  @PostMapping(value = "/courseprogress/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    int outlineId = body.getOutlineId();
    String blockly = body.getBlockly();
    String code = body.getCode();
    int level = body.getCurrentLevel();
    String name = authentication.getName();

    EduUserPythonCourseCriteria ex = new EduUserPythonCourseCriteria();
    ex.createCriteria().andOutlineIdEqualTo(outlineId).andLevelEqualTo(level)
        .andUsernameEqualTo(name);
    List<EduUserPythonCourseWithBLOBs> list = uservice.selectByExampleWithBLOBs(ex);

    EduUserPythonCourseWithBLOBs userCourse = null;

    // 更新操作
    if (list != null && list.size() > 0) {
      userCourse = list.get(0);
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      if (uservice.updateByPrimaryKeySelective(userCourse) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
    // 新增操作
    else {
      userCourse = new EduUserPythonCourseWithBLOBs();
      userCourse.setCreateTime(new Date());
      userCourse.setLastUpdateTime(new Date());
      userCourse.setOutlineId(outlineId);
      // 状态\n0:saved\n1:submitted\n2:passed\n-1:rejected
      // TODO 用枚举替换
      userCourse.setStatus(0);
      userCourse.setLevel(level);
      userCourse.setUserBlockly(blockly);
      userCourse.setUserCode(code);
      userCourse.setUsername(name);
      if (uservice.insertSelective(userCourse) > 0) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success");
      } else {
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }
  }


  private void buildLevels(com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder builder,
      int current, int from, int to) {
    com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.Builder lb =
        com.stepiot.proto.BreadcrumbProto.Breadcrumb.Level.newBuilder();
    int i = 1;
    for (int x = from; x <= to; x++) {
      lb.setPage("" + i++);
      lb.setLevel("" + x);
      lb.setTip("ddddd");
      lb.setEnabled(true);
      if (current == x) {
        lb.setActive(true);
      } else {
        lb.setActive(false);
      }
      builder.addLevels(lb.build());
      lb.clear();
    }

  }
}
