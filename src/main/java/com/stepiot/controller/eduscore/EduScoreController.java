package com.stepiot.controller.eduscore;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.EduClassScoreBean;
import com.stepiot.beans.EduNoticeBean;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.constants.Cons;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.model.EduNoticeAttachmentCriteria;
import com.stepiot.model.EduNoticeAttachmentCriteria.Criteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduClassService;
import com.stepiot.service.EduNoticeAttachmentService;
import com.stepiot.service.EduNoticeService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduScoreController extends BaseController {

  private static final int PAGE_SIZE = 24;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduNoticeService nservice;

  @Autowired
  private EduNoticeAttachmentService aService;

  @Autowired
  private EduClassService cService;


  static final TypeToken<StudentQueryBean> queryBean = new TypeToken<StudentQueryBean>() {};
  @Autowired
  private SelectService ss;

  @RequestMapping("/eduscore")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    long total = 0;
    if (map == null || map.getBody() == null) {
      total = fillPagination(model, 1, name, new StudentQueryBean());
      ss.singleClassSelect(model, true, name, "");
    } else {
      Req body = map.getBody();
      String reqSource = body.getReqSource();
      String levelThird = null;
      StudentQueryBean bean = null;
      // 请求从分页控件过来
      if (Cons.REQ_SOURCE_PAGINATION.equals(reqSource)) {
        String caches = body.getCaches();
        bean = toTypedBean(caches);
        levelThird = bean.getClassId();
      } else {
        levelThird = body.getLevelThird();
        bean = new StudentQueryBean();
        bean.setClassId(levelThird);
      }
      ss.singleClassSelect(model, true, name, bean.getClassId());
      total = fillPagination(model, map.getBody().getCurrentPage(), name, bean);
    }
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + total + "名学生");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "eduscore/slist";
  }

  @RequestMapping(value = "eduscore/slist")
  public String compose(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    ss.singleClassSelect(model, true, name, "");
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SEND));
    return "eduscore/slist";

  }

  @ResponseBody
  @PostMapping(value = "eduscore/countNews")
  public String countNews(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    long unreaded = nservice.countUnReadedNews(name);
    model.addAttribute("unReadedCounts", unreaded);
    if (unreaded > 0) {
      int showItemCounts = (int) Math.min(5, unreaded);
      List<EduNoticeBean> items = nservice.listLatestNews(showItemCounts, name);
      model.addAttribute("unReadedNews", items);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success",
          ImmutableBiMap.of("news", GsonUtils.fromObj(items), "counts", unreaded + ""));
    }
    return reservice.response(HttpStatus.OK, "", "global.message.update.success",
        ImmutableBiMap.of("counts", unreaded + ""));

  }

  @RequestMapping(value = "eduscore/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String noticeId = body.getId(0);
    Map<String, Object> maps = Maps.newHashMap();
    maps.put("username", name);
    maps.put("noticeId", noticeId);
    nservice.markNoticeAsReadById(name, Integer.parseInt(noticeId));
    List<EduNoticeBean> myNotice = nservice.listMyNotice(maps);
    EduNoticeBean noticeBean = myNotice.get(0);
    List<Integer> ids = noticeBean.getAttachmentIds();
    if (ids != null && ids.size() > 0) {
      EduNoticeAttachmentCriteria ex = new EduNoticeAttachmentCriteria();
      Criteria exam = ex.createCriteria();
      exam.andAttachmentIdIn(ids);
      List<EduNoticeAttachment> atts = aService.selectByExample(ex);
      model.addAttribute("attachments", atts);
    }
    model.addAttribute("data", myNotice.get(0));

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    return "eduscore/edit";

  }

  private long fillPagination(Model model, int current, String name, StudentQueryBean bean) {
    if (StringUtils.isEmpty(bean.getClassId())) {
      bean.setClassId("-1");
    }
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);
    map.put("classId", bean.getClassId());

    List<EduClassScoreBean> scores = cService.listClassScores(map);
    model.addAttribute("data", scores);

    long total = cService.countStudentByClassId(bean.getClassId());
    Pagination pagination =
        PaginationUtils.genPagination(current, total, PAGE_SIZE, beanToBase64Json(bean));
    if (pagination == null) {
      return total;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
    return total;
  }

  private StudentQueryBean toTypedBean(String queryJson) {
    try {
      if (StringUtils.isEmpty(queryJson)) {
        return null;
      }

      byte[] data = Base64.decodeBase64(queryJson);
      String json = new String(data, "UTF-8");
      StudentQueryBean bean = GsonUtils.toTypedBean(json, queryBean);
      return bean;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  private String beanToBase64Json(StudentQueryBean bean) {
    try {
      if (bean == null) {
        return null;
      }
      String json = GsonUtils.fromObj(bean);
      return Base64.encodeBase64URLSafeString(json.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }
}
