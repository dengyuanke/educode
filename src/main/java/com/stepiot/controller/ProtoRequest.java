package com.stepiot.controller;

import org.apache.commons.lang3.StringUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.stepiot.proto.HttpProto.Req;

public class ProtoRequest {
  private String k;

  public String getK() {
    return k;
  }

  public void setK(String k) {
    this.k = k;
  }

  public Req getBody() {
    if (StringUtils.isEmpty(k)) {
      return null;
    }
    if (k.length() % 2 != 0) {
      throw new java.lang.IllegalArgumentException();
    }
    char[] cs = k.toCharArray();
    byte[] proto = new byte[cs.length / 2];
    for (int i = 0; i < cs.length; i = i + 2) {
      proto[i / 2] = (byte) Integer.parseInt((cs[i] + "" + cs[i + 1]), 36);
    }
    try {
      Req request = Req.parseFrom(proto);
      return request;
    } catch (InvalidProtocolBufferException e) {
      return null;
    }
  }
}
