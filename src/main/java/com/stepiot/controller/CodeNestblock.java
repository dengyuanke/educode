
package com.stepiot.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;

import com.stepiot.aws.AmazonS3Service;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.google.common.collect.ImmutableBiMap;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduUserNestblockCriteria;
import com.stepiot.model.EduUserNestblockWithBLOBs;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduUserNestblockService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.RandomUtils;

@Controller
public class CodeNestblock extends BaseController {

  private static final int PAGE_SIZE = 8;

  private static final String BLOCKLY = "blockly/";

  @Autowired
  private AmazonS3Service s3;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduUserNestblockService bservice;


  @GetMapping("/codenestblock")
  public String codenestblock(HttpServletRequest request,
      @RequestParam(required = false) String key, Authentication auth, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "menu.code.nestblock");
    }


    EduUserNestblockWithBLOBs userCourse = new EduUserNestblockWithBLOBs();
    if (StringUtils.isNotBlank(key)) {
      key = new String(Base64.decodeBase64(key));
      String blockly;
      try {
        blockly = s3.getStrObject(key);
        userCourse.setBlocklyXml(blockly);
      } catch (Exception e) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
      }
    } else {
      userCourse.setBlocklyXml("");
    }
    int buttons = BcbCreator.CODE | BcbCreator.DOWNLOAD | BcbCreator.SHARE;
    if (auth != null && auth.isAuthenticated()) {
      buttons = buttons | BcbCreator.SAVEBLOCKLY;
    }
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), buttons));

    model.addAttribute("data", userCourse);
    return "mycode/newnestblock";
  }

  @GetMapping("/codenestblock/pub")
  public String pub(HttpServletRequest request, @RequestParam("key") String key,
      Authentication authentication, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "menu.code.nestblock");
    }

    if (StringUtils.isBlank(key)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
    }

    key = new String(Base64.decodeBase64(key));
    String blockly;
    try {
      blockly = s3.getStrObject(key);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
    }

    EduUserNestblockWithBLOBs userCourse = new EduUserNestblockWithBLOBs();
//    userCourse.setBlocklyXml(blockly);
    model.addAttribute("data", userCourse);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request),
        BcbCreator.CODE | BcbCreator.ADD | BcbCreator.DOWNLOAD | BcbCreator.SHARE));
    return "mycode/newnestblock";
  }

  @ResponseBody
  @PostMapping(value = "codenestblock/share")
  public String share(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String blocklyXml = body.getBlocklyXml();
    String key = BLOCKLY + RandomUtils.genPass(5);
    try {
      CompletableFuture<PutObjectResult> resule = s3.pubObject(blocklyXml.getBytes("UTF-8"), key);
      resule.get();
      return reservice.response(HttpStatus.OK, "",
          Base64.encodeBase64URLSafeString(key.getBytes()));
    } catch (Exception e) {
      e.printStackTrace();
      return reservice.response(HttpStatus.CONFLICT, "", "服务出现故障");
    }
  }

  @PostMapping("/codenestblock")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    fillPagination(model, map.getBody().getCurrentPage(), name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "mycode/newnestblock";
  }

  @PostMapping(value = "/codenestblock/delen")
  public String delen(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    String blocklyId = map.getBody().getPid();
    EduUserNestblockCriteria ex = new EduUserNestblockCriteria();
    com.stepiot.model.EduUserNestblockCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);
    bservice.deleteByExample(ex);

    fillPagination(model, 1, name);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "mycode/newnestblock";

  }

  @PostMapping(value = "/codenestblock/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    String blocklyId = map.getBody().getId(0);

    EduUserNestblockCriteria ex = new EduUserNestblockCriteria();
    com.stepiot.model.EduUserNestblockCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);
    EduUserNestblockWithBLOBs bit = bservice.selectByPrimaryKey(blocklyId);
    if (bit != null) {
      model.addAttribute("data", bit);
    }

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request),
        BcbCreator.SAVEBLOCKLY | BcbCreator.DELEN | BcbCreator.CODE));
    return "mycode/codenestblockadd";
  }

  private void fillPagination(Model model, int current, String name) {
    EduUserNestblockCriteria ex = new EduUserNestblockCriteria();
    ex.setPageSize(PAGE_SIZE);

    ex.setLimitStart(Math.max(current - 1, 0) * ex.getPageSize());
    com.stepiot.model.EduUserNestblockCriteria.Criteria ext = ex.createCriteria();
    ext.andUsernameEqualTo(name);
    List<EduUserNestblockWithBLOBs> data = bservice.selectByExampleWithBLOBs(ex);
    model.addAttribute("data", data);

    long total = bservice.countByExample(ex);
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> map = GsonUtils.toMap(json);
    model.addAttribute("pagination", map);
  }

  @GetMapping(value = "/codenestblock/add")
  public String project2(HttpServletRequest request, Authentication authentication, Model model) {

    String name = authentication.getName();
    fillPagination(model, 1, name);

    EduUserNestblockWithBLOBs block = new EduUserNestblockWithBLOBs();

    block.setBlocklyId(RandomUtils.uuid());
    block.setCreateDate(new Date());
    block.setTitleName("未命名" + RandomUtils.uuid());
    block.setUsername(name);

    bservice.insert(block);
    model.addAttribute("data", block);
    model.addAttribute("bcb",
        BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SAVEBLOCKLY));
    return "mycode/codenestblockadd";
  }

  @ResponseBody
  @PostMapping(value = "/codenestblock/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String blocklyId = body.getPid();
    String blocklyXml = body.getBlocklyXml();
    String titleName = body.getTitleName();
    String name = authentication.getName();
    EduUserNestblockWithBLOBs blockly = new EduUserNestblockWithBLOBs();
    blockly.setBlocklyXml(blocklyXml);
    blockly.setLastEdit(new Date());
    blockly.setTitleName(titleName);
    blockly.setUsername(name);
    blockly.setBlocklyId(blocklyId);
    // 新增
    if ("-1".equals(blocklyId)) {
      blocklyId = RandomUtils.genPass(12);
      blockly.setBlocklyId(blocklyId);
      bservice.insert(blockly);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success",
          ImmutableBiMap.of("blocklyId", blocklyId, "titleName", titleName));
    } else {
      EduUserNestblockCriteria ex = new EduUserNestblockCriteria();
      ex.createCriteria().andUsernameEqualTo(name).andBlocklyIdEqualTo(blocklyId);

      if (bservice.updateByExampleSelective(blockly, ex) == 1) {
        return reservice.response(HttpStatus.OK, "", "global.message.update.success",
            ImmutableBiMap.of("blocklyId", blocklyId, "titleName", titleName));
      } else {
        bservice.insert(blockly);
        return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
      }
    }


  }
}
