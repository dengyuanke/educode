package com.stepiot.controller.educharts;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.stepiot.beans.EduChartDataBean;
import com.stepiot.beans.EduClassCourseProgressBean;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.ChartProto.Chart;
import com.stepiot.proto.ChartProto.Chart.Data.Dataset;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduChartService;
import com.stepiot.service.EduClassService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;

@Controller
public class EduChartController extends BaseController {

  @Autowired
  private EduClassService cservice;

  @Autowired
  private EduChartService chartService;

  @Autowired
  private SelectService sis;

  static final TypeToken<StudentQueryBean> queryBean = new TypeToken<StudentQueryBean>() {};

  @RequestMapping("/chartstudent")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    Builder buider = getBcbBuilder(request);
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "charts/chartstudent";
  }


  @RequestMapping("/chartcode")
  public String chartcode(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    if (map == null || map.getBody() == null) {
      sis.singleClassSelect(model, true, name, "");
    } else {
      Req body = map.getBody();

      String levelThird = null;
      StudentQueryBean bean = new StudentQueryBean();

      levelThird = body.getLevelThird();

      bean.setClassId(levelThird);

      sis.singleClassSelect(model, true, name, bean.getClassId());
      Map<String, Object> param = Maps.newHashMap();
      param.put("classId", bean.getClassId());
      param.put("username", name);
      List<EduChartDataBean> list = cservice.chartClassCode(param);
      String chartData = buildChartData(list, CHART_CODE);
      model.addAttribute("chartData", chartData);
    }

    Builder buider = getBcbBuilder(request);
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "charts/chartstudent";
  }


  @RequestMapping("/chartscore")
  public String chartscore(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    if (map == null || map.getBody() == null) {
      sis.singleClassSelect(model, true, name, "");
    } else {
      Req body = map.getBody();

      String levelThird = null;
      StudentQueryBean bean = new StudentQueryBean();

      levelThird = body.getLevelThird();

      bean.setClassId(levelThird);

      sis.singleClassSelect(model, true, name, bean.getClassId());
      Map<String, Object> param = Maps.newHashMap();
      param.put("classId", bean.getClassId());
      param.put("username", name);
      List<EduChartDataBean> list = cservice.chartClassCode(param);
      String chartData = buildChartData(list, CHART_SCORE);
      model.addAttribute("chartData", chartData);
    }

    Builder buider = getBcbBuilder(request);
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "charts/chartstudent";
  }


  private String buildChartData(List<EduChartDataBean> data, int type) {
    List<String> labels = Lists.newArrayList();
    List<Dataset> datasets = Lists.newArrayList();

    Dataset.Builder blockLenBuilder = Chart.Data.Dataset.newBuilder();
    blockLenBuilder.setLabel("积木代码量");
    blockLenBuilder.setBackgroundColor(chartService.randomColor());
    blockLenBuilder.setBorderColor(blockLenBuilder.getBackgroundColor());
    Dataset.Builder codeLenBuilder = Chart.Data.Dataset.newBuilder();
    codeLenBuilder.setLabel("作业代码量");
    codeLenBuilder.setBackgroundColor(chartService.randomColor());
    codeLenBuilder.setBorderColor(codeLenBuilder.getBackgroundColor());
    Dataset.Builder freeLenBuilder = Chart.Data.Dataset.newBuilder();
    freeLenBuilder.setLabel("创作代码量");
    freeLenBuilder.setBackgroundColor(chartService.randomColor());
    freeLenBuilder.setBorderColor(freeLenBuilder.getBackgroundColor());
    Dataset.Builder scoreBuilder = Chart.Data.Dataset.newBuilder();
    scoreBuilder.setLabel("评估分值");
    scoreBuilder.setBackgroundColor(chartService.randomColor());
    scoreBuilder.setBorderColor(scoreBuilder.getBackgroundColor());

    for (int i = 0; i < data.size(); i++) {
      EduChartDataBean bean = data.get(i);
      labels.add(bean.getName());
      blockLenBuilder.addData(bean.getBlocklen());
      codeLenBuilder.addData(bean.getCodelen());
      scoreBuilder.addData(bean.getScore());
      freeLenBuilder.addData(bean.getFreeLen());
    }
    if (type == CHART_CODE) {
      datasets.add(freeLenBuilder.build());
      datasets.add(codeLenBuilder.build());
    } else if (type == CHART_SCORE) {
      datasets.add(scoreBuilder.build());
    } else {
      // TODO
    }



    String buildChartJson = chartService.buildChartJson("bar", labels, datasets);
    return buildChartJson;
  }

  static final int CHART_CODE = 1;
  static final int CHART_SCORE = 2;

  @RequestMapping("/chartcourse")
  public String chartcourse(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    Builder buider = getBcbBuilder(request);
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "charts/chartstudent";
  }


  @RequestMapping("/chartprogress")
  public String chartprogress(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }

    if (map == null || map.getBody() == null) {
      sis.singleClassSelect(model, true, name, "");
    } else {
      Req body = map.getBody();

      String levelThird = null;
      StudentQueryBean bean = new StudentQueryBean();

      levelThird = body.getLevelThird();

      bean.setClassId(levelThird);

      sis.singleClassSelect(model, true, name, bean.getClassId());
      Map<String, Object> param = Maps.newHashMap();
      param.put("classId", bean.getClassId());
      List<EduClassCourseProgressBean> progress = cservice.listClassCourseProgress(param);

      model.addAttribute("data", progress);
    }

    Builder buider = getBcbBuilder(request);
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "charts/cprogress";
  }
}
