package com.stepiot.controller.educollege;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduDepartment;
import com.stepiot.model.EduDepartmentCriteria;
import com.stepiot.model.EduDepartmentCriteria.Criteria;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduDepartmentManService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduDepartmentController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduDepartmentManService dService;

  @RequestMapping("/edudepartment")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1, name);
    } else {
      fillPagination(model, map.getBody().getCurrentPage(), name);
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.ADD));
    return "man/departmentlist";
  }


  @RequestMapping("/edudepartment/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.SAVE));
    return "man/departmentadd";
  }


  @ResponseBody
  @PostMapping(value = "/edudepartment/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String departmentId = body.getLevelTop();
    String departmentName = body.getValue();
    if (StringUtils.isAnyEmpty(departmentId, departmentName)) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }
    return insertOrUpdate(departmentId, departmentName);
  }

  private String insertOrUpdate(String departmentId, String departmentName) {


    EduDepartment depart = dService.selectByPrimaryKey(departmentId);
    if (depart != null) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }
    EduDepartmentCriteria ex = new EduDepartmentCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andDepartmentNameEqualTo(departmentName);
    long counts = dService.countByExample(ex);
    if (counts > 0) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    depart = new EduDepartment();
    depart.setDepartmentId(departmentId);
    depart.setDepartmentName(departmentName);

    if (dService.insertSelective(depart) > 0) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }


  private void fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<EduDepartment> list = dService.selectByExample(new EduDepartmentCriteria());

    model.addAttribute("data", list);

    Pagination pagination = PaginationUtils.genPagination(current, (long) list.size(), PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

}
