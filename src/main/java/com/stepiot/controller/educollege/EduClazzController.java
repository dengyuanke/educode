package com.stepiot.controller.educollege;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.constants.Cons;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduClass;
import com.stepiot.model.EduClassCriteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduClassService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduClazzController extends BaseController {

  private static final int PAGE_SIZE = 8;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private SelectService ss;

  @Autowired
  private EduClassService cService;

  @RequestMapping("/educlassman")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    long total = 0;
    if (map == null || map.getBody() == null) {
      total = fillPagination(model, 1, name, new StudentQueryBean());
      ss.allDepartmentSelect(model, true, "");
      ss.allDisciplineSelect(model, true, null, "");
    } else {

      Req body = map.getBody();

      String reqSource = body.getReqSource();

      String levelTop = null;
      String levelSecond = null;
      StudentQueryBean bean = null;
      // 请求从分页控件过来
      if (Cons.REQ_SOURCE_PAGINATION.equals(reqSource)) {
        String caches = body.getCaches();
        bean = toTypedBean(caches);
        levelTop = bean.getDepartmentId();
        levelSecond = bean.getDisciplineId();
      } else {
        levelTop = body.getLevelTop();
        levelSecond = body.getLevelSecond();
        bean = new StudentQueryBean();
        bean.setDisciplineId(levelSecond);
        bean.setDepartmentId(levelTop);
      }

      ss.allDepartmentSelect(model, true, bean.getDepartmentId());
      ss.allDisciplineSelect(model, true, bean.getDepartmentId(), bean.getDisciplineId());

      total = fillPagination(model, map.getBody().getCurrentPage(), name, bean);
    }
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + total + "个班级");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.ADD));
    return "man/classlist";
  }

  private long fillPagination(Model model, int current, String name, StudentQueryBean bean) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("disciplineId", bean.getDisciplineId());
    map.put("departmentId", bean.getDepartmentId());

    List<Map<String, Object>> classes = cService.listClasses(map);
    model.addAttribute("data", classes);

    map.remove("pageSize");
    map.remove("limitStart");
    long total = cService.countClasses(map);
    Pagination pagination =
        PaginationUtils.genPagination(current, total, PAGE_SIZE, beanToBase64Json(bean));
    if (pagination == null) {
      return total;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
    return total;
  }

  private StudentQueryBean toTypedBean(String queryJson) {
    try {
      if (StringUtils.isEmpty(queryJson)) {
        return null;
      }

      byte[] data = Base64.decodeBase64(queryJson);
      String json = new String(data, "UTF-8");
      StudentQueryBean bean = GsonUtils.toTypedBean(json, queryBean);
      return bean;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  @RequestMapping("/educlassman/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    ss.allDepartmentSelect(model, true, "");
    ss.allDisciplineSelect(model, true, null, "");
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.SAVE));
    return "man/classadd";
  }


  @ResponseBody
  @PostMapping(value = "/educlassman/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    String name = authentication.getName();
    Req body = map.getBody();
    String disciplineId = body.getLevelSecond();
    String classId = body.getLevelThird();
    String className = body.getValue();
    if (StringUtils.isAnyEmpty(disciplineId, classId, className)) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    return insertOrUpdate(disciplineId, classId, className, name);
  }

  private String insertOrUpdate(String disciplineId, String classId, String className,
      String username) {


    EduClass clazz = cService.selectByPrimaryKey(classId);
    if (clazz != null) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }
    EduClassCriteria ex = new EduClassCriteria();
    com.stepiot.model.EduClassCriteria.Criteria criteria = ex.createCriteria();
    criteria.andDisciplineIdEqualTo(disciplineId).andClassNameEqualTo(className);
    long counts = cService.countByExample(ex);
    if (counts > 0) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    clazz = new EduClass();
    clazz.setClassId(classId);
    clazz.setClassName(className);
    clazz.setDisciplineId(disciplineId);
    clazz.setCreateDate(new Date());
    clazz.setCreateUser(username);

    if (cService.insertSelective(clazz) > 0) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }

  private String beanToBase64Json(StudentQueryBean bean) {
    try {
      if (bean == null) {
        return null;
      }
      String json = GsonUtils.fromObj(bean);
      return Base64.encodeBase64URLSafeString(json.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  static final TypeToken<StudentQueryBean> queryBean = new TypeToken<StudentQueryBean>() {};


}
