package com.stepiot.controller.educollege;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.constants.Cons;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduStudentClassRef;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import com.stepiot.model.EduUserCriteria.Criteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduHmacService;
import com.stepiot.service.EduStudentRefService;
import com.stepiot.service.EduUserService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;
import com.stepiot.util.PassWordUtils;
import com.stepiot.util.ValUtils;

@Controller
public class EduStudentManController extends BaseController {

  private static final int PAGE_SIZE = 32;

  @Autowired
  private EduStudentRefService service;

  @Autowired
  private SelectService ss;

  @Autowired
  private EduHmacService hmac;

  @Autowired
  private EduUserService uservice;

  @Autowired
  private EmailValidator emailValidator;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduStudentRefService studentRef;

  @RequestMapping("/edustudents")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    long total = 0;
    if (map == null || map.getBody() == null) {
      total = fillPagination(model, 1, new StudentQueryBean());
      ss.allDepartmentSelect(model, true, "");
      ss.allDisciplineSelect(model, true, "", "");
      ss.allClassSelect(model, true, "", "", "");
    } else {
      Req body = map.getBody();
      String reqSource = body.getReqSource();
      String levelTop = null;
      String levelSecond = null;
      String levelThird = null;
      StudentQueryBean bean = null;
      // 请求从分页控件过来
      if (Cons.REQ_SOURCE_PAGINATION.equals(reqSource)) {
        String caches = body.getCaches();
        bean = toTypedBean(caches);
        levelTop = bean.getDepartmentId();
        levelSecond = bean.getDisciplineId();
        levelThird = bean.getClassId();
      } else {
        levelTop = body.getLevelTop();
        levelSecond = body.getLevelSecond();
        levelThird = body.getLevelThird();
        bean = new StudentQueryBean();
        bean.setClassId(levelThird);
        bean.setDisciplineId(levelSecond);
        bean.setDepartmentId(levelTop);
      }
      ss.allDepartmentSelect(model, true, bean.getDepartmentId());
      ss.allDisciplineSelect(model, true, bean.getDepartmentId(), bean.getDisciplineId());
      ss.allClassSelect(model, true, bean.getDisciplineId(), bean.getDepartmentId(),
          bean.getClassId());

      total = fillPagination(model, map.getBody().getCurrentPage(), bean);
    }
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + total + "名学生");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.ADD));
    return "edustudents/list";
  }

  @RequestMapping(value = "/edustudents/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    ss.allDepartmentSelect(model, true, "");
    ss.allDisciplineSelect(model, true, "", "");
    ss.allClassSelect(model, true, "", "", "");
    ss.genderSelector(model, true, "");
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.SAVE));
    return "edustudents/add";
  }

  @RequestMapping(value = "/edustudents/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    String username = body.getUsername();
    Map<String, Object> maps = Maps.newHashMap();
    maps.put("studentId", username);
    List<Map<String, Object>> students = service.listAllStudentsByMap(maps);
    Map<String, Object> student = students.get(0);
    String departmentId = (String) student.get("departmentId");
    String disciplineId = (String) student.get("disciplineId");
    String classId = (String) student.get("classId");


    HashMap<String, String> forHmac = Maps.newHashMap();
    forHmac.put("username", (String) student.get("username"));
    String signature = hmac.hmacSha1(forHmac);
    student.put("signature", signature);
    ss.allDepartmentSelect(model, false, departmentId);
    ss.allDisciplineSelect(model, false, departmentId, disciplineId);
    ss.allClassSelect(model, false, disciplineId, departmentId, classId);
    ss.genderSelector(model, true, (Integer) student.get("gender") + "");
    model.addAttribute("data", student);

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SAVE));
    return "edustudents/edit";
  }

  @ResponseBody
  @Transactional
  @PostMapping(value = "/edustudents/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {

    Req body = map.getBody();
    String username = body.getUsername();
    String signature = body.getSignature();
    if (StringUtils.isAnyEmpty(username, signature)) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

    Map<String, String> forSign = ImmutableBiMap.of("username", username);
    String signed = hmac.hmacSha1(forSign);
    if (!signature.equals(signed)) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

    EduUser eduUser = uservice.selectByPrimaryKey(username);

    String levelThird = body.getLevelThird();
    int gender = body.getGender();

    int age = body.getAge();
    String telephone = body.getTelephone();
    String email = body.getEmail();
    String name = body.getName();

    HashMap<String, String> maap = Maps.newHashMap();
    if (StringUtils.isNotEmpty(email) && !emailValidator.isValid(email)) {
      maap.putAll(reservice.of("email", "validate.email.wrong"));
    }
    if (StringUtils.isNotEmpty(telephone) && !ValUtils.isValidTelephone(telephone)) {
      maap.putAll(reservice.of("telephone", "validate.telephone.wrong"));
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }

    EduUserCriteria ex = new EduUserCriteria();
    Criteria ext = ex.createCriteria();
    if (StringUtils.isNotEmpty(telephone)) {
      ext.andTelephoneEqualTo(telephone).andUsernameNotEqualTo(username);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("telephone", "validate.telephone.exsits"));
      }
    }
    if (StringUtils.isNotEmpty(email)) {
      ex.clear();
      ext = ex.createCriteria();
      ext.andEmailEqualTo(email).andUsernameNotEqualTo(username);
      long counts = uservice.countByExample(ex);
      if (counts > 0) {
        maap.putAll(reservice.of("email", "validate.email.exsits"));
      }
    }

    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    if (StringUtils.isEmpty(email)) {
      email = null;
    }
    if (StringUtils.isEmpty(telephone)) {
      telephone = null;
    }
    eduUser.setAge(age);
    eduUser.setEmail(email);
    eduUser.setGender(gender);
    eduUser.setName(name);
    eduUser.setTelephone(telephone);

    ex.clear();
    ext = ex.createCriteria();
    ext.andUsernameEqualTo(username);

    EduStudentClassRef ref = studentRef.selectByStudentId(username);
    // 修改学生所在班级
    if (!ref.getClassId().equals(levelThird)) {
      ref.setClassId(levelThird);
      studentRef.updateByPrimaryKey(ref);
    }

    if (uservice.updateByExampleSelective(eduUser, ex) == 1) {
      return reservice.response(HttpStatus.OK, null, "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

  }

  @ResponseBody
  @Transactional
  @PostMapping(value = "/edustudents/save")
  public String save(Authentication authentication, @ModelAttribute ProtoRequest map, Model model) {

    Req body = map.getBody();
    String username = body.getUsername();
    String levelThird = body.getLevelThird();
    String name = body.getName();
    if (StringUtils.isAnyEmpty(username, name, levelThird)) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.save.fail");
    }

    EduUser eduUser = uservice.selectByPrimaryKey(username);
    if (eduUser != null) {
      // 该用户名已存在
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.exsits");
    }

    int gender = body.getGender();
    int age = body.getAge();
    String telephone = body.getTelephone();
    String email = body.getEmail();

    HashMap<String, String> maap = Maps.newHashMap();
    if (StringUtils.isNotEmpty(email) && !emailValidator.isValid(email)) {
      maap.putAll(reservice.of("email", "validate.email.wrong"));
    }
    if (StringUtils.isNotEmpty(telephone) && !ValUtils.isValidTelephone(telephone)) {
      maap.putAll(reservice.of("telephone", "validate.telephone.wrong"));
    }
    // 校验手机号邮箱地址
    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }


    EduUserCriteria ex = new EduUserCriteria();
    Criteria ext = ex.createCriteria();
    if (StringUtils.isNotEmpty(telephone)) {
      ext.andTelephoneEqualTo(telephone);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("telephone", "validate.telephone.exsits"));
      }
    }
    if (StringUtils.isNotEmpty(email)) {
      ex.clear();
      ext = ex.createCriteria();
      ext.andEmailEqualTo(email);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("email", "validate.email.exsits"));
      }
    }



    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    if (StringUtils.isEmpty(email)) {
      email = null;
    }
    if (StringUtils.isEmpty(telephone)) {
      telephone = null;
    }
    eduUser = new EduUser();
    eduUser.setAge(age);
    eduUser.setEmail(email);
    eduUser.setGender(gender);
    eduUser.setName(name);
    eduUser.setTelephone(telephone);
    eduUser.setEnabled((byte) 1);
    eduUser.setUsername(username);
    eduUser.setPassword(PassWordUtils.encodePassword(username));
    uservice.insertUserAndRole(eduUser, RoleEnum.ROLE_STUDENT);
    EduStudentClassRef ref = studentRef.selectByStudentId(username);
    // 修改学生所在班级
    if (ref != null && !ref.getClassId().equals(levelThird)) {
      ref.setClassId(levelThird);
      studentRef.updateByPrimaryKey(ref);
    } else {
      ref = new EduStudentClassRef();
      ref.setClassId(levelThird);
      ref.setStudentId(username);
      studentRef.insert(ref);
    }

    return reservice.response(HttpStatus.OK, null, "global.message.save.success");

  }

  private long fillPagination(Model model, int current, StudentQueryBean bean) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("classId", bean.getClassId());
    map.put("disciplineId", bean.getDisciplineId());
    map.put("departmentId", bean.getDepartmentId());

    List<Map<String, Object>> students = service.listAllStudentsByMap(map);
    model.addAttribute("data", students);

    map.remove("pageSize");
    map.remove("limitStart");
    long total = service.countAllStudents(map);
    Pagination pagination =
        PaginationUtils.genPagination(current, total, PAGE_SIZE, beanToBase64Json(bean));
    if (pagination == null) {
      return total;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
    return total;
  }

  private StudentQueryBean toTypedBean(String queryJson) {
    try {
      if (StringUtils.isEmpty(queryJson)) {
        return null;
      }

      byte[] data = Base64.decodeBase64(queryJson);
      String json = new String(data, "UTF-8");
      StudentQueryBean bean = GsonUtils.toTypedBean(json, queryBean);
      return bean;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  private String beanToBase64Json(StudentQueryBean bean) {
    try {
      if (bean == null) {
        return null;
      }
      String json = GsonUtils.fromObj(bean);
      return Base64.encodeBase64URLSafeString(json.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  static final TypeToken<StudentQueryBean> queryBean = new TypeToken<StudentQueryBean>() {};

}
