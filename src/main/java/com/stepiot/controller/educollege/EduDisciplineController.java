package com.stepiot.controller.educollege;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduDiscipline;
import com.stepiot.model.EduDisciplineCriteria;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduDisciplineService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduDisciplineController extends BaseController {

  private static final int PAGE_SIZE = 32;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private SelectService sis;

  @Autowired
  private EduDisciplineService dService;

  @RequestMapping("/edudiscipline")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    if (map == null || map.getBody() == null) {
      fillPagination(model, 1);
    } else {
      fillPagination(model, map.getBody().getCurrentPage());
    }

    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.ADD));
    return "man/disciplinelist";
  }


  @RequestMapping("/edudiscipline/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    sis.allDepartmentSelect(model, true, "");
    model.addAttribute("bcb", BcbCreator.create(getBcbBuilder(request), BcbCreator.SAVE));
    return "man/disciplineadd";
  }


  @ResponseBody
  @PostMapping(value = "/edudiscipline/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String departmentId = body.getLevelTop();
    String disciplineName = body.getValue();
    String disciplineId = body.getLevelSecond();
    if (StringUtils.isAnyEmpty(departmentId, disciplineName, disciplineId)) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    return insertOrUpdate(departmentId, disciplineId, disciplineName);
  }

  private String insertOrUpdate(String departmentId, String disciplineId, String disciplineName) {


    EduDiscipline discipline = dService.selectByPrimaryKey(disciplineId);
    if (discipline != null) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }
    EduDisciplineCriteria ex = new EduDisciplineCriteria();
    com.stepiot.model.EduDisciplineCriteria.Criteria criteria = ex.createCriteria();
    criteria.andDisciplineIdEqualTo(disciplineId).andDepartmentIdEqualTo(departmentId)
        .andDisciplineNameEqualTo(disciplineName);
    long counts = dService.countByExample(ex);
    if (counts > 0) {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

    discipline = new EduDiscipline();
    discipline.setDepartmentId(departmentId);
    discipline.setDisciplineId(disciplineId);
    discipline.setDisciplineName(disciplineName);

    if (dService.insertSelective(discipline) > 0) {
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    } else {
      return reservice.response(HttpStatus.CONFLICT, "", "global.message.update.fail");
    }

  }


  private void fillPagination(Model model, int current) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);

    List<Map<String, Object>> allDiscipline = dService.listAllDiscipline(map);

    model.addAttribute("data", allDiscipline);
    long counts = dService.countByExample(new EduDisciplineCriteria());
    Pagination pagination = PaginationUtils.genPagination(current, counts, PAGE_SIZE);
    if (pagination == null) {
      return;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
  }

}
