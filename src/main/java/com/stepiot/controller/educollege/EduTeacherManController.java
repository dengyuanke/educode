package com.stepiot.controller.educollege;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.protobuf.ProtocolStringList;
import com.stepiot.beans.EduTeacherQueryBean;
import com.stepiot.beans.EduTreeNodeBean;
import com.stepiot.constants.RoleEnum;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduTeacherClassRef;
import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import com.stepiot.model.EduUserCriteria.Criteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.service.EduClassService;
import com.stepiot.service.EduHmacService;
import com.stepiot.service.EduTeacherRefService;
import com.stepiot.service.EduUserService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PassWordUtils;
import com.stepiot.util.ValUtils;

@Controller
public class EduTeacherManController extends BaseController {

  @Autowired
  private EduTeacherRefService service;

  @Autowired
  private EduClassService cService;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduTeacherRefService tcRef;

  @Autowired
  private EmailValidator emailValidator;

  @Autowired
  private EduHmacService hmac;

  @Autowired
  private EduUserService uservice;

  @RequestMapping("/eduteacher")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    List<EduTeacherQueryBean> teachers = service.listAllTeachers(Maps.newHashMap());
    model.addAttribute("data", teachers);
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + teachers.size() + "名教师");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.ADD));
    return "eduteacher/list";
  }

  @RequestMapping(value = "/eduteacher/add")
  public String add(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    this.buildTree(model);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SAVE));
    return "eduteacher/add";
  }

  @RequestMapping(value = "/eduteacher/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    Req body = map.getBody();
    String username = body.getUsername();

    List<EduTeacherClassRef> list = tcRef.selectByTeacherId(username);
    Set<String> set = Sets.newHashSet();
    for (EduTeacherClassRef ref : list) {
      set.add(ref.getClassId());
    }
    this.buildTree(model, true, set, false);
    EduUser user = uservice.selectByPrimaryKey(username);
    HashMap<String, String> forHmac = Maps.newHashMap();
    forHmac.put("username", username);
    String signature = hmac.hmacSha1(forHmac);
    model.addAttribute("signature", signature);
    model.addAttribute("data", user);
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.SAVE));
    return "eduteacher/edit";
  }

  @Transactional
  @ResponseBody
  @PostMapping(value = "/eduteacher/apdate")
  public String apdate(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    Req body = map.getBody();
    String username = body.getUsername();
    String name = body.getName();
    String signature = body.getSignature();
    ProtocolStringList clazzIdList = body.getClazzIdList();
    if (StringUtils.isAnyEmpty(name, username, signature) || clazzIdList == null
        || clazzIdList.size() < 1) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

    Map<String, String> forSign = ImmutableBiMap.of("username", username);
    String signed = hmac.hmacSha1(forSign);
    if (!signature.equals(signed)) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

    EduUser eduUser = uservice.selectByPrimaryKey(username);

    if (eduUser == null) {
      // 该用户名不存在
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.update.fail");
    }

    int gender = body.getGender();
    String telephone = body.getTelephone();
    String email = body.getEmail();

    HashMap<String, String> maap = Maps.newHashMap();
    if (StringUtils.isNotEmpty(email) && !emailValidator.isValid(email)) {
      maap.putAll(reservice.of("email", "validate.email.wrong"));
    }
    if (StringUtils.isNotEmpty(telephone) && !ValUtils.isValidTelephone(telephone)) {
      maap.putAll(reservice.of("telephone", "validate.telephone.wrong"));
    }
    // 校验手机号邮箱地址
    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    EduUserCriteria ex = new EduUserCriteria();
    Criteria ext = ex.createCriteria();
    if (StringUtils.isNotEmpty(telephone)) {
      ext.andUsernameNotEqualTo(username);
      ext.andTelephoneEqualTo(telephone);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("telephone", "validate.telephone.exsits"));
      }
    }
    if (StringUtils.isNotEmpty(email)) {
      ex.clear();
      ext = ex.createCriteria();
      ext.andUsernameNotEqualTo(username);
      ext.andEmailEqualTo(email);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("email", "validate.email.exsits"));
      }
    }
    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    if (StringUtils.isEmpty(email)) {
      email = null;
    }
    if (StringUtils.isEmpty(telephone)) {
      telephone = null;
    }
    eduUser.setEmail(email);
    eduUser.setGender(gender);
    eduUser.setName(name);
    eduUser.setTelephone(telephone);
    eduUser.setUsername(username);
    eduUser.setEnabled((byte) 1);
    eduUser.setPassword(PassWordUtils.encodePassword(username));
    uservice.updateUserAndRole(eduUser, RoleEnum.ROLE_TEACHER);
    tcRef.deleteByTeacherId(username);
    for (int i = 0; i < clazzIdList.size(); i++) {
      EduTeacherClassRef ref = new EduTeacherClassRef();
      ref.setClassId(clazzIdList.get(i));
      ref.setTearchId(username);
      tcRef.insert(ref);
    }
    return reservice.response(HttpStatus.OK, null, "global.message.save.success");
  }


  @Transactional
  @ResponseBody
  @PostMapping(value = "/eduteacher/save")
  public String save(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    Req body = map.getBody();
    String username = body.getUsername();
    ProtocolStringList clazzIdList = body.getClazzIdList();
    String name = body.getName();
    if (StringUtils.isAnyEmpty(username, name) || clazzIdList == null || clazzIdList.size() < 1) {
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.save.fail");
    }

    EduUser eduUser = uservice.selectByPrimaryKey(username);
    if (eduUser != null) {
      // 该用户名已存在
      return reservice.response(HttpStatus.BAD_REQUEST, null, "global.message.exsits");
    }

    int gender = body.getGender();
    String telephone = body.getTelephone();
    String email = body.getEmail();

    HashMap<String, String> maap = Maps.newHashMap();
    if (StringUtils.isNotEmpty(email) && !emailValidator.isValid(email)) {
      maap.putAll(reservice.of("email", "validate.email.wrong"));
    }
    if (StringUtils.isNotEmpty(telephone) && !ValUtils.isValidTelephone(telephone)) {
      maap.putAll(reservice.of("telephone", "validate.telephone.wrong"));
    }
    // 校验手机号邮箱地址
    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    EduUserCriteria ex = new EduUserCriteria();
    Criteria ext = ex.createCriteria();
    if (StringUtils.isNotEmpty(telephone)) {
      ext.andTelephoneEqualTo(telephone);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("telephone", "validate.telephone.exsits"));
      }
    }
    if (StringUtils.isNotEmpty(email)) {
      ex.clear();
      ext = ex.createCriteria();
      ext.andEmailEqualTo(email);
      long counts = uservice.countByExample(ex);

      if (counts > 0) {
        maap.putAll(reservice.of("email", "validate.email.exsits"));
      }
    }
    if (!maap.isEmpty()) {
      return reservice.response(HttpStatus.BAD_REQUEST, maap);
    }
    if (StringUtils.isEmpty(email)) {
      email = null;
    }
    if (StringUtils.isEmpty(telephone)) {
      telephone = null;
    }
    eduUser = new EduUser();
    eduUser.setEmail(email);
    eduUser.setGender(gender);
    eduUser.setName(name);
    eduUser.setEnabled((byte) 1);
    eduUser.setTelephone(telephone);
    eduUser.setUsername(username);
    eduUser.setPassword(PassWordUtils.encodePassword(username));
    uservice.insertUserAndRole(eduUser, RoleEnum.ROLE_TEACHER);
    tcRef.deleteByTeacherId(username);
    for (int i = 0; i < clazzIdList.size(); i++) {
      EduTeacherClassRef ref = new EduTeacherClassRef();
      ref.setClassId(clazzIdList.get(i));
      ref.setTearchId(username);
      tcRef.insert(ref);
    }
    return reservice.response(HttpStatus.OK, null, "global.message.save.success");
  }


  private void buildTree(Model model) {
    this.buildTree(model, false, null, false);
  }

  private void buildTree(Model model, boolean opened, Set<String> selectedSet, boolean disabled) {
    if (selectedSet == null) {
      selectedSet = Sets.newHashSet();
    }
    Map<String, Object> param = Maps.newHashMap();
    List<Map<String, Object>> list = Lists.newArrayList();
    if (containsRole(RoleEnum.ROLE_COADMIN.getValue())) {
      list.addAll(cService.listClasses(param));
    } else {
      return;
    }
    Set<String> classes = Sets.newHashSet();
    Set<EduTreeNodeBean> classSet = Sets.newHashSet();
    Set<EduTreeNodeBean> disSet = Sets.newHashSet();
    Set<EduTreeNodeBean> depSet = Sets.newHashSet();
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> item = list.get(i);
      EduTreeNodeBean bean = new EduTreeNodeBean();
      bean.setId((String) item.get("classId"));
      bean.setText((String) item.get("className"));
      bean.setParent((String) item.get("disciplineId"));
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }

      classSet.add(bean);
      classes.add("\"" + (String) item.get("classId") + "\"");

      bean = new EduTreeNodeBean();
      bean.setId((String) item.get("disciplineId"));
      bean.setText((String) item.get("disciplineName"));
      bean.setParent((String) item.get("departmentId"));
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }
      disSet.add(bean);

      bean = new EduTreeNodeBean();
      bean.setId((String) item.get("departmentId"));
      bean.setText((String) item.get("departmentName"));
      bean.setParent("#");
      bean.setOpened(opened);
      bean.setDisabled(disabled);
      if (selectedSet.contains(bean.getId())) {
        bean.setSelected(true);
      }
      depSet.add(bean);
    }

    classSet.addAll(disSet);
    classSet.addAll(depSet);

    model.addAttribute("classes", classes);
    model.addAttribute("classTreeStr", GsonUtils.fromObj(classSet));
  }
}
