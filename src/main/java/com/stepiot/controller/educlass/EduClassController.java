package com.stepiot.controller.educlass;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.model.EduOptionsClass;
import com.stepiot.model.EduOptionsClassCriteria;
import com.stepiot.model.EduOptionsClassCriteria.Criteria;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduClassService;
import com.stepiot.service.EduOptionsClassService;
import com.stepiot.service.ProtoResponseService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduClassController extends BaseController {

  private static final int PAGE_SIZE = 32;

  @Autowired
  private ProtoResponseService reservice;

  @Autowired
  private EduClassService service;

  @Autowired
  private EduOptionsClassService optService;

  @RequestMapping("/educlass")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {

    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    long total = 0;
    if (map == null || map.getBody() == null) {
      total = fillPagination(model, 1, name);
    } else {
      total = fillPagination(model, map.getBody().getCurrentPage(), name);
    }
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + total + "个班级");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "educlass/list";
  }

  @ResponseBody
  @PostMapping(value = "/educlass/apdate")
  public String update(Authentication authentication, @ModelAttribute ProtoRequest map,
      Model model) {
    Req body = map.getBody();
    String clazzId = body.getClazzId(0);
    String value = body.getValue();

    EduOptionsClassCriteria ex = new EduOptionsClassCriteria();
    Criteria criteria = ex.createCriteria();
    criteria.andClazzIdEqualTo(clazzId);
    EduOptionsClass one = optService.selectByExampleForOne(ex);
    if (one == null) {
      one = new EduOptionsClass();
      one.setClazzId(clazzId);
      one.setOpenVideo(Integer.parseInt(value));
      optService.insert(one);
      return reservice.response(HttpStatus.OK, "", "global.message.save.success");
    } else {
      one.setOpenVideo(Integer.parseInt(value));
      optService.updateByPrimaryKey(one);
      return reservice.response(HttpStatus.OK, "", "global.message.update.success");
    }

  }

  private long fillPagination(Model model, int current, String name) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("username", name);

    List<Map<String, Object>> myClasses = service.listMyClasses(map);
    model.addAttribute("data", myClasses);

    long total = service.countClassByTeacherId(name);
    Pagination pagination = PaginationUtils.genPagination(current, total, PAGE_SIZE);
    if (pagination == null) {
      return total;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
    return total;
  }

}
