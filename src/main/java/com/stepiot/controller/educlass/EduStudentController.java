package com.stepiot.controller.educlass;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.beans.StudentQueryBean;
import com.stepiot.constants.Cons;
import com.stepiot.controller.ProtoRequest;
import com.stepiot.controller.base.BaseController;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;
import com.stepiot.proto.HttpProto.Req;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.service.EduStudentRefService;
import com.stepiot.service.SelectService;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.PaginationUtils;

@Controller
public class EduStudentController extends BaseController {

  private static final int PAGE_SIZE = 32;

  @Autowired
  private EduStudentRefService service;

  @Autowired
  private SelectService ss;

  @RequestMapping("/edustudent")
  public String pmyblockly(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String name = authentication.getName();
    if (!model.containsAttribute("titleName")) {
      model.addAttribute("titleName", "titlename.device.list");
    }
    long total = 0;
    if (map == null || map.getBody() == null) {
      total = fillPagination(model, 1, name, new StudentQueryBean());
      ss.departmentSelect(model, true, name, "");
      ss.disciplineSelect(model, true, name, "", "");
      ss.classSelect(model, true, name, "", "", "");
    } else {
      Req body = map.getBody();
      String reqSource = body.getReqSource();
      String levelTop = null;
      String levelSecond = null;
      String levelThird = null;
      StudentQueryBean bean = null;
      // 请求从分页控件过来
      if (Cons.REQ_SOURCE_PAGINATION.equals(reqSource)) {
        String caches = body.getCaches();
        bean = toTypedBean(caches);
        levelTop = bean.getDepartmentId();
        levelSecond = bean.getDisciplineId();
        levelThird = bean.getClassId();
      } else {
        levelTop = body.getLevelTop();
        levelSecond = body.getLevelSecond();
        levelThird = body.getLevelThird();
        bean = new StudentQueryBean();
        bean.setClassId(levelThird);
        bean.setDisciplineId(levelSecond);
        bean.setDepartmentId(levelTop);
      }
      ss.departmentSelect(model, true, name, bean.getDepartmentId());
      ss.disciplineSelect(model, true, name, bean.getDepartmentId(), bean.getDisciplineId());
      ss.classSelect(model, true, name, bean.getDisciplineId(), bean.getDepartmentId(),
          bean.getClassId());

      total = fillPagination(model, map.getBody().getCurrentPage(), name, bean);
    }
    Builder buider = getBcbBuilder(request);
    buider.setItem("总共" + total + "名学生");
    model.addAttribute("bcb", BcbCreator.create(buider, BcbCreator.NOOP));
    return "edustudent/list";
  }


  @RequestMapping(value = "/edustudent/edit")
  public String edit(HttpServletRequest request, Authentication authentication,
      @ModelAttribute ProtoRequest map, Model model) {
    String teacherId = authentication.getName();
    Req body = map.getBody();
    String username = body.getUsername();
    Map<String, Object> maps = Maps.newHashMap();
    maps.put("username", teacherId);
    maps.put("studentId", username);
    List<Map<String, Object>> students = service.listStudentsByMap(maps);
    Map<String, Object> student = students.get(0);
    String departmentId = (String) student.get("departmentId");
    String disciplineId = (String) student.get("disciplineId");
    String classId = (String) student.get("classId");

    ss.departmentSelect(model, false, teacherId, departmentId);
    ss.disciplineSelect(model, false, teacherId, departmentId, disciplineId);
    ss.classSelect(model, false, teacherId, disciplineId, departmentId, classId);
    model.addAttribute("data", student);

    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.NOOP));
    return "edustudent/edit";
  }

  private long fillPagination(Model model, int current, String name, StudentQueryBean bean) {
    Map<String, Object> map = Maps.newHashMap();
    // map.put("orderByClause", "outline_id");
    map.put("limitStart", Math.max(current - 1, 0) * PAGE_SIZE);
    map.put("pageSize", PAGE_SIZE);
    map.put("classId", bean.getClassId());
    map.put("username", name);
    map.put("disciplineId", bean.getDisciplineId());
    map.put("departmentId", bean.getDepartmentId());

    List<Map<String, Object>> students = service.listStudentsByMap(map);
    model.addAttribute("data", students);

    map.remove("pageSize");
    map.remove("limitStart");
    long total = service.countStudents(map);
    Pagination pagination =
        PaginationUtils.genPagination(current, total, PAGE_SIZE, beanToBase64Json(bean));
    if (pagination == null) {
      return total;
    }
    String json = null;
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(pagination.toBuilder());
    } catch (InvalidProtocolBufferException e) {
    }
    Map<String, Object> maps = GsonUtils.toMap(json);
    model.addAttribute("pagination", maps);
    return total;
  }

  private StudentQueryBean toTypedBean(String queryJson) {
    try {
      if (StringUtils.isEmpty(queryJson)) {
        return null;
      }

      byte[] data = Base64.decodeBase64(queryJson);
      String json = new String(data, "UTF-8");
      StudentQueryBean bean = GsonUtils.toTypedBean(json, queryBean);
      return bean;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  private String beanToBase64Json(StudentQueryBean bean) {
    try {
      if (bean == null) {
        return null;
      }
      String json = GsonUtils.fromObj(bean);
      return Base64.encodeBase64URLSafeString(json.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }

  }

  static final TypeToken<StudentQueryBean> queryBean = new TypeToken<StudentQueryBean>() {};

}
