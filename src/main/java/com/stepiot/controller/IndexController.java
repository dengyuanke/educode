
package com.stepiot.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.stepiot.controller.base.BaseController;
import com.stepiot.util.BcbCreator;

@Controller
public class IndexController extends BaseController {

  @GetMapping("/about")
  public String about(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "about";
  }

  @GetMapping("/noop")
  @ResponseBody
  public String noop(HttpServletRequest request, Authentication authentication, Model model) {
    return "noop";
  }

  @GetMapping("/admin")
  public String admin(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "admin";
  }

  @GetMapping("/admin**")
  public ModelAndView adminPage(HttpServletRequest request, Authentication authentication,
      Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    ModelAndView mv = new ModelAndView();
    mv.addObject("title", "Spring Security Hello World");
    mv.addObject("message", "This is protected page - Admin Page!");
    mv.setViewName("admin");
    return mv;

  }

  @GetMapping("/home")
  public String home(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "home";
  }


  @GetMapping({"/dynamicadds"})
  public String homes(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("titleName", "首页");
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/dynamicadd";
  }

  @GetMapping({"/chart"})
  public String dashboard(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/chart";
  }

  @GetMapping("/user")
  public String user(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user";
  }

  @GetMapping("/userall")
  public String userall(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/userall";
  }

  @GetMapping("/useradd")
  public String useradd(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/useradd";
  }

  @GetMapping("/sensor")
  public String sensor(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/sensor";
  }

  @GetMapping("/lab")
  public String lab(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/lab";
  }

  @GetMapping("/demo")
  public String demo(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/demo";
  }

  @GetMapping("/logic")
  public String logic(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/logic";
  }

  @GetMapping("/linkdevice")
  public String linkdevice(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/linkdevice";
  }

  @GetMapping("/dynamicadd")
  public String dynamicadd(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/dynamicadd";
  }

  @GetMapping("/statusshow")
  public String statusshow(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/statusshow";
  }

  @GetMapping("/download")
  public String download(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/download";
  }

  @GetMapping("/program")
  public String program(HttpServletRequest request, Authentication authentication, Model model) {
    model.addAttribute("name", System.currentTimeMillis());
    model.addAttribute("bcb", BcbCreator.create(super.getBcbBuilder(request), BcbCreator.LIST));
    return "user/program";
  }

  @GetMapping("/forbidden")
  public String forbidden(HttpServletRequest request, Authentication authentication, Model model) {
    return "error/403";
  }

}
