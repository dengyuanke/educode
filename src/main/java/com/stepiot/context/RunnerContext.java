package com.stepiot.context;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import com.google.common.collect.Maps;
import com.stepiot.iot.IotUserCore;

public class RunnerContext {

  private static final Map<String, ExecutorService> _runnerMap = Maps.newConcurrentMap();
  // private static Map<String, IotUserCore> iotUserCoreMap = Maps.newConcurrentMap();

  public static ExecutorService getUserExecutorService(final String username) {

    IotUserCore iotUser = IotUserCore.getInstance(username);

    synchronized (iotUser) {
      ExecutorService service = _runnerMap.get(username);
      if (service != null) {
        if (iotUser.hasJob()) {
          iotUser.setInterrupt(1);
          service.shutdown();
          try {
            service.awaitTermination(5, TimeUnit.SECONDS);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          service = newService(username);
          _runnerMap.put(username, service);
        } else {
          iotUser.setHasJob(true);
          iotUser.setInterrupt(0);
        }
        return service;
      }
      iotUser.setHasJob(true);
      service = newService(username);
      _runnerMap.put(username, service);
      return service;
    }

  }

  private static ExecutorService newService(String username) {
    ExecutorService service = Executors.newSingleThreadExecutor(new ThreadFactory() {
      int x = 0;

      @Override
      public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, username + "_" + x);
        thread.setDaemon(true);
        return thread;
      }
    });
    return service;
  }
}
