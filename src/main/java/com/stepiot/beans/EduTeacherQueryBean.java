package com.stepiot.beans;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.Lists;
import com.stepiot.model.EduClass;

public class EduTeacherQueryBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1875897879054723821L;
  private String teacherId;
  private String name;
  private String username;
  private String telephone;
  private String email;
  private Integer classCounts;
  private String classIds;
  private String classNames;

  public String getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(String teacherId) {
    this.teacherId = teacherId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Integer getClassCounts() {
    return classCounts;
  }

  public void setClassCounts(Integer classCounts) {
    this.classCounts = classCounts;
  }

  public List<EduClass> getClasses() {
    int counts = this.getClassCounts();
    if (counts > 0) {
      List<EduClass> list = Lists.newArrayList();
      String[] clazzIds = this.getClassIds();
      String[] clazzNames = this.getClassNames();
      int minGroupSize = Math.min(Math.min(clazzIds.length, clazzNames.length), 10);
      for (int i = 0; i < minGroupSize; i++) {
        EduClass clazz = new EduClass();
        clazz.setClassId(clazzIds[i]);
        clazz.setClassName(clazzNames[i]);
        if (StringUtils.isAnyEmpty(clazz.getClassId(), clazz.getClassName())) {
          break;
        }
        list.add(clazz);
      }
      if (list.size() < counts) {
        EduClass clazz = new EduClass();
        clazz.setClassId("-");
        clazz.setClassName("···");
        list.add(clazz);
      }
      return list;
    }
    return null;
  }

  public String[] getClassIds() {
    if (StringUtils.isNotEmpty(classIds)) {
      return StringUtils.split(classIds, ",");
    }
    return null;
  }

  public void setClassIds(String classIds) {
    this.classIds = classIds;

  }

  public String[] getClassNames() {
    if (StringUtils.isNotEmpty(classNames)) {
      return StringUtils.split(classNames, ",");
    }
    return null;
  }

  public void setClassNames(String classNames) {
    this.classNames = classNames;
  }


}
