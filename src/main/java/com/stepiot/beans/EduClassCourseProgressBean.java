package com.stepiot.beans;

public class EduClassCourseProgressBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2993691339071380334L;

  private int edited = 0;
  private String studentId = "";
  private String name = "";
  private String classId = "";
  private int total = 0;

  public int getEdited() {
    return edited;
  }

  public void setEdited(int edited) {
    this.edited = edited;
  }

  public String getPercent() {
    return (int) (edited * 1.0 / total * 100) + "%";
  }

  public String getStudentId() {
    return studentId;
  }

  public void setStudentId(String studentId) {
    this.studentId = studentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getClassId() {
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }


}
