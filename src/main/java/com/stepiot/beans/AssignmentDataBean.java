package com.stepiot.beans;

import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class AssignmentDataBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1225684298737369684L;
  private String name;
  private String code;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getUserBlockly() {
    if (StringUtils.isNotEmpty(userBlockly)) {
      try {
        return Base64.encodeBase64String(userBlockly.getBytes("UTF-8"));
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return "";
  }

  public void setUserBlockly(String userBlockly) {
    this.userBlockly = userBlockly;
  }

  private String username;
  private String userBlockly;


}
