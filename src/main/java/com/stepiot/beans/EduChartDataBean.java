package com.stepiot.beans;

public class EduChartDataBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4389638136052187210L;
  private int codelen;
  private int blocklen;
  private int freeTBlocklyLen;
  private int freeNBlocklyLen;
  private int score;

  public int getFreeLen() {
    return this.getFreeNBlocklyLen() + this.getFreeTBlocklyLen();
  }

  public int getFreeTBlocklyLen() {
    return freeTBlocklyLen;
  }

  public void setFreeTBlocklyLen(int freeTBlocklyLen) {
    this.freeTBlocklyLen = freeTBlocklyLen;
  }

  public int getFreeNBlocklyLen() {
    return freeNBlocklyLen;
  }

  public void setFreeNBlocklyLen(int freeNBlocklyLen) {
    this.freeNBlocklyLen = freeNBlocklyLen;
  }

  private String username;
  private String name;
  private int gender;

  public String getHtmlGender() {
    if (1 == gender) {
      return "男";
    } else if (2 == gender) {
      return "女";
    } else {
      return "未知";
    }
  }

  public int getCodelen() {
    return codelen;
  }

  public void setCodelen(int codelen) {
    this.codelen = codelen;
  }

  public int getBlocklen() {
    return blocklen;
  }

  public void setBlocklen(int blocklen) {
    this.blocklen = blocklen;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getGender() {
    return gender;
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

}
