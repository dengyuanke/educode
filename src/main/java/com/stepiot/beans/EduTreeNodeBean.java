package com.stepiot.beans;

public class EduTreeNodeBean implements java.io.Serializable {

  public EduTreeNodeBean() {
    this.state = new State();
  }


  public void setOpened(boolean opened) {
    this.getState().setOpened(opened);
  }

  public void setDisabled(boolean disabled) {
    this.getState().setDisabled(disabled);
  }

  public void setSelected(boolean selected) {
    this.getState().setSelected(selected);
  }

  /**
   * 
   */
  private static final long serialVersionUID = -5021675403670915575L;
  private String id = "";
  private String parent = "";


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EduTreeNodeBean other = (EduTreeNodeBean) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  private String text = "";
  private String icon = "";
  private State state = null;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getParent() {
    return parent;
  }

  public void setParent(String parent) {
    this.parent = parent;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  static final class State implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5755108620760924014L;
    private boolean opened = false;
    private boolean disabled = false;

    public boolean isOpened() {
      return opened;
    }

    public void setOpened(boolean opened) {
      this.opened = opened;
    }

    public boolean isDisabled() {
      return disabled;
    }

    public void setDisabled(boolean disabled) {
      this.disabled = disabled;
    }

    public boolean isSelected() {
      return selected;
    }

    public void setSelected(boolean selected) {
      this.selected = selected;
    }

    private boolean selected = false;
  }



}
