package com.stepiot.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.stepiot.constants.CourseStatus;
import com.stepiot.model.EduUserPythonCourse;

public class EduUserCourseProgressBean extends EduUserPythonCourse {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private String outlineName;

  private String coverage;

  public String getCoveragetc() {
    return coveragetc;
  }

  public void setCoveragetc(String coveragetc) {
    this.coveragetc = coveragetc;
  }

  private String coveragetc;

  public String getCoverage() {
    return coverage;
  }

  public void setCoverage(String coverage) {
    this.coverage = coverage;
  }

  public String getOutlineName() {
    return outlineName;
  }

  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  /**
   * @return lastUpdateTime 最后更新时间
   */
  public String getLastUpdateTimeStr() {
    Date date = super.getLastUpdateTime();
    if (date != null) {
      return simpleDateFormat.format(date);
    }
    return "";
  }

  public String getAccumulated() {
    Integer seconds = super.getAccumulatedSeconds();
    if (seconds != null) {
      int hours = seconds / 3600;
      int minutes = (seconds % 3600) / 60;
      seconds = seconds % 60;

      return String.format("%02d:%02d:%02d", hours, minutes, seconds);

    }
    return "N/A";
  }


  public void setOutlineName(String outlineName) {
    this.outlineName = outlineName;
  }

  public static final class Progress {

    private int level = 0;

    private int status = 0;

    private int index = 0;

    public int getIndex() {
      return index;
    }

    public void setIndex(int index) {
      this.index = index;
    }


    private String style = "grey-text text-lighten-1 mdi mdi-numeric-5-box";


    public int getLevel() {
      return level;
    }

    public int getStatus() {
      return status;
    }

    public String getStyle() {
      return style;
    }

    public void setLevel(int level) {
      this.level = level;
    }

    public void setStatus(int status) {
      this.status = status;
    }

    public void setStyle(String style) {
      this.style = style;
    }



  }

  private String getStyle(int id, int status) {
    if (status == CourseStatus.PASSED.getValue()) {
      return "breadcrumbs-icon green-text mdi mdi-numeric-" + id + "-box";
    } else if (status == CourseStatus.SAVED.getValue()) {
      return "breadcrumbs-icon green-text mdi mdi-numeric-" + id + "-box";
    } else if (status == CourseStatus.SUBMITTED.getValue()) {
      return "breadcrumbs-icon blue-text mdi mdi-numeric-" + id + "-box";
    } else if (status == CourseStatus.REJECTED.getValue()) {
      return "breadcrumbs-icon red-text text-lighten-1 mdi mdi-numeric-" + id + "-box";
    } else {
      return "breadcrumbs-icon grey-text text-lighten-1 mdi mdi-numeric-" + id + "-box";
    }
  }


  private int levelFrom = 0;

  private String levels = null;

  private int levelTo = 0;

  private List<Progress> progress = null;
  private Map<Integer, Progress> proMap = Maps.newHashMap();

  private String statuss = null;

  private void addProgress(Progress pro) {
    if (progress == null) {
      progress = Lists.newArrayList();
    }
    progress.add(pro);
  }

  public int getLevelFrom() {
    return levelFrom;
  }



  public String getLevels() {
    return levels;
  }

  public int getLevelTo() {
    return levelTo;
  }

  public List<Progress> getProgress() {
    return progress;
  }

  public String getStatuss() {
    return statuss;
  }

  public void setLevelFrom(int levelFrom) {
    this.levelFrom = levelFrom;
  }

  public void setLevels(String levels) {

    this.levels = levels;
  }

  public void setLevelTo(int levelTo) {
    this.levelTo = levelTo;
  }

  public void setPro() {

    int from = this.getLevelFrom();
    int to = this.getLevelTo();
    if (from <= 0) {
      return;
    }
    if (to < from) {
      return;
    }

    int index = 1;
    for (int i = from; i <= to; i++) {
      Progress pro = new Progress();
      pro.setIndex(index++);
      pro.setLevel(i);
      pro.setStatus(CourseStatus.NOTINITED.getValue());
      pro.setStyle(this.getStyle(pro.getIndex(), pro.getStatus()));
      proMap.put(i, pro);
      this.addProgress(pro);
    }

    if (StringUtils.isNotBlank(statuss) && StringUtils.isNotBlank(levels)) {

      String[] status = StringUtils.split(statuss, ',');
      String[] level = StringUtils.split(levels, ',');
      if (status.length != level.length) {
        // TODO 添加日志
        throw new java.lang.IllegalStateException("数据错误");
      }

      for (int i = 0; i < level.length; i++) {
        int levl = Integer.parseInt(level[i].trim());
        Progress pros = proMap.get(levl);
        if (pros == null) {
          continue;
        }

        int t_status = Integer.parseInt(status[i].trim());
        pros.setStatus(t_status);
        pros.setStyle(this.getStyle(pros.getIndex(), pros.getStatus()));

      }
    } else {

    }
  }

  public void setStatuss(String statuss) {
    this.statuss = statuss;
  }

}
