package com.stepiot.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.Lists;
import com.stepiot.model.EduNotice;

public class EduNoticeBean extends EduNotice {

  private String attachmentIds;
  private Integer recipientId;
  private Integer recipientStatus;
  private Date recipientTime;
  private String noticeBody;

  public String getToClasses() {
    return toClasses;
  }

  public String getPageCreateTime() {
    Date date = this.getCreateTime();
    if (date != null) {
      return new SimpleDateFormat("yyyy-MM-dd").format(this.getCreateTime());
    }
    return null;

  }

  public void setToClasses(String toClasses) {
    this.toClasses = toClasses;
  }


  public String getNoticeBody() {
    return noticeBody;
  }

  public String getOmittedNoticeSubject() {
    return StringUtils.abbreviate(this.getNoticeBody(), 12);
  }

  public String getOmittedNoticeBody() {
    return StringUtils.abbreviate(this.getNoticeBody(), 12);
  }


  public void setNoticeBody(String noticeBody) {
    this.noticeBody = noticeBody;
  }


  private String toClasses;



  /**
   * 
   */
  private static final long serialVersionUID = 1L;


  public List<Integer> getAttachmentIds() {
    if (StringUtils.isBlank(attachmentIds)) {
      return null;
    }
    String[] ids = StringUtils.split(attachmentIds, ',');

    if (ids != null && ids.length > 0) {
      List<Integer> ls = Lists.newArrayList();
      for (int i = 0; i < ids.length; i++) {
        ls.add(Integer.parseInt(ids[i].trim()));
      }
      return ls;
    }
    return null;
  }


  public void setAttachmentIds(String attachmentIds) {
    this.attachmentIds = attachmentIds;
  }


  public Integer getRecipientId() {
    return recipientId;
  }


  public void setRecipientId(Integer recipientId) {
    this.recipientId = recipientId;
  }


  public Integer getRecipientStatus() {
    return recipientStatus;
  }


  public void setRecipientStatus(Integer recipientStatus) {
    this.recipientStatus = recipientStatus;
  }


  public Date getRecipientTime() {
    return recipientTime;
  }


  public void setRecipientTime(Date recipientTime) {
    this.recipientTime = recipientTime;
  }

}
