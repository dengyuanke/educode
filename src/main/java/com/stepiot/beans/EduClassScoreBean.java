package com.stepiot.beans;

import com.stepiot.constants.Cons;

public class EduClassScoreBean implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4389638136052187210L;
  private int score;
  private String username;
  private String name;
  private int age;
  private int gender;
  private String avatar;


  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getUsername() {
    return username;
  }

  public String getStyleClass() {
    if (score >= Cons.SCORE_HIGH) {
      return "greensea";
    }
    if (score >= Cons.SCORE_MIDDLE) {
      return "orange";
    }
    return "red accent-2";
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getGender() {
    return gender;
  }

  public String getHtmlGender() {
    if (1 == gender) {
      return "男";
    } else if (2 == gender) {
      return "女";
    } else {
      return "未知";
    }
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

}
