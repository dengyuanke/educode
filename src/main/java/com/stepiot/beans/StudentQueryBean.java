package com.stepiot.beans;

import org.apache.commons.lang3.StringUtils;

public class StudentQueryBean implements java.io.Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 8898621071820391436L;
  private String classId = null;
  private String disciplineId = null;

  public String getClassId() {
    if (StringUtils.isAnyEmpty(classId)) {
      return null;
    }
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public String getDisciplineId() {
    if (StringUtils.isAnyEmpty(disciplineId)) {
      return null;
    }
    return disciplineId;
  }

  public void setDisciplineId(String disciplineId) {
    this.disciplineId = disciplineId;
  }

  public String getDepartmentId() {
    if (StringUtils.isEmpty(departmentId)) {
      return null;
    }
    return departmentId;
  }

  public void setDepartmentId(String departmentId) {
    this.departmentId = departmentId;
  }

  private String departmentId = null;
}
