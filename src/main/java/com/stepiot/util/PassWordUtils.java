package com.stepiot.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.google.common.collect.Lists;

public class PassWordUtils {

  private static SecureRandom random = new SecureRandom();
  private static int STRENGTH = 10;
  private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(STRENGTH, random);

  public static String encodePassword(String password) {
    return encoder.encode(password);
  }

  public static boolean matches(CharSequence rawPassword, String encodedPassword) {
    return encoder.matches(rawPassword, encodedPassword);
  }


  public static void main(String[] args) {
    // System.out.println(PassWordUtils.encodePassword("GFvBxaSFNG5NdjdYS"));
    System.out.println(PassWordUtils.encodePassword("18883336614"));
  }


  public static void main1(String args[]) throws IOException {

    List<String> l = Lists.newArrayList();

    l.add("lvchuang");


    BufferedWriter b = new BufferedWriter(new FileWriter("/Users/denny/ddd.txt"));
    for (int i = 0; i < l.size(); i++) {
      String password = PassWordUtils.encodePassword(l.get(i));
      b.write(password + "\n");
      System.out.println(i);
      b.flush();
    }

  }

}
