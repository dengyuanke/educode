package com.stepiot.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;

/**
 * 
 * @author denny 2018-09-07
 *
 */
public class PathUtils {

  public static String curTime() {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Shanghai"), Locale.CHINA);
    SimpleDateFormat format = new SimpleDateFormat("yyyyMM/ddHH/mmss");
    format.setCalendar(cal);
    return format.format(new Date());
  }

  public static String md5path(String name) {
    String md5Hex = DigestUtils.md5Hex(name);
    return md5Hex.substring(0, 2) + "/" + md5Hex.substring(2, 4) + "/";
  }

  public static String getFileName(String http) {

    try {
      URL url = new URL(http);
      return FilenameUtils.getName(url.getPath());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return null;

  }

}
