package com.stepiot.util;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * 密钥工具
 * 
 * @author 张云鹏 2018-06-25
 *
 */
public class KeyUtils {

  /**
   * 将PEM格式的公钥转化为X509EncodedKeySpec格式的公钥
   * 
   * @param path
   * @return
   */
  public static PublicKey pemPubKeyToX509EncodedKey(byte[] pemFileBytes) {

    PublicKey publicKey = null;
    try {
      String key = new String(pemFileBytes, "UTF-8");
      key = trimPemKey(key);
      X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.decodeBase64(key));
      publicKey = KeyFactory.getInstance("RSA").generatePublic(publicSpec);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return publicKey;
  }

  public static String trimPemKey(String pem) {
    String result = pem.replace("-----BEGIN PUBLIC KEY-----", "");
    result = result.replace("-----END PUBLIC KEY-----", "");
    result = result.replaceAll("\\n", "");
    result = result.replaceAll("\\r", "");
    return result;
  }

  public static boolean isValidPublicPem(String pem) {

    if (pem == null) {
      return false;
    }
    if (!pem.startsWith("-----BEGIN PUBLIC KEY-----")) {
      return false;
    }
    if (!pem.endsWith("-----END PUBLIC KEY-----")) {
      return false;
    }

    try {
      pem = trimPemKey(pem);
      X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.decodeBase64(pem));
      KeyFactory.getInstance("RSA").generatePublic(publicSpec);
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  /**
   * 将PEM格式（这里的PEM已经去除了文件头部和尾部的非公钥内容的字符串，并去除掉了每行中的换行符）转化为Java对象的公钥
   * 
   * @param base64PubKey
   * @return
   */
  public static PublicKey pemToX509EncodedKey(String base64PubKey) {

    PublicKey publicKey = null;
    try {
      X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.decodeBase64(base64PubKey));
      publicKey = KeyFactory.getInstance("RSA").generatePublic(publicSpec);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return publicKey;
  }

}
