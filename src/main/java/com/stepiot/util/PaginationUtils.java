package com.stepiot.util;

import java.util.Map;
import org.springframework.ui.Model;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.proto.PaginationProto.Pagination;
import com.stepiot.proto.PaginationProto.Pagination.Item.Builder;

/**
 * 
 * @author 张云鹏 2018-07-07
 *
 */
public class PaginationUtils {

  public static final int DEFAULT_PAGE_SIZE = 7;
  private static final int MAX_PAGE_SIZE = 200;
  private static final int MIN_PAGE_SIZE = 1;
  private static final int MIN_TOTAL_AMOUNT = 0;

  private static final int PAGINATION_SIZE = 8;

  private static Pagination _genPagination(Integer currentPage, Integer pageSize, Long totalAmount,
      String cache) {

    com.stepiot.proto.PaginationProto.Pagination.Form.Builder fbuilder =
        Pagination.Form.newBuilder();
    fbuilder.setK("");

    pageSize = Math.max(Math.min(pageSize, MAX_PAGE_SIZE), MIN_PAGE_SIZE);
    totalAmount = Math.max(MIN_TOTAL_AMOUNT, totalAmount);
    int totalPage = (int) Math.ceil(((double) totalAmount / pageSize));
    currentPage = Math.min(Math.max(currentPage, 1), totalPage);
    if (totalPage <= 1) {
      return null;
    }
    com.stepiot.proto.PaginationProto.Pagination.Pn.Builder previous = Pagination.Pn.newBuilder();
    previous.setHref("" + (currentPage - 1));
    previous.setIcon("chevron_left");

    com.stepiot.proto.PaginationProto.Pagination.Pn.Builder next = Pagination.Pn.newBuilder();
    next.setHref("" + (currentPage + 1));
    next.setIcon("chevron_right");

    previous.setEnabled(currentPage > 1);
    if (previous.getEnabled()) {
      previous.setClazz("pagination-page");
    }
    next.setEnabled(currentPage < totalPage);
    if (next.getEnabled()) {
      next.setClazz("pagination-page");
    }

    com.stepiot.proto.PaginationProto.Pagination.Builder pb = Pagination.newBuilder();
    pb.setCache(cache);
    pb.setPrevious(previous);
    pb.setNext(next);

    if (totalPage <= PAGINATION_SIZE) {
      for (int i = 1; i <= totalPage; i++) {
        com.stepiot.proto.PaginationProto.Pagination.Item.Builder it = Pagination.Item.newBuilder();
        it.setHref("" + i);
        it.setPageNum(i);
        it.setActive(currentPage == it.getPageNum());
        pb.addItems(it);
      }

      pb.setForm(fbuilder.build());
      return pb.build();

    }

    int loop = Math.min(PAGINATION_SIZE, totalPage);

    int m = PAGINATION_SIZE / 2;

    if (currentPage <= PAGINATION_SIZE / 2 + 1) {
      m = (currentPage - 1);
    } else if (totalPage - currentPage < PAGINATION_SIZE / 2 + 1) {
      m = (PAGINATION_SIZE - 1) - (totalPage - currentPage);
    }

    for (int i = 1; i <= loop; i++) {
      com.stepiot.proto.PaginationProto.Pagination.Item.Builder it = Pagination.Item.newBuilder();
      it.setPageNum(currentPage - (m--));
      it.setHref("" + it.getPageNum());
      it.setActive(currentPage == it.getPageNum());
      pb.addItems(it);
    }
    pb.getItemsBuilder(0).setHref("" + 1);
    pb.getItemsBuilder(0).setPageNum(1);
    pb.getItemsBuilder(PAGINATION_SIZE - 1).setHref("" + totalPage);
    pb.getItemsBuilder(PAGINATION_SIZE - 1).setPageNum(totalPage);

    if (currentPage > PAGINATION_SIZE / 2 + 1) {
      Builder itemb = pb.getItemsBuilder(1);
      itemb.setHref("...");
    }
    if (currentPage <= (totalPage - (PAGINATION_SIZE / 2 + 1))) {
      Builder itemb = pb.getItemsBuilder(pb.getItemsCount() - 2);
      itemb.setHref("...");
    }
    pb.setForm(fbuilder.build());
    return pb.build();
  }

  @SuppressWarnings("unused")
  private static Pagination genPagination(Long totalAmount, Integer pageSize) {
    return _genPagination(1, pageSize, totalAmount, "");
  }

  public static Pagination genPagination(Integer currentPage, Long totalAmount, Integer pageSize) {
    return _genPagination(currentPage, pageSize, totalAmount, "");
  }

  public static Pagination genPagination(Integer currentPage, Long totalAmount, Integer pageSize,
      String cache) {
    return _genPagination(currentPage, pageSize, totalAmount, cache);
  }

  @SuppressWarnings("unused")
  private static void fillPagination(Model model, Integer current, Long total, Integer pageSize) {
    Pagination pagination = PaginationUtils.genPagination(current, total, pageSize);
    if (pagination != null) {

      String json = null;
      try {
        json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
            .print(pagination.toBuilder());
      } catch (InvalidProtocolBufferException e) {
      }
      Map<String, Object> map = GsonUtils.toMap(json);
      model.addAttribute("pagination", map);
    }
  }
}
