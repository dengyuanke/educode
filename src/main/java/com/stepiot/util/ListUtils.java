package com.stepiot.util;

import java.util.List;
import com.google.common.collect.Lists;

public class ListUtils {

  public static List<Integer> stringListToIntegerList(List<String> lists) {
    List<Integer> listi = Lists.newArrayList();
    for (int i = 0; i < lists.size(); i++) {
      listi.add(Integer.parseInt(lists.get(i)));
    }
    return listi;
  }

}
