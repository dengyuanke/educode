package com.stepiot.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author 张云鹏 重构于2015-01-13 增加字符旋转
 *
 */
public class RecaptchaUtils {
  private static final Logger log = LoggerFactory.getLogger(RecaptchaUtils.class);

  private static SecureRandom random = new SecureRandom();
  private static final Font fixedsys = new Font("Fixedsys", Font.CENTER_BASELINE, 20);
  private static final Font roman = new Font("Times New Roman", Font.ROMAN_BASELINE, 18);
  private static final int width = 100;// 图片宽
  private static final int height = 26;// 图片高
  private static final int lineSize = 16;// 干扰线数量
  /*
   * 获得颜色
   */

  private static Color getRandColor(int fc) {
    int r = fc + random.nextInt(7);
    int g = fc + random.nextInt(9);
    int b = fc + random.nextInt(5);
    return new Color(r, g, b);
  }

  /**
   * 生成随机图片
   */
  public static byte[] genRecaptcha(String letters) {
    // BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
    Graphics2D g = image.createGraphics();// 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
    g.fillRect(0, 0, width, height);
    g.setFont(roman);
    g.setColor(getRandColor(114));
    // 绘制干扰线
    for (int i = 0; i <= lineSize; i++) {
      drawLine(g);
    }
    // 绘制随机字符
    drawString(g, letters);
    g.dispose();
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    try {
      ImageIO.write(image, "JPEG", bout);// 将内存中的图片通过流动形式输出到客户端
      return bout.toByteArray();
    } catch (Exception e) {
      log.error("Exception", e);
    }
    return null;
  }

  /*
   * 绘制字符串
   */
  private static void drawString(Graphics2D g, String rand) {
    char[] rands = rand.toCharArray();
    for (int i = 0; i < rand.length(); i++) {
      g.setColor(new Color(random.nextInt(101), random.nextInt(111), random.nextInt(121)));
      g.translate(random.nextInt(3), random.nextInt(3));
      AffineTransform affineTransform = new AffineTransform();
      affineTransform.rotate(Math.toRadians(radias[random.nextInt(radias.length)]), 11, 0);
      Font rotatedFont = fixedsys.deriveFont(affineTransform);
      g.setFont(rotatedFont);
      g.drawChars(rands, i, 1, 13 * (i + 1), 16);
    }
  }

  /**
   * 字符串旋转角度，用于随机获取要旋转的角度
   */
  private static final int radias[] = {-16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4,
      -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

  /*
   * 绘制干扰线
   */
  private static void drawLine(Graphics2D g) {
    int x = random.nextInt(width);
    int y = random.nextInt(height);
    int xl = random.nextInt(27);
    int yl = random.nextInt(17);
    g.drawLine(x, y, xl + x, yl + y);
  }

}
