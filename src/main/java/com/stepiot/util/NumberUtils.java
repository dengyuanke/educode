package com.stepiot.util;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 系统订单号生成类
 * 
 * @author 张云鹏 2015-04-10
 * 
 */
public class NumberUtils {

  private static AtomicInteger as = new AtomicInteger(0);
  // 自1970年1月1日0时0分0秒0毫秒起，至2015年1月1日0时0分0秒0毫秒的毫秒数
  private static final long BASE = 1420041600000L;

  private static final int INT_MAX = 100000;

  private static final long MAX = 100000L;

  private static AtomicLong sq = new AtomicLong(0L);

  /**
   * 返回永不重复的订单号 线程安全 平均1毫秒可生成60000个订单号，一秒钟约可生成6000万个订单号（AMDA10-5750M 2.5GHz x64）
   */
  public static int createUniqueInt() {
    return (int) (millis() / 1000) * INT_MAX
        + ((as.getAndIncrement() & Integer.MAX_VALUE) % INT_MAX);
  }

  /**
   * 返回永不重复的订单号 线程安全 平均1毫秒可生成60000个订单号，一秒钟约可生成6000万个订单号（AMDA10-5750M 2.5GHz x64）
   */
  public static long createUniqueLong() {
    return millis() * MAX + ((sq.getAndIncrement() & Long.MAX_VALUE) % MAX);
  }

  private static long millis() {
    return System.currentTimeMillis() - BASE;
  }

  public static void mainxx(String args[]) {
    System.out.print(createUniqueInt());
  }
}
