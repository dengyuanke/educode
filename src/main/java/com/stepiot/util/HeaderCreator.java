package com.stepiot.util;

import java.util.Map;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.proto.HeaderProto.Header;
import com.stepiot.proto.HeaderProto.Header.Builder;

public class HeaderCreator {

  public static int SELECT = Integer.parseInt("1000000000000000", 2);

  public static Map<String, Object> create(String title, int button) {
    Builder builder = Header.newBuilder();

    Header header = builder.setTitle(title).setSelect((button & SELECT) == SELECT).build();

    try {
      String json = JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields()
          .print(header);
      Map<String, Object> map = GsonUtils.toMap(json);
      return map;
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
    return null;
  }

}
