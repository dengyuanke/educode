package com.stepiot.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * json工具类
 * 
 * @author 张云鹏 2016-01-22
 *
 */
public class GsonUtils {

  private static final Gson gson = new Gson();

  private static final Type tlist = new TypeToken<List<?>>() {}.getType();

  private static final Type tmap = new TypeToken<Map<String, Object>>() {}.getType();

  public static <T> String fromObj(T obj) {
    return gson.toJson(obj);
  }

  /**
   * 将Json InputStream转换为Map对象
   * 
   * @param stream
   * @return
   */
  public static Map<String, Object> streamToMap(InputStream stream) {
    try {
      return toMap(IOUtils.toString(stream, Charset.forName("UTF-8")));
    } catch (IOException e) {
    }
    return null;
  }

  /**
   * 将List转换为json字符串
   * 
   * @param map
   * @return
   */
  public static String toJson(List<?> list) {
    return gson.toJson(list);
  }

  /**
   * 将Map转换为json字符串
   * 
   * @param map
   * @return
   */
  public static String toJson(Map<String, Object> map) {
    return gson.toJson(map);
  }

  /**
   * 将json字符串转化为Map对象
   * 
   * @param jsonStr
   * @return
   */
  public static List<?> toList(String jsonStr) {
    return gson.fromJson(jsonStr, tlist);
  }

  /**
   * 将json字符串转化为Map对象
   * 
   * @param jsonStr
   * @return
   */
  public static <T> T toTypedList(String jsonStr, TypeToken<T> type) {
    return gson.fromJson(jsonStr, type.getType());
  }


  /**
   * 将json字符串转化为Map对象
   * 
   * @param jsonStr
   * @return
   */
  public static <T> T toTypedBean(String jsonStr, TypeToken<T> type) {
    return gson.fromJson(jsonStr, type.getType());
  }

  /**
   * 将json字符串转化为Map对象
   * 
   * @param jsonStr
   * @return
   */
  public static Map<String, Object> toMap(String jsonStr) {
    return gson.fromJson(jsonStr, tmap);
  }

  public static <T> T toObj(String json, Class<T> clazz) {
    if (json == null || json.isEmpty()) {
      return null;
    }
    return gson.fromJson(json, clazz);
  }
}
