package com.stepiot.util;

import java.util.Map;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb;
import com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder;

public class BcbCreator {


  public static int ADD = Integer.parseInt("1000000000000000000000000000000", 2);
  public static int EDIT = Integer.parseInt("0100000000000000000000000000000", 2);
  public static int DELIST = Integer.parseInt("0010000000000000000000000000000", 2);
  public static int UPDATE = Integer.parseInt("0001000000000000000000000000000", 2);
  public static int DELEN = Integer.parseInt("0000100000000000000000000000000", 2);
  public static int BACK = Integer.parseInt("0000010000000000000000000000000", 2);
  public static int SOVE = Integer.parseInt("0000001000000000000000000000000", 2);
  public static int SAVEBLOCKLY = Integer.parseInt("0000000100000000000000000000000", 2);
  public static int DOACTION = Integer.parseInt("0000000010000000000000000000000", 2);
  public static int DOWNLOAD = Integer.parseInt("0000000001000000000000000000000", 2);
  public static int CODE = Integer.parseInt("0000000000100000000000000000000", 2);
  public static int COMPLETE = Integer.parseInt("0000000000010000000000000000000", 2);
  public static int HISTORY = Integer.parseInt("0000000000001000000000000000000", 2);
  public static int UPLOAD = Integer.parseInt("0000000000000100000000000000000", 2);
  public static int DOWNHEX = Integer.parseInt("0000000000000010000000000000000", 2);
  public static int NOOP = Integer.parseInt("0000000000000001000000000000000", 2);
  public static int PAIR = Integer.parseInt("0000000000000000100000000000000", 2);
  public static int SHARE = Integer.parseInt("0000000000000000010000000000000", 2);
  public static int RUNBUTTON = Integer.parseInt("0000000000000000001000000000000", 2);
  public static int RESETBUTTON = Integer.parseInt("0000000000000000000100000000000", 2);
  public static int COLOR = Integer.parseInt("0000000000000000000010000000000", 2);
  public static int SAVE = Integer.parseInt("0000000000000000000001000000000", 2);
  public static int SPEED = Integer.parseInt("0000000000000000000000100000000", 2);
  public static int MARKASREAD = Integer.parseInt("0000000000000000000000010000000", 2);
  public static int PLAY = Integer.parseInt("0000000000000000000000001000000", 2);
  public static int SEND = Integer.parseInt("0000000000000000000000000100000", 2);
  public static int ANSWER = Integer.parseInt("0000000000000000000000000010000", 2);
  public static int DOWNPPT = Integer.parseInt("0000000000000000000000000001000", 2);
  public static int SETTODEFAULT = Integer.parseInt("0000000000000000000000000000100", 2);
  public static int PREVIOUS = Integer.parseInt("0000000000000000000000000000010", 2);
  public static int NEXT = Integer.parseInt("0000000000000000000000000000001", 2);


  public static int LIST = ADD | EDIT | DELIST;
  public static int ENTITY = UPDATE | DELEN;

  public static Map<String, Object> create(Builder builder, int button) {
    return create(builder, "", button);
  }

  public static Map<String, Object> build(Builder builder) {

    Breadcrumb bcb = builder.build();

    try {
      String json =
          JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields().print(bcb);
      Map<String, Object> map = GsonUtils.toMap(json);
      return map;
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Map<String, Object> create(Builder builder, String backToUrl, int button) {

    Breadcrumb bcb = builder.setBackToUrl(backToUrl).setAdd((button & ADD) == ADD)
        .setDelist((button & DELIST) == DELIST).setEdit((button & EDIT) == EDIT)
        .setUpdate((button & UPDATE) == UPDATE).setDelen((button & DELEN) == DELEN)
        .setBack((button & BACK) == BACK).setSove((button & SOVE) == SOVE)
        .setSaveblockly((button & SAVEBLOCKLY) == SAVEBLOCKLY)
        .setDoAction((button & DOACTION) == DOACTION).setDownload((button & DOWNLOAD) == DOWNLOAD)
        .setCode((button & CODE) == CODE).setComplete((button & COMPLETE) == COMPLETE)
        .setHistory((button & HISTORY) == HISTORY).setUpload((button & UPLOAD) == UPLOAD)
        .setDownhex((button & DOWNHEX) == DOWNHEX).setNoop((button & NOOP) == NOOP)
        .setPair((button & PAIR) == PAIR).setShare((button & SHARE) == SHARE)
        .setRunButton((button & RUNBUTTON) == RUNBUTTON).setColor((button & COLOR) == COLOR)
        .setResetButton((button & RESETBUTTON) == RESETBUTTON).setSave((button & SAVE) == SAVE)
        .setSpeed((button & SPEED) == SPEED).setMarkAsRead((button & MARKASREAD) == MARKASREAD)
        .setPlay((button & PLAY) == PLAY).setSend((button & SEND) == SEND)
        .setAnswer((button & ANSWER) == ANSWER).setDownppt((button & DOWNPPT) == DOWNPPT)
        .setNext((button & NEXT) == NEXT).setPrevious((button & PREVIOUS) == PREVIOUS)
        .setSetToDefault((button & SETTODEFAULT) == SETTODEFAULT).build();

    try {
      String json =
          JsonFormat.printer().preservingProtoFieldNames().includingDefaultValueFields().print(bcb);
      Map<String, Object> map = GsonUtils.toMap(json);
      return map;
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
    return null;
  }

}
