
package com.stepiot.util;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordHasher {

  private static Logger logger = LoggerFactory.getLogger(PasswordHasher.class);

  private static final String ALGORITHM = "PBKDF2WithHmacSHA256";

  private static final int ITERATIONS = 901;

  private static final int KEY_LENGTH = 24 * 8;

  private static final int SALT_LENGTH = 12;

  /**
   * This method creates a new PBKDF2 password (in mosquitto-auth-plug format) from a plain
   * password.
   *
   * @author Manuel Domínguez Dorado - manuel.dominguez@enzinatec.com
   * @param plainPassword The plain password used to generate the corresponding PBKDF2 password (in
   *        mosquitto-auth-plug) format.
   * @return The generated PBKDF2 password in mosquitto-auth-plug format (usually, it will be stored
   *         in a MySQL database).
   * @since 1.0
   */
  public static String createPassword(String plainPassword) {
    byte someBytes[] = new byte[PasswordHasher.SALT_LENGTH];
    Random randomGenerator = new Random();
    randomGenerator.nextBytes(someBytes);
    String encodedSalt = Base64.getEncoder().encodeToString(someBytes);

    SecretKeyFactory f = null;
    try {
      f = SecretKeyFactory.getInstance(ALGORITHM);
    } catch (NoSuchAlgorithmException ex) {
      logger.error(null, ex);
    }
    KeySpec ks = new PBEKeySpec(plainPassword.toCharArray(), encodedSalt.getBytes(),
        PasswordHasher.ITERATIONS, PasswordHasher.KEY_LENGTH);
    SecretKey s;
    try {
      s = f.generateSecret(ks);
      String encodedKey = Base64.getEncoder().encodeToString(s.getEncoded());
      String hashedKey =
          "PBKDF2$sha256$" + PasswordHasher.ITERATIONS + "$" + encodedSalt + "$" + encodedKey;
      return hashedKey;
    } catch (InvalidKeySpecException ex) {
      logger.error(null, ex);
    }
    return "";
  }

  /**
   * This method compares a plain password and a PBKDF2 password (in mosquitto-auth-plug format) to
   * know whether the password match the PBKDF2 hash.
   *
   * @author Manuel Domínguez Dorado - manuel.dominguez@enzinatec.com
   * @param plainPassword Tha plain password to be compared to the PBKDF2 hash.
   * @param hashedPasword The PBKDF2 password in mosquitto-auth-plug format (usualli it is stored in
   *        a MySQL database).
   * @return true, if password matches the PBKDF2 hash. false on the contrary.
   * @since 1.0
   */
  public static boolean isValidPassword(String plainPassword, String hashedPasword) {
    String[] encodedPassword = hashedPasword.split("\\$");
    int encodedIterations = Integer.parseInt(encodedPassword[2]);
    byte[] encodedSalt = encodedPassword[3].getBytes(Charset.forName("UTF-8"));
    String encodedHash = encodedPassword[4];
    SecretKeyFactory f = null;
    try {
      f = SecretKeyFactory.getInstance(ALGORITHM);
    } catch (NoSuchAlgorithmException e) {
      logger.error("Need a Java implementation with cryptography.");
    }
    KeySpec ks = new PBEKeySpec(plainPassword.toCharArray(), encodedSalt, encodedIterations,
        PasswordHasher.KEY_LENGTH);
    SecretKey s = null;
    try {
      s = f.generateSecret(ks);
    } catch (InvalidKeySpecException e) {
      logger.error("Encoded password is corrupt.");
    }
    return encodedHash.equals(Base64.getEncoder().encodeToString(s.getEncoded()));
  }

  public static void mainxx(String args[]) {
    System.out.print(createPassword("123456"));
  }
}
