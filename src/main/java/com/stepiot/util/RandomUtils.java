package com.stepiot.util;

import java.security.SecureRandom;
import java.util.UUID;

public class RandomUtils {

  public static String uuid() {
    return UUID.randomUUID().toString().replaceAll("-", "_");
  }

  public static int randomNumberBetween(final int start, final int end) {
    if (end <= start) {
      return start;
    } else {
      int[] longs = new int[end - start + 1];
      int tstart = start;
      for (int i = 0; i < longs.length; i++) {
        longs[i] = tstart++;
      }
      return longs[random.nextInt(end - start + 1)];
    }
  }

  private static final int max = 0xFFF;
  private static final char[] ascii = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
      'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e',
      'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
      'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

  private static final char[] asciiExlude =
      {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
          'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm',
          'n', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6', '7', '8'};

  private static final SecureRandom random = new SecureRandom();

  public static String genPass(final int len) {
    return genPass(len, false);
  }

  /**
   * 生成密码
   * 
   * @param len
   * @param exclude 排除容易混淆的字符
   * @return
   */
  public static String genPass(final int len, boolean exclude) {
    char[] abc = ascii;

    if (exclude) {
      abc = asciiExlude;
    }
    int ln = len & max;
    StringBuilder sb = new StringBuilder(ln);
    for (int i = 0; i < ln; i++) {
      sb.append(abc[random.nextInt(abc.length)]);
    }
    return sb.toString();
  }

  public static String genPass(final int len, char[] est) {
    if (est == null || est.length >= ascii.length) {
      return genPass(len, false);
    }

    int ln = len & max;
    StringBuilder sb = new StringBuilder(ln);
    w: while (true) {
      if (ln == 0) {
        break w;
      }
      char rnd = ascii[random.nextInt(ascii.length)];
      for (int x = 0; x < est.length; x++) {
        if (rnd == est[x]) {
          continue w;
        }
      }
      sb.append(rnd);
      ln--;
    }
    return sb.toString();
  }

  public static void mainxx(String args[]) {
    System.out.println(uuid());
  }
}
