package com.stepiot.util;

import org.apache.commons.lang3.StringUtils;

public class UrlUtils {


  public static String trimToRoot(String URI) {
    if (StringUtils.countMatches(URI, "/") > 1) {
      return "/" + StringUtils.substringBetween(URI, "/");
    }
    return URI;
  }
}
