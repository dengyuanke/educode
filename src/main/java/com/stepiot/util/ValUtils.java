package com.stepiot.util;

import org.apache.commons.lang3.StringUtils;

public class ValUtils {

  public static boolean isValidUsername(int min, int max, String username) {
    if (StringUtils.isBlank(username)) {
      return false;
    }
    if (username.length() < min || username.length() > max) {
      return false;
    }

    char[] cs = username.toCharArray();
    for (int i = 0; i < cs.length; i++) {
      if (cs[i] < 48) {
        return false;
      }
      if (cs[i] <= 57) {
        continue;
      }
      if (cs[i] < 65) {
        return false;
      }
      if (cs[i] <= 90) {
        continue;
      }
      if (cs[i] == 95) {
        continue;
      }
      if (cs[i] < 97) {
        return false;
      }
      if (cs[i] <= 122) {
        continue;
      }

    }

    return true;

  }

  public static boolean isValidPassword(int min, int max, String password) {
    if (StringUtils.isBlank(password)) {
      return false;
    }
    if (password.length() < min || password.length() > max) {
      return false;
    }

    char[] cs = password.toCharArray();
    boolean upperCase = false;
    boolean lowerCase = false;
    boolean number = false;

    for (int i = 0; i < cs.length; i++) {
      if (cs[i] < 33) {
        return false;
      }
      if (cs[i] > 127) {
        return false;
      }
      if (cs[i] >= 65 && cs[i] <= 90) {
        upperCase = true;
      }
      if (cs[i] >= 97 && cs[i] <= 122) {
        lowerCase = true;
      }
      if (cs[i] >= 48 && cs[i] <= 57) {
        number = true;
      }
    }
    if (upperCase && lowerCase && number) {
      return true;
    }

    return false;

  }

  public static boolean isValidTelephone(String telephone) {

    if (StringUtils.isBlank(telephone)) {
      return false;
    }

    if (telephone.length() != 11) {
      return false;
    }

    char[] cs = telephone.toCharArray();

    for (int i = 0; i < cs.length; i++) {
      if (cs[i] < 48) {
        return false;
      }
      if (cs[i] > 57) {
        return false;
      }
    }

    if (cs[0] != '1') {
      return false;
    }

    return true;

  }

}
