package com.stepiot.util;

import org.apache.commons.lang3.StringUtils;
import com.stepiot.controller.ProtoResponse;
import com.stepiot.proto.HttpProto;

public class ProtoUtils {

  public static String buildResponse(HttpProto.Res response) {
    byte[] bs = response.toByteArray();
    StringBuilder sb = new StringBuilder(bs.length * 2);
    for (int i = 0; i < bs.length; i++) {
      sb.append(StringUtils.leftPad(Integer.toUnsignedString(bs[i] & 0xff, 36), 2, "0"));
    }
    ProtoResponse res = new ProtoResponse();
    res.setK(sb.toString());
    String json = GsonUtils.fromObj(res);
    return json;
  }

  public static String buildRequest(HttpProto.Req request) {
    byte[] bs = request.toByteArray();
    StringBuilder sb = new StringBuilder(bs.length * 2);
    for (int i = 0; i < bs.length; i++) {
      sb.append(StringUtils.leftPad(Integer.toUnsignedString(bs[i] & 0xff, 36), 2, "0"));
    }

    return sb.toString();
  }
}
