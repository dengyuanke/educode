package com.stepiot.util;

import javax.servlet.http.HttpSession;

/**
 * Session工具类
 * 
 * @author 张云鹏 2016-01-26
 *
 */
public class SessionUtils {

  /**
   * 返回Session中指定类型的对象
   * 
   * @param mem
   * @param key
   * @param clazz
   * @return
   */
  public static <T> T getObj(HttpSession session, String key, Class<T> clazz) {
    Object obj = session.getAttribute(key);

    if (obj == null) {
      return null;
    }
    return clazz.cast(obj);
  }

}
