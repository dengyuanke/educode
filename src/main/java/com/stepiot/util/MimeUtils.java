package com.stepiot.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author 张云鹏 2015-04-10
 * 
 */
public class MimeUtils {

  public static String getMaterialcssByMimetype(String mimeType) {
    if (StringUtils.isEmpty(mimeType)) {
      return "mdi-file";
    }
    if (mimeType.indexOf("image") == 0) {
      return "mdi-file-image";
    }
    if (mimeType.indexOf("video") == 0) {
      return "mdi-file-video";
    }
    if (mimeType.indexOf("audio") == 0) {
      return "mdi-file-music";
    }
    if (mimeType.indexOf("text") == 0) {
      return "mdi-file-document";
    }
    if (mimeType
        .indexOf("application/vnd.openxmlformats-officedocument.wordprocessingml.document") == 0) {
      return "mdi-file-word";
    }
    if (mimeType.indexOf("application/vnd.ms-excel") == 0) {
      return "mdi-file-excel";
    }
    if (mimeType
        .indexOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") == 0) {
      return "mdi-file-excel";
    }
    if (mimeType.indexOf("application/vnd.ms-powerpoint") == 0) {
      return "mdi-file-powerpoint";
    }
    if (mimeType.indexOf(
        "application/vnd.openxmlformats-officedocument.presentationml.presentation") == 0) {
      return "mdi-file-powerpoint";
    }
    if (mimeType.indexOf("application/pdf") == 0) {
      return "mdi-file-pdf";
    }
    return "mdi-file";
  }

}
