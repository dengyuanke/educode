package com.stepiot.netty;

import java.io.UnsupportedEncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.stepiot.model.EduUserCourseJuniorWithBLOBs;
import com.stepiot.service.EduUserCourseJuniorService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

@Service
@Sharable
public class StepTcpServerHandler extends SimpleChannelInboundHandler<byte[]> {

  @Autowired
  private EduUserCourseJuniorService service;
  private static final byte[] DA = new byte[] {0x0d, 0x0a};
  private static final byte[] NO = "no".getBytes();

  @Autowired
  public StepTcpServerHandler(ApplicationContext context) {}

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) {

    System.out.println("channelReadComplete::");
    // ctx.writeAndFlush(Unpooled.EMPTY_BUFFER.array()).addListener(ChannelFutureListener.CLOSE);
    ctx.flush();
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    cause.printStackTrace();
    ctx.close();
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object obj) throws Exception {
    byte data[] = (byte[]) obj;
    System.out.println("channelRead::" + new String(data));

    EduUserCourseJuniorWithBLOBs bs = service.selectByPrimaryKey("sd_fds_fsdf_ds");
    ByteBuf buff = null;
    if (bs == null || bs.getCourseStatus() != 1) {
      buff = Unpooled.copiedBuffer(Unpooled.copiedBuffer(NO), Unpooled.copiedBuffer(DA));
      ctx.write(buff.array());// .addListener(ChannelFutureListener.CLOSE);
    }
    if (bs.getCourseStatus() == 1) {
      String blocklyCode = bs.getBlocklyCode();
      try {
        EduUserCourseJuniorWithBLOBs blobs = new EduUserCourseJuniorWithBLOBs();
        blobs.setCourseUid("sd_fds_fsdf_ds");
        blobs.setCourseStatus(2);
        service.updateByPrimaryKeySelective(blobs);
        ctx.write(blocklyCode.getBytes("UTF-8"));// .addListener(ChannelFutureListener.CLOSE);
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
    }
    ctx.flush();
    super.channelRead(ctx, obj);
  }

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, byte[] bytes) throws Exception {

    System.out.println("channelRead0::" + new String(bytes));
  }
}
