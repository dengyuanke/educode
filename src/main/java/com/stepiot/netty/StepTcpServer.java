package com.stepiot.netty;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.stepiot.service.EduUserCourseJuniorService;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

@Component
public class StepTcpServer {

  InetSocketAddress sa;

  private ApplicationContext context;

  @Autowired
  private EduUserCourseJuniorService service;

  @Autowired
  public StepTcpServer(ApplicationContext context) {
    this.context = context;
    try {
      Environment env = context.getEnvironment();
      InetAddress address = InetAddress.getByName(env.getProperty("netty.server.bind"));
      sa = new InetSocketAddress(address, Integer.parseInt(env.getProperty("netty.server.port")));
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
  }

  public void run() throws Exception {

    service.selectByPrimaryKey("sd_fds_fsdf_ds");
    EventLoopGroup bossGroup = new NioEventLoopGroup();
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    try {
      ServerBootstrap b = new ServerBootstrap();

      b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
          .handler(new LoggingHandler(LogLevel.INFO))
          .childHandler(new StepTcpServerInitializer(context))
          .option(ChannelOption.SO_BACKLOG, 1024).childOption(ChannelOption.TCP_NODELAY, true)
          .childOption(ChannelOption.SO_KEEPALIVE, true);

      ChannelFuture f = b.bind(sa).sync();

      f.channel().closeFuture().sync();
    } finally {
      workerGroup.shutdownGracefully();
      bossGroup.shutdownGracefully();
    }
  }

}
