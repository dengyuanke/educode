/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License, version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.stepiot.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Simplistic telnet client.
 */
public final class StepTcpClient {

  static final String HOST = "127.0.0.1";
  static final int PORT = 10111;

  public static void main11(String[] args) throws Exception {

    EventLoopGroup group = new NioEventLoopGroup();
    try {
      Bootstrap b = new Bootstrap();
      b.group(group).channel(NioSocketChannel.class).option(ChannelOption.SO_BACKLOG, 1024)
          .option(ChannelOption.SO_KEEPALIVE, true).handler(new StepTcpClientInitializer());

      // Start the connection attempt.
      ChannelFuture future = b.connect(HOST, PORT).sync();

      future.channel().closeFuture().sync();

    } finally {
      group.shutdownGracefully();
    }
  }
}
