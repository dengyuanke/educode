package com.stepiot.netty;

import org.springframework.context.ApplicationContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

public class StepTcpServerInitializer extends ChannelInitializer<SocketChannel> {


  private StepTcpServerHandler stepTcpServerHandler;

  public StepTcpServerInitializer(ApplicationContext context) {
    super();
    stepTcpServerHandler = context.getBean(StepTcpServerHandler.class);
  }

  @Override
  public void initChannel(SocketChannel ch) {
    ChannelPipeline pipeline = ch.pipeline();

    // Decoders
    pipeline.addLast("frameDecoder",
        new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
    pipeline.addLast("bytesDecoder", new ByteArrayDecoder());

    // Encoder
    pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
    pipeline.addLast("bytesEncoder", new ByteArrayEncoder());

    pipeline.addLast(stepTcpServerHandler);
  }

}
