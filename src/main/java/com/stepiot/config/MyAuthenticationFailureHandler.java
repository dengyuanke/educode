package com.stepiot.config;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import com.stepiot.constants.Cons;
import com.stepiot.service.LoginUserBean;

@Service
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {

  @Autowired
  ApplicationContext appContext;

  @Autowired
  SessionLocaleResolver localeResolver;

  private static final String key = "validate.invalid.username.or.password";


  @Override
  public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse res,
      AuthenticationException exp) throws IOException, ServletException {
    String messageKey = key;
    if (exp != null) {
      if (Cons.LOGIN_BLOCKED_MESSAGE.equals(exp.getMessage())) {
        messageKey = Cons.LOGIN_BLOCKED_MESSAGE;
      }
    }
    String username = req.getParameter("username");
    String password = req.getParameter("password");
    LoginUserBean userbean = new LoginUserBean();
    userbean.setUsername(username);
    userbean.setPassword(password);
    userbean.setError(appContext.getMessage(messageKey, null, localeResolver.resolveLocale(req)));
    req.getSession().setAttribute(LoginUserBean.SESSION_KEY, userbean);

    res.sendRedirect("/login?error=true");
  }

}
