package com.stepiot.config;

import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.amazonaws.regions.Regions;
import org.apache.commons.validator.routines.EmailValidator;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.stepiot.util.RandomUtils;

@Configuration
@EnableRedisHttpSession
public class WebConfigurer implements WebMvcConfigurer {

  @Value("${stepiot.static.dns}")
  private String dnstatic;

  @Value("${stepiot.static.deve}")
  private String devestatic;

  @Value("${spring.redis.host}")
  private String redishost;

  @Value("${spring.redis.port}")
  private Integer redisport;


  @Autowired
  private ApplicationContext appContext;

  @Autowired
  LocaleInterceptor localeInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(localeChangeInterceptor());
    registry.addInterceptor(localeInterceptor);

  }

  @Bean
  public LocaleResolver localeResolver() {
    return sessionLocaleResolver();
  }

  @Bean
  public RedissonClient redisson() {
    Config config = new Config();
    config.useReplicatedServers().setScanInterval(2000) // 主节点变化扫描间隔时间
        .addNodeAddress("redis://" + redishost + ":" + redisport);

    return Redisson.create(config);
  }

  @Bean
  @Qualifier("userCodeRunner")
  public ExecutorService userCodeRunner() {
    ExecutorService userCodeRunner =
        Executors.newFixedThreadPool(1, UserCodeRunnerFactory.getInstance());

    return userCodeRunner;
  }

  @Bean
  public SessionLocaleResolver sessionLocaleResolver() {
    SessionLocaleResolver slr = new SessionLocaleResolver();
    slr.setDefaultLocale(Locale.GERMAN);
    return slr;
  }

  @Bean
  public LocaleChangeInterceptor localeChangeInterceptor() {
    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
    lci.setParamName("lang");
    return lci;
  }

  @Bean
  public EmailValidator emailValidate() {
    return EmailValidator.getInstance();
  }

  @Bean
  public AmazonS3 amazonS3() {
    return AmazonS3ClientBuilder.standard().withRegion(Regions.CN_NORTHWEST_1).build();
  }

  @Bean
  public static ConfigureRedisAction configureRedisAction() {
    return ConfigureRedisAction.NO_OP;
  }

  @Bean
  public ServletContextInitializer initializer() {
    return new ServletContextInitializer() {
      @Override
      public void onStartup(final ServletContext context) throws ServletException {

        Locale locale = LocaleContextHolder.getLocale();
        String version = RandomUtils.genPass(8);
        context.setAttribute("version", version);
        context.setAttribute("vstatic", "?v=" + version);
        context.setAttribute("dnstatic", dnstatic);
        context.setAttribute("devestatic", devestatic);
        setRandomBgImage(context);
        context.setAttribute("sysname", appContext.getMessage("system.name", null, locale));

      }
    };
  }

  @Autowired
  private void setRandomBgImage(final ServletContext context) {
    Random random = new Random();

    final String[] bgs = {"1.jpg", "2.jpg", "3.jpg"};

    ScheduledExecutorService exe = Executors.newSingleThreadScheduledExecutor();
    exe.scheduleAtFixedRate(new Runnable() {

      @Override
      public void run() {
        context.setAttribute("bgimg", bgs[random.nextInt(bgs.length)]);
      }
    }, 10, 20, TimeUnit.SECONDS);
  }

  @Bean
  public HttpSessionEventPublisher httpSessionEventPublisher() {
    return new HttpSessionEventPublisher();
  }

  @Bean
  public EduSessionListener mySessionListener() {
    return new EduSessionListener();
  }

  @Bean
  public RequestContextListener requestContextListener() {
    RequestContextListener requestContextListener = new RequestContextListener();
    return requestContextListener;
  }
}
