package com.stepiot.config;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import com.google.common.collect.Maps;
import com.stepiot.iot.IotUserCore;

/**
 * 
 * @author denny
 *
 */
public class UserCodeRunnerFactory implements ThreadFactory {



  public static final String USER_CODE_RUNNER_PREFIX = "UserCodeRunner";

  private static final Map<String, Thread> threadMap = Maps.newConcurrentMap();
  public static final Map<String, Future<String>> futureMap = Maps.newConcurrentMap();

  private UserCodeRunnerFactory() {

  }

  int x = 0;

  private static UserCodeRunnerFactory instance = new UserCodeRunnerFactory();

  public static UserCodeRunnerFactory getInstance() {
    return instance;
  }

  @Autowired
  @Qualifier("userCodeRunner")
  private ExecutorService runner;

  @Override
  public Thread newThread(Runnable r) {
    String key = USER_CODE_RUNNER_PREFIX + getUserThreadName();
    String username = "physicsfamily";
    IotUserCore iotUserCore = IotUserCore.getInstance(username);
    if (threadMap.containsKey(key)) {
      Thread thread = threadMap.get(key);
      while (thread.isAlive()) {
        // thread.interrupt();
        iotUserCore.setInterrupt(1);
        try {
          Thread.sleep(200);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

      }

    }
    Thread t = new Thread(r, key + x++);
    // t.setDaemon(false);
    threadMap.put(key, t);
    iotUserCore.setInterrupt(0);
    return t;
  }

  private String getUserThreadName() {
    UserDetails details = UserContextHolder.getUserDetails();
    if (details != null) {
      return details.getUsername();
    }
    return null;
  }

}
