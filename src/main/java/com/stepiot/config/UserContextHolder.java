package com.stepiot.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * @author denny
 *
 */
public class UserContextHolder {

  public static UserDetails getUserDetails() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return null;
    }
    UserDetails details = (UserDetails) authentication.getPrincipal();
    if (details == null) {
      return null;
    }
    return details;
  }

}
