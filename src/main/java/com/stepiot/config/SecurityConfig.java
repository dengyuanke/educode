package com.stepiot.config;

import java.io.IOException;
import java.nio.charset.Charset;
import javax.sql.DataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.proto.MenuProto.Menu;
import com.stepiot.proto.MenuProto.MenuMap;
import com.stepiot.proto.MenuProto.MenuMap.Builder;
import com.stepiot.service.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private AccessDeniedHandler accessDeniedHandler;

  @Autowired
  private AuthenticationFailureHandler failureHandler;

  @Autowired
  ApplicationContext appContext;


  @Bean
  public MenuMap buildMenu() {
    com.stepiot.proto.MenuProto.Menu.Builder menuBuilder = Menu.newBuilder();
    try {
      Resource[] resources = appContext.getResources("classpath:menu_*.json");
      Builder roleBuilder = MenuMap.newBuilder();
      for (int i = 0; i < resources.length; i++) {
        Resource resource = resources[i];

        JsonFormat.parser().merge(
            IOUtils.toString(resource.getInputStream(), Charset.forName("UTF-8")),
            menuBuilder.clear());
        roleBuilder.putRoleMenu(
            "ROLE_" + StringUtils.substringBetween(resource.getFilename(), "_", ".").toUpperCase(),
            menuBuilder.build());
      }

      return roleBuilder.build();
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests()
        .antMatchers("/login**", "/css/**", "/downloads", "/faq", "/help", "/recaptcha",
            "/recovery", "/vcode", "/health", "/recoveried", "/newpassword", "/signup", "/fonts/**",
            "/images/**", "/code/**", "/vendors/**", "/about", "/vphone", "/tphone", "/done",
            "/xblockly/**", "/pub", "/noop/**")
        .permitAll().antMatchers("/admin*").hasRole("ADMIN").anyRequest().authenticated().and()
        .formLogin().loginPage("/login").failureHandler(failureHandler).permitAll().and().logout()
        .logoutUrl("/logout").logoutSuccessUrl("/login").permitAll().and().exceptionHandling()
        .accessDeniedHandler(accessDeniedHandler).and().logout().deleteCookies("SESSION").and()
        .rememberMe().key("uniqueAndSecret").tokenRepository(persistentTokenRepository())
        .tokenValiditySeconds(1209600);

  }

  @Autowired
  private DataSource dataSource;

  @Autowired
  private CustomUserDetailsService userDetailService;

  @Bean
  public PersistentTokenRepository persistentTokenRepository() {
    JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
    db.setDataSource(dataSource);
    return db;
  }

  private DaoAuthenticationProvider authProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setUserDetailsService(userDetailService);
    provider.setPasswordEncoder(passwordEncoder());
    return provider;
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.jdbcAuthentication().dataSource(dataSource)
        .usersByUsernameQuery(
            "SELECT username, password, enabled, email, telephone FROM edu_user where username = ?")
        .authoritiesByUsernameQuery("SELECT username, role FROM edu_user_role WHERE username = ?");
    auth.authenticationProvider(authProvider());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
