package com.stepiot.config;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.stepiot.constants.Cons;
import com.stepiot.model.EduConfigPersonal;
import com.stepiot.model.EduUser;
import com.stepiot.proto.MenuProto.Menu;
import com.stepiot.proto.MenuProto.MenuItem.Builder;
import com.stepiot.proto.MenuProto.MenuMap;
import com.stepiot.service.EduConfigPersonalService;
import com.stepiot.service.EduConfigSystemService;
import com.stepiot.service.EduUserService;
import com.stepiot.service.UserBean;
import com.stepiot.util.BcbCreator;
import com.stepiot.util.GsonUtils;
import com.stepiot.util.UrlUtils;

@Component
public class LocaleInterceptor implements HandlerInterceptor {

  @Autowired
  private EduConfigPersonalService personalConfig;

  @Autowired
  private EduConfigSystemService sysConfig;

  @Autowired
  private EduUserService uservice;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    String uri = UrlUtils.trimToRoot(request.getRequestURI());
    if (uri.equals("/")) {
//      response.sendRedirect("/pythoncourse/outline");
      request.getRequestDispatcher("/pythoncourse/outline").forward(request,response);
      return false;
    }
    return HandlerInterceptor.super.preHandle(request, response, handler);
  }

  @Autowired
  private MenuMap roleMap;

  private Map<String, Set<String>> linkMapSet;

  private static final String ANON = "ROLE_ANON";

  @Autowired
  private void injectsLink(MenuMap roleMap) {
    Map<String, Menu> map = roleMap.getRoleMenuMap();
    Set<Entry<String, Menu>> set = map.entrySet();
    Iterator<Entry<String, Menu>> it = set.iterator();

    linkMapSet = Maps.newHashMap();
    while (it.hasNext()) {
      Entry<String, Menu> entry = it.next();
      linkMapSet.put(entry.getKey(), injectLink(entry.getValue()));
    }
  }

  private Set<String> injectLink(Menu menu) {
    Set<String> linkSet = Sets.newHashSet();
    com.stepiot.proto.MenuProto.Menu.Builder mbuilder = menu.toBuilder();
    List<Builder> list = mbuilder.getItemBuilderList();
    for (int i = 0; i < list.size(); i++) {
      Builder itemb = list.get(i);
      List<Builder> itemcs = itemb.getItemBuilderList();
      for (int j = 0; j < itemcs.size(); j++) {
        Builder itemc = itemcs.get(j);
        linkSet.add(itemc.getLink());
      }
    }
    return linkSet;
  }

  @Autowired
  SessionLocaleResolver localeResolver;

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {
    Locale locale = localeResolver.resolveLocale(request);
    LocaleContextHolder.setLocale(locale);
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return;
    }
    String requestURI = request.getRequestURI();
    // 非匿名用户
    if (!(authentication instanceof AnonymousAuthenticationToken)) {
      UserDetails userDetails = (UserDetails) authentication.getPrincipal();
      if (modelAndView == null) {
        return;
      }
      List<EduConfigPersonal> configs = personalConfig.getAllThemeConfig(userDetails.getUsername());
      if (configs != null && configs.size() > 0) {
        for (int i = 0; i < configs.size(); i++) {
          EduConfigPersonal pconfig = configs.get(i);
          modelAndView.getModel().put(pconfig.getConfigItem(), pconfig.getConfigValue());
        }
      }
      requestURI = UrlUtils.trimToRoot(requestURI);
      String role = userDetails.getAuthorities().iterator().next().toString();
      if (linkMapSet.get(role).contains(requestURI)) {
        buildRoleMenu(role, modelAndView.getModel(), requestURI, request);
      }
      // 无权访问该页面
      else {
//        response.sendRedirect("/");
        request.getRequestDispatcher("/").forward(request,response);
        return;
      }
      if (!requestURI.equals("/userprofile")) {
        // 检查是否已经修改过初始密码，没有的话，跳转到信息补全页面，强制修改用户密码
        Date lut = null;
        if (userDetails instanceof UserBean) {
          UserBean bean = (UserBean) userDetails;
          lut = bean.getPasswordLut();
        } else {
          EduUser user = uservice.selectByPrimaryKey(userDetails.getUsername());
          lut = user.getPasswordLut();
        }
        if (lut == null) {
//          response.sendRedirect("/userprofile");
          request.getRequestDispatcher("/userprofile").forward(request,response);
          return;
        }
      }
    }
    // 如果为匿名用户
    else {
      Set<String> anonSet = linkMapSet.get(ANON);
      if (anonSet != null && anonSet.contains(requestURI)) {
        buildRoleMenu(ANON, modelAndView.getModel(), requestURI, request);
      }
    }
    if (modelAndView != null) {
      Map<String, Object> model = modelAndView.getModel();
      model.put("localeStr", locale.getLanguage().toString());

      // 如果个人没有设置主题，则使用系统主题
      if (model.get(Cons.CONFIG_BLOCKLY_THEME) == null) {
        Map<String, Object> themeConfig = sysConfig.getConfigByKey(Cons.CONFIG_BLOCKLY_THEME);
        model.put(Cons.CONFIG_BLOCKLY_THEME, themeConfig.get("configValue"));
      }
      if (model.get(Cons.CONFIG_SYS_THEME) == null) {
        Map<String, Object> themeConfig = sysConfig.getConfigByKey(Cons.CONFIG_SYS_THEME);
        model.put(Cons.CONFIG_SYS_THEME, themeConfig.get("configValue"));
      }
    }
    HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
  }

  private void buildRoleMenu(String role, Map<String, Object> model, final String uri,
      HttpServletRequest request) {
    Menu menu = roleMap.getRoleMenuOrThrow(role);
    buildMenu(model, uri, menu, request);
  }

  private void buildMenu(Map<String, Object> model, final String uri, Menu menu,
      HttpServletRequest request) {
    String json;
    com.stepiot.proto.MenuProto.Menu.Builder mbuilder = menu.toBuilder();
    List<Builder> list = mbuilder.getItemBuilderList();
    outer: for (int i = 0; i < list.size(); i++) {
      Builder itemb = list.get(i);
      List<Builder> itemcs = itemb.getItemBuilderList();
      for (int j = 0; j < itemcs.size(); j++) {
        Builder itemc = itemcs.get(j);
        if (itemc.getLink().equals(uri)) {
          itemc.setActive("true");
          itemb.setActive("true");
          model.put("titleName", itemc.getName());
          model.put("currentIcon", itemc.getIcon());// 2019-03-06前用material design icon
          model.put("currentMdicon", itemc.getMdicon());// 修改为https://materialdesignicons.com/字体
          com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder breadbuilder =
              (com.stepiot.proto.BreadcrumbProto.Breadcrumb.Builder) request
                  .getAttribute(Cons.REQUEST_ATTR_BREADCRUMB_BUILDER);
          breadbuilder.setSubject(itemc.getName());
          breadbuilder.setSubjectIcon(itemc.getMdicon());

          model.put("bcb", BcbCreator.build(breadbuilder));
          model.put("breadbuilder", breadbuilder);
          model.put("baseUri", uri);
          model.put("fullUri", request.getRequestURI());
          break outer;
        }
      }
    }
    try {
      json = JsonFormat.printer().preservingProtoFieldNames().print(mbuilder);
      Map<String, Object> map = GsonUtils.toMap(json);
      model.put("menu", map);
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }
}
