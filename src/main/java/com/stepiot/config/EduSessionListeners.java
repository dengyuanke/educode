package com.stepiot.config;

import org.springframework.context.event.EventListener;
import org.springframework.session.events.SessionCreatedEvent;
import org.springframework.session.events.SessionDestroyedEvent;
import org.springframework.session.events.SessionExpiredEvent;
import org.springframework.stereotype.Component;

@Component
public class EduSessionListeners {

  @EventListener
  public void sessionDestroyed(SessionDestroyedEvent event) {}

  @EventListener
  public void sessionCreated(SessionCreatedEvent event) {}

  @EventListener
  public void sessionExired(SessionExpiredEvent event) {}

}
