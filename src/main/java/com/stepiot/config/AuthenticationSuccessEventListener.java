package com.stepiot.config;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;
import com.stepiot.model.EduLogLogin;
import com.stepiot.service.EduLogLoginService;
import com.stepiot.service.LoginAttemptService;

@Component
public class AuthenticationSuccessEventListener
    implements ApplicationListener<AuthenticationSuccessEvent> {

  @Autowired
  private LoginAttemptService loginAttemptService;

  @Autowired
  private EduLogLoginService logService;

  @Autowired
  private HttpServletRequest request;

  @Override
  public void onApplicationEvent(AuthenticationSuccessEvent event) {
    String name = event.getAuthentication().getName();
    event.getAuthentication();
    EduLogLogin log = new EduLogLogin();
    log.setUsername(name);
    log.setLoginTime(new Date());
    log.setIpaddress(getClientIP());
    logService.insert(log);
    loginAttemptService.loginSucceeded(name);
  }

  private String getClientIP() {
    String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader == null) {
      return request.getRemoteAddr();
    }
    return xfHeader.split(",")[0];
  }
}
