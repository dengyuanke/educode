
package com.stepiot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ServletComponentScan
@EnableCaching
@EnableAsync
@MapperScan("com.stepiot.dao")
public class CodeStepiot {// implements CommandLineRunner {

  public static void main(String[] args) {// throws IOException, NoSuchAlgorithmException,
                                          // InvalidKeySpecException {

    SpringApplication.run(CodeStepiot.class, args);
  }

  // @Autowired
  // private StepTcpServer server;

  // @Override
  // public void run(String... args) throws Exception {
  // server.run();

  // CqtbiTest.main(null);

  // }

}
