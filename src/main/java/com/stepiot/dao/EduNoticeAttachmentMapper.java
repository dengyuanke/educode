package com.stepiot.dao;

import com.stepiot.model.EduNoticeAttachment;
import com.stepiot.model.EduNoticeAttachmentCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduNoticeAttachmentMapper {
    long countByExample(EduNoticeAttachmentCriteria example);

    int deleteByExample(EduNoticeAttachmentCriteria example);

    int deleteByPrimaryKey(Integer attachmentId);

    int insert(EduNoticeAttachment record);

    int insertSelective(EduNoticeAttachment record);

    List<EduNoticeAttachment> selectByExample(EduNoticeAttachmentCriteria example);

    EduNoticeAttachment selectByPrimaryKey(Integer attachmentId);

    int updateByExampleSelective(@Param("record") EduNoticeAttachment record, @Param("example") EduNoticeAttachmentCriteria example);

    int updateByExample(@Param("record") EduNoticeAttachment record, @Param("example") EduNoticeAttachmentCriteria example);

    int updateByPrimaryKeySelective(EduNoticeAttachment record);

    int updateByPrimaryKey(EduNoticeAttachment record);

    EduNoticeAttachment selectByExampleForOne(EduNoticeAttachmentCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer attachmentId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduNoticeAttachmentCriteria example);

    List<Map<String, Object>> selectMapByExample(EduNoticeAttachmentCriteria example);
}