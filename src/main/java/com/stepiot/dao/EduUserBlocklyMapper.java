package com.stepiot.dao;

import com.stepiot.model.EduUserBlockly;
import com.stepiot.model.EduUserBlocklyCriteria;
import com.stepiot.model.EduUserBlocklyWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserBlocklyMapper {
    long countByExample(EduUserBlocklyCriteria example);

    int deleteByExample(EduUserBlocklyCriteria example);

    int deleteByPrimaryKey(String blocklyId);

    int insert(EduUserBlocklyWithBLOBs record);

    int insertSelective(EduUserBlocklyWithBLOBs record);

    List<EduUserBlocklyWithBLOBs> selectByExampleWithBLOBs(EduUserBlocklyCriteria example);

    List<EduUserBlockly> selectByExample(EduUserBlocklyCriteria example);

    EduUserBlocklyWithBLOBs selectByPrimaryKey(String blocklyId);

    int updateByExampleSelective(@Param("record") EduUserBlocklyWithBLOBs record, @Param("example") EduUserBlocklyCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserBlocklyWithBLOBs record, @Param("example") EduUserBlocklyCriteria example);

    int updateByExample(@Param("record") EduUserBlockly record, @Param("example") EduUserBlocklyCriteria example);

    int updateByPrimaryKeySelective(EduUserBlocklyWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserBlocklyWithBLOBs record);

    int updateByPrimaryKey(EduUserBlockly record);

    EduUserBlockly selectByExampleForOne(EduUserBlocklyCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String blocklyId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserBlocklyCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserBlocklyCriteria example);
}