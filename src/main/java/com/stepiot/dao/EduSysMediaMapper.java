package com.stepiot.dao;

import com.stepiot.model.EduSysMedia;
import com.stepiot.model.EduSysMediaCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduSysMediaMapper {
    long countByExample(EduSysMediaCriteria example);

    int deleteByExample(EduSysMediaCriteria example);

    int deleteByPrimaryKey(Integer mediaId);

    int insert(EduSysMedia record);

    int insertSelective(EduSysMedia record);

    List<EduSysMedia> selectByExample(EduSysMediaCriteria example);

    EduSysMedia selectByPrimaryKey(Integer mediaId);

    int updateByExampleSelective(@Param("record") EduSysMedia record, @Param("example") EduSysMediaCriteria example);

    int updateByExample(@Param("record") EduSysMedia record, @Param("example") EduSysMediaCriteria example);

    int updateByPrimaryKeySelective(EduSysMedia record);

    int updateByPrimaryKey(EduSysMedia record);

    EduSysMedia selectByExampleForOne(EduSysMediaCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer mediaId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduSysMediaCriteria example);

    List<Map<String, Object>> selectMapByExample(EduSysMediaCriteria example);
}