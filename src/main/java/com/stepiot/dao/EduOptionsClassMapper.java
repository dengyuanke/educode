package com.stepiot.dao;

import com.stepiot.model.EduOptionsClass;
import com.stepiot.model.EduOptionsClassCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduOptionsClassMapper {
    long countByExample(EduOptionsClassCriteria example);

    int deleteByExample(EduOptionsClassCriteria example);

    int deleteByPrimaryKey(Integer optId);

    int insert(EduOptionsClass record);

    int insertSelective(EduOptionsClass record);

    List<EduOptionsClass> selectByExample(EduOptionsClassCriteria example);

    EduOptionsClass selectByPrimaryKey(Integer optId);

    int updateByExampleSelective(@Param("record") EduOptionsClass record, @Param("example") EduOptionsClassCriteria example);

    int updateByExample(@Param("record") EduOptionsClass record, @Param("example") EduOptionsClassCriteria example);

    int updateByPrimaryKeySelective(EduOptionsClass record);

    int updateByPrimaryKey(EduOptionsClass record);

    EduOptionsClass selectByExampleForOne(EduOptionsClassCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer optId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduOptionsClassCriteria example);

    List<Map<String, Object>> selectMapByExample(EduOptionsClassCriteria example);
}