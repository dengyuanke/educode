package com.stepiot.dao;

import com.stepiot.model.AiEduUserCourse;
import com.stepiot.model.AiEduUserCourseCriteria;
import com.stepiot.model.AiEduUserCourseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface AiEduUserCourseMapper {
    long countByExample(AiEduUserCourseCriteria example);

    int deleteByExample(AiEduUserCourseCriteria example);

    int deleteByPrimaryKey(String courseUid);

    int insert(AiEduUserCourseWithBLOBs record);

    int insertSelective(AiEduUserCourseWithBLOBs record);

    List<AiEduUserCourseWithBLOBs> selectByExampleWithBLOBs(AiEduUserCourseCriteria example);

    List<AiEduUserCourse> selectByExample(AiEduUserCourseCriteria example);

    AiEduUserCourseWithBLOBs selectByPrimaryKey(String courseUid);

    int updateByExampleSelective(@Param("record") AiEduUserCourseWithBLOBs record, @Param("example") AiEduUserCourseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") AiEduUserCourseWithBLOBs record, @Param("example") AiEduUserCourseCriteria example);

    int updateByExample(@Param("record") AiEduUserCourse record, @Param("example") AiEduUserCourseCriteria example);

    int updateByPrimaryKeySelective(AiEduUserCourseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(AiEduUserCourseWithBLOBs record);

    int updateByPrimaryKey(AiEduUserCourse record);

    AiEduUserCourse selectByExampleForOne(AiEduUserCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String courseUid);

    java.util.Map<String, Object> selectMapByExampleForOne(AiEduUserCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(AiEduUserCourseCriteria example);
}