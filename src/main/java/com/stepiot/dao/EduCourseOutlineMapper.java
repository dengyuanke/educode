package com.stepiot.dao;

import com.stepiot.model.EduCourseOutline;
import com.stepiot.model.EduCourseOutlineCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduCourseOutlineMapper {
    long countByExample(EduCourseOutlineCriteria example);

    int deleteByExample(EduCourseOutlineCriteria example);

    int deleteByPrimaryKey(Integer outlineId);

    int insert(EduCourseOutline record);

    int insertSelective(EduCourseOutline record);

    List<EduCourseOutline> selectByExample(EduCourseOutlineCriteria example);

    EduCourseOutline selectByPrimaryKey(Integer outlineId);

    int updateByExampleSelective(@Param("record") EduCourseOutline record, @Param("example") EduCourseOutlineCriteria example);

    int updateByExample(@Param("record") EduCourseOutline record, @Param("example") EduCourseOutlineCriteria example);

    int updateByPrimaryKeySelective(EduCourseOutline record);

    int updateByPrimaryKey(EduCourseOutline record);

    EduCourseOutline selectByExampleForOne(EduCourseOutlineCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer outlineId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduCourseOutlineCriteria example);

    List<Map<String, Object>> selectMapByExample(EduCourseOutlineCriteria example);
}