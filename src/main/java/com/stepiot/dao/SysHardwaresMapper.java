package com.stepiot.dao;

import com.stepiot.model.SysHardwares;
import com.stepiot.model.SysHardwaresCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface SysHardwaresMapper {
    long countByExample(SysHardwaresCriteria example);

    int deleteByExample(SysHardwaresCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysHardwares record);

    int insertSelective(SysHardwares record);

    List<SysHardwares> selectByExample(SysHardwaresCriteria example);

    SysHardwares selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysHardwares record, @Param("example") SysHardwaresCriteria example);

    int updateByExample(@Param("record") SysHardwares record, @Param("example") SysHardwaresCriteria example);

    int updateByPrimaryKeySelective(SysHardwares record);

    int updateByPrimaryKey(SysHardwares record);

    SysHardwares selectByExampleForOne(SysHardwaresCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer id);

    java.util.Map<String, Object> selectMapByExampleForOne(SysHardwaresCriteria example);

    List<Map<String, Object>> selectMapByExample(SysHardwaresCriteria example);
}