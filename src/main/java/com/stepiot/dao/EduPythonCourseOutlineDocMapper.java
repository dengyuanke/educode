package com.stepiot.dao;

import com.stepiot.model.EduPythonCourseOutlineDoc;
import com.stepiot.model.EduPythonCourseOutlineDocCriteria;
import com.stepiot.model.EduPythonCourseOutlineDocWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduPythonCourseOutlineDocMapper {
    long countByExample(EduPythonCourseOutlineDocCriteria example);

    int deleteByExample(EduPythonCourseOutlineDocCriteria example);

    int deleteByPrimaryKey(Integer outlineId);

    int insert(EduPythonCourseOutlineDocWithBLOBs record);

    int insertSelective(EduPythonCourseOutlineDocWithBLOBs record);

    List<EduPythonCourseOutlineDocWithBLOBs> selectByExampleWithBLOBs(EduPythonCourseOutlineDocCriteria example);

    List<EduPythonCourseOutlineDoc> selectByExample(EduPythonCourseOutlineDocCriteria example);

    EduPythonCourseOutlineDocWithBLOBs selectByPrimaryKey(Integer outlineId);

    int updateByExampleSelective(@Param("record") EduPythonCourseOutlineDocWithBLOBs record, @Param("example") EduPythonCourseOutlineDocCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduPythonCourseOutlineDocWithBLOBs record, @Param("example") EduPythonCourseOutlineDocCriteria example);

    int updateByExample(@Param("record") EduPythonCourseOutlineDoc record, @Param("example") EduPythonCourseOutlineDocCriteria example);

    int updateByPrimaryKeySelective(EduPythonCourseOutlineDocWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduPythonCourseOutlineDocWithBLOBs record);

    int updateByPrimaryKey(EduPythonCourseOutlineDoc record);

    EduPythonCourseOutlineDoc selectByExampleForOne(EduPythonCourseOutlineDocCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer outlineId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduPythonCourseOutlineDocCriteria example);

    List<Map<String, Object>> selectMapByExample(EduPythonCourseOutlineDocCriteria example);
}