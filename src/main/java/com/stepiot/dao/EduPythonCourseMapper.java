package com.stepiot.dao;

import com.stepiot.model.EduPythonCourse;
import com.stepiot.model.EduPythonCourseCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduPythonCourseMapper {
    long countByExample(EduPythonCourseCriteria example);

    int deleteByExample(EduPythonCourseCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(EduPythonCourse record);

    int insertSelective(EduPythonCourse record);

    List<EduPythonCourse> selectByExample(EduPythonCourseCriteria example);

    EduPythonCourse selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") EduPythonCourse record, @Param("example") EduPythonCourseCriteria example);

    int updateByExample(@Param("record") EduPythonCourse record, @Param("example") EduPythonCourseCriteria example);

    int updateByPrimaryKeySelective(EduPythonCourse record);

    int updateByPrimaryKey(EduPythonCourse record);

    EduPythonCourse selectByExampleForOne(EduPythonCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduPythonCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(EduPythonCourseCriteria example);
}