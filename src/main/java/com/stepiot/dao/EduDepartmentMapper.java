package com.stepiot.dao;

import com.stepiot.model.EduDepartment;
import com.stepiot.model.EduDepartmentCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduDepartmentMapper {
    long countByExample(EduDepartmentCriteria example);

    int deleteByExample(EduDepartmentCriteria example);

    int deleteByPrimaryKey(String departmentId);

    int insert(EduDepartment record);

    int insertSelective(EduDepartment record);

    List<EduDepartment> selectByExample(EduDepartmentCriteria example);

    EduDepartment selectByPrimaryKey(String departmentId);

    int updateByExampleSelective(@Param("record") EduDepartment record, @Param("example") EduDepartmentCriteria example);

    int updateByExample(@Param("record") EduDepartment record, @Param("example") EduDepartmentCriteria example);

    int updateByPrimaryKeySelective(EduDepartment record);

    int updateByPrimaryKey(EduDepartment record);

    EduDepartment selectByExampleForOne(EduDepartmentCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String departmentId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduDepartmentCriteria example);

    List<Map<String, Object>> selectMapByExample(EduDepartmentCriteria example);
}