package com.stepiot.dao;

import com.stepiot.model.EduConfigPersonal;
import com.stepiot.model.EduConfigPersonalCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduConfigPersonalMapper {
    long countByExample(EduConfigPersonalCriteria example);

    int deleteByExample(EduConfigPersonalCriteria example);

    int deleteByPrimaryKey(Integer configId);

    int insert(EduConfigPersonal record);

    int insertSelective(EduConfigPersonal record);

    List<EduConfigPersonal> selectByExample(EduConfigPersonalCriteria example);

    EduConfigPersonal selectByPrimaryKey(Integer configId);

    int updateByExampleSelective(@Param("record") EduConfigPersonal record, @Param("example") EduConfigPersonalCriteria example);

    int updateByExample(@Param("record") EduConfigPersonal record, @Param("example") EduConfigPersonalCriteria example);

    int updateByPrimaryKeySelective(EduConfigPersonal record);

    int updateByPrimaryKey(EduConfigPersonal record);

    EduConfigPersonal selectByExampleForOne(EduConfigPersonalCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer configId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduConfigPersonalCriteria example);

    List<Map<String, Object>> selectMapByExample(EduConfigPersonalCriteria example);
}