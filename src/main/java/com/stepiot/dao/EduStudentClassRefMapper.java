package com.stepiot.dao;

import com.stepiot.model.EduStudentClassRef;
import com.stepiot.model.EduStudentClassRefCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduStudentClassRefMapper {
    long countByExample(EduStudentClassRefCriteria example);

    int deleteByExample(EduStudentClassRefCriteria example);

    int deleteByPrimaryKey(Integer refId);

    int insert(EduStudentClassRef record);

    int insertSelective(EduStudentClassRef record);

    List<EduStudentClassRef> selectByExample(EduStudentClassRefCriteria example);

    EduStudentClassRef selectByPrimaryKey(Integer refId);

    int updateByExampleSelective(@Param("record") EduStudentClassRef record, @Param("example") EduStudentClassRefCriteria example);

    int updateByExample(@Param("record") EduStudentClassRef record, @Param("example") EduStudentClassRefCriteria example);

    int updateByPrimaryKeySelective(EduStudentClassRef record);

    int updateByPrimaryKey(EduStudentClassRef record);

    EduStudentClassRef selectByExampleForOne(EduStudentClassRefCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer refId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduStudentClassRefCriteria example);

    List<Map<String, Object>> selectMapByExample(EduStudentClassRefCriteria example);
}