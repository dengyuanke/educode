package com.stepiot.dao;

import com.stepiot.model.EduUserTortoise;
import com.stepiot.model.EduUserTortoiseCriteria;
import com.stepiot.model.EduUserTortoiseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserTortoiseMapper {
    long countByExample(EduUserTortoiseCriteria example);

    int deleteByExample(EduUserTortoiseCriteria example);

    int deleteByPrimaryKey(String blocklyId);

    int insert(EduUserTortoiseWithBLOBs record);

    int insertSelective(EduUserTortoiseWithBLOBs record);

    List<EduUserTortoiseWithBLOBs> selectByExampleWithBLOBs(EduUserTortoiseCriteria example);

    List<EduUserTortoise> selectByExample(EduUserTortoiseCriteria example);

    EduUserTortoiseWithBLOBs selectByPrimaryKey(String blocklyId);

    int updateByExampleSelective(@Param("record") EduUserTortoiseWithBLOBs record, @Param("example") EduUserTortoiseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserTortoiseWithBLOBs record, @Param("example") EduUserTortoiseCriteria example);

    int updateByExample(@Param("record") EduUserTortoise record, @Param("example") EduUserTortoiseCriteria example);

    int updateByPrimaryKeySelective(EduUserTortoiseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserTortoiseWithBLOBs record);

    int updateByPrimaryKey(EduUserTortoise record);

    EduUserTortoise selectByExampleForOne(EduUserTortoiseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String blocklyId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserTortoiseCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserTortoiseCriteria example);
}