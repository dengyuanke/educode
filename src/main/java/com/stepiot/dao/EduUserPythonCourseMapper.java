package com.stepiot.dao;

import com.stepiot.model.EduUserPythonCourse;
import com.stepiot.model.EduUserPythonCourseCriteria;
import com.stepiot.model.EduUserPythonCourseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserPythonCourseMapper {
    long countByExample(EduUserPythonCourseCriteria example);

    int deleteByExample(EduUserPythonCourseCriteria example);

    int deleteByPrimaryKey(Integer userCourseId);

    int insert(EduUserPythonCourseWithBLOBs record);

    int insertSelective(EduUserPythonCourseWithBLOBs record);

    List<EduUserPythonCourseWithBLOBs> selectByExampleWithBLOBs(EduUserPythonCourseCriteria example);

    List<EduUserPythonCourse> selectByExample(EduUserPythonCourseCriteria example);

    EduUserPythonCourseWithBLOBs selectByPrimaryKey(Integer userCourseId);

    int updateByExampleSelective(@Param("record") EduUserPythonCourseWithBLOBs record, @Param("example") EduUserPythonCourseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserPythonCourseWithBLOBs record, @Param("example") EduUserPythonCourseCriteria example);

    int updateByExample(@Param("record") EduUserPythonCourse record, @Param("example") EduUserPythonCourseCriteria example);

    int updateByPrimaryKeySelective(EduUserPythonCourseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserPythonCourseWithBLOBs record);

    int updateByPrimaryKey(EduUserPythonCourse record);

    EduUserPythonCourse selectByExampleForOne(EduUserPythonCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer userCourseId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserPythonCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserPythonCourseCriteria example);
}