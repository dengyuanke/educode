package com.stepiot.dao;

import com.stepiot.model.EduPythonCourseOutlineAnswer;
import com.stepiot.model.EduPythonCourseOutlineAnswerCriteria;
import com.stepiot.model.EduPythonCourseOutlineAnswerWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduPythonCourseOutlineAnswerMapper {
    long countByExample(EduPythonCourseOutlineAnswerCriteria example);

    int deleteByExample(EduPythonCourseOutlineAnswerCriteria example);

    int deleteByPrimaryKey(Integer answerId);

    int insert(EduPythonCourseOutlineAnswerWithBLOBs record);

    int insertSelective(EduPythonCourseOutlineAnswerWithBLOBs record);

    List<EduPythonCourseOutlineAnswerWithBLOBs> selectByExampleWithBLOBs(EduPythonCourseOutlineAnswerCriteria example);

    List<EduPythonCourseOutlineAnswer> selectByExample(EduPythonCourseOutlineAnswerCriteria example);

    EduPythonCourseOutlineAnswerWithBLOBs selectByPrimaryKey(Integer answerId);

    int updateByExampleSelective(@Param("record") EduPythonCourseOutlineAnswerWithBLOBs record, @Param("example") EduPythonCourseOutlineAnswerCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduPythonCourseOutlineAnswerWithBLOBs record, @Param("example") EduPythonCourseOutlineAnswerCriteria example);

    int updateByExample(@Param("record") EduPythonCourseOutlineAnswer record, @Param("example") EduPythonCourseOutlineAnswerCriteria example);

    int updateByPrimaryKeySelective(EduPythonCourseOutlineAnswerWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduPythonCourseOutlineAnswerWithBLOBs record);

    int updateByPrimaryKey(EduPythonCourseOutlineAnswer record);

    EduPythonCourseOutlineAnswer selectByExampleForOne(EduPythonCourseOutlineAnswerCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer answerId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduPythonCourseOutlineAnswerCriteria example);

    List<Map<String, Object>> selectMapByExample(EduPythonCourseOutlineAnswerCriteria example);
}