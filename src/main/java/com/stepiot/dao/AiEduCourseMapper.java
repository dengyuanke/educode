package com.stepiot.dao;

import com.stepiot.model.AiEduCourse;
import com.stepiot.model.AiEduCourseCriteria;
import com.stepiot.model.AiEduCourseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface AiEduCourseMapper {
    long countByExample(AiEduCourseCriteria example);

    int deleteByExample(AiEduCourseCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(AiEduCourseWithBLOBs record);

    int insertSelective(AiEduCourseWithBLOBs record);

    List<AiEduCourseWithBLOBs> selectByExampleWithBLOBs(AiEduCourseCriteria example);

    List<AiEduCourse> selectByExample(AiEduCourseCriteria example);

    AiEduCourseWithBLOBs selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") AiEduCourseWithBLOBs record, @Param("example") AiEduCourseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") AiEduCourseWithBLOBs record, @Param("example") AiEduCourseCriteria example);

    int updateByExample(@Param("record") AiEduCourse record, @Param("example") AiEduCourseCriteria example);

    int updateByPrimaryKeySelective(AiEduCourseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(AiEduCourseWithBLOBs record);

    int updateByPrimaryKey(AiEduCourse record);

    AiEduCourse selectByExampleForOne(AiEduCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(AiEduCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(AiEduCourseCriteria example);
}