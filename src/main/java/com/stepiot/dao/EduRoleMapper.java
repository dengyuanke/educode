package com.stepiot.dao;

import com.stepiot.model.EduRole;
import com.stepiot.model.EduRoleCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduRoleMapper {
    long countByExample(EduRoleCriteria example);

    int deleteByExample(EduRoleCriteria example);

    int deleteByPrimaryKey(String role);

    int insert(EduRole record);

    int insertSelective(EduRole record);

    List<EduRole> selectByExample(EduRoleCriteria example);

    EduRole selectByPrimaryKey(String role);

    int updateByExampleSelective(@Param("record") EduRole record, @Param("example") EduRoleCriteria example);

    int updateByExample(@Param("record") EduRole record, @Param("example") EduRoleCriteria example);

    int updateByPrimaryKeySelective(EduRole record);

    int updateByPrimaryKey(EduRole record);

    EduRole selectByExampleForOne(EduRoleCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String role);

    java.util.Map<String, Object> selectMapByExampleForOne(EduRoleCriteria example);

    List<Map<String, Object>> selectMapByExample(EduRoleCriteria example);
}