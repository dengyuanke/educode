package com.stepiot.dao;

import com.stepiot.model.MicrobitUserCourse;
import com.stepiot.model.MicrobitUserCourseCriteria;
import com.stepiot.model.MicrobitUserCourseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface MicrobitUserCourseMapper {
    long countByExample(MicrobitUserCourseCriteria example);

    int deleteByExample(MicrobitUserCourseCriteria example);

    int deleteByPrimaryKey(String courseUid);

    int insert(MicrobitUserCourseWithBLOBs record);

    int insertSelective(MicrobitUserCourseWithBLOBs record);

    List<MicrobitUserCourseWithBLOBs> selectByExampleWithBLOBs(MicrobitUserCourseCriteria example);

    List<MicrobitUserCourse> selectByExample(MicrobitUserCourseCriteria example);

    MicrobitUserCourseWithBLOBs selectByPrimaryKey(String courseUid);

    int updateByExampleSelective(@Param("record") MicrobitUserCourseWithBLOBs record, @Param("example") MicrobitUserCourseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") MicrobitUserCourseWithBLOBs record, @Param("example") MicrobitUserCourseCriteria example);

    int updateByExample(@Param("record") MicrobitUserCourse record, @Param("example") MicrobitUserCourseCriteria example);

    int updateByPrimaryKeySelective(MicrobitUserCourseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(MicrobitUserCourseWithBLOBs record);

    int updateByPrimaryKey(MicrobitUserCourse record);

    MicrobitUserCourse selectByExampleForOne(MicrobitUserCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String courseUid);

    java.util.Map<String, Object> selectMapByExampleForOne(MicrobitUserCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(MicrobitUserCourseCriteria example);
}