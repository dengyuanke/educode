package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;
import com.stepiot.beans.EduUserCourseProgressBean;

public interface EduUserPythonCourseMapperExt {
  List<Map<String, Object>> listMyCourse(Map<String, Object> map);

  Map<String, Object> selectByUserCourseId(Map<String, Object> map);

  List<EduUserCourseProgressBean> listMyCourseProgress(Map<String, Object> map);


}
