package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;

public interface EduStudentMapperExt {

  List<Map<String, Object>> listStudentsByMap(Map<String, Object> map);

  List<Map<String, Object>> listAllStudentsByMap(Map<String, Object> map);

  long countStudents(Map<String, Object> map);

  long countAllStudents(Map<String, Object> map);

  List<Map<String, Object>> selectStudentsById(Map<String, Object> map);

  List<Map<String, Object>> selectClassInfoById(Map<String, Object> map);



}
