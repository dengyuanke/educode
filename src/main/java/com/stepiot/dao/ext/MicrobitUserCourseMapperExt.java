package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;

public interface MicrobitUserCourseMapperExt {
  List<Map<String, Object>> listMyCourse(Map<String, Object> map);
}
