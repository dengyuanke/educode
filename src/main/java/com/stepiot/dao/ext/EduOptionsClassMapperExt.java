package com.stepiot.dao.ext;

import java.util.Map;

public interface EduOptionsClassMapperExt {

  public Map<String, Object> loadClassOptionsByStudentId(Map<String, Object> param);

}
