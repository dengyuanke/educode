package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;
import com.stepiot.beans.EduTeacherQueryBean;

public interface EduTeacherClassRefMapperExt {

  public List<EduTeacherQueryBean> listAllTeachers(Map<String, Object> map);
}
