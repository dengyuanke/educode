package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;
import com.stepiot.beans.EduNoticeBean;

public interface EduNoticeMapperExt {

  List<EduNoticeBean> listMyNotice(Map<String, Object> map);

  List<EduNoticeBean> listSentNotice(Map<String, Object> map);

  List<EduNoticeBean> listUnReadedNews(Map<String, Object> map);

}
