package com.stepiot.dao.ext;

import java.util.List;
import java.util.Map;
import com.stepiot.beans.AssignmentDataBean;
import com.stepiot.beans.EduChartDataBean;
import com.stepiot.beans.EduClassCourseProgressBean;
import com.stepiot.beans.EduClassScoreBean;

public interface EduClassMapperExt {
  List<Map<String, Object>> listClasses(Map<String, Object> map);

  List<Map<String, Object>> listMyClasses(Map<String, Object> map);

  List<Map<String, Object>> feedDepSelector(Map<String, Object> map);

  List<EduClassScoreBean> listClassScores(Map<String, Object> map);

  List<EduChartDataBean> chartClassCode(Map<String, Object> map);

  List<AssignmentDataBean> queryAssignment(Map<String, Object> map);

  List<EduClassCourseProgressBean> listClassCourseProgress(Map<String, Object> map);


}
