package com.stepiot.dao;

import com.stepiot.model.EduUser;
import com.stepiot.model.EduUserCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserMapper {
    long countByExample(EduUserCriteria example);

    int deleteByExample(EduUserCriteria example);

    int deleteByPrimaryKey(String username);

    int insert(EduUser record);

    int insertSelective(EduUser record);

    List<EduUser> selectByExample(EduUserCriteria example);

    EduUser selectByPrimaryKey(String username);

    int updateByExampleSelective(@Param("record") EduUser record, @Param("example") EduUserCriteria example);

    int updateByExample(@Param("record") EduUser record, @Param("example") EduUserCriteria example);

    int updateByPrimaryKeySelective(EduUser record);

    int updateByPrimaryKey(EduUser record);

    EduUser selectByExampleForOne(EduUserCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String username);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserCriteria example);
}