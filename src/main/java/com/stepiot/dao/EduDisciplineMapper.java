package com.stepiot.dao;

import com.stepiot.model.EduDiscipline;
import com.stepiot.model.EduDisciplineCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduDisciplineMapper {
    long countByExample(EduDisciplineCriteria example);

    int deleteByExample(EduDisciplineCriteria example);

    int deleteByPrimaryKey(String disciplineId);

    int insert(EduDiscipline record);

    int insertSelective(EduDiscipline record);

    List<EduDiscipline> selectByExample(EduDisciplineCriteria example);

    EduDiscipline selectByPrimaryKey(String disciplineId);

    int updateByExampleSelective(@Param("record") EduDiscipline record, @Param("example") EduDisciplineCriteria example);

    int updateByExample(@Param("record") EduDiscipline record, @Param("example") EduDisciplineCriteria example);

    int updateByPrimaryKeySelective(EduDiscipline record);

    int updateByPrimaryKey(EduDiscipline record);

    EduDiscipline selectByExampleForOne(EduDisciplineCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String disciplineId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduDisciplineCriteria example);

    List<Map<String, Object>> selectMapByExample(EduDisciplineCriteria example);
}