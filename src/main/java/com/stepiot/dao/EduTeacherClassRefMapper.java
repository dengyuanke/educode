package com.stepiot.dao;

import com.stepiot.model.EduTeacherClassRef;
import com.stepiot.model.EduTeacherClassRefCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduTeacherClassRefMapper {
    long countByExample(EduTeacherClassRefCriteria example);

    int deleteByExample(EduTeacherClassRefCriteria example);

    int deleteByPrimaryKey(Integer refId);

    int insert(EduTeacherClassRef record);

    int insertSelective(EduTeacherClassRef record);

    List<EduTeacherClassRef> selectByExample(EduTeacherClassRefCriteria example);

    EduTeacherClassRef selectByPrimaryKey(Integer refId);

    int updateByExampleSelective(@Param("record") EduTeacherClassRef record, @Param("example") EduTeacherClassRefCriteria example);

    int updateByExample(@Param("record") EduTeacherClassRef record, @Param("example") EduTeacherClassRefCriteria example);

    int updateByPrimaryKeySelective(EduTeacherClassRef record);

    int updateByPrimaryKey(EduTeacherClassRef record);

    EduTeacherClassRef selectByExampleForOne(EduTeacherClassRefCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer refId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduTeacherClassRefCriteria example);

    List<Map<String, Object>> selectMapByExample(EduTeacherClassRefCriteria example);
}