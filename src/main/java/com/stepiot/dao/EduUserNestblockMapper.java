package com.stepiot.dao;

import com.stepiot.model.EduUserNestblock;
import com.stepiot.model.EduUserNestblockCriteria;
import com.stepiot.model.EduUserNestblockWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserNestblockMapper {
    long countByExample(EduUserNestblockCriteria example);

    int deleteByExample(EduUserNestblockCriteria example);

    int deleteByPrimaryKey(String blocklyId);

    int insert(EduUserNestblockWithBLOBs record);

    int insertSelective(EduUserNestblockWithBLOBs record);

    List<EduUserNestblockWithBLOBs> selectByExampleWithBLOBs(EduUserNestblockCriteria example);

    List<EduUserNestblock> selectByExample(EduUserNestblockCriteria example);

    EduUserNestblockWithBLOBs selectByPrimaryKey(String blocklyId);

    int updateByExampleSelective(@Param("record") EduUserNestblockWithBLOBs record, @Param("example") EduUserNestblockCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserNestblockWithBLOBs record, @Param("example") EduUserNestblockCriteria example);

    int updateByExample(@Param("record") EduUserNestblock record, @Param("example") EduUserNestblockCriteria example);

    int updateByPrimaryKeySelective(EduUserNestblockWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserNestblockWithBLOBs record);

    int updateByPrimaryKey(EduUserNestblock record);

    EduUserNestblock selectByExampleForOne(EduUserNestblockCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String blocklyId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserNestblockCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserNestblockCriteria example);
}