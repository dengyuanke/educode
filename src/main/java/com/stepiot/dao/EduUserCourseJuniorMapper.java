package com.stepiot.dao;

import com.stepiot.model.EduUserCourseJunior;
import com.stepiot.model.EduUserCourseJuniorCriteria;
import com.stepiot.model.EduUserCourseJuniorWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserCourseJuniorMapper {
    long countByExample(EduUserCourseJuniorCriteria example);

    int deleteByExample(EduUserCourseJuniorCriteria example);

    int deleteByPrimaryKey(String courseUid);

    int insert(EduUserCourseJuniorWithBLOBs record);

    int insertSelective(EduUserCourseJuniorWithBLOBs record);

    List<EduUserCourseJuniorWithBLOBs> selectByExampleWithBLOBs(EduUserCourseJuniorCriteria example);

    List<EduUserCourseJunior> selectByExample(EduUserCourseJuniorCriteria example);

    EduUserCourseJuniorWithBLOBs selectByPrimaryKey(String courseUid);

    int updateByExampleSelective(@Param("record") EduUserCourseJuniorWithBLOBs record, @Param("example") EduUserCourseJuniorCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserCourseJuniorWithBLOBs record, @Param("example") EduUserCourseJuniorCriteria example);

    int updateByExample(@Param("record") EduUserCourseJunior record, @Param("example") EduUserCourseJuniorCriteria example);

    int updateByPrimaryKeySelective(EduUserCourseJuniorWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserCourseJuniorWithBLOBs record);

    int updateByPrimaryKey(EduUserCourseJunior record);

    EduUserCourseJunior selectByExampleForOne(EduUserCourseJuniorCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String courseUid);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserCourseJuniorCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserCourseJuniorCriteria example);
}