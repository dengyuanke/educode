package com.stepiot.dao;

import com.stepiot.model.EduWiki;
import com.stepiot.model.EduWikiCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduWikiMapper {
    long countByExample(EduWikiCriteria example);

    int deleteByExample(EduWikiCriteria example);

    int deleteByPrimaryKey(Integer wikiId);

    int insert(EduWiki record);

    int insertSelective(EduWiki record);

    List<EduWiki> selectByExampleWithBLOBs(EduWikiCriteria example);

    List<EduWiki> selectByExample(EduWikiCriteria example);

    EduWiki selectByPrimaryKey(Integer wikiId);

    int updateByExampleSelective(@Param("record") EduWiki record, @Param("example") EduWikiCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduWiki record, @Param("example") EduWikiCriteria example);

    int updateByExample(@Param("record") EduWiki record, @Param("example") EduWikiCriteria example);

    int updateByPrimaryKeySelective(EduWiki record);

    int updateByPrimaryKeyWithBLOBs(EduWiki record);

    int updateByPrimaryKey(EduWiki record);

    EduWiki selectByExampleForOne(EduWikiCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer wikiId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduWikiCriteria example);

    List<Map<String, Object>> selectMapByExample(EduWikiCriteria example);
}