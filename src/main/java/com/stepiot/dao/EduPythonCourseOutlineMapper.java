package com.stepiot.dao;

import com.stepiot.model.EduPythonCourseOutline;
import com.stepiot.model.EduPythonCourseOutlineCriteria;
import com.stepiot.model.EduPythonCourseOutlineWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduPythonCourseOutlineMapper {
    long countByExample(EduPythonCourseOutlineCriteria example);

    int deleteByExample(EduPythonCourseOutlineCriteria example);

    int deleteByPrimaryKey(Integer outlineId);

    int insert(EduPythonCourseOutlineWithBLOBs record);

    int insertSelective(EduPythonCourseOutlineWithBLOBs record);

    List<EduPythonCourseOutlineWithBLOBs> selectByExampleWithBLOBs(EduPythonCourseOutlineCriteria example);

    List<EduPythonCourseOutline> selectByExample(EduPythonCourseOutlineCriteria example);

    EduPythonCourseOutlineWithBLOBs selectByPrimaryKey(Integer outlineId);

    int updateByExampleSelective(@Param("record") EduPythonCourseOutlineWithBLOBs record, @Param("example") EduPythonCourseOutlineCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduPythonCourseOutlineWithBLOBs record, @Param("example") EduPythonCourseOutlineCriteria example);

    int updateByExample(@Param("record") EduPythonCourseOutline record, @Param("example") EduPythonCourseOutlineCriteria example);

    int updateByPrimaryKeySelective(EduPythonCourseOutlineWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduPythonCourseOutlineWithBLOBs record);

    int updateByPrimaryKey(EduPythonCourseOutline record);

    EduPythonCourseOutline selectByExampleForOne(EduPythonCourseOutlineCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer outlineId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduPythonCourseOutlineCriteria example);

    List<Map<String, Object>> selectMapByExample(EduPythonCourseOutlineCriteria example);
}