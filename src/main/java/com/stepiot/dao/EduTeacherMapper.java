package com.stepiot.dao;

import com.stepiot.model.EduTeacher;
import com.stepiot.model.EduTeacherCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduTeacherMapper {
    long countByExample(EduTeacherCriteria example);

    int deleteByExample(EduTeacherCriteria example);

    int deleteByPrimaryKey(Integer teacherId);

    int insert(EduTeacher record);

    int insertSelective(EduTeacher record);

    List<EduTeacher> selectByExample(EduTeacherCriteria example);

    EduTeacher selectByPrimaryKey(Integer teacherId);

    int updateByExampleSelective(@Param("record") EduTeacher record, @Param("example") EduTeacherCriteria example);

    int updateByExample(@Param("record") EduTeacher record, @Param("example") EduTeacherCriteria example);

    int updateByPrimaryKeySelective(EduTeacher record);

    int updateByPrimaryKey(EduTeacher record);

    EduTeacher selectByExampleForOne(EduTeacherCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer teacherId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduTeacherCriteria example);

    List<Map<String, Object>> selectMapByExample(EduTeacherCriteria example);
}