package com.stepiot.dao;

import com.stepiot.model.EduCourseJunior;
import com.stepiot.model.EduCourseJuniorCriteria;
import com.stepiot.model.EduCourseJuniorWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduCourseJuniorMapper {
    long countByExample(EduCourseJuniorCriteria example);

    int deleteByExample(EduCourseJuniorCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(EduCourseJuniorWithBLOBs record);

    int insertSelective(EduCourseJuniorWithBLOBs record);

    List<EduCourseJuniorWithBLOBs> selectByExampleWithBLOBs(EduCourseJuniorCriteria example);

    List<EduCourseJunior> selectByExample(EduCourseJuniorCriteria example);

    EduCourseJuniorWithBLOBs selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") EduCourseJuniorWithBLOBs record, @Param("example") EduCourseJuniorCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduCourseJuniorWithBLOBs record, @Param("example") EduCourseJuniorCriteria example);

    int updateByExample(@Param("record") EduCourseJunior record, @Param("example") EduCourseJuniorCriteria example);

    int updateByPrimaryKeySelective(EduCourseJuniorWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduCourseJuniorWithBLOBs record);

    int updateByPrimaryKey(EduCourseJunior record);

    EduCourseJunior selectByExampleForOne(EduCourseJuniorCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduCourseJuniorCriteria example);

    List<Map<String, Object>> selectMapByExample(EduCourseJuniorCriteria example);
}