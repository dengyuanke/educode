package com.stepiot.dao;

import com.stepiot.model.EduNoticeRecipient;
import com.stepiot.model.EduNoticeRecipientCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduNoticeRecipientMapper {
    long countByExample(EduNoticeRecipientCriteria example);

    int deleteByExample(EduNoticeRecipientCriteria example);

    int deleteByPrimaryKey(Integer recipientId);

    int insert(EduNoticeRecipient record);

    int insertSelective(EduNoticeRecipient record);

    List<EduNoticeRecipient> selectByExample(EduNoticeRecipientCriteria example);

    EduNoticeRecipient selectByPrimaryKey(Integer recipientId);

    int updateByExampleSelective(@Param("record") EduNoticeRecipient record, @Param("example") EduNoticeRecipientCriteria example);

    int updateByExample(@Param("record") EduNoticeRecipient record, @Param("example") EduNoticeRecipientCriteria example);

    int updateByPrimaryKeySelective(EduNoticeRecipient record);

    int updateByPrimaryKey(EduNoticeRecipient record);

    EduNoticeRecipient selectByExampleForOne(EduNoticeRecipientCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer recipientId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduNoticeRecipientCriteria example);

    List<Map<String, Object>> selectMapByExample(EduNoticeRecipientCriteria example);
}