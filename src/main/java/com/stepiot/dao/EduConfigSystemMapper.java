package com.stepiot.dao;

import com.stepiot.model.EduConfigSystem;
import com.stepiot.model.EduConfigSystemCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduConfigSystemMapper {
    long countByExample(EduConfigSystemCriteria example);

    int deleteByExample(EduConfigSystemCriteria example);

    int deleteByPrimaryKey(String configItem);

    int insert(EduConfigSystem record);

    int insertSelective(EduConfigSystem record);

    List<EduConfigSystem> selectByExample(EduConfigSystemCriteria example);

    EduConfigSystem selectByPrimaryKey(String configItem);

    int updateByExampleSelective(@Param("record") EduConfigSystem record, @Param("example") EduConfigSystemCriteria example);

    int updateByExample(@Param("record") EduConfigSystem record, @Param("example") EduConfigSystemCriteria example);

    int updateByPrimaryKeySelective(EduConfigSystem record);

    int updateByPrimaryKey(EduConfigSystem record);

    EduConfigSystem selectByExampleForOne(EduConfigSystemCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String configItem);

    java.util.Map<String, Object> selectMapByExampleForOne(EduConfigSystemCriteria example);

    List<Map<String, Object>> selectMapByExample(EduConfigSystemCriteria example);
}