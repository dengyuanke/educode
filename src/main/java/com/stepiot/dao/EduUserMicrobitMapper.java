package com.stepiot.dao;

import com.stepiot.model.EduUserMicrobit;
import com.stepiot.model.EduUserMicrobitCriteria;
import com.stepiot.model.EduUserMicrobitWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserMicrobitMapper {
    long countByExample(EduUserMicrobitCriteria example);

    int deleteByExample(EduUserMicrobitCriteria example);

    int deleteByPrimaryKey(String blocklyId);

    int insert(EduUserMicrobitWithBLOBs record);

    int insertSelective(EduUserMicrobitWithBLOBs record);

    List<EduUserMicrobitWithBLOBs> selectByExampleWithBLOBs(EduUserMicrobitCriteria example);

    List<EduUserMicrobit> selectByExample(EduUserMicrobitCriteria example);

    EduUserMicrobitWithBLOBs selectByPrimaryKey(String blocklyId);

    int updateByExampleSelective(@Param("record") EduUserMicrobitWithBLOBs record, @Param("example") EduUserMicrobitCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserMicrobitWithBLOBs record, @Param("example") EduUserMicrobitCriteria example);

    int updateByExample(@Param("record") EduUserMicrobit record, @Param("example") EduUserMicrobitCriteria example);

    int updateByPrimaryKeySelective(EduUserMicrobitWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserMicrobitWithBLOBs record);

    int updateByPrimaryKey(EduUserMicrobit record);

    EduUserMicrobit selectByExampleForOne(EduUserMicrobitCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String blocklyId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserMicrobitCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserMicrobitCriteria example);
}