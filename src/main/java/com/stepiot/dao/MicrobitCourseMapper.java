package com.stepiot.dao;

import com.stepiot.model.MicrobitCourse;
import com.stepiot.model.MicrobitCourseCriteria;
import com.stepiot.model.MicrobitCourseWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface MicrobitCourseMapper {
    long countByExample(MicrobitCourseCriteria example);

    int deleteByExample(MicrobitCourseCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(MicrobitCourseWithBLOBs record);

    int insertSelective(MicrobitCourseWithBLOBs record);

    List<MicrobitCourseWithBLOBs> selectByExampleWithBLOBs(MicrobitCourseCriteria example);

    List<MicrobitCourse> selectByExample(MicrobitCourseCriteria example);

    MicrobitCourseWithBLOBs selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") MicrobitCourseWithBLOBs record, @Param("example") MicrobitCourseCriteria example);

    int updateByExampleWithBLOBs(@Param("record") MicrobitCourseWithBLOBs record, @Param("example") MicrobitCourseCriteria example);

    int updateByExample(@Param("record") MicrobitCourse record, @Param("example") MicrobitCourseCriteria example);

    int updateByPrimaryKeySelective(MicrobitCourseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(MicrobitCourseWithBLOBs record);

    int updateByPrimaryKey(MicrobitCourse record);

    MicrobitCourse selectByExampleForOne(MicrobitCourseCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(MicrobitCourseCriteria example);

    List<Map<String, Object>> selectMapByExample(MicrobitCourseCriteria example);
}