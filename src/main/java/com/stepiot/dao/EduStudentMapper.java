package com.stepiot.dao;

import com.stepiot.model.EduStudent;
import com.stepiot.model.EduStudentCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduStudentMapper {
    long countByExample(EduStudentCriteria example);

    int deleteByExample(EduStudentCriteria example);

    int deleteByPrimaryKey(Integer studentId);

    int insert(EduStudent record);

    int insertSelective(EduStudent record);

    List<EduStudent> selectByExample(EduStudentCriteria example);

    EduStudent selectByPrimaryKey(Integer studentId);

    int updateByExampleSelective(@Param("record") EduStudent record, @Param("example") EduStudentCriteria example);

    int updateByExample(@Param("record") EduStudent record, @Param("example") EduStudentCriteria example);

    int updateByPrimaryKeySelective(EduStudent record);

    int updateByPrimaryKey(EduStudent record);

    EduStudent selectByExampleForOne(EduStudentCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer studentId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduStudentCriteria example);

    List<Map<String, Object>> selectMapByExample(EduStudentCriteria example);
}