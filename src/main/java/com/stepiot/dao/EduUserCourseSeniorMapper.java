package com.stepiot.dao;

import com.stepiot.model.EduUserCourseSenior;
import com.stepiot.model.EduUserCourseSeniorCriteria;
import com.stepiot.model.EduUserCourseSeniorWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserCourseSeniorMapper {
    long countByExample(EduUserCourseSeniorCriteria example);

    int deleteByExample(EduUserCourseSeniorCriteria example);

    int deleteByPrimaryKey(String courseUid);

    int insert(EduUserCourseSeniorWithBLOBs record);

    int insertSelective(EduUserCourseSeniorWithBLOBs record);

    List<EduUserCourseSeniorWithBLOBs> selectByExampleWithBLOBs(EduUserCourseSeniorCriteria example);

    List<EduUserCourseSenior> selectByExample(EduUserCourseSeniorCriteria example);

    EduUserCourseSeniorWithBLOBs selectByPrimaryKey(String courseUid);

    int updateByExampleSelective(@Param("record") EduUserCourseSeniorWithBLOBs record, @Param("example") EduUserCourseSeniorCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserCourseSeniorWithBLOBs record, @Param("example") EduUserCourseSeniorCriteria example);

    int updateByExample(@Param("record") EduUserCourseSenior record, @Param("example") EduUserCourseSeniorCriteria example);

    int updateByPrimaryKeySelective(EduUserCourseSeniorWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserCourseSeniorWithBLOBs record);

    int updateByPrimaryKey(EduUserCourseSenior record);

    EduUserCourseSenior selectByExampleForOne(EduUserCourseSeniorCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String courseUid);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserCourseSeniorCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserCourseSeniorCriteria example);
}