package com.stepiot.dao;

import com.stepiot.model.EduPubDown;
import com.stepiot.model.EduPubDownCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduPubDownMapper {
    long countByExample(EduPubDownCriteria example);

    int deleteByExample(EduPubDownCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(EduPubDown record);

    int insertSelective(EduPubDown record);

    List<EduPubDown> selectByExample(EduPubDownCriteria example);

    EduPubDown selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EduPubDown record, @Param("example") EduPubDownCriteria example);

    int updateByExample(@Param("record") EduPubDown record, @Param("example") EduPubDownCriteria example);

    int updateByPrimaryKeySelective(EduPubDown record);

    int updateByPrimaryKey(EduPubDown record);

    EduPubDown selectByExampleForOne(EduPubDownCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer id);

    java.util.Map<String, Object> selectMapByExampleForOne(EduPubDownCriteria example);

    List<Map<String, Object>> selectMapByExample(EduPubDownCriteria example);
}