package com.stepiot.dao;

import com.stepiot.model.EduCourseSenior;
import com.stepiot.model.EduCourseSeniorCriteria;
import com.stepiot.model.EduCourseSeniorWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduCourseSeniorMapper {
    long countByExample(EduCourseSeniorCriteria example);

    int deleteByExample(EduCourseSeniorCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(EduCourseSeniorWithBLOBs record);

    int insertSelective(EduCourseSeniorWithBLOBs record);

    List<EduCourseSeniorWithBLOBs> selectByExampleWithBLOBs(EduCourseSeniorCriteria example);

    List<EduCourseSenior> selectByExample(EduCourseSeniorCriteria example);

    EduCourseSeniorWithBLOBs selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") EduCourseSeniorWithBLOBs record, @Param("example") EduCourseSeniorCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduCourseSeniorWithBLOBs record, @Param("example") EduCourseSeniorCriteria example);

    int updateByExample(@Param("record") EduCourseSenior record, @Param("example") EduCourseSeniorCriteria example);

    int updateByPrimaryKeySelective(EduCourseSeniorWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduCourseSeniorWithBLOBs record);

    int updateByPrimaryKey(EduCourseSenior record);

    EduCourseSenior selectByExampleForOne(EduCourseSeniorCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduCourseSeniorCriteria example);

    List<Map<String, Object>> selectMapByExample(EduCourseSeniorCriteria example);
}