package com.stepiot.dao;

import com.stepiot.model.EduUserMedia;
import com.stepiot.model.EduUserMediaCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserMediaMapper {
    long countByExample(EduUserMediaCriteria example);

    int deleteByExample(EduUserMediaCriteria example);

    int deleteByPrimaryKey(String mediaUid);

    int insert(EduUserMedia record);

    int insertSelective(EduUserMedia record);

    List<EduUserMedia> selectByExample(EduUserMediaCriteria example);

    EduUserMedia selectByPrimaryKey(String mediaUid);

    int updateByExampleSelective(@Param("record") EduUserMedia record, @Param("example") EduUserMediaCriteria example);

    int updateByExample(@Param("record") EduUserMedia record, @Param("example") EduUserMediaCriteria example);

    int updateByPrimaryKeySelective(EduUserMedia record);

    int updateByPrimaryKey(EduUserMedia record);

    EduUserMedia selectByExampleForOne(EduUserMediaCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String mediaUid);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserMediaCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserMediaCriteria example);
}