package com.stepiot.dao;

import com.stepiot.model.EduLogLogin;
import com.stepiot.model.EduLogLoginCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduLogLoginMapper {
    long countByExample(EduLogLoginCriteria example);

    int deleteByExample(EduLogLoginCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(EduLogLogin record);

    int insertSelective(EduLogLogin record);

    List<EduLogLogin> selectByExample(EduLogLoginCriteria example);

    EduLogLogin selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EduLogLogin record, @Param("example") EduLogLoginCriteria example);

    int updateByExample(@Param("record") EduLogLogin record, @Param("example") EduLogLoginCriteria example);

    int updateByPrimaryKeySelective(EduLogLogin record);

    int updateByPrimaryKey(EduLogLogin record);

    EduLogLogin selectByExampleForOne(EduLogLoginCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer id);

    java.util.Map<String, Object> selectMapByExampleForOne(EduLogLoginCriteria example);

    List<Map<String, Object>> selectMapByExample(EduLogLoginCriteria example);
}