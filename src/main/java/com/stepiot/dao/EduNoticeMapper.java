package com.stepiot.dao;

import com.stepiot.model.EduNotice;
import com.stepiot.model.EduNoticeCriteria;
import com.stepiot.model.EduNoticeWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduNoticeMapper {
    long countByExample(EduNoticeCriteria example);

    int deleteByExample(EduNoticeCriteria example);

    int deleteByPrimaryKey(Integer noticeId);

    int insert(EduNoticeWithBLOBs record);

    int insertSelective(EduNoticeWithBLOBs record);

    List<EduNoticeWithBLOBs> selectByExampleWithBLOBs(EduNoticeCriteria example);

    List<EduNotice> selectByExample(EduNoticeCriteria example);

    EduNoticeWithBLOBs selectByPrimaryKey(Integer noticeId);

    int updateByExampleSelective(@Param("record") EduNoticeWithBLOBs record, @Param("example") EduNoticeCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduNoticeWithBLOBs record, @Param("example") EduNoticeCriteria example);

    int updateByExample(@Param("record") EduNotice record, @Param("example") EduNoticeCriteria example);

    int updateByPrimaryKeySelective(EduNoticeWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduNoticeWithBLOBs record);

    int updateByPrimaryKey(EduNotice record);

    EduNotice selectByExampleForOne(EduNoticeCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer noticeId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduNoticeCriteria example);

    List<Map<String, Object>> selectMapByExample(EduNoticeCriteria example);
}