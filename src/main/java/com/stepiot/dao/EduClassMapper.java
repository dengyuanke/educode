package com.stepiot.dao;

import com.stepiot.model.EduClass;
import com.stepiot.model.EduClassCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduClassMapper {
    long countByExample(EduClassCriteria example);

    int deleteByExample(EduClassCriteria example);

    int deleteByPrimaryKey(String classId);

    int insert(EduClass record);

    int insertSelective(EduClass record);

    List<EduClass> selectByExample(EduClassCriteria example);

    EduClass selectByPrimaryKey(String classId);

    int updateByExampleSelective(@Param("record") EduClass record, @Param("example") EduClassCriteria example);

    int updateByExample(@Param("record") EduClass record, @Param("example") EduClassCriteria example);

    int updateByPrimaryKeySelective(EduClass record);

    int updateByPrimaryKey(EduClass record);

    EduClass selectByExampleForOne(EduClassCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String classId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduClassCriteria example);

    List<Map<String, Object>> selectMapByExample(EduClassCriteria example);
}