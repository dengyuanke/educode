package com.stepiot.dao;

import com.stepiot.model.EduCoursePrimary;
import com.stepiot.model.EduCoursePrimaryCriteria;
import com.stepiot.model.EduCoursePrimaryWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduCoursePrimaryMapper {
    long countByExample(EduCoursePrimaryCriteria example);

    int deleteByExample(EduCoursePrimaryCriteria example);

    int deleteByPrimaryKey(Integer courseId);

    int insert(EduCoursePrimaryWithBLOBs record);

    int insertSelective(EduCoursePrimaryWithBLOBs record);

    List<EduCoursePrimaryWithBLOBs> selectByExampleWithBLOBs(EduCoursePrimaryCriteria example);

    List<EduCoursePrimary> selectByExample(EduCoursePrimaryCriteria example);

    EduCoursePrimaryWithBLOBs selectByPrimaryKey(Integer courseId);

    int updateByExampleSelective(@Param("record") EduCoursePrimaryWithBLOBs record, @Param("example") EduCoursePrimaryCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduCoursePrimaryWithBLOBs record, @Param("example") EduCoursePrimaryCriteria example);

    int updateByExample(@Param("record") EduCoursePrimary record, @Param("example") EduCoursePrimaryCriteria example);

    int updateByPrimaryKeySelective(EduCoursePrimaryWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduCoursePrimaryWithBLOBs record);

    int updateByPrimaryKey(EduCoursePrimary record);

    EduCoursePrimary selectByExampleForOne(EduCoursePrimaryCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer courseId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduCoursePrimaryCriteria example);

    List<Map<String, Object>> selectMapByExample(EduCoursePrimaryCriteria example);
}