package com.stepiot.dao;

import com.stepiot.model.EduUserCoursePrimary;
import com.stepiot.model.EduUserCoursePrimaryCriteria;
import com.stepiot.model.EduUserCoursePrimaryWithBLOBs;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserCoursePrimaryMapper {
    long countByExample(EduUserCoursePrimaryCriteria example);

    int deleteByExample(EduUserCoursePrimaryCriteria example);

    int deleteByPrimaryKey(String courseUid);

    int insert(EduUserCoursePrimaryWithBLOBs record);

    int insertSelective(EduUserCoursePrimaryWithBLOBs record);

    List<EduUserCoursePrimaryWithBLOBs> selectByExampleWithBLOBs(EduUserCoursePrimaryCriteria example);

    List<EduUserCoursePrimary> selectByExample(EduUserCoursePrimaryCriteria example);

    EduUserCoursePrimaryWithBLOBs selectByPrimaryKey(String courseUid);

    int updateByExampleSelective(@Param("record") EduUserCoursePrimaryWithBLOBs record, @Param("example") EduUserCoursePrimaryCriteria example);

    int updateByExampleWithBLOBs(@Param("record") EduUserCoursePrimaryWithBLOBs record, @Param("example") EduUserCoursePrimaryCriteria example);

    int updateByExample(@Param("record") EduUserCoursePrimary record, @Param("example") EduUserCoursePrimaryCriteria example);

    int updateByPrimaryKeySelective(EduUserCoursePrimaryWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EduUserCoursePrimaryWithBLOBs record);

    int updateByPrimaryKey(EduUserCoursePrimary record);

    EduUserCoursePrimary selectByExampleForOne(EduUserCoursePrimaryCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(String courseUid);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserCoursePrimaryCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserCoursePrimaryCriteria example);
}