package com.stepiot.dao;

import com.stepiot.model.EduTeacherCourseRef;
import com.stepiot.model.EduTeacherCourseRefCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduTeacherCourseRefMapper {
    long countByExample(EduTeacherCourseRefCriteria example);

    int deleteByExample(EduTeacherCourseRefCriteria example);

    int deleteByPrimaryKey(Integer refId);

    int insert(EduTeacherCourseRef record);

    int insertSelective(EduTeacherCourseRef record);

    List<EduTeacherCourseRef> selectByExample(EduTeacherCourseRefCriteria example);

    EduTeacherCourseRef selectByPrimaryKey(Integer refId);

    int updateByExampleSelective(@Param("record") EduTeacherCourseRef record, @Param("example") EduTeacherCourseRefCriteria example);

    int updateByExample(@Param("record") EduTeacherCourseRef record, @Param("example") EduTeacherCourseRefCriteria example);

    int updateByPrimaryKeySelective(EduTeacherCourseRef record);

    int updateByPrimaryKey(EduTeacherCourseRef record);

    EduTeacherCourseRef selectByExampleForOne(EduTeacherCourseRefCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer refId);

    java.util.Map<String, Object> selectMapByExampleForOne(EduTeacherCourseRefCriteria example);

    List<Map<String, Object>> selectMapByExample(EduTeacherCourseRefCriteria example);
}