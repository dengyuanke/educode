package com.stepiot.dao;

import com.stepiot.model.EduUserRole;
import com.stepiot.model.EduUserRoleCriteria;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface EduUserRoleMapper {
    long countByExample(EduUserRoleCriteria example);

    int deleteByExample(EduUserRoleCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(EduUserRole record);

    int insertSelective(EduUserRole record);

    List<EduUserRole> selectByExample(EduUserRoleCriteria example);

    EduUserRole selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EduUserRole record, @Param("example") EduUserRoleCriteria example);

    int updateByExample(@Param("record") EduUserRole record, @Param("example") EduUserRoleCriteria example);

    int updateByPrimaryKeySelective(EduUserRole record);

    int updateByPrimaryKey(EduUserRole record);

    EduUserRole selectByExampleForOne(EduUserRoleCriteria example);

    java.util.Map<String, Object> selectMapByPrimaryKey(Integer id);

    java.util.Map<String, Object> selectMapByExampleForOne(EduUserRoleCriteria example);

    List<Map<String, Object>> selectMapByExample(EduUserRoleCriteria example);
}