package com.stepiot.aws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.common.base.Charsets;

@Service
public class AmazonS3Service {

  private static final String BUCKET = "deve";

  @Autowired
  private AmazonS3 s3;

  @Async
  public CompletableFuture<PutObjectResult> pubObject(byte[] data, String key) {
    ObjectMetadata meta = meta(data);

    PutObjectRequest obj = new PutObjectRequest(BUCKET, key, new ByteArrayInputStream(data), meta);

    PutObjectResult putObject = s3.putObject(obj);
    return CompletableFuture.completedFuture(putObject);
  }

  public String getStrObject(String key) {
    S3Object obj = s3.getObject(BUCKET, key);
    S3ObjectInputStream content = obj.getObjectContent();
    String contents = null;
    try {
      contents = StreamUtils.copyToString(content, Charsets.UTF_8);
      return contents;
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (obj != null) {
        try {
          obj.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return contents;
  }


  public byte[] getS3Object(String key) throws IOException {
    S3Object obj = s3.getObject(BUCKET, key);
    S3ObjectInputStream content = obj.getObjectContent();
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    IOUtils.copy(content, bout);
    return bout.toByteArray();
  }



  private static ObjectMetadata meta(byte[] data) {
    ObjectMetadata meta = new ObjectMetadata();
    meta.setContentLength(data.length);
    meta.setContentType(new Tika().detect(data));
    meta.setContentMD5(Base64.encodeBase64String(DigestUtils.md5(data)));
    return meta;
  }
}
