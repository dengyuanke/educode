package com.stepiot.constants;

import java.util.Map;
import com.google.common.collect.Maps;

public enum CourseStatus {
  SAVED(0), SUBMITTED(1), PASSED(2), NOTINITED(3), REJECTED(-1);

  private int id;

  private static Map<Integer, CourseStatus> map = Maps.newHashMap();
  static {
    for (CourseStatus courseType : CourseStatus.values()) {
      map.put(courseType.id, courseType);
    }
  }

  CourseStatus(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }

  public static CourseStatus valueOf(int courseType) {
    return (CourseStatus) map.get(courseType);
  }

  public int getValue() {
    return id;
  }

}
