package com.stepiot.constants;

import java.util.Map;
import com.google.common.collect.Maps;

public enum CourseType {

  NULL(-1), SPACE(1), TURTLE(2), NESTBLOCK(3), PYTHON(4), IOT(5), WABC(6);

  private int id;

  private static Map<Integer, CourseType> map = Maps.newHashMap();
  static {
    for (CourseType courseType : CourseType.values()) {
      map.put(courseType.id, courseType);
    }
  }

  CourseType(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }

  public static CourseType valueOf(int courseType) {
    return (CourseType) map.get(courseType);
  }

  public int getValue() {
    return id;
  }

}
