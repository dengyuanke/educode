package com.stepiot.constants;

import java.util.Map;
import com.google.common.collect.Maps;

public enum RoleEnum {

  ROLE_COADMIN("ROLE_COADMIN"), ROLE_DEV("ROLE_DEV"), ROLE_ROOT("ROLE_ROOT"), ROLE_STUDENT(
      "ROLE_STUDENT"), ROLE_TEACHER("ROLE_TEACHER"), ROLE_USER("ROLE_USER");

  private String id;

  private static Map<String, RoleEnum> map = Maps.newHashMap();
  static {
    for (RoleEnum courseType : RoleEnum.values()) {
      map.put(courseType.id, courseType);
    }
  }

  RoleEnum(String id) {
    this.id = id;
  }

  public String getId() {
    return this.id;
  }

  public String getValue() {
    return id;
  }

}
