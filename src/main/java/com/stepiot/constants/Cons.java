package com.stepiot.constants;

public class Cons {
  public static final String BOOTBLOCKLY = "";// "<xml xmlns=\"http://www.w3.org/1999/xhtml\"
                                              // id=\"workspaceBlocks\"
                                              // style=\"display:none\"><block type=\"stepiot_boot\"
                                              // deletable=\"false\" id=\"9^t:A4NLCoF:X^Wat^[L\"
                                              // x=\"64\" y=\"32\"></block></xml>";

  public static final String REQUEST_ATTR_BREADCRUMB_BUILDER = "BREADCRUMB_BUILDER";


  /**
   * 积木主题设置
   */
  public static final String CONFIG_BLOCKLY_THEME = "blocklyTheme";
  /**
   * 系统主题设置
   */
  public static final String CONFIG_SYS_THEME = "sysTheme";


  public static final String REQ_SOURCE_PAGINATION = "pagination";
  public static final int SCORE_HIGH = 5700;
  public static final int SCORE_MIDDLE = 4500;
  public static final int SCORE_LOW = 10;

  public static final int STUDENT_FAQ_WIKI_ID = 18;
  public static final int TEACHER_FAQ_WIKI_ID = 19;

  public static final int STUDENT_HELP_WIKI_ID = 20;
  public static final int TEACHER_HELP_WIKI_ID = 21;

  public static final String LOGIN_BLOCKED_MESSAGE = "system.login.blocked";



}
