package com.stepiot.constants;

import java.util.Map;
import com.google.common.collect.Maps;

public enum NoticeRecipientStatus {
  UNREADED(0), READED(1);

  private int id;

  private static Map<Integer, NoticeRecipientStatus> map = Maps.newHashMap();
  static {
    for (NoticeRecipientStatus status : NoticeRecipientStatus.values()) {
      map.put(status.id, status);
    }
  }

  NoticeRecipientStatus(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }

  public static NoticeRecipientStatus valueOf(int status) {
    return (NoticeRecipientStatus) map.get(status);
  }

  public int getValue() {
    return id;
  }

}
