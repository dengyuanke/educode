/**
 * Blockly Games: Bootloader
 *
 * Copyright 2014 Google Inc.
 * https://github.com/google/blockly-games
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Load the correct language pack for the current application.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

(function() {
  // Application path.
  var appName = 'maze';

  // Supported languages (consistent across all apps).
  window['BlocklyGamesLanguages'] = [
    'ar', 'be', 'be-tarask', 'bg', 'bn', 'br', 'cs', 'da', 'de', 'el', 'en',
    'es', 'eu', 'fa', 'fi', 'fr', 'gl', 'ha', 'he', 'hi', 'hu', 'hy', 'ia',
    'id', 'ig', 'is', 'it', 'ja', 'kab', 'ko', 'lt', 'lv', 'ms', 'my', 'nb',
    'nl', 'pl', 'pms', 'pt', 'pt-br', 'ro', 'ru', 'sc', 'sk', 'sl', 'sq',
    'sr', 'sv', 'th', 'tr', 'uk', 'ur', 'vi', 'yo', 'zh-hans', 'zh-hant'
  ];

  // Use a series of heuristics that determine the likely language of this user.
  // First choice: The URL specified language.

  if (window['BlocklyGamesLanguages'].indexOf('en') != -1) {
    // Save this explicit choice as cookie.
    var exp = (new Date(Date.now() + 2 * 31536000000)).toUTCString();
    document.cookie = 'lang=' + escape('en') + '; expires=' + exp + 'path=/';
  } else {

  }
  window['BlocklyGamesLang'] = 'en';

  // Load the chosen language pack.
  var script = document.createElement('script');
  var debug = true;

  script.src = appName + '/generated/' + 'en' +
      (debug ? '/uncompressed.js' : '/compressed.js');
  script.type = 'text/javascript';
  document.head.appendChild(script);
})();
